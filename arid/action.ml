(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*)

(*

*  What is this file?

**    action.ml

*  What does it do?

**    Declares types for actions, action queues and the gamestate. It 
also provides the interface between the toplevel (arid.ml) and the rest 
of the action system.

*)


type gamestate =
   Continue
 | Savecontinue
 | Switchcontinue
 | Save
 | Quit
 | Victory
 | Unprocessed
;;

type action =
 {
   actor : int;
   mutable time : int;
   mutable variety : Actpit.action_variety;
   subinfo1 : int;
   subinfo2 : int;
 }
;;

let proto_action =
 {
   actor = (-1);
   time = (-1);
   variety = Actpit.Request;
   subinfo1 = 0;
   subinfo2 = 0;
 }
;;

type actionq =
 {
   mutable act : action list;
   mutable gametime : int;
   mutable prev_gametime : int;
   mutable prior_prev_gametime : int;
   mutable interrupt_activity : bool;
   mutable interrupt_pass : bool;
 }
;;

let proto_actionq =
 {
   act = [];
   gametime = 0;
   prev_gametime = 0;
   prior_prev_gametime = 0;
   interrupt_activity = false;
   interrupt_pass = false;
 }
;;

let get_actorid act = act.actor;;

let get_actlist actq = actq.act;;

let set_actlist actq v = actq.act <- v;;

let set_time act v = act.time <- v;;

let get_time act = act.time;;

let get_gametime actq = actq.gametime;;

let set_gametime actq v = actq.gametime <- v;;

let get_prevtime actq = actq.prev_gametime;;

let set_prevtime actq v = actq.prev_gametime <- v;;

let get_prior_prevtime actq = actq.prior_prev_gametime;;

let set_prior_prevtime actq v = actq.prior_prev_gametime <- v;;

let set_interrupt_activity actq v = actq.interrupt_activity <- v;;

let set_interrupt_pass actq v = actq.interrupt_pass <- v;;

let should_interrupt actq = actq.interrupt_activity;;

let interrupt_pass actq = actq.interrupt_pass;;

let new_actionq pcid = {proto_actionq with act = 
                          [
                            {proto_action with actor = pcid ;
                            time = 0;
                            variety = Actpit.Request}
                          ]
                       }
;;

let new_action_request pid t =
   {proto_action with
      actor = pid;
      time = t;
      variety = Actpit.Request
   }
;;

let new_action_pass pid starttime t =
   {proto_action with
      actor = pid;
      time = t;
      variety = Actpit.Pass;
      subinfo1 = starttime
   }
;;

let new_action_follow pid t ns =
   {
      actor = pid;
      time = t;
      variety = Actpit.Follow;
      subinfo1 = ns;
      subinfo2 = ns
   }
;;

let new_missile_trail pid t x y =
   {
      actor = pid;
      time = t;
      variety = Actpit.Missile_trail;
      subinfo1 = x;
      subinfo2 = y;
   }
;;

let new_remove_missile_trail pid t x y =
   {
      actor = pid;
      time = t;
      variety = Actpit.Remove_missile_trail;
      subinfo1 = x;
      subinfo2 = y;
   }
;;

let rec real_insert_action act q ispc =
      match q with
         [] -> [act]
       | head :: tail -> 
            if not ispc then
               if act.time < head.time then act :: head :: tail
               else head :: real_insert_action act tail ispc
            else
               if act.time <= head.time then act :: head :: tail
               else head :: real_insert_action act tail ispc
;;

let insert_action act actq ispc =
   actq.act <- real_insert_action act actq.act ispc
;;

let rec suspend_act actq act =
   let continue = split actq.act act in
      (
         actq.act <- continue;
(*         actq.suspended_act <- act :: actq.suspended_act *)
      )
   and split actlist act =
      match actlist with
         [] -> []
       | head :: tail ->
            if head = act then split tail act
            else head :: (split tail act)
;;

let remove_person actq pson =
   let mc = List.length actq.act - 1 in
   if mc >= 0 then
     (
      let count = ref 0 in
      while !count < (List.length actq.act) do
         let act = List.nth actq.act !count in
            if act.actor = Person.get_id pson then
               (suspend_act actq act)
            else count := !count + 1
      done
     )
;;

let missile_trail actq actor time ispc x y =
   let actor_id = Person.get_id actor in
   let next = new_missile_trail actor_id time x y in
      insert_action next actq ispc
;;

let remove_missile_trail actq actor time ispc x y =
   let actor_id = Person.get_id actor in
   let next = new_remove_missile_trail actor_id time x y in
      insert_action next actq ispc
;;

let next_act actq actor time ispc =
   Person.set_next_action_time actor time;
   let actor_id = Person.get_id actor in
   let next = new_action_request actor_id time in
      insert_action next actq ispc
;;

let next_pass actq actor starttime time ispc =
   Person.set_next_action_time actor time;
   let actor_id = Person.get_id actor in
   let next = new_action_pass actor_id starttime time in
      insert_action next actq ispc
;;

let next_follow actq actor_id time ispc nsubinf =
   let next = new_action_follow actor_id time nsubinf in
      insert_action next actq ispc
;;

let add_new_person actq actor gametime =
   next_act actq actor gametime false
;;

(*
let add_personlist actq plist =
   let gametime = actq.gametime in
   let mc = List.length plist - 1 in
   if mc >= 0 then
      for count = 0 to mc do
         let pson = List.nth plist count in
            add_new_person actq pson gametime
      done
;;
*)

let rec actlist_without act actlist =
   match actlist with
      [] -> []
    | head :: tail ->
         if head = act then actlist_without act tail
         else head :: (actlist_without act tail)
;;

let remove_personlist actq plist =
   let mc = List.length plist - 1 in
   if mc >= 0  then
      for count = 0 to mc do
         let pson = List.nth plist count in
            remove_person actq pson
      done
;;

let acts_left actq = List.length actq.act;;

let rec strip_sync_actlist actl ntime newl =
   match actl with
      [] -> newl
    | head :: tail ->
         if not (duplicate_action head newl) then
            strip_sync_actlist tail ntime
               (newl @ [head])
         else strip_sync_actlist tail ntime newl
and duplicate_action a l =
   match l with
      [] -> false
    | head :: tail ->
         if head.actor = a.actor 
(*         || (not (a.variety = Actpit.Request)) *)
         then
            true
         else duplicate_action a tail
;;

let remove_duplicates_and_update actq time =
   actq.act <- strip_sync_actlist actq.act time [];
;;

let activate actq plist =
 try
   let mc = List.length plist - 1 in
   if mc >= 0 then
      for count = 0 to mc do
         let pson = List.nth plist count in
            let act_time =
               let expected_time = Person.get_next_action_time pson in
                  if expected_time < actq.prior_prev_gametime then
                     actq.gametime
                  else
                     expected_time
            in
               add_new_person actq pson act_time
      done;
      remove_duplicates_and_update actq actq.gametime
 with Failure("nth") -> 
    raise (Failure ("Activating person failed, nth"))
;;

let time_of_next_act actq =
   match actq.act with
      [] -> actq.gametime
    | head :: tail -> 
         head.time
;;

let dirty_tile overmap actq (lx, ly) =
   let tile = Mapstruc.get_rtile overmap lx ly in
      Mapstruc.dirty_tile overmap (lx, ly);
      if actq.prev_gametime <= (Mapblock.vis_time tile) then
         actq.interrupt_pass <- true
;;

(*
let rec time_of_next_act actq =
   check_next_act_time actq.act actq.gametime
and check_next_act_time actl least_time =
   match actl with
      [] -> least_time
    | head :: tail -> 
         check_next_act_time tail (min least_time head.time)
;;
*)

let get_variety act = act.variety;;

let set_variety act v = act.variety <- v;;

let act_time actor act = 
   let base = 
      if Actpit.is_walk act.variety then
         Person.get_walk_speed actor
      else Person.get_act_speed actor
   in
   let delay = (Actpit.get_speed act.variety) in
      (delay * base) / 100
;;

let get_subinfo1 act = act.subinfo1;;

let get_subinfo2 act = act.subinfo2;;

let get_subdatpair act = act.subinfo1, act.subinfo2;;

