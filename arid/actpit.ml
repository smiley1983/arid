(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    actpit.ml

*  What does it do?

**    Action type Pit - Provides type definitions which need to be 
accessible to higher level action functions. It also provides the base 
times for completing those actions, and potentially other information 
(such as whether or not the action should be animated).

*)

(******************
 ** Action types **
 ******************)

type action_variety =
 | Template_action
 | Request
 | Help
 | Look_mode
 | Short_look
 | Act_quit
 | Bump_n
 | Bump_s
 | Bump_e
 | Bump_w
 | Bump_ne
 | Bump_nw
 | Bump_se
 | Bump_sw
 | Follow_downstair
 | Follow_upstair 
 | Show_inventory
 | Pick_up
 | Drop_it
 | Multi_drop
 | Attack
 | Throw
 | Cast_spell
 | Show_equipment
 | Drink
 | Read
 | Examine
 | Follow
 | Pass
 | Show_skills
 | Animation
 | Missile_trail
 | Remove_missile_trail
 | Wizard_show_actionq
 | Wizard_vision
;;

let get_speed act_type =
 match act_type with
 | Template_action -> 100
 | Request
 | Help
 | Look_mode
 | Short_look
 | Act_quit -> 0
 | Bump_n 
 | Bump_s 
 | Bump_e
 | Bump_w
 | Bump_ne
 | Bump_nw
 | Bump_se
 | Bump_sw -> 100
 | Follow_downstair
 | Follow_upstair -> 151
 | Show_inventory -> 0
 | Pick_up -> 80
 | Drop_it -> 60
 | Multi_drop -> 60
 | Attack -> 100
 | Throw -> 90
 | Cast_spell -> 100
 | Show_equipment -> 100
 | Drink -> 100
 | Read -> 100
 | Examine -> 0
 | Follow -> 100
 | Pass -> -1
 | Show_skills -> 0
 | Animation -> 0
 | Missile_trail -> 0
 | Remove_missile_trail -> 0
 | Wizard_show_actionq -> 0
 | Wizard_vision -> 0
;;

type animation =
 | Could
 | Should
 | Do_not
;;

let could_animate v =
 match v with
 | Template_action -> Do_not
 | Request
 | Help
 | Look_mode
 | Short_look
 | Act_quit -> Do_not
 | Bump_n 
 | Bump_s 
 | Bump_e
 | Bump_w
 | Bump_ne
 | Bump_nw
 | Bump_se
 | Bump_sw -> Could
 | Follow_downstair
 | Follow_upstair -> Could
 | Show_inventory -> Do_not
 | Pick_up
 | Drop_it
 | Multi_drop -> Do_not
 | Attack -> Could
 | Throw -> Could
 | Cast_spell -> Could
 | Show_equipment -> Do_not
 | Drink -> Could
 | Read -> Could
 | Examine -> Do_not
 | Follow -> Could
 | Pass -> Do_not
 | Show_skills -> Do_not
 | Animation -> Should
 | Missile_trail -> Should
 | Remove_missile_trail -> Do_not
 | Wizard_vision -> Do_not
 | Wizard_show_actionq -> Do_not
;;

let is_walk a =
   match a with
    | Bump_n
    | Bump_s
    | Bump_e
    | Bump_w
    | Bump_ne
    | Bump_nw
    | Bump_se
    | Bump_sw
    | Follow_downstair
    | Follow_upstair 
    | Follow -> true
    | _ -> false
;;


