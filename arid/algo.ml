(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    algo.ml

*  What does it do?

**    Provides implementations for a bunch of common algorithms. Also 
provides the wrapper to the Mersenne Twister, which is part of the 
mathlib library. That implementation was written by Shawn Wagner, not 
me. See the file mersenne.ml for its licensing details (LGPL).

*)


type random_holder =
   {
      mutable mersenne_t : Mersenne.t
   }
;;

(*

NOTE: This mersenne twister wrapper must be initialised EXTERNALLY on a 
session-by-session basis. I suppose I could save the twister state in 
the session. Probably will eventually. Still, every session must modify 
the following *instance* of mersenne_t for the random generation to 
work!

*)

let uni_mersenne =
    {
      mersenne_t = (Mersenne.make `CurrentTime)
    }
;;

let get_mersenne_state t = t.mersenne_t;;

let set_mersenne_state t v = t.mersenne_t <- v;;

let rec find_next_power2 n p =
   if p < n then
      find_next_power2 n (p lsl 1)
   else
      p
;;

let rec masked_bounded_mersenne bound bitmask =
   let r = Mersenne.int uni_mersenne.mersenne_t in
      let result = r land bitmask in
         if result >= bound then
            masked_bounded_mersenne bound bitmask
         else
            result
;;

let mersenne bound = 
   if bound < 2 then
      0
   else
      let bitmask = (find_next_power2 bound 1) - 1 in
         masked_bounded_mersenne bound bitmask
;;

let step_dir d =
   if d < 0 then -1
   else 1
;;

let rec x_step cx cy ex ey step_x step_y half_x dx dy error_count
=
   if (cx, cy) = (ex, ey) then [(cx, cy)]
   else
      let new_error = ref (error_count + dy) in
         let (new_cx, new_cy) =
            if !new_error > half_x then
               (new_error := !new_error - dx;
                ((cx + step_x), (cy + step_y))
               )
            else ((cx + step_x), cy)
         in
            (cx, cy) ::
            (x_step new_cx new_cy ex ey step_x step_y half_x dx dy
                    !new_error)
;;

let rec y_step cx cy ex ey step_x step_y half_y dx dy error_count
=
   if (cx, cy) = (ex, ey) then [(cx, cy)]
   else
      let new_error = ref (error_count + dx) in
         let (new_cx, new_cy) =
            if !new_error > half_y then
               (new_error := !new_error - dy;
                ((cx + step_x), (cy + step_y))
               )
            else (cx, (cy + step_y))
         in
            (cx, cy) ::
            (y_step new_cx new_cy ex ey step_x step_y half_y dx dy 
                    !new_error)
;;

let bresenham_line lx1 ly1 lx2 ly2 =
   let (dx, dy) = ((lx2 - lx1), (ly2 - ly1)) in
      let (step_x, step_y, new_dx, new_dy) =
         ((step_dir dx), (step_dir dy), (abs dx), (abs dy))
      in
         if new_dx > new_dy then
            let half_x = new_dx / 2 in
               x_step lx1 ly1 lx2 ly2 step_x step_y half_x new_dx new_dy
                      0
         else
            let half_y = new_dy / 2 in
               y_step lx1 ly1 lx2 ly2 step_x step_y half_y new_dx new_dy
                      0
;;

type cardinal_dir =
   North
 | West
 | South
 | East
 | Northwest
 | Northeast
 | Southwest
 | Southeast
 | Centre
;;

let choose_cardinal dx dy =
   if dx = 0 && dy < 0 then North
   else if dx = 0 && dy > 0 then South
   else if dx < 0 && dy = 0 then West
   else if dx > 0 && dy = 0 then East
   else if dx < 0 && dy < 0 then
      match mersenne 2 with
         0 -> North
       | _ -> West
   else if dx > 0 && dy < 0 then
      match mersenne 2 with
         0 -> North
       | _ -> East
   else if dx < 0 && dy > 0 then
      match mersenne 2 with
         0 -> South
       | _ -> West
   else (* if dx > 0 && dy > 0 then *)
      match mersenne 2 with
         0 -> South
       | _ -> East
;;

let fixed_cardinal dx dy =
   if dx = 0 && dy < 0 then North
   else if dx = 0 && dy > 0 then South
   else if dx < 0 && dy = 0 then West
   else if dx > 0 && dy = 0 then East
   else if dx < 0 && dy < 0 then Northwest
   else if dx > 0 && dy < 0 then Northeast
   else if dx < 0 && dy > 0 then Southwest
   else (* if dx > 0 && dy > 0 then *) Southeast
;;

let roll_dice nd n =
   let rslt = ref 0 in
      for count = 0 to (nd - 1) do
         rslt := !rslt + (mersenne (max 1 n)) +1
      done
   ; !rslt
;;

let cardinal_step x y c =
   match c with
      North -> x, (y - 1)
    | South -> x, (y + 1)
    | West -> (x - 1), y
    | East -> (x + 1), y
    | Northwest -> (x - 1), (y - 1)
    | Northeast -> (x + 1), (y - 1)
    | Southwest -> (x - 1), (y + 1)
    | Southeast -> (x + 1), (y + 1)
    | Centre -> x, y
;;

let number_cardinal r =
   match r with
      8 -> North
    | 2 -> South
    | 4 -> West
    | 6 -> East
    | 7 -> Northwest
    | 9 -> Northeast
    | 1 -> Southwest
    | 3 -> Southeast
    | 5 -> Centre
    | _ -> raise Not_found
;;

let random_cardinal n =
   let r = mersenne n in
   match r with
      0 -> North
    | 1 -> South
    | 2 -> West
    | 3 -> East
    | 4 -> Northwest
    | 5 -> Northeast
    | 6 -> Southwest
    | _ -> Southeast
;;

let rec find_inner_spiral n =
   step_find_inner_spiral n 1 0
and step_find_inner_spiral n count prev_count =
   if count > 100 then raise Not_found
   else if count * count < n then
      step_find_inner_spiral n (count + 1) count
   else prev_count
;;

let spiral_coords edge_length which_edge final_edge =
   let diag = (edge_length - 2) / 2 in
   match which_edge with
      0 -> ( ((0 - diag) + final_edge), (0 - diag) )
    | 1 -> ( diag, ((0 - diag) + final_edge) )
    | 2 -> ( (diag - final_edge), diag)
    | _ -> ( (0 - diag), (diag - final_edge) )
;;

let spiral_nth n =
   if n > 0 then
      let inner_circ = find_inner_spiral n in
         let steps_left = n - (inner_circ * inner_circ) in
            let edge_length = inner_circ + 1 in
               let which_edge = steps_left / edge_length in
               let final_edge = steps_left mod edge_length in
                  spiral_coords edge_length which_edge final_edge
   else raise Not_found
;;

let random_bounded_crd x1 y1 x2 y2 =
   let dx = x2 - x1 in
   let dy = y2 - y1 in
      let rx = mersenne (max 1 dx) in
      let ry = mersenne (max 1 dy) in
         (x1 + rx), (y1 + ry)
;;

let rec list_without l i =
   match l with
      [] -> []
    | head :: tail ->
         if head = i then list_without tail i
         else head :: list_without tail i
;;

let random_in_list l =
   let len = List.length l - 1 in
   let c = mersenne len in
   List.nth l c
;;

let pick_random_from_reflist l =
   let len = List.length !l - 1 in
   let c = mersenne len in
      let chosen = List.nth !l c in
         l := list_without !l chosen;
         chosen
;;

(*
This has moved to english.ml

let consonants =
   ['b';'c';'d';'f';'g';'h';'j';'k';'l';'m';'n';'p';'q';'r';'s';'t';'v';
    'w';'x';'y';'z'
   ]
;;

let vowels =
   ['a';'e';'i';'o';'u']
;;

let english_prefixed_singular_label label =
   let firstletter = Char.lowercase label.[0] in
      if (List.mem firstletter vowels) then
         "an " ^ label
      else
         "a " ^ label
;;

let english_prefixed_pluralised_label label number =
   if number = 1 then
      let firstletter = Char.lowercase label.[0] in
         if (List.mem firstletter vowels) then
            "an " ^ label
         else
            "a " ^ label
   else
      let n = string_of_int number in
         n ^ " " ^ label ^ "s"
;;

let english_specific_label label number =
   if number = 1 then
      "the " ^ label
   else
      let n = string_of_int number in
         "the " ^ n ^ " " ^ label ^ "s"
;;

*)
