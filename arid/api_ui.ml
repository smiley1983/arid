
(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    api_ui.ml

*  What does it do?

**    Provides an object via which a big messy cyclic dependency can be 
set up by arid.ml stretching from api_ui down to the base level ui 
(uilink.ml and below) and everywhere in between. This object is never 
involved in any marshalling of data structures, and no references to it 
are kept by any data structure at any time, so I believe this is okay. 
If someone would like to coach me on a better solution, I'd be grateful 
but I don't expect any such offers to be forthcoming. :)

*)


let rec count_back_lines menulist hilit limit =
   if hilit < 0 || limit < 1 then
      hilit + 1
   else count_back_lines menulist (hilit - 1) (limit - 
      (menulist#nth hilit)#get_numlines)
;;

let set_menulist_startline menulist wheight =
   (menulist#nth 0)#set_start_line 0;
   (menulist#nth 0)#set_page_start 0;
   (menulist#nth 0)#set_page 0;
   for count = 1 to ((List.length menulist#get_index) - 1) do
    (menulist#nth count)#set_start_line
       (((menulist#nth (count - 1))#get_start_line +
       (menulist#nth (count - 1))#get_numlines) + 1);
    if ((List.length menulist#get_index) ) / (wheight / 2) = 0 then
      (menulist#nth count)#set_page 0
    else
      if ((( (menulist#nth count)#get_start_line ) 
          / (wheight + 2))
         ) <= (menulist#nth (count - 1))#get_page 
      then
         (
          (menulist#nth count)#set_page 
           ((menulist#nth (count - 1))#get_page);
          (menulist#nth count)#set_page_start
           ((menulist#nth (count - 1))#get_page_start)
         )
      else
         (
          (menulist#nth count)#set_page 
           (((menulist#nth (count - 1))#get_page) + 1);
          (menulist#nth count)#set_page_start (count - 1)
(*          ((menulist#nth (count))#get_start_line)*)
         )
   done
;;

let set_menulist_linelength menulist cursor =
   for count = 0 to ((List.length menulist#get_index) - 1 ) do
      (menulist#nth count)#set_num_lines cursor#getboundx
   done
;;

let rec search_for_index (c:char) (menulist:Menulist.menulist) choice 
      count 
=
  try
   (if count = List.length menulist#get_index then -1
    else
    if (choice + 1) = (List.length menulist#get_index) then
       search_for_index c menulist (-1) (count + 1)
    else
       if (menulist#get_item(List.nth menulist#get_index 
          (choice + 1)))#get_itemtype = "option" then
             (if (List.nth menulist#get_index (choice + 1)) = c then
                 (choice + 1)
              else search_for_index c menulist (choice + 1) (count + 1)
             )
       else search_for_index c menulist (choice + 1) (count + 1)
   )
  with _ -> -1
;;

let rec move_menu_choice_up (menulist:Menulist.menulist) choice =
   match (choice - 1) with
      -1 -> move_menu_choice_up menulist (List.length
         menulist#get_index)
    | n -> if (menulist#get_item (List.nth menulist#get_index n)
              )#get_itemtype =
                 "option" then n
         else move_menu_choice_up menulist n
;;

let rec move_menu_choice_down (menulist:Menulist.menulist) choice =
   if List.length menulist#get_index = 0 then 0 else
   if (choice + 1) = (List.length menulist#get_index) then
      move_menu_choice_down menulist (-1)
   else
      if (menulist#get_item(List.nth menulist#get_index 
         (choice + 1)))#get_itemtype = "option" then
            (choice + 1)
      else move_menu_choice_down menulist (choice + 1)
;;

let rec move_menu_screen_up (menulist:Menulist.menulist) choice =
   let orig = (menulist#nth choice)#get_page in
      real_move_menu_screen_up menulist choice 0 orig
and real_move_menu_screen_up menulist choice count original_screen =
   if count >= List.length (menulist#get_index) then 0
   else if choice < 0 then 
      real_move_menu_screen_up menulist 
                               (List.length menulist#get_index - 1)
                               (count + 1) original_screen
   else
   match (choice - 1) with
      -1 -> real_move_menu_screen_up menulist (List.length
         menulist#get_index) (count + 1) original_screen
    | n -> if not ((menulist#get_item (List.nth menulist#get_index n)
                   )#get_page = original_screen
                  ) then n
         else real_move_menu_screen_up menulist n (count + 1)
                                       original_screen
;;

let rec move_menu_screen_down (menulist:Menulist.menulist) choice =
   let orig = (menulist#nth choice)#get_page in
      real_move_menu_screen_down menulist choice 0 orig
and real_move_menu_screen_down menulist choice count original_screen =
   if count >= List.length (menulist#get_index) then 0 else
   if (choice + 1) >= (List.length menulist#get_index) then
      real_move_menu_screen_down menulist (-1) (count + 1) original_screen
   else
      if not ((menulist#get_item(List.nth menulist#get_index 
             (choice + 1)))#get_page = original_screen) then
               (choice + 1)
      else real_move_menu_screen_down menulist (choice + 1)
                                      (count + 1) original_screen
;;

class toplevel_ui option =
   object (self)
      val cset = Boxes.get_cursor_set option
      val base_ui = Uilink.new_ui option
      val option = option
      method initialise =
         base_ui#initialise
      method ask q = 
        (
         self#clear_win "msg";
         self#clear_win "msg";
         self#reset_cursor "msg";
         self#set_colour (U_colour.conv_colour_s "grey");
         self#box_ask q "msg"
        )
      method end_ui = base_ui#end_ui
      method colour_say thing colour =
         (
             self#set_colour colour;
             self#box_say thing "msg"
         )
      method colour_say_up thing colour =
         (
             self#set_colour colour;
             self#box_say_up thing "msg"
         )
      method say thing = 
         (
             self#set_colour (U_colour.conv_colour_s "grey");
             self#box_say thing "msg"
         )
      method set_colour colour = base_ui#set_colour colour
      method box_putsymbol symbol cursorstring = 
         base_ui#box_putsymbol symbol (cset#get_cursor cursorstring)
      method box_ask q cursorstring = 
         base_ui#box_ask q (cset#get_cursor cursorstring)
      method box_say_up thing cursorstring =
         base_ui#box_say_up thing (cset#get_cursor cursorstring)
      method box_say thing cursorstring =
         base_ui#box_say thing (cset#get_cursor cursorstring)
      method clear_win cursorstring= base_ui#clear_win
                                     (cset#get_cursor cursorstring)
      method clear_say thing =
         (
(*
            self#clear_win "msg";
            self#clear_win "msg";
*)
            self#reset_cursor "msg";
            self#say thing
         )
      method in_key = base_ui#in_key
      method unblocked_key = base_ui#unblocked_key
      method reset_box boxstring off_x off_y boundx boundy =
         cset#changebox boxstring off_x off_y boundx boundy
      method display_scrolling_menu (menulist:Menulist.menulist)
       (hilit:int) (prev_hilit:int) height=
         let count = ref 
            (menulist#nth hilit)#get_page_start
         in
         let lines_end = ref false in
         let index = menulist#get_index in
         let maxlength = List.length index in
            while ( !count < maxlength )
             && !lines_end = false do
               (if !count > (-1) then
                  let prevcolour = option#get_c_colour in
                  (if (!count) = hilit then
                     (self#set_colour option#get_menu_fc;
                      self#box_say "*-> " "temp"
                     )
                   else self#set_colour option#get_menu_bc
                   ;
                   self#box_say (String.make 1 
                                (List.nth index (!count))) "temp";
                   if (menulist#nth (!count))#get_itemtype = "option"
                   then
                     (
                      if (menulist#nth (!count))#get_selected then
                         self#box_say " +  " "temp"
                      else self#box_say " -  " "temp"
                     )
                   else
                      (self#set_colour option#get_menu_hc;)
                   ;
                   self#box_say (menulist#nth (!count))#get_content
                                "temp";
                   self#set_colour prevcolour
                  )
               );
               lines_end := self#scrollbox_newline_over_more;
               count := (!count + 1)
            done;
            if ( !count < maxlength ) then
               self#box_say "(more...)" "temp"
      method scrollbox_newline_over =
         let tc = (cset#get_cursor "temp") in
            if tc#gety = (tc#getboundy -1 ) then true else
               (tc#newline ();
               if tc#gety = (tc#getboundy -1 ) then true else
                  (tc#newline ();
                  if tc#gety = (tc#getboundy -1) then true else false
                  )
               )
      method scrollbox_newline_over_more =
         let tc = (cset#get_cursor "temp") in
            if tc#gety = (tc#getboundy - 1 ) then true else
               (tc#newline ();
               if tc#gety = (tc#getboundy - 1 ) then true else
                  (tc#newline ();
                  if tc#gety = (tc#getboundy - 1) then true else false
                  )
               )
      method display_menu (menulist:Menulist.menulist) (hilit:int)=
         let index = (menulist#get_index) in
            for count = 1 to List.length menulist#get_index do
               let prevcolour = option#get_c_colour in
                  (if (count-1) = hilit then
                     self#set_colour option#get_menu_fc;
                   self#box_say (String.make 1 
                                (List.nth index (count-1))) "temp";
                   if (menulist#nth (count - 1))#get_itemtype = "option"
                   then
                      self#box_say " )  " "temp"
                   else
                      (self#set_colour option#get_menu_hc;)
                   ;
                   self#box_say (menulist#nth (count-1))#get_content
                                "temp";
                   self#box_say "\n\n" "temp";
                   self#set_colour prevcolour
                  )
            done
      method reset_cursor cursorstring =
         (cset#get_cursor cursorstring)#reset
      method choose_scrolling_menuitem (uiopt:Ui_opt.option) menulist
            (height:int) pagetrigger
      =
(*
         let choice = ref (move_menu_choice_down menulist (-1)) in
*)
         let choice = ref pagetrigger in
         let prevchoice = ref !choice in
         let keypress = ref " " in
            set_menulist_linelength menulist (cset#get_cursor "temp");
            set_menulist_startline menulist (cset#get_cursor
                                            "temp")#getboundy;
            while not (!keypress = "enter") do
               ( self#clear_win "temp";
                (cset#get_cursor "temp")#reset;
                self#display_scrolling_menu menulist !choice
                   !prevchoice height;
                base_ui#sync_screen;
                prevchoice := !choice;
                keypress := base_ui#in_key;
                if !keypress = option#get_reverse_key "key_previous" 
                then
                   choice := move_menu_choice_up menulist !choice
                else if 
                   ( !keypress = option#get_reverse_key "key_next" )
                || ( !keypress = " " )
                then
                   choice := move_menu_choice_down menulist !choice
                else if !keypress = "enter" then ()
                else if !keypress = "" then ()
                else if (String.length !keypress) > 1 then ()
                else 
                   let kp_index = search_for_index !keypress.[0] 
                      menulist !choice 0
                   in
                   if not (kp_index = (-1)) then
                     choice := kp_index
                   else
                     (* () *) raise (Failure !keypress) (* DEBUG *)
               ) (*self#say (string_of_int 
                  (menulist#nth !choice)#get_page)*)
            done;
         (menulist#nth_index !choice), !choice
      method display_fast_menu (menulist:Menulist.menulist) hilit height 
            begincount
      =
         let count = ref begincount in
         let lines_end = ref false in
         let index = menulist#get_index in
         let maxlength = List.length index in
            while ( !count < maxlength )
             && !lines_end = false do
               (if !count > (-1) then
                  let prevcolour = option#get_c_colour in
(* *)             (
                   self#set_colour option#get_menu_bc
                   ;
                   self#box_say (String.make 1 
                                (List.nth index (!count))) "temp";
                   if (menulist#nth (!count))#get_itemtype = "option"
                   then
                     (
                      if (menulist#nth (!count))#get_selected then
                         self#box_say " +  " "temp"
                      else self#box_say " -  " "temp"
                     )
                   else
                      (self#set_colour option#get_menu_hc;)
                   ;
                   self#box_say (menulist#nth (!count))#get_content
                                "temp";
                   self#set_colour prevcolour
                  )
               );
               lines_end := self#scrollbox_newline_over_more;
               count := (!count + 1)
            done;
            if ( !count < maxlength ) then
              (
               self#set_colour option#get_menu_bc;
               self#box_say "(more...)" "temp";
               false
              )
            else true
      method choose_fast_menuitem (uiopt:Ui_opt.option) menulist
            (height:int) pagetrigger
      =
(*
         let choice = ref (move_menu_choice_down menulist (-1)) in
*)
         let choice = ref pagetrigger in
         let chosen = ref false in
         let keypress = ref " " in
            set_menulist_linelength menulist (cset#get_cursor "temp");
            set_menulist_startline menulist (cset#get_cursor
                                            "temp")#getboundy;
            while not !chosen do
               ( self#clear_win "temp";
                (cset#get_cursor "temp")#reset;
                let begincount =
                   (menulist#nth !choice)#get_page_start
                in
                let finished =
                    self#display_fast_menu menulist !choice height 
                       begincount
                in
                base_ui#sync_screen;
                keypress := base_ui#in_key;
                if !keypress = 
                      option#get_reverse_key "key_previous" 
                then
                   choice := move_menu_screen_up menulist !choice
                else if 
                   (!keypress = option#get_reverse_key "key_next")
                then
                   choice := move_menu_screen_down menulist !choice
                else if (!keypress = " ") then
                  (
                   if finished then raise Not_found
                   else choice := move_menu_screen_down menulist !choice
                  )
                else if !keypress = "enter" then raise Not_found
                else if !keypress = "" then ()
                else if (String.length !keypress) > 1 then ()
                else 
                   let kp_index =
                      (search_for_index !keypress.[0]
                             menulist 0 0 ) 
                   in
                if not (kp_index = (-1))
                then
                    (
                     choice := kp_index;
                     chosen := true
                    )
                  else
                     raise (Failure !keypress)
               )
            done;
         (menulist#nth_index !choice), !choice
      method choose_menuitem uiopt menulist height =
        let result, _ =
           if uiopt#is_fast_menu then
              (self#choose_fast_menuitem uiopt menulist height 0)
           else
              (self#choose_scrolling_menuitem uiopt menulist height 0)
        in
            menulist#get_item result
      method choose_menuindex uiopt menulist height =
         let result, _ =
            if uiopt#is_fast_menu then
               (self#choose_fast_menuitem uiopt menulist height 0)
            else
               (self#choose_scrolling_menuitem uiopt menulist height 0)
         in result
      method choose_many_indices uiopt menulist height =
         let finished = ref false in
         let retkey = ref "" in
         let page = ref 0 in
         while not !finished do
           try
            let selection, nextpage =
               if uiopt#is_fast_menu then
                  (self#choose_fast_menuitem uiopt menulist height 
                   !page)
               else
                  (self#choose_scrolling_menuitem uiopt menulist height
                   !page)
            in
               page := nextpage;
               let item = (menulist#get_item selection) in
                  let prev = item#get_selected in
                     item#set_selected (not prev)
           with Not_found -> (finished := true)
            | Failure v -> (finished := true; retkey := v)
         done;
         (self#selected_indices menulist, !retkey)
      method selected_indices menulist =
         let rslt = ref [] in
         let index = menulist#get_index in
            for count = 0 to (List.length index - 1) do
               let cur_ind = (List.nth index count) in
                  let item = menulist#get_item cur_ind in
                     if item#get_selected then
                        rslt := !rslt @ [cur_ind]
            done;
            !rslt
      method sync = base_ui#sync_screen
      method dump_vistile_chunk overmap bx by wdt hgt =
         self#reset_cursor "map";
         let s = ref Symbol.Magical_special in
         for county = by to (by + hgt) do
            for countx = bx to (bx + wdt) do
               let (symbol,colour,_) =
                 Vismap.get_true_symb_col overmap countx county in
                 (
                  self#set_colour colour;
                  s := symbol;
                  self#box_putsymbol !s "map"
                 )
            done;
            self#box_say "\n" "map"
         done
      method dump_timed_vistile_chunk overmap bx by wdt hgt time =
         self#reset_cursor "map";
         let s = ref Symbol.Magical_special in
         for county = by to (by + hgt) do
            for countx = bx to (bx + wdt) do
               let (symbol,colour,special_colour) =
                 Vismap.get_symb_col overmap countx county in
               let vistime = Vismap.get_vistime overmap countx county in
                 if vistime >= time then
                 (
                     self#set_colour colour;
                     s := symbol;
                     self#box_putsymbol !s "map"
                 )
                 else if (vistime = (-1))
                 then
                 (
                     self#set_colour (U_colour.conv_colour_s "black");
                     s := Symbol.Void;
                     self#box_putsymbol !s "map"
                 )
                 else
                 (
                     self#set_colour special_colour;
                     s := symbol;
                     self#box_putsymbol !s "map"
                 )
            done;
            self#box_say "\n" "map"
         done
      method dump_timed_shortlook_vistile_chunk 
                overmap bx by wdt hgt lx ly time
      =
         self#reset_cursor "map";
         let s = ref Symbol.Magical_special in
         for county = by to (by + hgt) do
            for countx = bx to (bx + wdt) do
               let (symbol,colour,special_colour) =
                 Vismap.get_symb_col overmap countx county in
               let vistime = Vismap.get_vistime overmap countx county in
                 if countx = lx && county = ly then
                 (
                     self#set_colour
                        (if vistime >= time then colour
                        else U_colour.conv_colour_s "grey");
                     s := Symbol.Magical_special;
                     self#box_putsymbol !s "map"
                 )
                 else if vistime >= time then
                 (
                     self#set_colour colour;
                     s := symbol;
                     self#box_putsymbol !s "map"
                 )
                 else if (vistime = (-1))
                 then
                 (
                     self#set_colour (U_colour.conv_colour_s "black");
                     s := Symbol.Void;
                     self#box_putsymbol !s "map"
                 )
                 else
                 (
                     self#set_colour special_colour;
                     s := symbol;
                     self#box_putsymbol !s "map"
                 )
            done;
            self#box_say "\n" "map"
         done
      method dump_wizard_shortlook_vistile_chunk 
                overmap bx by wdt hgt lx ly
      =
         self#reset_cursor "map";
         let s = ref Symbol.Magical_special in
         for county = by to (by + hgt) do
            for countx = bx to (bx + wdt) do
               let (symbol,colour,special_colour) =
                 Vismap.get_true_symb_col overmap countx county in
(* apparently this is unused.
               let vistime = Vismap.get_vistime overmap countx county in
*)
                 if countx = lx && county = ly then
                 (
                     self#set_colour colour;
                     s := Symbol.Magical_special;
                     self#box_putsymbol !s "map"
                 )
                 else
                 (
                     self#set_colour special_colour;
                     s := symbol;
                     self#box_putsymbol !s "map"
                 )
            done;
            self#box_say "\n" "map"
         done
      method dump_missile_shortlook_vistile_chunk 
                overmap bx by wdt hgt lx ly time
      =
         self#reset_cursor "map";
         let s = ref Symbol.Magical_special in
         for county = by to (by + hgt) do
            for countx = bx to (bx + wdt) do
               let (symbol,colour,special_colour) =
                 Vismap.get_symb_col overmap countx county in
               let phystime = Vismap.get_msltime overmap countx county 
               in
               let vistime = Vismap.get_vistime overmap countx county in
                 if countx = lx && county = ly then
                 (
                     self#set_colour
                        (if phystime >= time then colour
                        else U_colour.conv_colour_s "grey");
                     s := Symbol.Magical_special;
                     self#box_putsymbol !s "map"
                 )
                 else if phystime >= time then
                 (
                     self#set_colour colour;
                     s := symbol;
                     self#box_putsymbol !s "map"
                 )
                 else if (vistime = (-1))
                 then
                 (
                     self#set_colour (U_colour.conv_colour_s "black");
                     s := Symbol.Void;
                     self#box_putsymbol !s "map"
                 )
                 else
                 (
                     self#set_colour special_colour;
                     s := symbol;
                     self#box_putsymbol !s "map"
                 )
            done;
            self#box_say "\n" "map"
         done
(* Don't know what I was doing here. Whatever it was, it didn't get 
used.
      method animate_throw_vistile_chunk 
                overmap bx by wdt hgt lx ly to_x to_y tsym tcolour time
      =
         self#dump_timed_shortlook_vistile_chunk 
               overmap bx by wdt hgt lx ly time;
         let point = Algo.bresenham_line lx ly to_x to_y in
            ()
         let s = String.make 1 tsym in
         self#set_colour tcolour;
         for count = 0 to (List.length point) - 1 do
            let (cx, cy) = (List.nth point count) in
               ()
         done
*)
      method dump_vistile_chunk_fs overmap bx by wdt hgt =
         self#reset_cursor "temp";
         let s = ref Symbol.Magical_special in
         for county = by to (by + hgt) do
            for countx = bx to (bx + wdt) do
               let (symbol,colour,_) =
                 Vismap.get_true_symb_col overmap countx county in
                 (
                  self#set_colour colour;
                  s := symbol;
                  self#box_putsymbol !s "temp"
                 )
            done;
            self#box_say "\n" "temp"
         done
      method dump_timed_vistile_chunk_fs overmap bx by wdt hgt time =
         self#reset_cursor "temp";
         let s = ref Symbol.Magical_special in
         for county = by to (by + hgt) do
            for countx = bx to (bx + wdt) do
               let (symbol,colour,special_colour) =
                 Vismap.get_symb_col overmap countx county in
               let vistime = Vismap.get_vistime overmap countx county in
                 if vistime >= time then
                 (
                     self#set_colour colour;
                     s := symbol;
                     self#box_putsymbol !s "temp"
                 )
                 else if (vistime = (-1))
                 then
                 (
                     self#set_colour (U_colour.conv_colour_s "black");
                     s := Symbol.Void ;
                     self#box_putsymbol !s "temp"
                 )
                 else
                 (
                     self#set_colour special_colour;
                     s := symbol;
                     self#box_putsymbol !s "temp"
                 )
            done;
            self#box_say "\n" "temp"
         done
(*
      method dump_mapchunk (omap:Mapstruc.mapstruc) (plane:string)
      (xs:int) (ys:int) (wide:int) (high:int) =
          self#reset_cursor "map";
          let s = String.make 1 '*' in
          for county = ys to (ys + high) do
             for countx = xs to (xs + wide) do
                let (symbol,colour) =
                  Mapstruc.get_symb_col omap plane countx county in
                  (
                   self#set_colour colour;
                   s.[0] <- symbol;
                   self#box_putsymbol !s "map"
                  )
             done;
             self#box_say "\n" "map"
          done
*)
      method display_wtile_terrain_type wt =
         self#set_colour (U_colour.conv_colour_s "grey");
            let wtt = Mapplane.get_wt_terrain wt in
               self#box_say ((Mapplane.string_of_wtile_terrain wtt) 
                      ^ "\n") "stat"
      method display_loc_and_wtile_terrain ms lx ly =
         self#set_colour (U_colour.conv_colour_s "grey");
         let cplane = Mapstruc.get_cplane ms in
         let wt = Mapplane.wtile_of_rtile cplane lx ly in
         self#box_say ((string_of_int lx) ^ " " ^ (string_of_int ly)
            ^ " : "   ) "stat";
         if not (wt == Mapplane.wtile) then
            let wtt = Mapplane.get_wt_terrain wt in
               self#box_say ((Mapplane.string_of_wtile_terrain wtt) 
                      ^ ".\n") "stat"
         else
            self#box_say "on wtile. " "stat"
      method show_actionq actq =
         self#set_colour U_colour.White;
         self#fullscreen "temp";
         self#reset_cursor "temp";
         let actl = Action.get_actlist actq in
            for count = 0 to (List.length actl) - 1 do
               let this_act = List.nth actl count in
               let actorid = Action.get_actorid this_act in
                  self#box_say ((string_of_int 
                        (Action.get_time this_act)) ^ "   ") "temp";
                  self#box_say ((string_of_int actorid) ^ "\n") "temp"
            done;
          self#sync;
          ignore (self#in_key)
      method show_actionq_plus actq act =
         self#set_colour U_colour.White;
         self#fullscreen "temp";
         self#reset_cursor "temp";
         let actl = Action.get_actlist actq in
            for count = 0 to (List.length actl) - 1 do
               let this_act = List.nth actl count in
               let actorid = Action.get_actorid this_act in
                  self#box_say ((string_of_int 
                        (Action.get_time this_act)) ^ "   ") "temp";
                  self#box_say ((string_of_int actorid) ^ "\n") "temp"
            done;
               let tactorid = Action.get_actorid act in
                  self#box_say ((string_of_int 
                        (Action.get_time act)) ^ "   ") "temp";
                  self#box_say ((string_of_int tactorid) ^ "\n") "temp";
          self#sync;
          ignore (self#in_key)
      method box_mimic_ret_height boxtarget boxsource =
         let boxt = cset#get_cursor boxtarget in
         let boxs = cset#get_cursor boxsource in
            boxt#complete_reset
               boxs#get_x_offset
               boxs#get_y_offset
               boxs#getboundx
               boxs#getboundy;
            boxs#getboundy
      method fullscreen boxtarget =
         let boxt = cset#get_cursor boxtarget in
            boxt#complete_reset 0 0 
               (option#get_screen_chars_wide)
               (option#get_screen_chars_high)
      method fullscreen_ret_h boxtarget =
         let boxt = cset#get_cursor boxtarget in
            boxt#complete_reset 0 0 
               (option#get_screen_chars_wide)
               (option#get_screen_chars_high);
         option#get_screen_chars_high
      method line_up cursorstring =
         (cset#get_cursor cursorstring)#lineup ()
      method last_line cursorstring =
         (cset#get_cursor cursorstring)#lastline ()
      method special_font_init () =
         self#fullscreen "temp";
         self#clear_win "temp";
         self#set_colour (U_colour.conv_colour_s "grey");
         let s = String.make 1 'a' in
            for countx = 0 to 15 do
               for county = 0 to 15 do
                  let target = ((countx * 16) + county) in
                  s.[0] <- Char.chr target;
                  if (target > 16) && (target < 240) then
                     self#box_say s "temp"
                  else self#box_say " " "temp"
               done;
               self#box_say "\n" "temp"
            done;
            self#sync;
            ignore (self#in_key)
      method show_keybindings (uiopt:Ui_opt.option) =
(* 3.09 detected unused binding
        let crs = cset#get_cursor "temp" in
*)
           ignore (self#in_key)
(*
        (
         self#set_colour (U_colour.conv_colour_s "grey");
         self#fullscreen "temp";
         self#clear_win "temp";
         self#reset_cursor "temp";
         self#box_say ("** In short, the most important keys are:") 
                      "temp";
         self#box_say ("\n look: " ^ uiopt#get_key_look) "temp";
         crs#halfline;
         self#box_say ("| get: " ^ uiopt#get_key_get) "temp";
         self#box_say ("\n drop: " ^ uiopt#get_key_drop) "temp";
         crs#halfline;
         self#box_say ("| equipment: " ^ uiopt#get_key_equipment) 
                      "temp"; 
         self#box_say ("\n examine: " ^ uiopt#get_key_examine) 
                      "temp";
         crs#halfline;
         self#box_say ("| inventory: " ^ uiopt#get_key_inventory)
                      "temp";
         self#box_say ("\n next / more (long menus): " 
                       ^ uiopt#get_key_next) "temp";
         crs#halfline;
         self#box_say ("| previous: " ^ uiopt#get_key_previous) "temp";
         self#box_say ("\n pass (do nothing): " ^ uiopt#get_key_pass) 
                      "temp"; 
         crs#halfline;
         self#box_say ("| walk to attack\n\n") "temp";
         self#box_say ((String.make 18 ' ') ^
               "    ----====  Keybindings:  ====----\n") "temp";

         self#box_say ((String.make 21 ' ') ^ "-= Movement =-\n north: " 
               ^ uiopt#get_key_up) "temp";
         crs#halfline;
         self#box_say ("| south: " ^ uiopt#get_key_down) "temp";

         self#box_say ("\n west: " ^ uiopt#get_key_left) "temp";
         crs#halfline;
         self#box_say ("| east: " ^ uiopt#get_key_right) "temp";

         self#box_say ("\n northwest: " ^ uiopt#get_key_nw) "temp";
         crs#halfline;
         self#box_say ("| northeast: " ^ uiopt#get_key_ne) "temp";

         self#box_say ("\n southwest: " ^ uiopt#get_key_sw) "temp";
         crs#halfline;
         self#box_say ("| southeast: " ^ uiopt#get_key_se) "temp";
         self#box_say ("\n" ^ ( String.make 18 ' ' ) 
                            ^ "=-_Other commands_-=") "temp";
         self#box_say ("\n scroll map: " ^ uiopt#get_key_viewmap ^
                       " then long_walk+dir"
                      )
               "temp";
         crs#halfline;
         self#box_say ("| help: " ^ uiopt#get_key_help) "temp";

         self#box_say ("\n look: " ^ uiopt#get_key_look) "temp";
         crs#halfline;
         self#box_say ("| save & quit: " ^ uiopt#get_key_quit) "temp";

         self#box_say ("\n get: " ^ uiopt#get_key_get) "temp";
         crs#halfline;
         self#box_say ("| long walk: " ^ uiopt#get_key_longwalk) "temp";

         self#box_say ("\n follow stairs down: " ^ 
                       uiopt#get_key_downstair) "temp";
         crs#halfline;
         self#box_say ("| follow stairs up: " ^ uiopt#get_key_upstair)
                      "temp";

         self#box_say ("\n inventory: " ^ uiopt#get_key_inventory)
                      "temp";
         crs#halfline;
         self#box_say ("| option file = arid.rc") 
                      "temp"; 
         self#box_say ("\n drop: " ^ uiopt#get_key_drop) "temp";
         crs#halfline;
         self#box_say ("| multi-drop: " ^ uiopt#get_key_multidrop)
                      "temp";
(* gap *)
         self#box_say ("\n next / more (menus / messages): " 
                       ^ uiopt#get_key_next) "temp";
         crs#halfline;
         self#box_say ("| previous: " ^ uiopt#get_key_previous) "temp";
         self#box_say ("\n throw: " ^ uiopt#get_key_throw) "temp";
         crs#halfline;
         self#box_say ("| enter: " ^ uiopt#get_key_enter) "temp";
         self#box_say ("\n cast spell: " ^ uiopt#get_key_cast_spell) 
                      "temp";
         crs#halfline;
         self#box_say ("| equipment: " ^ uiopt#get_key_equipment) 
                      "temp"; 
         self#box_say ("\n examine: " ^ uiopt#get_key_examine) 
                      "temp";
         crs#halfline;
         self#box_say ("| pass (do nothing): " ^ uiopt#get_key_pass) 
                      "temp"; 
         self#sync;
        ); ignore (self#in_key);
        self#clear_win "temp";
*)
   end
;;

let new_toplevel_ui option = new toplevel_ui option
