(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    arid.ml

*  What does it do?

**    Declares the type for the game sessions, handles toplevel file 
operation requests and provides the toplevel game playloop.

*)

type action_processor_variety =
 | Standard_actproc
 | New_actproc
 | Banishment_actproc
;;

type player_information =
   {
      mutable people_last_seen : Person.person list;
      mutable surrounding_tiles : Terrain.terrain_variety array array
   }
;;

let proto_player_information =
   let pls = [] in
   let st = Array.make_matrix 3 3 Terrain.Sand in
      {
         people_last_seen = pls;
         surrounding_tiles = st
      }
;;

type session =
 {
   iid : Uaid.uaid;
   mutable chrname : string;
   mutable overmap : Mapstruc.mhold;
   mutable omap_filename : string;
   mutable own_filename : string;
   mutable species : Person.species;
   mutable pc : Person.person;
   mutable current_mapname : string;
   mutable actionq : Action.actionq;
   mutable msg_bank : Message.msgbank;
   mutable scenario : Scenemap.scenario;
   mutable plot : Plot.plot;
   mutable pc_info : player_information;
   mutable actproc : action_processor_variety;
   mutable item_prototypes : (I_base.base * Item.item) list;
 }
;;

let proto_session =
 {
   iid = Uaid.get_new_uaid (-1);
   chrname = "";
   overmap = Mapstruc.proto_mhold;
   omap_filename = "";
   own_filename = "";
   species = Person.Nothing;
   pc = Person.proto_person;
   current_mapname = "errormap";
   actionq = Action.proto_actionq;
   msg_bank = Message.proto_msgbank;
   scenario = Scenemap.proto_scenario;
   plot = Plot.proto_plot;
   pc_info = proto_player_information;
   actproc = New_actproc;
   item_prototypes = [];
 }
;;

type safe_bound =
 {
   mutable bx : int;
   mutable by : int;
   mutable ex : int;
   mutable ey : int
 }
;;

let proto_safe_bound = 
 {
   bx = 0;
   by = 0;
   ex = 64;
   ey = 64;
 }
;;

(*
 - The first two actprocs both point to the same thing. Why?
 - They are a stub for creation of new games with different command 
   sets.
      Just add a new type of actproc, update all the places where it 
   refers to Standard_actproc and New_actproc, and insert your new 
   action processor where appropriate.
*)

let get_update_visuals actproc =
   match actproc with
    | Standard_actproc -> S_ahandl.update_visuals
    | New_actproc -> S_ahandl.update_visuals
    | Banishment_actproc -> S_ahandl.update_visuals
;;

let get_actproc actproc =
   match actproc with
    | Standard_actproc -> S_ahandl.process_action
    | New_actproc -> S_ahandl.process_action
    | Banishment_actproc -> B_ahandl.process_action
;;

let fix_mapname session = 
   let map = Mapstruc.get_map session.overmap in
   let plane = Mapstruc.get_cplane map in
   let p_ref = Mapplane.get_p_ref plane in
   let nm = W_plane.planename p_ref in
      session.current_mapname <- nm
;;

let save_session session =   
   Action.remove_personlist session.actionq (Mapstruc.plist
                       (Mapstruc.get_map session.overmap));
   fix_mapname session;
   session.msg_bank <- Message.pared_msg_bank session.msg_bank;
   Mapstruc.save_mapstruc (Mapstruc.get_map session.overmap);
   Mapstruc.set_map session.overmap Mapstruc.m_struct;
   let fnm = session.own_filename in
      let ochan = open_out_bin fnm in
         (Marshal.to_channel ochan session [];
          close_out ochan
         )
;;

let get_name ui uiopt =
   let greeting = "This program comes with ABSOLUTELY NO WARRANTY:\n"
         ^ "See LICENSE for details\n "
         ^ "(C) 2004-2006 Jude Hungerford\n"
         ^  "\n   Welcome to ARID!\n\n"
         ^ "How shall I address you? "
   in
   let formal_address = ui#box_ask greeting (uiopt#get_key "big_window") 
   in
      (ui#say "\n Greetings, ";
       ui#say formal_address;
       ui#say "\n";
       ui#sync;
      );
   if formal_address = "" then "a" else
   formal_address
;;

let rootsav_name cname = cname ^ ".sav";;

let sav_exist cname =
   Sys.file_exists (rootsav_name cname)
;;

let real_load_session cnm =
   let filename = (rootsav_name cnm) in
      let ichan = open_in_bin filename in
         let session = Marshal.from_channel ichan in
         (close_in ichan;
          session
         )
;;

let load_session cname =
(* DEBUG
   print_string "Loading session\n";
/DEBUG *)
   let lsession = real_load_session cname in
      Mapstruc.set_map lsession.overmap (
                       Map_api.load_mapstruc
                          lsession.current_mapname 
                          lsession.omap_filename 
                          lsession.pc cname 
                          (Mapstruc.get_fid lsession.overmap)
                          lsession.iid lsession.item_prototypes
                         );
      lsession
;;

let setup_player_character ui ui_option session =
   let lx, ly = ((Scenemap.get_start_lx session.scenario),
                 (Scenemap.get_start_ly session.scenario) 
                )
   in
      let start_x, start_y =
(*         lx, ly *)
         if not (lx = -1) then lx, ly
         else
            let map = Map_api.get_map session.overmap in
            let plane = Mapstruc.get_cplane map in
            let tbx = Mapplane.get_totalbound_x plane in
            let tby = Mapplane.get_totalbound_y plane in
              (
               Dungeon.random_open_tile plane 0 0 tbx tby
              )
(* *)
      in
         Person.set_locx session.pc start_x;
         Person.set_locy session.pc start_y;
   match Scenemap.get_variety session.scenario with
     Scenario.Ascending_1 ->
      (
       Person.add_head session.pc "head";
       Person.add_torso session.pc "torso";
       Person.add_hand session.pc "right hand";
       Person.add_hand session.pc "left hand";
       Person.add_feet session.pc "feet";
(*       Person.gain_spell Item.spell_magic_dart session.pc; *)
      )
    | Scenario.Tiny_rl ->
         let st = Person.get_stat session.pc in
            Person.add_head session.pc "head";
            Person.add_torso session.pc "torso";
            Person.add_hand session.pc "right hand";
            Person.add_hand session.pc "left hand";
    | Scenario.Banishment -> 
       (
          Person.add_head session.pc "head";
          Person.add_torso session.pc "torso";
          Person.add_hand session.pc "right hand";
          Person.add_hand session.pc "left hand";
          Person.add_feet session.pc "feet";
(*
          let st = Person.get_stat session.pc in
               Stat.grant_stat st Stat.Magic_b 20;
               Stat.grant_stat st Stat.Muscle_b 20
*)
             let pspecies = 
                Specsel.pick_banishment_species ui ui_option
             in
             session.species <- pspecies;
             Specsel.generate_character_b ui ui_option pspecies
                session.pc
       )
    | _ -> 
       (
          Person.add_head session.pc "head";
            Person.add_torso session.pc "torso";
            Person.add_hand session.pc "right hand";
            Person.add_hand session.pc "left hand";
       )
;;

let potion_names = ref
   [
      ("bubbling yellow potion", U_colour.Brightyellow);
      ("murky green potion", U_colour.Green);
      ("grey potion", U_colour.White)
   ]
;;

let new_session cnm ui ui_option =
(* DEBUG
   print_string "New session\n";
/DEBUG *)
   let new_iid = Uaid.get_new_uaid (-1) in
   let new_scenario, link, new_plot, prototypes, use_actproc = 
      match ui_option#get_scenario with
       | "Banishment" ->
            (Scenemap.new_scenario Scenario.Banishment
             W_plane.b_pit),
             W_plane.banish_link,
             Plot.proto_plot,
             (B_item.new_prototype_list potion_names ),
             Banishment_actproc
       | _ (* "Ascending_1" *) ->
            (Scenemap.new_scenario Scenario.Ascending_1 
             W_plane.ascending_1_baselevel),
             W_plane.ascending_1_link,
             Plot.ascending1_plot, [], Standard_actproc
   in
   let startplane_ref = Scenemap.get_start_plane new_scenario in
   let bsizex = W_plane.get_bsize_x startplane_ref in
   let bsizey = W_plane.get_bsize_y startplane_ref in
   let session =
   { proto_session with
      iid = new_iid;
      chrname = cnm;
      overmap = Map_api.new_map
         (Scenemap.get_start_plane new_scenario)
         bsizex bsizey new_iid;
      own_filename = rootsav_name cnm;
      scenario = new_scenario;
      plot = new_plot;
      actproc = use_actproc;
      item_prototypes = prototypes
   } in
      (
       ui#say "Creating new world...\n";
       (match (Scenemap.get_variety session.scenario) with
          Scenario.Test ->
            (
(*
             session.species <- Specsel.pick_species ui ui_option
*)
            )
        | Scenario.Ascending_1 ->
            (
            )
        | Scenario.Banishment ->
           (
            Message.add_msg session.msg_bank 
                         ( Action.get_gametime session.actionq )
                         Message.Player
                         "You have been banished.";
           )
        | _ -> ()
       )
       ;
       ui#sync;
       fix_mapname session;
       Map_api.mapstruc_init (Mapstruc.get_map session.overmap)
                              session.current_mapname bsizex bsizey
                              cnm (Mapstruc.get_fid session.overmap)
                              session.scenario link;
       session.omap_filename <- 
          Mapstruc.get_msfilename (Mapstruc.get_map session.overmap);
       session.pc <- Person.new_player cnm session.iid;
(* This is necessary before setup_player *)
       save_session session;
       let nsession = load_session cnm in
(* *)
       setup_player_character ui ui_option nsession;
       nsession.actionq <- 
          Action.new_actionq (Person.get_id nsession.pc);
       save_session nsession;
       load_session cnm
      )
;;

(*
   NOTE - the get_session function must initialise the mersenne twister 
in algo.ml from above.
*)

let get_session ui ui_option =
   let seed = Random.int (1000000) in
   let mersennet = (Mersenne.make (`Seed seed)) in
   let mersenne_hold = Algo.uni_mersenne in
      Algo.set_mersenne_state mersenne_hold mersennet;
   let cname = get_name ui ui_option in
    let ses =
      match (sav_exist cname) with
         true -> 
            ui#say "Welcome back, "; ui#say cname; ui#say "\n";
            load_session cname
       | false -> 
            let ns = new_session cname ui ui_option in
               save_session ns;
               load_session cname
    in (
        Command.prep_fix_vismap ses.overmap ses.pc;
        Action.activate ses.actionq
                        (Mapstruc.plist
                         (Mapstruc.get_map ses.overmap)
                        );
        ses
       )
;;

type tshold =
 {
   mutable s: session
 }
;;

let switch_from_open_portal ui session portl fromplane lx ly =
            Plot.check_stair session.plot (Stair.get_plotlink portl);
            Map_api.switch_planes session.overmap 
               (Stair.get_to_plane portl)
               session.omap_filename 
               session.pc
               portl
               session.chrname
               (Mapstruc.get_fid session.overmap)
               session.iid session.scenario session.item_prototypes;
            Message.add_msg session.msg_bank 
                         ( Action.get_gametime session.actionq )
                         Message.Player
                         ("You follow the winding " ^ 
                            (Stair.get_name portl) ^ "."
                         );
           let portlnow = 
              Mapstruc.stair_on_plane_loc 
                 (Mapstruc.get_map session.overmap) fromplane lx ly 
           in
           Person.set_locx session.pc (Stair.get_to_lx portlnow);
           Person.set_locy session.pc (Stair.get_to_ly portlnow);
           Command.set_vision_distance 
                 (Mapstruc.get_cplane 
                    (Mapstruc.get_map session.overmap)
                 ) session.pc;
;;

let switch_levels ui session =
   try
     (
      Action.remove_personlist session.actionq (Mapstruc.plist
                          (Mapstruc.get_map session.overmap));
      let omap = Mapstruc.get_map session.overmap in
      let fromplane = Mapstruc.get_cplane omap in
      let lx = Person.get_locx session.pc in
      let ly = Person.get_locy session.pc in
         let portl = Mapstruc.stair_on_loc omap lx ly in
         if Stair.is_open portl then
          (
           try
             switch_from_open_portal ui session portl fromplane lx ly
           with Not_found -> 
              print_string "\nfailed in arid.sf open_portal"
          )
         else
           (
            try
               Message.add_msg session.msg_bank
                         ( Action.get_gametime session.actionq )
                         Message.Player
                         ("The " ^ 
                            (Stair.get_name portl) ^ " is closed."
                         )
            with Not_found -> ()
           );
         Action.Continue
     )
   with Not_found -> 
     (
      (print_string ("Something went wrong switching levels:\n"
          ^ "Not found\n")
      );
      Action.Continue
     )
    | Failure ("Victory") -> 
            (Action.Victory)
;;

let init_vmap session protolist =
   let plane = (Mapstruc.get_cplane
         (Mapstruc.get_map session.overmap)) in
      if Mapplane.is_big plane then
         Command.fix_vismap plane 
           (Mapstruc.get_map session.overmap)
           (Person.get_locx session.pc) (Person.get_locy session.pc)
      else
         Command.fix_vismap plane 
           (Mapstruc.get_map session.overmap)
           ((Mapplane.get_tp_size_x plane) / 2)
           ((Mapplane.get_tp_size_y plane) / 2)
   ;
   Command.paint_vismap (Mapstruc.get_map session.overmap) 
                        session.current_mapname session.pc
                        (Action.get_gametime session.actionq) protolist
;;

let play shold ui uiopt =
   let session = shold.s in
   let cmap = Mapstruc.get_map session.overmap in
   let actproc = get_actproc session.actproc in
      let nxt =
         (actproc ui uiopt session.overmap
            (Mapstruc.current_mapname cmap)
            session.pc session.actionq session.iid session.msg_bank
            session.item_prototypes
         )
      in
      match nxt with
         Action.Continue -> Action.Continue
       | Action.Savecontinue ->
            (
             Command.paint_vismap cmap session.current_mapname
                                  session.pc 
                                  (Action.get_gametime session.actionq)
                                  session.item_prototypes;
             Vismap.paint_dirty cmap (Mapstruc.get_cplane cmap);
             save_session session;
             (shold.s <- (load_session session.chrname));
             let plist = 
                (Mapstruc.plist
                 (Mapstruc.get_map shold.s.overmap)
                )
             in
             init_vmap shold.s shold.s.item_prototypes; 
             Action.activate shold.s.actionq plist;
             Action.Continue
            )
       | Action.Switchcontinue ->
            let omap = Mapstruc.get_map session.overmap in
            Command.paint_vismap omap session.current_mapname
                                 session.pc
                                 (Action.get_gametime session.actionq)
                                 session.item_prototypes;
            Vismap.paint_dirty omap (Mapstruc.get_cplane omap);
            let retval = switch_levels ui session in
            save_session session;
            (shold.s <- (load_session session.chrname));
            init_vmap shold.s shold.s.item_prototypes;
            Action.activate shold.s.actionq
                            (Mapstruc.plist
                                (Mapstruc.get_map shold.s.overmap)
                            );
            retval
       | _ ->
         (save_session session;
(*
          ui#special_font_init ();
*)
          Action.Quit
         )
;;

let delete_toplevel_savefile shold = 
   let cname = shold.s.chrname in
   save_session shold.s;
   let new_s = load_session cname in
   Mapstruc.delete_mapstruc new_s.overmap;
   try
      Sys.remove new_s.own_filename;
   with _ -> ()
;;

let splash_death_screen shold ui uiopt = 
   let s = shold.s in
   let cmap = (Map_api.get_map s.overmap) in
   let act = Action.new_action_request 0 (Action.get_gametime s.actionq) 
   in
   let planename = (Mapstruc.current_mapname cmap) in
   let update_visuals = get_update_visuals s.actproc in
      update_visuals ui uiopt cmap planename s.pc s.pc act 
            s.actionq s.msg_bank true s.item_prototypes;
   print_string "\n\n   You died.\n\n" 
;;

let splash_victory shold ui uiopt =
   let s = shold.s in
   let cmap = (Map_api.get_map s.overmap) in
   let act = Action.new_action_request 0 (Action.get_gametime s.actionq) 
   in
   let planename = (Mapstruc.current_mapname cmap) in
   let update_visuals = get_update_visuals s.actproc in
      update_visuals ui uiopt cmap planename s.pc s.pc act 
            s.actionq s.msg_bank true s.item_prototypes;
   print_string "\n\nYou have won the game!\n\n";
   print_string ("Your final score was: " ^ 
                 (string_of_int (Person.get_health shold.s.pc))
                 ^ "\n\n")
;;

let rec playloop shold ui uiopt =
   if not (Person.is_dead shold.s.pc) then
      let nxt = play shold ui uiopt in
         match nxt with
               Action.Continue -> playloop shold ui uiopt
             | Action.Victory -> 
                  splash_victory shold ui uiopt
             | _ -> print_string ("\n\n\nThankyou for playing!\n\n" ^
                  "ARID is distributed under the GNU GPL, and comes \
                   with \nABSOLUTELY NO WARRANTY: see LICENSE \
                   for details.\n\n")
   else
     (
(*
      delete_toplevel_savefile shold;
      splash_death_screen ui uiopt shold.s.pc;
*)
     )
;;

let fix_person_loc session =
   let lx = Person.get_locx session.pc in
      if not (lx = -1) then ()
      else
         let new_lx, new_ly = 
            let map = Map_api.get_map session.overmap in
            let plane = Mapstruc.get_cplane map in
            let tbx = Mapplane.get_totalbound_x plane in
            let tby = Mapplane.get_totalbound_y plane in
              (
               Dungeon.random_open_tile plane 0 0 tbx tby
              )
         in
         (
            Person.set_locx session.pc new_lx;
            Person.set_locy session.pc new_ly
         )
;;

let begin_game =
   Random.self_init ();
   let ui_option = Ui_opt.get_option in
      let ui = Api_ui.new_toplevel_ui ui_option in
       try
        (
         ui#initialise;
         let session = get_session ui ui_option in
            fix_person_loc session;
            init_vmap session session.item_prototypes;
            let shold = {s = session} in
            try 
              (
               playloop shold ui ui_option;
               if Person.is_dead shold.s.pc then
                 (
                  let cname = shold.s.chrname in
                  save_session shold.s;
                  let final_session = load_session cname in
                  delete_toplevel_savefile 
                        {s = final_session}; 
                  splash_death_screen shold ui ui_option;
                 )
               ; ui#end_ui
              )
             with e ->
              (
               print_string 
                  ("\n\nERROR: Playloop failed on exception :\n" 
                   ^ Printexc.to_string e ^
                   "\nPlease report this bug to the developer.\n\n");
               ui#end_ui;
(*
               print_string 
                  ("Attempting to save game. It will probably fail. "
                  ^ "\nSorry.\n\n");
               save_session session
*)
              )
        )
       with e ->
        (
         print_string 
            ("\n\nERROR: program failed on exception :\n" 
             ^ Printexc.to_string e ^
             "\nPlease report this bug to the developer.\n\n");
         ui#end_ui
        )
;;

begin_game;;
