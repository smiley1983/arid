(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*)

(*

*  What is this file?

**    b_ahandl.ml

*  What does it do?

**    Provides a string of action handlers for the Banishment module. 
See s_ahandl.ml for details.

*)

let proc_act_show_skills ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Show_skills -> 
        (
         let ispc = S_ahandl.resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in
            Command.display_skills ui uiopt actor;
            Action.next_act actq actor acttime
                  (Person.is_pc actor);
            Action.Continue
        )
    | _ -> Action.Unprocessed
;;

(*
let proc_act_ ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist protolist
=
   match act.Action.variety with
    | Actpit. -> 
        (
         let ispc = S_ahandl.resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in

        )
    | _ -> Action.Unprocessed
;;
*)

let do_bump ui uiopt mhold planename pc actor act actq dx dy iid msgbank
   protolist
=
   let acttime = Action.get_time act in
   let gtime = Action.get_gametime actq in
   let ax, ay = Person.get_loc actor in
   let px, py = Person.get_loc pc in
   let ms = Map_api.get_map mhold in
   let plane = Mapstruc.get_cplane ms in
      let (lx, ly) = (ax + dx), (ay + dy) in
         let ppl = Mapstruc.people_on_loc plane lx ly in
            if ppl = [] && not ((lx, ly) = (px, py)) then
               S_ahandl.do_walk ui uiopt mhold planename actor act actq
                     dx dy msgbank protolist
            else if
               Bcommand.attack_spot ms plane pc actor msgbank
                     gtime iid lx ly protolist
            then
                  (
                    Action.set_variety act Actpit.Attack;
                    let delay = Action.act_time actor act in
                    Action.set_time act (acttime + delay);
                    Action.next_act actq actor
                          (acttime + delay) (Person.is_pc actor);
                   Action.Continue
                  )
            else
                  (
                   Action.next_act actq actor acttime
                         (Person.is_pc actor);
                   Action.Continue
                  )
;;


let proc_act_bump_n ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_n ->
        (
         do_bump ui uiopt mhold planename pc actor act actq 0 (-1)
               iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_e ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_e ->
        (
            do_bump ui uiopt mhold planename pc actor
                    act actq 1 0 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_s ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_s ->
        (
            do_bump ui uiopt mhold planename pc actor
                    act actq 0 1 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_w ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_w ->
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq (-1) 0 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_ne ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_ne ->
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq 1 (-1) iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_nw ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_nw ->
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq (-1) (-1) iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;
let proc_act_bump_se ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_se ->
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq 1 1 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_sw ui uiopt mhold planename pc actor act actq iid
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_sw ->
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq (-1) 1 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;


let processor_chain_b = 
[
      S_ahandl.proc_act_request;
      S_ahandl.proc_act_help;
      S_ahandl.proc_act_short_look;
      S_ahandl.proc_act_look_mode;
      proc_act_bump_n;
      proc_act_bump_e;
      proc_act_bump_s;
      proc_act_bump_w;
      proc_act_bump_ne;
      proc_act_bump_nw;
      proc_act_bump_se;
      proc_act_bump_sw;
      S_ahandl.proc_act_follow_downstair;
      S_ahandl.proc_act_follow_upstair;
      S_ahandl.proc_act_show_inventory;
      S_ahandl.proc_act_show_equipment;
      S_ahandl.proc_act_pick_up;
      S_ahandl.proc_act_drop_it;
      S_ahandl.proc_act_multi_drop;
      S_ahandl.proc_act_attack;
      S_ahandl.proc_act_throw;
      S_ahandl.proc_act_cast_spell;
      S_ahandl.proc_act_drink;
      S_ahandl.proc_act_read;
      S_ahandl.proc_act_examine;
      S_ahandl.proc_act_pass;
      proc_act_show_skills;
      S_ahandl.proc_act_follow;
      S_ahandl.proc_act_missile_trail;
      S_ahandl.proc_act_remove_missile_trail;
      S_ahandl.proc_act_wizard_show_actionq;
      S_ahandl.proc_act_switch_wizard_vision;
      S_ahandl.proc_act_other;
]
;;

let process_action ui uiopt overmap planename pc actq iid msgbank 
      protolist
=
 try
   match Action.get_actlist actq with
      [] -> (print_string "action q empty!" ; Action.Quit)
    | head :: tail ->
        (
         let actorid = Action.get_actorid head in
         Action.set_actlist actq tail;
         let actor = ref
            (Mapstruc.person_of_id (Mapstruc.get_map overmap)
                                   actorid
            )
         in (* FIXME - this should be handled by the above function*)
           if actorid = (Person.get_id pc) then
               (
                actor := pc
               )
           ;
           if not (Person.get_id !actor = - 1) then
             (
              S_ahandl.chain_process_action ui uiopt overmap planename 
                    pc !actor head actq iid msgbank processor_chain_b 
                    protolist

             )
           else (
              Action.Continue)
        )
 with Failure ("PC_DIED") -> raise (Failure ("PC_DIED"))
    | Not_found -> raise (Failure ("Failed to process action"))
;;

