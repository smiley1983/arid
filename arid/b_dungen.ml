(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    b_dungen.ml

*  What does it do?

**    Provides dungeon generators for the Banishment module.

*)

type fillstat =
 | Empty
 | Rectangle
 | Corridor
;;

type dungeon_cell =
   {
      mutable fill_status : fillstat;
      mutable open_space : (int * int) list;
      mutable item_priority : (int * int) list;
      mutable connects_to : (int * int) list;
      mutable has_stair : bool
   }
;;

type drawing_board =
   {
      mutable cell : dungeon_cell array array
   }
;;

let initialise_bcells b_grid wide high =
   for count_y = 0 to (high - 1) do
      for count_x = 0 to (wide - 1) do
         b_grid.(count_x).(count_y) <- 
            {
             fill_status = Empty; open_space = []; item_priority = [];
             connects_to = []; has_stair = false
            }
      done
   done
;;

let empty_drawing_board cells_wide cells_high =
   let dungeon_cell = 
         {
          fill_status = Empty;
          open_space = [];
          item_priority = [];
          connects_to = [];
          has_stair = false
         } 
   in
   let cells = 
      Array.make_matrix cells_wide cells_high dungeon_cell
   in
      {
         cell = cells
      }
;;

let random_cell cells_wide cells_high =
   let pick_x = 
         if cells_wide < 2 then 0
         else (Algo.mersenne (cells_wide))
   in
   let pick_y =
         if cells_high < 2 then 0
         else (Algo.mersenne (cells_high))
   in
      pick_x, pick_y
;;

let pref_random_cell cells_wide cells_high opencells =
   match opencells with
      [] -> random_cell cells_wide cells_high
    | _ ->
         let choices = List.length opencells in
            let choice = 
               if choices > 1 then
                  Algo.mersenne choices 
               else 0
            in
               try
                  List.nth opencells choice
               with e ->
                  print_string ((Printexc.to_string e) ^ "\n");
                  random_cell cells_wide cells_high
;;

let rec trim_list ol cell =
   match ol with
      [] -> []
    | head :: tail ->
         if head = cell then
            trim_list tail cell
         else
            head :: trim_list tail cell
;;

let cell_is_open b_grid cells_wide cells_high (cx, cy) =
   if cx < 0 || cy < 0 || cx >= cells_wide || cy >= cells_high then
      raise Not_found
   else
      let cell = b_grid.(cx).(cy) in
         if cell.fill_status = Empty then
            true
         else
            false
;;

let check_ocell b_grid cells_wide cells_high lx ly cmp =
   try
      if (cell_is_open b_grid cells_wide cells_high (lx, ly)) = cmp
      then 
         [(lx, ly)]
      else 
        []
   with Not_found -> []
;;

let cells_next_to b_grid cells_wide cells_high (lx, ly) cmp =
   let cw = check_ocell b_grid cells_wide cells_high (lx - 1) ly
            cmp 
   in
   let ce = check_ocell b_grid cells_wide cells_high (lx + 1) ly
            cmp
   in
   let cn = check_ocell b_grid cells_wide cells_high lx (ly - 1)
            cmp
   in
   let cs = check_ocell b_grid cells_wide cells_high lx (ly + 1)
            cmp
   in
      let full_list = cw @ ce @ cn @ cs in
(* DEBUG
         print_string ((string_of_int (List.length full_list))
               ^ "found " ^ (if cmp then "true" else "false") ^ "\n");
/ DEBUG *)
         full_list
;;

let choose_connection connect_to =
   let choices = List.length connect_to in
      let choice =
         if choices < 1 then raise Not_found
         else if choices < 2 then 0
         else Algo.mersenne choices
      in
         List.nth connect_to choice
;;

let fixed_connect_cell_east plane portal lx1 ly1 cells_wide cells_high
   cell_width cell_height b_grid cx cy
   offset_x offset_y end_x end_y
=
   let fill_x = offset_x in
   let fill_y = offset_y + (max 1 (cell_height / 2)) in
      Dungeon.fill_tile plane fill_x fill_y Terrain.Stone_floor
;;

let fixed_connect_cell_west plane portal lx1 ly1 cells_wide cells_high
   cell_width cell_height b_grid cx cy
   offset_x offset_y end_x end_y
=
   let fill_x = end_x + 1 in
   let fill_y = offset_y + (max 1 (cell_height / 2)) in
      Dungeon.fill_tile plane fill_x fill_y Terrain.Stone_floor
;;

let fixed_connect_cell_north plane portal lx1 ly1 cells_wide cells_high
   cell_width cell_height b_grid cx cy
   offset_x offset_y end_x end_y
=
   let fill_x = offset_x + (max 1 (cell_width / 2)) in
   let fill_y = offset_y in
      Dungeon.fill_tile plane fill_x fill_y Terrain.Stone_floor
;;

let fixed_connect_cell_south plane portal lx1 ly1 cells_wide cells_high
   cell_width cell_height b_grid cx cy
   offset_x offset_y end_x end_y
=
   let fill_x = offset_x + (max 1 (cell_width / 2)) in
   let fill_y = end_y + 1 in
      Dungeon.fill_tile plane fill_x fill_y Terrain.Stone_floor
;;

let fixed_connect_cell plane portal lx1 ly1 cells_wide cells_high
      cell_width cell_height b_grid cx cy connect_to
      offset_x offset_y end_x end_y
=
   if connect_to = [] then ()
   else
    try
      let connect_to_x, connect_to_y = choose_connection connect_to in
         if connect_to_x < cx then
            fixed_connect_cell_east plane portal lx1 ly1 
                  cells_wide cells_high
                  cell_width cell_height b_grid cx cy
                  offset_x offset_y end_x end_y
         else if connect_to_x > cx then
            fixed_connect_cell_west plane portal lx1 ly1 
                  cells_wide cells_high
                  cell_width cell_height b_grid cx cy
                  offset_x offset_y end_x end_y
         else if connect_to_y < cy then
            fixed_connect_cell_north plane portal lx1 ly1 
                  cells_wide cells_high
                  cell_width cell_height b_grid cx cy
                  offset_x offset_y end_x end_y
         else if connect_to_y > cy then
            fixed_connect_cell_south plane portal lx1 ly1 
                  cells_wide cells_high
                  cell_width cell_height b_grid cx cy
                  offset_x offset_y end_x end_y
     with e -> print_string ("fixed_connect " ^ Printexc.to_string e)
;;

let pickup_opentile cell =
   if cell.open_space = [] then raise Not_found
   else
      let choices = List.length cell.open_space in
      let choice = Algo.mersenne choices in
         let result =
            List.nth cell.open_space choice
         in
            if List.length cell.open_space > 1 then
               cell.open_space <- 
                  (Algo.list_without cell.open_space result);
            result
;;

let choose_opentile cell =
   if cell.open_space = [] then raise Not_found
   else
      let choices = List.length cell.open_space in
      let choice = Algo.mersenne choices in
            List.nth cell.open_space choice
;;

let choose_priority_openspace cell =
   if cell.item_priority = [] then choose_opentile cell
   else
      let choices = List.length cell.item_priority in
      let choice = Algo.mersenne choices in
         List.nth cell.item_priority choice
;;

let pickup_priority_openspace cell =
   if cell.item_priority = [] then pickup_opentile cell
   else
      let choices = List.length cell.item_priority in
      let choice = Algo.mersenne choices in
         let result =
            List.nth cell.item_priority choice
         in
            cell.item_priority <- 
               (Algo.list_without cell.item_priority result);
            result
;;

let dungeon1_connect_cell plane lx1 ly1 lx2 ly2
      cells_wide cells_high
      cell_width cell_height b_grid cx cy connect_to
      offset_x offset_y end_x end_y
=
   if connect_to = [] then ()
   else
      let connect_to_x, connect_to_y = choose_connection connect_to in
         let fromcell = b_grid.cell.(cx).(cy) in
         let tocell = b_grid.cell.(connect_to_x).(connect_to_y) in
         fromcell.connects_to <- 
               (connect_to_x, connect_to_y) :: fromcell.connects_to;
         tocell.connects_to <- (cx, cy) :: tocell.connects_to;
          try
            let from_lx, from_ly = choose_opentile fromcell in
            let to_lx, to_ly = choose_opentile tocell in
               let opentiles =
                  Dungeon.passageconnect_points_ret_opentiles plane
                     from_lx from_ly to_lx to_ly lx1 ly1 lx2 ly2
               in
                  fromcell.open_space <- 
                     fromcell.open_space @ opentiles;
          with Not_found -> print_string "connection not made\n"
;;

let carve_maze_cell 
   plane portal lx1 ly1 lx2 ly2 cells_wide cells_high 
   cell_width cell_height b_grid (cx, cy)
=
   let this_cell = b_grid.cell.(cx).(cy) in
      this_cell.fill_status <- Rectangle;
   let offset_x = lx1 + (cx * cell_width)  in
   let offset_y = ly1 + (cy * cell_height)  in
      let end_x = offset_x + (cell_width - 1) in
      let end_y = offset_y + (cell_height - 1) in
         Dungeon.carve_rectangle plane 
            (offset_x + 1) (offset_y + 1) end_x end_y
            Terrain.Stone_floor;
         this_cell.open_space <- 
            (end_x, end_y) :: this_cell.open_space;
   let connect_to = 
      cells_next_to b_grid.cell cells_wide cells_high (cx, cy) false
   in
      fixed_connect_cell plane portal lx1 ly1 cells_wide cells_high
            cell_width cell_height b_grid cx cy connect_to
            offset_x offset_y end_x end_y
;;

let shop_exit plane lx ly gx gy cardinal =
   let dx = gx - lx in
   let dy = gy - ly in
   let bx, by =
      match cardinal with
       | Algo.North -> (lx + (dx / 2)), (ly + 1)
       | Algo.South -> (lx + (dx / 2)), gy
       | Algo.East -> gx, (ly + (dy / 2))
       | _ -> (lx + 1), (ly + (dy / 2))
   in
      Dungeon.fill_tile plane bx by Terrain.Sand
;;

let carve_shop_cell 
   plane portal lx1 ly1 lx2 ly2 cells_wide cells_high 
   cell_width cell_height b_grid (cx, cy)
=
   let this_cell = b_grid.cell.(cx).(cy) in
      this_cell.fill_status <- Rectangle;
   let offset_x = lx1 + (cx * cell_width) + 1 in
   let offset_y = ly1 + (cy * cell_height) + 1 in
      let end_x = offset_x + (cell_width - 1 ) in
      let end_y = offset_y + (cell_height - 1) in
         Dungeon.carve_rectangle plane 
            (offset_x + 1) (offset_y + 1) end_x end_y
            Terrain.Rock_wall;
         Dungeon.carve_rectangle plane 
            (offset_x + 2) (offset_y + 2) (end_x - 1) (end_y - 1)
            Terrain.Stone_floor;
         this_cell.open_space <- 
            ((end_x - 1), (end_y - 1)) :: this_cell.open_space;
      let rc = Algo.random_cardinal 4 in
         shop_exit plane offset_x offset_y end_x end_y rc
;;

let add_room_cell plane portal b_grid cx cy lx ly gx gy =
   let this_cell = b_grid.cell.(cx).(cy) in
   let dx = gx - (lx + 1) in
   let dy = gy - (ly + 1) in
      let hdx = max 1 (dx / 2 - 1) in
      let hdy = max 1 (dy / 2 - 1) in
         let lx2 = (lx + (Algo.mersenne hdx)) + 1 in
         let ly2 = (ly + (Algo.mersenne hdy)) + 1 in
         let gx2 = gx - (Algo.mersenne hdx) in
         let gy2 = gy - (Algo.mersenne hdy) in
         let dx2 = Algo.mersenne (gx2 - lx2) in
         let dy2 = Algo.mersenne (gy2 - ly2) in
            this_cell.open_space 
                  <- ((lx2 + dx2, ly2 + dy2)) :: this_cell.open_space;
         let points = 
            Dungeon.carve_rectangle_ret_points plane 
                  lx2 ly2 gx2 gy2 Terrain.Stone_floor;
         in
            this_cell.open_space <- this_cell.open_space @ points;
(* DEBUG
            print_string (string_of_int (List.length points) ^ "\n")
/DEBUG *)
;;

let add_corridor_cell plane portal b_grid cx cy lx ly gx gy =
   let this_cell = b_grid.cell.(cx).(cy) in
   let px, py = Algo.random_bounded_crd (lx + 1) (ly + 1) gx gy in
      this_cell.item_priority <- (px, py) :: this_cell.item_priority;
      this_cell.open_space <- 
         ((px), (py)) :: this_cell.open_space;
      Dungeon.fill_tile plane px py Terrain.Stone_floor
;;

let get_offset_and_ends lx1 ly1 cell_width cell_height cx cy =
   let offset_x = lx1 + (cx * cell_width)  in
   let offset_y = ly1 + (cy * cell_height)  in
   let end_x = offset_x + (cell_width - 1) in
   let end_y = offset_y + (cell_height - 1) in
      offset_x, offset_y, end_x, end_y
;;

let carve_dungeon1_cell 
   plane portal lx1 ly1 lx2 ly2 cells_wide cells_high 
   cell_width cell_height b_grid (cx, cy)
=
   let offset_x, offset_y, end_x, end_y =
      get_offset_and_ends lx1 ly1 cell_width cell_height cx cy
   in
   let offset_x = lx1 + (cx * cell_width)  in
   let offset_y = ly1 + (cy * cell_height)  in
   let end_x = offset_x + (cell_width - 1) in
   let end_y = offset_y + (cell_height - 1) in
   let connect_to = 
      cells_next_to b_grid.cell cells_wide cells_high (cx, cy) false
   in
   let chance = 
      if (List.length connect_to) = 0 then
         0
      else
         Algo.mersenne 3 
   in
   let fillwith =
      if chance = 0 then
        (
         add_room_cell plane portal b_grid cx cy offset_x offset_y 
               end_x end_y;
         Rectangle
        )
      else
        (
         add_corridor_cell plane portal b_grid cx cy 
               offset_x offset_y end_x end_y;
         Corridor
        )
   in
      b_grid.cell.(cx).(cy).fill_status <- fillwith;
   ;
      dungeon1_connect_cell plane lx1 ly1 lx2 ly2
            cells_wide cells_high
            cell_width cell_height b_grid cx cy connect_to
            offset_x offset_y end_x end_y
   
;;

let cell_creature plane iid this_cell creaturef =
   let lx, ly = pickup_opentile this_cell in
   let this_id = Uaid.new_aid iid in
      let critter = creaturef lx ly this_id in
         let mb = Mapplane.grid_get_mbcontent plane lx ly in
         let plist = Mapblock.get_person mb in
            Person.add_person critter plist
;;

let cell_giant_centipede plane iid this_cell =
   cell_creature plane iid this_cell B_person.new_giant_centipede
;;

let rec cell_cskel_pack plane iid this_cell repeat =
   match repeat with
      0 -> ()
    | n ->
         (
            cell_creature plane iid this_cell 
                  B_person.new_canine_skeleton;
            cell_cskel_pack plane iid this_cell (n - 1)
         )
;;

let populate_cell plane iid b_grid this_cell
      dangerlevel 
=
(*
   match dangerlevel with
*)
         match (Algo.mersenne 3) with
          | 0 | 1 -> cell_giant_centipede plane iid this_cell
          | _ -> 
               cell_cskel_pack plane iid this_cell 
                     ((Algo.mersenne 2) + 3)
;;

let place_cell_item plane iid this_cell itemf prototype =
   let lx, ly = pickup_opentile this_cell in
   let this_id = Uaid.new_aid iid in
      let thing = itemf lx ly this_id prototype in
         let mb = Mapplane.grid_get_mbcontent plane lx ly in
            Mapblock.gain_item mb thing
;;

let cell_item plane iid b_grid this_cell
      rewardlevel prototype
=
(*
   match rewardlevel with
*)
         match (Algo.mersenne 3) with
          | 0 ->
               place_cell_item plane iid this_cell B_item.healing_potion
                  prototype
          | 1 -> 
               place_cell_item plane iid this_cell B_item.longsword 
                  prototype
          | _ -> 
               place_cell_item plane iid this_cell B_item.apple
                  prototype
;;

let rec place_items plane iid b_grid cells_wide cells_high opencells 
   prototype repeat 
=
   if repeat < 1 then ()
   else
      let cx, cy = pref_random_cell cells_wide cells_high !opencells in
         opencells := trim_list !opencells (cx, cy);
      let this_cell = b_grid.cell.(cx).(cy) in
         cell_item plane iid b_grid this_cell 0 prototype;
   place_items plane iid b_grid cells_wide cells_high opencells
      prototype (repeat - 1)
;;

let rec place_critters plane iid b_grid cells_wide cells_high opencells 
   repeat 
=
   if repeat < 1 then ()
   else
      let cx, cy = pref_random_cell cells_wide cells_high !opencells in
         opencells := trim_list !opencells (cx, cy);
      let this_cell = b_grid.cell.(cx).(cy) in
         populate_cell plane iid b_grid this_cell 0;
   place_critters plane iid b_grid cells_wide cells_high opencells
      (repeat - 1)
;;

let place_stair plane b_grid stair cells_wide cells_high opencells =
   let cx, cy = pref_random_cell cells_wide cells_high !opencells in
      opencells := trim_list !opencells (cx, cy);
      let this_cell = b_grid.cell.(cx).(cy) in
       try
(* DEBUG
         print_string "stair\n";
/DEBUG *)
         let lx, ly = 
            if this_cell.fill_status = Corridor then
               pickup_priority_openspace this_cell 
            else
               pickup_opentile this_cell
         in
            Stair.set_lx stair lx;
            Stair.set_ly stair ly;
            this_cell.has_stair <- true
       with Not_found -> print_string "\nstair not placed"
;;

let rec recursive_place_portals plane portal b_grid 
      cells_wide cells_high opencells
=
   match portal with
      [] -> ()
    | head :: tail ->
         place_stair plane b_grid head cells_wide cells_high opencells ;
         recursive_place_portals plane tail b_grid cells_wide cells_high
               opencells
;;

let add_features plane portal iid b_grid cells_wide cells_high 
   prototype 
=
   let open_cells = ref [] in
      for countx = 0 to (cells_wide - 1) do
         for county = 0 to (cells_high - 1) do
            open_cells := (countx, county) :: !open_cells
         done
      done;
      recursive_place_portals plane portal b_grid cells_wide cells_high
            open_cells;
      place_critters plane iid b_grid cells_wide cells_high open_cells
         (4 + (Algo.mersenne 3));
      place_items plane iid b_grid cells_wide cells_high open_cells
         prototype (8 + (Algo.mersenne 3))
;;

let rec recursive_carve_maze1 plane portal iid lx1 ly1 lx2 ly2
   cells_wide cells_high cell_width cell_height b_grid openlist 
   carvecell prototype
=
   match openlist with
    | [] -> 
         let plane_portals = Mapstruc.stairs_on_plane plane portal in
         let present_portals = 
            Stair.inbound_portals plane_portals lx1 ly1 lx2 ly2
         in
            add_features plane present_portals iid b_grid
                  cells_wide cells_high prototype
    | _ ->
         let choices = List.length openlist in
         let choice =
            if choices < 2 then 0
            else Algo.mersenne choices
         in
            let this_cell = List.nth openlist choice in
               carvecell plane portal lx1 ly1 lx2 ly2
                     cells_wide cells_high 
                     cell_width cell_height b_grid this_cell;
               let new_openlist = 
                  (trim_list openlist this_cell)
                  @ cells_next_to b_grid.cell 
                        cells_wide cells_high this_cell true
               in
                  recursive_carve_maze1 plane portal iid lx1 ly1 lx2 ly2
                     cells_wide cells_high cell_width cell_height b_grid
                     new_openlist carvecell prototype
;;

let carve_intelligent_dungeon1 plane portal iid lx1 ly1 lx2 ly2
   finalborder_x finalborder_y cells_wide cells_high 
   cell_width cell_height b_grid carvecell prototype
=
      let pre_pick_x, pre_pick_y = 
            (random_cell (cells_wide - 1) (cells_high - 1))
      in 
      let pick_x, pick_y =
            (pre_pick_x + 1), (pre_pick_y + 1)
      in
         recursive_carve_maze1 plane portal iid lx1 ly1 lx2 ly2
            cells_wide cells_high cell_width cell_height 
            b_grid [(pick_x, pick_y)] carvecell prototype
;;

let size_up_intelligent_dungeon1 plane portal lx1 ly1 lx2 ly2 
   cell_width cell_height 
=
   let dx = (lx2 - lx1) in
   let dy = (ly2 - ly1) in
      let remainder_x = dx mod cell_width in
      let remainder_y = dy mod cell_height in
         let multiple_x = 
            if cell_width > 0 then
               dx / cell_width 
            else 1
         in
         let multiple_y = 
            if cell_height > 0 then
               dy / cell_height 
            else 1
         in
         if ((multiple_x < 1) || (multiple_y < 1))
            || ((multiple_x = 1) && (remainder_x < 1))
            || ((multiple_y = 1) && (remainder_y < 1))
         then
            raise (Failure "Dungeon too small!")
         else
            let rx, mx =
               if (remainder_x < 1) then
                  (remainder_x + cell_width), (multiple_x - 1)
               else
                  remainder_x, multiple_x
            in
            let ry, my =
               if (remainder_y < 1) then
                  (remainder_y + cell_height), (multiple_y - 1)
               else
                  remainder_y, multiple_y
            in
            rx, ry, mx, my
;;

let rec strip_existing_connection cell choices =
   match choices with [] -> []
    | head :: tail ->
         if List.mem head cell.connects_to then
            strip_existing_connection cell tail
         else
            head :: strip_existing_connection cell tail
;;

let connect_deadends plane lx1 ly1 lx2 ly2 
      finalborder_x finalborder_y cells_wide cells_high
      cell_width cell_height b_grid
=
   for countx = 0 to (cells_wide - 1) do
      for county = 0 to (cells_high - 1) do
         let this_cell = b_grid.cell.(countx).(county) in
            if (not this_cell.has_stair)
            && ((List.length this_cell.connects_to) < 2)
            && (this_cell.fill_status = Corridor)
            then
               let bloated_connect_to = 
                  cells_next_to b_grid.cell cells_wide cells_high 
                  (countx, county) false
               in
               let connect_to = 
                  strip_existing_connection 
                        b_grid.cell.(countx).(county) bloated_connect_to
               in
                  let offset_x, offset_y, end_x, end_y =
                     get_offset_and_ends lx1 ly1 cell_width cell_height
                           countx county
                  in
                  dungeon1_connect_cell plane lx1 ly1 lx2 ly2
                     cells_wide cells_high
                     cell_width cell_height b_grid countx county 
                     connect_to offset_x offset_y end_x end_y
      done
   done;
;;

let pit_of_banishment plane portal iid lx1 ly1 lx2 ly2 prototype =
   let cell_width = 12 in
   let cell_height = 5 in
   let finalborder_x, finalborder_y, cells_wide, cells_high =
      size_up_intelligent_dungeon1 plane portal lx1 ly1 lx2 ly2 
            cell_width cell_height
   in
      let b_grid = empty_drawing_board cells_wide cells_high in
         initialise_bcells b_grid.cell cells_wide cells_high;
      carve_intelligent_dungeon1 plane portal iid lx1 ly1 lx2 ly2
            finalborder_x finalborder_y cells_wide cells_high
            cell_width cell_height b_grid carve_dungeon1_cell
            prototype;
      connect_deadends plane lx1 ly1 lx2 ly2 
            finalborder_x finalborder_y cells_wide cells_high
            cell_width cell_height b_grid
;;

let perfect_maze plane portal iid lx1 ly1 lx2 ly2 prototype =
   let cell_width = 2 in
   let cell_height = 2 in
   let finalborder_x, finalborder_y, cells_wide, cells_high =
      size_up_intelligent_dungeon1 plane portal lx1 ly1 lx2 ly2 
            cell_width cell_height
   in
      let b_grid = empty_drawing_board cells_wide cells_high in
         initialise_bcells b_grid.cell cells_wide cells_high;
      carve_intelligent_dungeon1 plane portal iid lx1 ly1 lx2 ly2
            finalborder_x finalborder_y cells_wide cells_high
            cell_width cell_height b_grid carve_maze_cell prototype
;;

let shop_maze plane portal iid lx1 ly1 lx2 ly2 prototype =
(*    print_string "\nBuilding shop maze\n"; *)
   let cell_width = 11 in
   let cell_height = 6 in
   let finalborder_x, finalborder_y, cells_wide, cells_high =
      size_up_intelligent_dungeon1 plane portal lx1 ly1 
            (lx2 - 1) (ly2 - 1) cell_width cell_height
   in
      let b_grid = empty_drawing_board cells_wide cells_high in
         initialise_bcells b_grid.cell cells_wide cells_high;
      Dungeon.carve_rectangle plane (lx1 + 1) (ly1 + 1) 
            (lx2 - 1) (ly2 - 1) Terrain.Stone_floor;
      carve_intelligent_dungeon1 plane portal iid lx1 ly1 lx2 ly2
            finalborder_x finalborder_y cells_wide cells_high
            cell_width cell_height b_grid carve_shop_cell prototype
;;

let basic_stair_connections plane portal lx1 ly1 lx2 ly2 =
   let bx1 = lx1 + 1 in
   let by1 = ly1 + 1 in
   let bx2 = lx2 - 1 in
   let by2 = ly2 - 1 in
      let plane_portals = Mapstruc.stairs_on_plane plane portal in
         let present_portals = 
            Stair.inbound_portals plane_portals bx1 by1 bx2 by2
         in
            if present_portals = [] then 
               print_string "No stairs on this level";
            Dungeon.connect_portals 
               plane present_portals bx1 by1 bx2 by2;
;;
