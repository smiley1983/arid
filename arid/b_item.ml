(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    b_item.ml

*  What does it do?

**    Provides items to the Banishment module

*)

let prototype_corpse () =
 {
   Item.proto_item_base with
      Item.symbol = Symbol.Food;
      Item.colour = U_colour.Red;
      Item.iname = "corpse";
      Item.surface_name = "corpse";
      Item.desc = "A dead body.";
      Item.appearance = "A dead body.";
 }
;;

let prototype_pile_of_bones () =
 {
   Item.proto_item_base with
      Item.symbol = Symbol.Food;
      Item.colour = U_colour.White;
      Item.iname = "pile of bones";
      Item.surface_name = "pile of bones";
      Item.desc = "Whatever owned these is long dead.";
      Item.appearance = "Whatever owned these is long dead.";
 }
;;

let prototype_apple () =
 let c = Algo.random_in_list [U_colour.Red; U_colour.Green] in
 {
   Item.proto_item_base with
      Item.symbol = Symbol.Food;
      Item.colour = c;
      Item.iname = "apple";
      Item.surface_name = "apple";
      Item.desc = "A bit shrivelled, but it looks edible.";
      Item.appearance = "A bit shrivelled, but it looks edible.";
 }
;;

let prototype_longsword () =
   {
      Item.proto_item_base with
         Item.symbol = Symbol.Sharp_weapon;
         Item.colour = U_colour.Cyan;
         Item.iname = "longsword";
         Item.surface_name = "longsword";
         Item.desc = "An old and battered long sword.";
         Item.appearance = "An old and battered long sword.";
   }
;;

let prototype_canine_skeleton () =
   {
      Item.proto_item_base with
         Item.symbol = Symbol.Skeleton;
         Item.colour = U_colour.White;
         Item.iname = "canine skeleton";
         Item.surface_name = "canine skeleton";
         Item.desc = "This dog died some time ago, but it's moving."
               ^ " And angry.";
         Item.appearance = "This dog died some time ago, but it's"
               ^ " moving. And angry.";
   }
;;

let prototype_giant_centipede_corpse () =
   {
      Item.proto_item_base with
         Item.symbol = Symbol.Food;
         Item.colour = U_colour.Brown;
         Item.iname = "giant centipede corpse";
         Item.surface_name = "giant centipede corpse";
         Item.desc = "Its insides don't look pretty.";
         Item.appearance = "Its insides don't look pretty."
   }
;;

let prototype_giant_centipede () =
   {
      Item.proto_item_base with
         Item.symbol = Symbol.Crawler;
         Item.colour = U_colour.Green;
         Item.iname = "giant centipede";
         Item.surface_name = "giant centipede";
         Item.desc = "Like a normal centipede,"
               ^ " except it's the size of a large python";
         Item.appearance = "Like a normal centipede,"
               ^ " except it's the size of a large python";
   }
;;

let prototype_pc () =
   {
      Item.proto_item_base with
         Item.symbol = Symbol.Person_unique;
         Item.colour = U_colour.White;
         Item.iname = "you";
         Item.surface_name = "you";
         Item.desc = "you, the traveller.";
         Item.appearance = "you, the traveller.";
   }
;;

let prototype_healing_potion surface_names =
(*
   let snl = List.length !surface_names - 1 in
      let c = Algo.mersenne c in
      let surfn = List.nth snl surface_names in
         surface_names := (Algo.list_without !surface_names surfn);
*)
   let surfn, colour = 
      Algo.pick_random_from_reflist surface_names 
   in
   {
      Item.proto_item_base with
         Item.symbol = Symbol.Potion;
         Item.colour = colour;
         Item.iname = "healing potion";
         Item.surface_name = surfn;
         Item.desc = "A magical restorative drink.";
         Item.appearance = surfn;
         stackable = true;
   }
;;

let new_prototype_list potion_names =
  [(I_base.Longsword, prototype_longsword ());
   (I_base.Corpse, prototype_corpse ());
   (I_base.Healing_potion, (prototype_healing_potion potion_names));
   (I_base.Giant_centipede, (prototype_giant_centipede ()));
   (I_base.Canine_skeleton, (prototype_canine_skeleton ()));
   (I_base.PC, (prototype_pc ()));
   (I_base.Pile_of_bones, (prototype_pile_of_bones ()));
   (I_base.Giant_centipede_corpse, 
         (prototype_giant_centipede_corpse ()));
   (I_base.Apple, (prototype_apple ()));
  ]
;;

let longsword lx ly iid prototype =
   {
      Item.proto_item with
         Item.locx = lx;
         Item.locy = ly;
         Item.itype = [(Item.Evasion, (ref 2)); 
               (Item.Damage, (ref 7))];
         Item.base = I_base.Longsword;
         Item.id = iid;
   }
(*
   let proto = List.assoc "longsword" prototype in
   {
      proto with
         Item.locx = lx;
         Item.locy = ly;
         Item.id = iid;
         Item.itype = [(Item.Evasion, (ref 2)); 
               (Item.Damage, (ref 7))];
   }
*)
;;

let healing_potion lx ly iid prototype =
   {
      Item.proto_item with
         Item.locx = lx;
         Item.locy = ly;
         Item.itype = [(Item.Potion, (ref 0)); 
               (Item.Healing, (ref 2))];
         Item.base = I_base.Healing_potion;
         Item.id = iid;
   }
(*
   let proto = List.assoc "healing potion" prototype in
   {
      proto with
         Item.locx = lx;
         Item.locy = ly;
         Item.id = iid;
         Item.itype = [(Item.Potion, (ref 0)); 
               (Item.Healing, (ref 2))];
   }
*)
;;

let special_corpse lx ly s c iid n d protolist =
   if List.mem_assoc n protolist then
     {
      Item.proto_item with
         Item.locx = lx;
         Item.locy = ly;
         Item.id = iid;
         base = n
     }
   else
     {
      Item.proto_item with
         Item.locx = lx;
         Item.locy = ly;
         Item.id = iid;
         base = I_base.Corpse
     }
;;

let apple lx ly iid prototype =
   {
      Item.proto_item with
         Item.locx = lx;
         Item.locy = ly;
         Item.itype = [(Item.Food, (ref 2))];
         Item.base = I_base.Apple;
         Item.id = iid;
   }
;;
