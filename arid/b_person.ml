(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    b_person.ml

*  What does it do?

**    Provides people to the Banishment module

*)

let new_giant_centipede lx ly iid =
   let np =
      Person.new_person lx ly iid Symbol.Crawler U_colour.Green
         I_base.Giant_centipede
   in
   let st = Person.get_stat np in
      Stat.grant_stat st Stat.Magic_b 0;
      Stat.grant_stat st Stat.Muscle_b 8;
      Stat.grant_stat st Stat.Intrinsic_armour 12;
      Stat.set_max_health st 10;
      Stat.set_health st 10;
      Person.gain_attribute np Item.Insect;
   np
;;

let new_canine_skeleton lx ly iid =
   let np =
      Person.new_person lx ly iid Symbol.Skeleton U_colour.White
         I_base.Canine_skeleton
   in
   let st = Person.get_stat np in
      Stat.grant_stat st Stat.Magic_b 0;
      Stat.grant_stat st Stat.Muscle_b 8;
      Stat.set_max_health st 6;
      Stat.set_health st 6;
      Person.set_walk_speed np 70;
      Person.gain_attribute np Item.Skeleton;
   np
;;

let corpse_of_person pson lx ly iid protolist =
   let id = Uaid.new_aid iid in
   let symbol, colour, nm, desc =
      if Item.has_attribute pson.Person.person_item Item.Skeleton then
         Symbol.Food, U_colour.White, I_base.Pile_of_bones,
         "A pile of bones."
      else
         let c, desc = 
            Person.colour_and_desc_of_corpse pson protolist 
         in
            Symbol.Food, c,
            I_base.Corpse,
(*
            ((Language.get_iname pson.Person.person_item protolist) 
             ^ " corpse"),
*)
             desc
   in
      B_item.special_corpse lx ly symbol colour id nm desc protolist
;;


