(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    bcommand.ml

*  What does it do?

**    Provides command functions specific to the Banishment module.

*)

let kill_person pson ms plane iid protolist =
   let lx = Person.get_locx pson in
   let ly = Person.get_locy pson in
   let mb = Mapplane.grid_get_mbcontent plane lx ly in
      let plist = Mapblock.get_person mb in
         Person.remove_person pson plist;
         let corpse = B_person.corpse_of_person pson lx ly iid protolist 
         in
            Mapblock.gain_item mb corpse;
         Mapstruc.dirty_tile ms (lx, ly)
;;

let attack_spot ms plane pc actor msgbank time iid lx ly protolist =
   let plx = Person.get_locx pc in
   let ply = Person.get_locy pc in
   let person =
      if (plx, ply) = (lx, ly) then
         pc :: (Mapstruc.people_on_loc plane lx ly)
      else Mapstruc.people_on_loc plane lx ly
   in
      if not (person = []) then
        (
         Combat.attack ms actor (List.nth person 0) msgbank (time + 1) 
               protolist;
         if Person.get_status (List.nth person 0) = Person.Dead then
            kill_person (List.nth person 0) ms plane iid protolist;
         true
        )
      else true
;;

