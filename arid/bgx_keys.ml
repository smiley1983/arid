(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    bgx_keys.ml

*  What does it do?

**    Provides some basic key conversion for the ui_basgx display 
module. This module is deprecated; use curses for now, and this one will 
be replaced with ui_sdl when I can be bothered wrapping SDL and its 
international keycode nightmade.

*)


class keyset =
   object
      method get_key (character:char) =
(*
         print_string ((String.make 1 character) ^ "\n");
*)
         match character with
          | '\001' -> "ctrl+a"
          | '\002' -> "ctrl+b"
          | '\003' -> "ctrl+c"
          | '\004' -> "ctrl+d"
          | '\005' -> "ctrl+e"
          | '\006' -> "ctrl+f"
          | '\007' -> "ctrl+g"
          | '\008' -> "backsp" (* Don't ask me... *)
          | '\009' -> "ctrl+i"
          | '\010' -> "ctrl+j"
          | '\011' -> "ctrl+k"
          | '\012' -> "ctrl+l"
          | '\013' -> "enter" (* Again, I don't know... *)
          | '\014' -> "ctrl+n"
          | '\015' -> "ctrl+o"
          | '\016' -> "ctrl+p"
          | '\017' -> "ctrl+q"
          | '\018' -> "ctrl+r"
          | '\019' -> "ctrl+s"
          | '\020' -> "ctrl+t"
          | '\021' -> "ctrl+u"
          | '\022' -> "ctrl+v"
          | '\023' -> "ctrl+w"
          | '\024' -> "ctrl+x"
          | '\025' -> "ctrl+y"
          | '\026' -> "ctrl+z"
          | '8' -> "kp8"
          | '2' -> "kp2"
          | '4' -> "kp4"
          | '6' -> "kp6"
          | '1' -> "kp1"
          | '3' -> "kp3"
          | '7' -> "kp7"
          | '9' -> "kp9"
          | '=' -> "equals"
          | _ -> String.make 1 character
   end
;;

let new_keyset = new keyset
