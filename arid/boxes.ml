(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    boxes.ml

*  What does it do?

**    Provides low level understanding of windowing concepts for low 
level UI.

*)


class curs_set option =
   object (self)
      val cursorset =
         [("map",Cursor.new_cursor
            option#get_map_x option#get_map_y
            option#get_map_wide option#get_map_high);
          ("stat",Cursor.new_cursor
            option#get_stat_x option#get_stat_y
            option#get_stat_wide option#get_stat_high);
         ("msg",Cursor.new_cursor
            option#get_msg_x option#get_msg_y
            option#get_msg_wide option#get_msg_high);
         ("temp",Cursor.new_cursor 0 0 20 10)
        ]
      method get_cursor boxstring =
         try
            List.assoc boxstring cursorset
         with Not_found ->
            List.assoc "msg" cursorset
      method changebox boxstring off_x off_y boundx boundy =
         let ccursor = self#get_cursor boxstring in
            ccursor#complete_reset off_x off_y boundx boundy
   end
;;

let get_cursor_set option = new curs_set option
