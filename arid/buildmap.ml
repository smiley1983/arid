(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    buildmap.ml

*  What does it do?

**    Provides coarse and fine scale map building functions. Some things 
are delegated to dungeon.ml

*)

let pre_existing_portal_links ms pre_plane_to is_down =
   let reslt = ref [] in
   let spec_reslt = ref [] in
      let portal = Mapstruc.get_portal ms in
      for count = 0 to ((List.length portal) - 1) do
         let portl = List.nth portal count in
            if Stair.is_two_way portl then
            if not ((Stair.leads_down portl) = is_down) then
            if Stair.get_to_plane portl = pre_plane_to then
               if not (Stair.get_is_special_link portl) then
                  reslt := portl :: !reslt
               else 
                  spec_reslt := portl :: !spec_reslt
      done;
   !reslt, !spec_reslt
;;

let list_next_pre_plane ms pre_plane is_down =
   try 
      let r = W_plane.next_p_ref pre_plane is_down in
      let plex = Mapstruc.plane_exist ms r in
         if (plex = -1) then
            (Mapstruc.unbuilt_plane_reference ms r;
            [r])
         else [List.nth (Mapstruc.get_planeref ms) plex]
   with Not_found -> []
;;

let gather_special_links linkstruc nplane =
   let ls_list = W_plane.get_links linkstruc in
   let reslt = ref [] in
   let pre_plane = Mapplane.get_p_ref nplane in
   for count = 0 to (List.length ls_list - 1) do
      let p_link = List.nth ls_list count in
         if not (W_plane.is_exit_done p_link) then
           (
            let build_now, is_down =
               if W_plane.build_now p_link pre_plane then
                  (true, true)
               else if W_plane.reverse_build_now p_link pre_plane then
                  (true, false)
               else (false, false)
            in
               if build_now then
                (
                  W_plane.exit_is_done p_link;
                  reslt := (count, is_down) :: !reslt
                )
           )
   done;
   !reslt
;;

(*
let message_links_built ms msgbank time =
   let msg = ref "" in
   let linkstruc = Mapstruc.get_link ms in
   let ls_list = W_plane.get_links linkstruc in
      for count = 0 to (List.length ls_list - 1) do
         let p_ref = List.nth ls_list count in
            let msgpart1 = W_plane.get_from_prefix p_ref in
            let msgpart2 = match W_plane.is_exit_done p_ref with
               true -> "True"
             | false -> "False"
            in
            msg := !msg ^ msgpart1 ^ " built: " ^ msgpart2 ^ "\n"
      done;
   Message.add_msg msgbank time Message.Debug !msg
;;
*)

let add_plot_event_to_stair stair w_link =
   let source_plotlink = W_plane.get_plotlink w_link in
      if not (source_plotlink = []) then
         Stair.add_plotlink_lots stair source_plotlink
;;

let add_special_link ms nplane =
   let from = Mapplane.get_p_ref nplane in
   let special_links = gather_special_links (Mapstruc.get_link ms)
                          nplane 
   in
   if not (List.length special_links = 0) then
      for count = 0 to (List.length special_links - 1) do
         let lnkcount, is_down = (List.nth special_links count) in
         let ls_list = W_plane.get_links (Mapstruc.get_link ms) in
         let speclnk = List.nth ls_list lnkcount in
         let toplane = W_plane.ref_of_exit speclnk is_down in
         let tolx, toly = 
            if (not is_down)
            && (not ((W_plane.get_from_lx speclnk = -1))) then
               (
                ref (W_plane.get_from_lx speclnk),
                ref (W_plane.get_from_ly speclnk)
               )
            else ref (-1), ref (-1)
         in
         let (lx, ly) = 
            if (not ((W_plane.get_from_lx speclnk) = (-1)))
            && is_down then
               ((W_plane.get_from_lx speclnk), 
               (W_plane.get_from_ly speclnk))
            else
               Mapplane.random_bounded_loc nplane 
         in
         let new_stair = Stair.new_spec_stair 
                      from toplane tolx toly lx ly is_down in
         (
            add_plot_event_to_stair new_stair speclnk;
(* DEBUG
            print_string "adding link to map structure\n";
            print_string ((string_of_int lx) ^ ", " ^
                  (string_of_int ly) ^ "\n");
/DEBUG *)
            Mapstruc.add_portal ms new_stair
         )
      done
;;

let count_normal_link ms pre_plane is_down =
   let rslt = ref [] in
   let nmbr = ref 0 in
   let rndm = Algo.mersenne 16 in
      if rndm < 5 then nmbr := 1
      else if rndm < 12 then nmbr := 2
      else nmbr := 3;
   for count = 0 to !nmbr do
      rslt := !rslt @ (list_next_pre_plane ms pre_plane is_down)
   done;
   !rslt
;;

let add_normal_link ms nplane pre_plane is_down =
   let lnks = count_normal_link ms pre_plane is_down in
   if not (lnks = []) then
   let p_from = Mapplane.get_p_ref nplane in
      for count = 0 to (List.length lnks - 1) do
         let lnk = List.nth lnks count in
         let (lx, ly) = Mapplane.random_bounded_loc nplane in
         let colour = U_colour.conv_colour_s "grey" in
         let new_stair = 
           if is_down then
            (Stair.new_down_stair p_from lnk (ref (-1)) (ref (-1)) 
                  lx ly colour)
           else
            (Stair.new_up_stair p_from lnk (ref (-1)) (ref (-1))
                  lx ly colour)
         in
             Mapstruc.add_portal ms new_stair
      done
;;

let link_stairs_up ms nplane pre_exist = 
   let from = Mapplane.get_p_ref nplane in
   for count = 0 to (List.length pre_exist - 1) do
      let prex = List.nth pre_exist count in
      let (lx, ly) = 
         let contendx = Stair.get_to_lx prex in
         if not (contendx = -1) then
            contendx, (Stair.get_to_ly prex)
         else
            Mapplane.random_bounded_loc nplane 
      in
      let new_stair = Stair.partner_of prex from lx ly in
         Mapstruc.add_portal ms new_stair
   done
;;

let link_stairs_eitherway ms nplane pre_exist = 
   let from = Mapplane.get_p_ref nplane in
   for count = 0 to (List.length pre_exist - 1) do
      let prex = List.nth pre_exist count in
      let (lx, ly) = 
         let contendx = Stair.get_to_lx prex in
         if not (contendx = -1) then
            contendx, (Stair.get_to_ly prex)
         else
            Mapplane.random_bounded_loc nplane 
      in
      let new_stair = Stair.partner_of prex from lx ly in
         Mapstruc.add_portal ms new_stair
   done
;;

let add_blank_stairs_down ms nplane =
   add_normal_link ms nplane (Mapplane.get_p_ref nplane) true
;;

let link_stairs_down ms nplane pre_exist = 
   link_stairs_up ms nplane pre_exist
;;

let add_blank_stairs_up ms nplane =
   add_normal_link ms nplane (Mapplane.get_p_ref nplane) false
;;

(* factor out this duplication - DONE ?*)

(*
let add_stairs_up ms nplane =
   let pre_exist, spec_exist = pre_existing_portal_links ms 
                     (Mapplane.get_p_ref nplane) false in
      if pre_exist = [] then add_blank_stairs_up ms nplane
      else link_stairs_eitherway ms nplane pre_exist;
      if not (spec_exist = []) then
         link_stairs_eitherway ms nplane spec_exist
;;
*)

let add_directional_stairs ms nplane blank_stairs is_down =
   let pre_exist, spec_exist = pre_existing_portal_links ms 
                     (Mapplane.get_p_ref nplane) is_down in
      if pre_exist = [] then blank_stairs ms nplane
      else link_stairs_eitherway ms nplane pre_exist;
      if not (spec_exist = []) then
         link_stairs_eitherway ms nplane spec_exist
;;

let add_stairs ms nplane =
   add_special_link ms nplane;
   add_directional_stairs ms nplane add_blank_stairs_up false;
   add_directional_stairs ms nplane add_blank_stairs_down true;
;;

(*
type cardinal_dir =
   North
 | South
 | East
 | West
 | Centre
;;
*)

(*
terrains:
0 = void
1 = plain floor
2 = grass
3 = rock wall
4 = big tree
5 = road
6 = river
7 = small tree
8 = dense shrub
9 = starting road
10 = stone bridge
*)

(*
let kit_mb_with_stones mb iid =
   let nstones = (Algo.mersenne 2) + (Algo.mersenne 2) in
   for count = 0 to nstones do
      let (lx, ly) = Mapblock.random_open_loc mb in
(*
         let iid = Mapblock.get_iid mb in (*handy reference point to
                                           the session item id *)
*)
            let id = Uaid.new_aid iid in
            let ilist = Mapblock.get_item mb in
               let itm = Item.stone lx ly id in
                  Item.add_item itm ilist
   done
;;

let kit_mb_with_humans mb iid =
   let nhum = (* (Algo.mersenne 2) + (Algo.mersenne 2) *) 0 in
   for count = 0 to nhum do
      let (lx, ly) = Mapblock.random_open_loc mb in
(*
         let iid = Mapblock.get_iid mb in (*handy reference point to
                                           the session item id *)
*)
            let id = Uaid.new_aid iid in
            let plist = Mapblock.get_person mb in
               let pson = Person.new_human lx ly id in
                  Person.add_person pson plist
   done;
;;
*)

let place_river mb x y =
   let rtile = Mapblock.get_mb_rtile mb x y in
   let athere = Mapblock.get_terrain rtile in
      let to_add = Terrain.mix_terrain athere Terrain.River in
      Mapblock.set_terrain rtile to_add
;;

let place_stream mb x y =
   let rtile = Mapblock.get_mb_rtile mb x y in
   let athere = Mapblock.get_terrain rtile in
      let to_add = Terrain.mix_terrain athere Terrain.Stream in
      Mapblock.set_terrain rtile to_add
;;

let place_road mb x y =
   let rtile = Mapblock.get_mb_rtile mb x y in
   let athere = Mapblock.get_terrain rtile in
      let to_add = Terrain.mix_terrain athere Terrain.Road in
      Mapblock.set_terrain rtile to_add
;;

let starting_road mb x y =
   let rtile = Mapblock.get_mb_rtile mb x y in
      Mapblock.set_terrain rtile Terrain.Starting_road
;;

let place_sea mb x y =
   let rt = Mapblock.get_mb_rtile mb x y in
      Mapblock.set_terrain rt Terrain.Sea
;;

let place_fading_sand mb x y =
   let chance = Algo.mersenne 100 in
      if chance <= 35 then
          let rt = Mapblock.get_mb_rtile mb x y in
             Mapblock.set_terrain rt Terrain.Sand
;;

let place_fading_grass mb x y =
   let chance = Algo.mersenne 100 in
      if chance <= 35 then
         (let rndm = Algo.mersenne 64 in
          let rt = Mapblock.get_mb_rtile mb x y in
             if rndm < 62 then
                 Mapblock.set_terrain rt Terrain.Grass
             else if rndm < 63 then
                 Mapblock.set_terrain rt Terrain.Dense_shrub
             else
                 Mapblock.set_terrain rt Terrain.Big_tree
         )
;;

let place_fading_forest mb x y =
   let chance = Algo.mersenne 100 in
      if chance >= 65 then
         (let rndm = Algo.mersenne 32 in
          let rt = Mapblock.get_mb_rtile mb x y in
              if rndm < 5 then
                  Mapblock.set_terrain rt Terrain.Grass
              else if rndm < 25 then
                  Mapblock.set_terrain rt Terrain.Floor
              else if rndm < 26 then
                  Mapblock.set_terrain rt Terrain.Small_tree
              else if rndm < 29 then
                  Mapblock.set_terrain rt Terrain.Dense_shrub
              else
                  Mapblock.set_terrain rt Terrain.Big_tree
         )
;;

let place_terrain mb x y fv =
   match fv with
      Feature.Road ->
         place_road mb x y
    | Feature.River -> place_river mb x y
    | Feature.Stream -> place_stream mb x y
    | Feature.Forest_triangle_fade -> 
           place_fading_forest mb x y
    | Feature.Grass_triangle_fade -> place_fading_grass mb x y
    | Feature.Sea_triangle_fade -> place_sea mb x y
    | Feature.Sand_triangle_fade -> place_fading_sand mb x y
    | _ -> ()
;;

let rec step_bresenham_feature mb point feature_variety =
   match point with 
      [] -> ()
    | (x, y) :: tail ->
       (
        (
         match feature_variety with
            Feature.Road ->
               place_road mb x y
          | _ -> place_river mb x y
        ) ; step_bresenham_feature mb tail feature_variety
       )
;;

let build_bresenham_feature mb current feature_variety =
   let start_x = Mapplane.get_feature_x1 current in
   let start_y = Mapplane.get_feature_y1 current in
   let end_x = Mapplane.get_feature_x2 current in
   let end_y = Mapplane.get_feature_y2 current in
      let point = Algo.bresenham_line start_x start_y end_x end_y in
         step_bresenham_feature mb point feature_variety
;;

let walk_step dx dy cx cy =
   if dx = 0 && dy < 0 then (cx, (cy - 1))        (* North *)
   else if dx = 0 && dy > 0 then (cx, (cy + 1))   (* South *)
   else if dx < 0 && dy = 0 then ((cx -1), cy)    (* West *)
   else if dx > 0 && dy = 0 then ((cx + 1), cy)   (* East *)
   else if dx < 0 && dy < 0 then
      match Algo.mersenne 2 with
         0 -> (* North *) (cx, (cy - 1))
       | _ -> (* West *) ((cx -1), cy)
   else if dx > 0 && dy < 0 then
      match Algo.mersenne 2 with
         0 -> (* North *) (cx, (cy - 1))
       | _ -> (* East *) ((cx + 1), cy)
   else if dx < 0 && dy > 0 then
      match Algo.mersenne 2 with
         0 -> (* South *) (cx, (cy + 1))
       | _ -> (* West *) ((cx -1), cy)
   else (* if dx > 0 && dy > 0 then *)
      match Algo.mersenne 2 with
         0 -> (* South *) (cx, (cy + 1))
       | _ -> (* East *) ((cx + 1), cy)
;;

let rec step_walking_feature mb cx cy ex ey feature_variety =
   let (dx, dy) = ((ex - cx), (ey - cy)) in
      let (nx, ny) = walk_step dx dy cx cy in
       (
         place_terrain mb cx cy feature_variety;
         if  (cx, cy) = (ex, ey) then () else
            step_walking_feature mb nx ny ex ey feature_variety
       )
(*and step_walking_feature mb bx by ex ey feature_variety =*)
;;

let build_walking_feature mb current feature_variety =
   let start_x = Mapplane.get_feature_x1 current in
   let start_y = Mapplane.get_feature_y1 current in
   let end_x = Mapplane.get_feature_x2 current in
   let end_y = Mapplane.get_feature_y2 current in
(* 3.09 warning
      let point = Algo.bresenham_line start_x start_y end_x end_y in
*)
         step_walking_feature mb start_x start_y end_x end_y
                              feature_variety
;;

let rec step_triangle_line mb cx cy dx dy ex ey fv =
   place_terrain mb cx cy fv;
   let finish =
      if dx = 0 then
         if (dy < 0) && (cy > ey) then false
         else if (dy > 0) && (cy < ey) then false
         else true
      else
         if (dx < 0) && (cx > ex) then false
         else if (dx > 0) && (cx < ex) then false
         else true
   in
      if finish then ()
      else step_triangle_line mb (cx + dx) (cy + dy) dx dy ex ey fv
;;

(*
   Step over each point on the edge of the triangle and call the
   line-painting function
*)

let rec step_ra_triangle mb line dx dy cx cy fv =
   match line with
      [] -> ()
    | (x, y) :: tail ->
        (
         let rf = (Algo.mersenne 3) - 1 in
            step_triangle_line mb (x + (rf * dx)) 
                               (y + (rf * dy)) dx dy cx cy fv;
            step_ra_triangle mb tail dx dy cx cy fv
        )
;;

(* 
   The following function initialises and then executes the painting of
   a right-angled triangle terrain fade
*)

let ra_triangle_fade mb line start_x start_y end_x end_y cx cy fv =
   let dx, dy =
      if start_x = cx then
        (
         if cx < end_x then (-1, 0)
         else (1, 0)
        )
      else
        (
         if cy < end_y then (0, -1)
         else (0, 1)
        )
   in
      step_ra_triangle mb line dx dy cx cy fv
;;

(*
   In an obtuse triangle with its base flat on one side of the screen,
   what is the common right angled corner point of the two right angled
   triangles within?
*)

let corner_point start_x start_y end_x end_y extra_x extra_y =
(*   print_string "Getting corner point...\n"; *)
   if start_x = end_x then (start_x, extra_y)
   else if start_y = end_y then (extra_x, start_y)
   else (print_string "Corner point error" ; raise Not_found)
;;

let build_triangle_fade mb current feature_variety =
(*   print_string "building triangle\n"; *)
   let start_x = Mapplane.get_feature_x1 current in
   let start_y = Mapplane.get_feature_y1 current in
   let end_x = Mapplane.get_feature_x2 current in
   let end_y = Mapplane.get_feature_y2 current in
   let extra_x = Mapplane.get_extra_x current in
   let extra_y = Mapplane.get_extra_y current in
      let cx, cy = corner_point start_x start_y end_x end_y extra_x
                                extra_y
      in
      if not ((cx, cy) = (start_x, start_y)) then
         let l1 = Algo.bresenham_line start_x start_y extra_x extra_y in
            ra_triangle_fade mb l1 start_x start_y extra_x extra_y cx cy
                             feature_variety
      ;
      if not ((cx, cy) = (end_x, end_y)) then
         let l2 = Algo.bresenham_line end_x end_y extra_x extra_y in
            ra_triangle_fade mb l2 end_x end_y extra_x extra_y cx cy
                             feature_variety
;;

let add_stone_circle mb feat =
(*   print_string "building stone circle\n"; *)
   let lx = Mapplane.get_from_x feat in
   let ly = Mapplane.get_from_y feat in
   let radius = Mapplane.get_feature_x2 feat in
   let step = Mapplane.get_feature_y2 feat in
   let count = ref 0 in
   for county = (ly - radius) to (ly + radius) do
      for countx = (lx - radius) to (lx + radius) do
         let ox, oy = (countx - lx), (county - ly) in
            if float_of_int ((ox * ox) + (oy * oy)) /. 
               float_of_int radius >= float_of_int radius 
            && float_of_int ((ox * ox) + (oy * oy)) /. 
               float_of_int (radius + 1) <= 
               ((float_of_int radius) +. 0.1) (* +. 1.0) *)
            then
               if (!count + 1) >= step then
                  (
                     count := 0;
(*                     print_string "adding block \n"; *)
                     let rt = Mapblock.get_mb_rtile mb countx county 
                     in
                        if not (rt == Mapblock.rtile) then
                           Mapblock.set_terrain rt Terrain.Rock_wall
(*
                        else
                           print_string ("rtile error \n" ^
                           (string_of_int countx) ^ ", "
                           ^ (string_of_int county))
*)
                  )
               else
                  count := !count + 1
      done
   done
;;

(* this next function should probably be a single rec, not a pair. *)

let rec build_features mb feature =
   match feature with
      [] -> ()
    | head :: tail ->
         next_feature mb head tail
and next_feature mb current next =
   let variety = Mapplane.get_feature_variety current in
      if (variety = Feature.Forest_triangle_fade)
      || (variety = Feature.Grass_triangle_fade)
      || (variety = Feature.Sea_triangle_fade)
      || (variety = Feature.Sand_triangle_fade)
      then 
         ( (* print_string "Building triangle fade..."; *)
         build_triangle_fade mb current variety )
      else if variety = Feature.Stone_circle then
         add_stone_circle mb current
      else
         ( (* print_string "Building walking feature..."; *)
         build_walking_feature mb current variety)
   ;
   build_features mb next
;;

let solid_block mb wt iid terraintype =
   let sizex = (Mapblock.get_blocksize_x mb - 1) in
   let sizey = (Mapblock.get_blocksize_y mb - 1) in
      for countx = 0 to sizex do
         for county = 0 to sizey do
            let rt = Mapblock.get_mb_rtile mb countx county in
               Mapblock.set_terrain rt terraintype
         done
      done;
      let feature = Mapplane.get_features wt in
         build_features mb feature
;;

let grass_block mb wt iid =
   let sizex = (Mapblock.get_blocksize_x mb - 1) in
   let sizey = (Mapblock.get_blocksize_y mb - 1) in
      for countx = 0 to sizex do
         for county = 0 to sizey do
            let rt = Mapblock.get_mb_rtile mb countx county in
             (  let rndm = Algo.mersenne 64 in
              if rndm < 62 then
                  Mapblock.set_terrain rt Terrain.Grass
              else if rndm < 63 then 
                  Mapblock.set_terrain rt Terrain.Dense_shrub
              else Mapblock.set_terrain rt Terrain.Big_tree
             )
         done
      done;
(*
      Dungeon.kit_mb_with_items mb iid;
*)
(*
      kit_mb_with_humans mb iid;
*)
      let feature = Mapplane.get_features wt in
         build_features mb feature
;;

let forest_block mb wt iid =
   let sizex = (Mapblock.get_blocksize_x mb - 1) in
   let sizey = (Mapblock.get_blocksize_y mb - 1) in
      for countx = 0 to sizex do
         for county = 0 to sizey do
            let rt = Mapblock.get_mb_rtile mb countx county in
             (  let rndm = Algo.mersenne 32 in
              if rndm < 5 then
                  Mapblock.set_terrain rt Terrain.Grass
              else if rndm < 25 then
                  Mapblock.set_terrain rt Terrain.Floor
              else if rndm < 26 then
                  Mapblock.set_terrain rt Terrain.Small_tree
              else if rndm < 29 then
                  Mapblock.set_terrain rt Terrain.Dense_shrub
              else
                  Mapblock.set_terrain rt Terrain.Big_tree
             )
         done
      done;
(*
      kit_mb_with_humans mb iid;
*)
      let feature = Mapplane.get_features wt in
         build_features mb feature
;;

let individual_block mb plane wt iid x y =
   match (Mapplane.get_wt_terrain wt) with
      Mapplane.Grass -> grass_block mb wt iid
    | Mapplane.Sea -> solid_block mb wt iid Terrain.Sea
    | Mapplane.Sand -> solid_block mb wt iid Terrain.Sand
    | Mapplane.Solid_hedge -> solid_block mb wt iid Terrain.Dense_shrub
    | _ -> forest_block mb wt iid
;;

let coarse_build plane scenario =
(* DEBUG
   print_string "Coarse build\n";
/DEBUG *)
   let variety = Mapplane.get_plane_variety plane in
      match variety with
         W_plane.Overland -> 
            Coarse_b.coarse_build_overland plane scenario
       | W_plane.Banishment_dungeon ->
            Coarse_b.banishment_dungeon plane scenario
                  Feature.Banishment_dungeon
       | W_plane.Perfect_maze ->
            Coarse_b.perfect_maze plane scenario
                  Feature.Perfect_maze
       | W_plane.Shop_maze ->
            Coarse_b.shop_maze plane scenario
                  Feature.Shop_maze
       | W_plane.Dungeon  -> 
            Coarse_b.coarse_build_dungeon plane scenario
                  Feature.Dungeon_large
       | W_plane.Stone_dungeon  -> 
            Coarse_b.coarse_build_dungeon plane scenario
                  Feature.Stone_dungeon
       | W_plane.Steel_dungeon  -> 
            Coarse_b.coarse_build_dungeon plane scenario
                  Feature.Steel_dungeon
       | W_plane.Dank_pit  -> 
            Coarse_b.coarse_build_dungeon plane scenario
                  Feature.Dank_pit
       | W_plane.Abomination_lair  -> 
            Coarse_b.coarse_build_dungeon plane 
                  scenario Feature.Abomination_lair_m
       | W_plane.Hopeless_cycle  -> 
            Coarse_b.coarse_build_dungeon plane 
                  scenario Feature.Hopeless_cycle_m
       | W_plane.Twisted_tunnel  -> 
            Coarse_b.coarse_build_dungeon plane 
                  scenario Feature.Twisted_tunnel_m
       | W_plane.Circular_room  -> 
            Coarse_b.coarse_build_circleroom plane 
                  scenario
       | W_plane.Rectangular_room  -> 
            Coarse_b.coarse_build_rectangleroom plane 
                  scenario 1 1 22 22
       | W_plane.Triangular_room  -> 
            Coarse_b.coarse_build_triangleroom plane 
                  scenario 1 1 22 22
       | W_plane.Hole_in_rock -> 
            Coarse_b.coarse_build_holeinrock plane scenario
(*       | _ -> 
            Coarse_b.generic_terrain_build plane *)
;;

(*
relative_coord returns the block-scale co-ordinates which correspond to
the fine plane-scale co-ords which are passed in.
*)

let relative_coord x y bsizex bsizey =
   (x mod bsizex), (y mod bsizey)
;;

(*
the following relative_block function determines which block a certain
set of fine plane-scale co-ords would be in. NOTE that it does not take
account of the fact that we will be operating in an environment with an
offset.
*)

let relative_block x y bsize =
   (x / bsize) , (y / bsize)
;;

let fill_special_block mb cplane wt iid x y =
   let ter = Mapplane.get_wt_terrain wt in
      match ter with
         Mapplane.Ruin_grassy ->
            grass_block mb wt iid
       | _ ->
            let sizex = (Mapblock.get_blocksize_x mb - 1) in
            let sizey = (Mapblock.get_blocksize_y mb - 1) in
               for countx = 0 to sizex do
                  for county = 0 to sizey do
                     let rt = Mapblock.get_mb_rtile mb countx county in
                        Mapblock.set_terrain rt Terrain.Grass
                  done
               done;
;;

(* this function should not be needed, or at least it shouldn't work 
like this *)

let rec get_special_feature wt =
   let lst = Mapplane.get_features wt in
      next_s_feature lst
and next_s_feature l =
   match l with
      [] -> raise Not_found
    | head :: tail ->
         let fv = Mapplane.get_feature_variety head in
            if fv = Feature.Nothing then
               next_s_feature tail
            else head
;;

let build_large_special plane x1 y1 x2 y2 =
   for county = y1 to (y2 - 1) do
      for countx = x1 to (x2 - 1) do
         let rt = Mapplane.grid_get_rtile plane countx county in
             Mapblock.set_terrain rt Terrain.Floor
      done
   done
;;

let terrain_fill plane x1 y1 x2 y2 ter =
  try
   for county = y1 to y2 do
      for countx = x1 to x2 do
         let rt = Mapplane.grid_get_rtile plane countx county in
             Mapblock.set_terrain rt ter
      done
   done
  with _ -> raise (Failure ("terrain_fill"))
;;

let place_pillars plane x1 y1 x2 y2 number =
   for count = 0 to (number - 1) do
      let lx = x1 + (Algo.mersenne (x2 - x1)) in
      let ly = y1 + (Algo.mersenne (y2 - y1)) in
         let rt = Mapplane.grid_get_rtile plane lx ly in
            Mapblock.set_terrain rt Terrain.Rock_wall
   done
;;

let build_ruin plane x1 y1 x2 y2 =
   place_pillars plane x1 y1 x2 y2 16;
;;

let build_banishment_dungeon plane portal iid x1 y1 x2 y2 protolist =
   terrain_fill plane x1 y1 x2 y2 Terrain.Rock_wall;
   B_dungen.pit_of_banishment plane portal iid x1 y1 x2 y2 protolist
;;

let fill_and_build plane portal iid x1 y1 x2 y2 tfill fbuild prototype =
   terrain_fill plane x1 y1 x2 y2 tfill;
   fbuild plane portal iid x1 y1 x2 y2 prototype
;;

let build_dungeon plane portal x1 y1 x2 y2 =
(*   print_string "\n building dungeon \n"; *)
   terrain_fill plane x1 y1 x2 y2 Terrain.Earth_wall;
(*   place_pillars plane x1 y1 x2 y2 16; *)
   Dungeon.carve_level plane portal (x1) (y1) (x2) (y2)
;;

let build_circleroom plane portal x1 y1 x2 y2 =
   terrain_fill plane x1 y1 x2 y2 Terrain.Rock_wall;
   Dungeon.carve_circular_room plane portal (x1) (y1) (x2) (y2)
;;

let fill_block cname fid iid plane x y wt =
   let bsizex = Mapplane.get_pblocksize_x plane in
   let bsizey = Mapplane.get_pblocksize_y plane in
   let new_block = Mapblock.new_block bsizex bsizey x y in
      Mapblock.init_mapblock new_block;
      fill_special_block new_block plane wt iid x y;
      Mapplane.set_wt_built wt true;
      Mapplane.set_wt_loaded wt true;
      Mapplane.set_wtile_content wt new_block;
      let fname = Mapplane.new_filename cname fid in
         Mapplane.set_filename plane x y fname;
         Mapplane.grid_set_wtile plane x y wt;
         Mapplane.save_grid plane x y
;;

let fill_blocks_empty cname fid iid plane x1 y1 x2 y2 =
         try
   for county = y1 to y2 do
      for countx = x1 to x2 do
            let wt = Mapplane.grid_get_wtile plane countx county in
               fill_block cname fid iid plane countx county wt
      done
   done
         with e ->
            raise (Failure ("fill_blocks_empty failed"))
;;

(*LOOKINTO*)

let build_special_block cplane portal iid x1 y1 x2 y2 feature_variety
   prototype
=
(* DEBUG
   print_string "\n building special block... \n";
/DEBUG *)
   let bsizex = Mapplane.get_pblocksize_x cplane in
   let bsizey = Mapplane.get_pblocksize_y cplane in
   let fine_x1 = x1 * bsizex in
   let fine_y1 = y1 * bsizey in
   let fine_x2 = ((x2 + 1) * bsizex) - 1 in
   let fine_y2 = ((y2 + 1) * bsizey) - 1 in
      match feature_variety with
         Feature.Ruin_large -> 
            build_ruin cplane fine_x1 fine_y1 fine_x2 fine_y2
       | Feature.Dungeon_large -> 
            build_dungeon cplane portal fine_x1 fine_y1 fine_x2 fine_y2
       | Feature.Banishment_dungeon ->
            build_banishment_dungeon cplane portal iid
                  fine_x1 fine_y1 fine_x2 fine_y2 prototype
       | Feature.Perfect_maze ->
            fill_and_build cplane portal iid
                  fine_x1 fine_y1 fine_x2 fine_y2 
                  Terrain.Rock_wall
                  B_dungen.perfect_maze
                  prototype
       | Feature.Shop_maze ->
            fill_and_build cplane portal iid
                  fine_x1 fine_y1 fine_x2 fine_y2 
                  Terrain.Rock_wall
                  B_dungen.shop_maze
                  prototype
       | _ ->
            build_circleroom cplane portal fine_x1 fine_y1 
                  fine_x2 fine_y2
;;

let special_feature cplane portal x y wt cname fid iid prototype =
   let specialf = get_special_feature wt in
   let (br_x1, br_y1, br_x2, br_y2, fv) =
     (
      Mapplane.get_from_x specialf,
      Mapplane.get_from_y specialf,
      Mapplane.get_to_x specialf,
      Mapplane.get_to_y specialf,
      Mapplane.get_feature_variety specialf
     )
   in
      fill_blocks_empty cname fid iid cplane 
         br_x1 br_y1 br_x2 br_y2
      ;
     try
        build_special_block cplane portal iid br_x1 br_y1 br_x2 br_y2 
              fv prototype
     with e -> 
        raise (Failure ("Failed in build_special_block due to "
        ^ Printexc.to_string e))
;;
