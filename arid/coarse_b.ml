(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    coarse_b.ml

*  What does it do?

**    Provides coarse map building functions for buildmap.ml

*)

let random_coarse_terrain v =
   match Algo.mersenne v with
    | 0 -> Mapplane.Grass
    | 1 -> Mapplane.Forest
    | _ -> Mapplane.Special
;;

let tiny_feature plane lx1 ly1 psize feature_variety =
   let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
      let bsizex = Mapplane.get_pblocksize_x plane in
      let bsizey = Mapplane.get_pblocksize_y plane in
         let fine_lx1 = (Algo.mersenne (bsizex - 2)) + 1 in
         let fine_ly1 = (Algo.mersenne (bsizey - 2)) + 1 in
         let fine_lx2 = (Algo.mersenne (bsizex - 2)) + 1 in
         let fine_ly2 = (Algo.mersenne (bsizey - 2)) + 1 in
         let feature =
            Mapplane.new_feature fine_lx1 fine_ly1 fine_lx2 fine_ly2 
                  Algo.Centre feature_variety
(*
            match feature_variety with
               Mapplane.Road ->
                  (Mapplane.new_road fine_lx1 fine_ly1 fine_lx2
                                        fine_ly2)
             | _ -> (Mapplane.new_river fine_lx1 fine_ly1 fine_lx2
                                        fine_ly2)
*)
         in
            Mapplane.add_feature wt feature
;;

let random_south_border bsizex bsizey =
   let lx = Algo.mersenne (bsizex - 1) in
      let ly = (bsizey - 1) in
         (Algo.South, lx, ly)
;;

let random_north_border bsizex bsizey =
   let lx = Algo.mersenne (bsizex - 1) in
      let ly = 0 in
         (Algo.North, lx, ly)
;;

let random_east_border bsizex bsizey =
   let ly = Algo.mersenne (bsizey - 1) in
      let lx = (bsizex - 1) in
         (Algo.East, lx, ly)
;;

let random_west_border bsizex bsizey =
   let ly = Algo.mersenne (bsizey - 1) in
      let lx = 0 in
         (Algo.West, lx, ly)
;;

let random_north_or_east_border bsizex bsizey =
   match Algo.mersenne 2 with
      0 -> random_north_border bsizex bsizey
    | _ -> random_east_border bsizex bsizey
;;

let random_north_or_west_border bsizex bsizey =
   match Algo.mersenne 2 with
      0 -> random_north_border bsizex bsizey
    | _ -> random_west_border bsizex bsizey
;;

let random_south_or_east_border bsizex bsizey =
   match Algo.mersenne 2 with
      0 -> random_south_border bsizex bsizey
    | _ -> random_east_border bsizex bsizey
;;

let random_south_or_west_border bsizex bsizey =
   match Algo.mersenne 2 with
      0 -> random_south_border bsizex bsizey
    | _ -> random_west_border bsizex bsizey
;;

let choose_rndpoint_border bsizex bsizey dx dy =
   if dx = 0 && dy < 0 then random_north_border bsizex bsizey
   else if dx = 0 && dy > 0 then random_south_border bsizex bsizey
   else if dx < 0 && dy = 0 then random_west_border bsizex bsizey
   else if dx > 0 && dy = 0 then random_east_border bsizex bsizey
   else if dx < 0 && dy < 0 then
      random_north_or_west_border bsizex bsizey
   else if dx > 0 && dy < 0 then
      random_north_or_east_border bsizex bsizey
   else if dx < 0 && dy > 0 then
      random_south_or_west_border bsizex bsizey
   else (* if dx > 0 && dy > 0 then *)
      random_south_or_east_border bsizex bsizey
;;

let appropriate_feature lx1 ly1 lx2 ly2 feature_variety =
   Mapplane.new_feature lx1 ly1 lx2 ly2 Algo.Centre feature_variety
;;

let end_coarse_feature cardinal prev plane lx1 ly1 lx2 ly2 bsizex bsizey
      feature_variety
=
   let fine_lx1, fine_ly1 =
      match cardinal with
         Algo.North -> ((Mapplane.get_feature_x2 prev), (bsizey - 1))
       | Algo.South -> ((Mapplane.get_feature_x2 prev), 0)
       | Algo.East -> (0, (Mapplane.get_feature_y2 prev))
       | _ (* West *) -> ((bsizex - 1), (Mapplane.get_feature_y2 prev))
   in
      let fine_lx2 = (Algo.mersenne (bsizex - 2)) + 1 in
      let fine_ly2 = (Algo.mersenne (bsizey - 2)) + 1 in
         let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
            let feature = appropriate_feature fine_lx1 fine_ly1
                                              fine_lx2 fine_ly2
                                              feature_variety
            in
               Mapplane.add_feature wt feature
;;

let rec begin_coarse_feature plane lx1 ly1 lx2 ly2 feature_variety =
(*   print_string "Building coarse feature \n"; *)
   let dx = (lx2 - lx1) in
   let dy = (ly2 - ly1) in
   let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
      let bsizex = Mapplane.get_pblocksize_x plane in
      let bsizey = Mapplane.get_pblocksize_y plane in
         let fine_lx1 = (Algo.mersenne (bsizex - 2)) + 1 in
         let fine_ly1 = (Algo.mersenne (bsizey - 2)) + 1 in
         let (cardinal, fine_lx2, fine_ly2) =
            choose_rndpoint_border bsizex bsizey dx dy
         in
            let feature = appropriate_feature fine_lx1 fine_ly1
                             fine_lx2 fine_ly2 feature_variety
            in
               Mapplane.add_feature wt feature
               ; next_coarse_feature cardinal feature plane lx1 ly1
                       lx2 ly2 bsizex bsizey feature_variety
and next_coarse_feature cardinal prev plane plx1 ply1 lx2 ly2 
   bsizex bsizey feature_variety
=
   let lx1, ly1 , fine_lx1, fine_ly1 =
      match cardinal with
         Algo.North -> (plx1, (ply1 - 1),
                   (Mapplane.get_feature_x2 prev),
                   (bsizey - 1)
                  )
       | Algo.South -> (plx1, (ply1 + 1),
                   (Mapplane.get_feature_x2 prev),
                   0
                  )
       | Algo.East -> ((plx1 + 1), ply1,
                  0, (Mapplane.get_feature_y2 prev)
                 )
       | _ (* West *) -> ((plx1 - 1), ply1,
                  (bsizex - 1), (Mapplane.get_feature_y2 prev)
                 )
   in
      if (lx1 = lx2) && (ly1 = ly2) then
         end_coarse_feature cardinal prev plane lx1 ly1
               fine_lx1 fine_ly1 bsizex bsizey feature_variety
      else
        (
         let (dx, dy) = ((lx2 - lx1), (ly2 - ly1)) in
            let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
               let (new_cardinal, fine_lx2, fine_ly2) =
                  choose_rndpoint_border bsizex bsizey dx dy
               in
                  let feature =
                     appropriate_feature fine_lx1 fine_ly1
                                         fine_lx2 fine_ly2
                                         feature_variety
                  in
                     Mapplane.add_feature wt feature
                     ;
                     next_coarse_feature new_cardinal feature plane
                           lx1 ly1 lx2 ly2 bsizex bsizey
                           feature_variety
        )
;;

let coarse_feature plane lx1 ly1 lx2 ly2 psize feature_variety =
   if lx1 = lx2 && ly1 = ly2 then
      tiny_feature plane lx1 ly1 psize feature_variety
   else begin_coarse_feature plane lx1 ly1 lx2 ly2 feature_variety
;;

let end_located_coarse_feature cardinal prev plane lx1 ly1 lx2 ly2
      fine_x2 fine_y2 bsizex bsizey feature_variety
=
   let fine_lx1, fine_ly1 =
      match cardinal with
         Algo.North -> ((Mapplane.get_feature_x2 prev), (bsizey - 1))
       | Algo.South -> ((Mapplane.get_feature_x2 prev), 0)
       | Algo.East -> (0, (Mapplane.get_feature_y2 prev))
       | _ (* West *) -> ((bsizex - 1), (Mapplane.get_feature_y2 prev))
   in
   let (fine_lx2, fine_ly2) =
      (
         (fine_x2 mod bsizex),
         (fine_y2 mod bsizey)
      )
   in
         let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
            let feature = appropriate_feature fine_lx1 fine_ly1
                                              fine_lx2 fine_ly2
                                              feature_variety
            in
               Mapplane.add_feature wt feature
;;


let rec begin_located_coarse_feature plane lx1 ly1 lx2 ly2
   fine_x1 fine_y1 end_fine_x2 end_fine_y2 bsizex bsizey feature_variety
=
(*   print_string "Building located coarse feature \n"; *)
   let (fine_lx1, fine_ly1) =
      (
         (fine_x1 mod bsizex),
         (fine_y1 mod bsizey)
      )
   in
   let dx = (lx2 - lx1) in
   let dy = (ly2 - ly1) in
   let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
      let bsizex = Mapplane.get_pblocksize_x plane in
      let bsizey = Mapplane.get_pblocksize_y plane in
         let (cardinal, fine_lx2, fine_ly2) =
            choose_rndpoint_border bsizex bsizey dx dy
         in
            let feature = appropriate_feature fine_lx1 fine_ly1
                             fine_lx2 fine_ly2 feature_variety
            in
               Mapplane.add_feature wt feature
               ; next_located_coarse_feature cardinal feature plane lx1
                     ly1 lx2 ly2 end_fine_x2 end_fine_y2 bsizex bsizey
                     feature_variety
and next_located_coarse_feature cardinal prev plane plx1 ply1 lx2 ly2
      end_fine_x2 end_fine_y2 bsizex bsizey feature_variety
=
   let lx1, ly1 , fine_lx1, fine_ly1 =
      match cardinal with
         Algo.North -> (plx1, (ply1 - 1),
                   (Mapplane.get_feature_x2 prev),
                   (bsizey - 1)
                  )
       | Algo.South -> (plx1, (ply1 + 1),
                   (Mapplane.get_feature_x2 prev),
                   0
                  )
       | Algo.East -> ((plx1 + 1), ply1,
                  0, (Mapplane.get_feature_y2 prev)
                 )
       | _ (* West *) -> ((plx1 - 1), ply1,
                  (bsizex - 1), (Mapplane.get_feature_y2 prev)
                 )
   in
      if (lx1 = lx2) && (ly1 = ly2) then
         end_located_coarse_feature cardinal prev plane lx1 ly1
                         fine_lx1 fine_ly1 end_fine_x2 end_fine_y2
                         bsizex bsizey feature_variety
      else
        (
         let (dx, dy) = ((lx2 - lx1), (ly2 - ly1)) in
            let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
               let (new_cardinal, fine_lx2, fine_ly2) =
                  choose_rndpoint_border bsizex bsizey dx dy
               in
                  let feature =
                     appropriate_feature fine_lx1 fine_ly1
                                         fine_lx2 fine_ly2
                                         feature_variety
                  in
                     Mapplane.add_feature wt feature
                     ;
                     next_located_coarse_feature new_cardinal feature
                           plane lx1 ly1 lx2 ly2 end_fine_x2 end_fine_y2 
                           bsizex bsizey feature_variety
        )
;;

let tiny_located_feature plane lx1 ly1 fine_x1 fine_y1 fine_x2 fine_y2
      psize bsizex bsizey feature_variety
=
   let wt = Mapplane.grid_get_wtile plane lx1 ly1 in
   let (fine_lx1, fine_ly1, fine_lx2, fine_ly2) =
      (
         (fine_x1 mod bsizex),
         (fine_y1 mod bsizey),
         (fine_x2 mod bsizex),
         (fine_y2 mod bsizey)
      )
   in
         let feature =
            Mapplane.new_feature
               fine_lx1 fine_ly1 fine_lx2 fine_ly2 
               Algo.Centre feature_variety
         in
            Mapplane.add_feature wt feature
;;

let located_coarse_feature plane fine_x1 fine_y1 fine_x2 fine_y2 psize
   bsizex bsizey feature_variety
=
   let (lx1, ly1, lx2, ly2) =
      (
         (fine_x1 / bsizex),
         (fine_y1 / bsizey),
         (fine_x2 / bsizex),
         (fine_y2 / bsizey)
      )
   in
   if lx1 = lx2 && ly1 = ly2 then
      tiny_located_feature plane lx1 ly1 fine_x1 fine_y1 fine_x2
         fine_y2 psize bsizex bsizey feature_variety
   else begin_located_coarse_feature plane lx1 ly1 lx2 ly2 fine_x1
      fine_y1 fine_x2 fine_y2 bsizex bsizey feature_variety
;;


let add_stone_circle_feature plane bsizex bsizey fx fy radius step =
   let cx, cy = (fx / bsizex), (fy / bsizey) in
   let lx, ly = (fx mod bsizex), (fy mod bsizey) in
   let wt = Mapplane.grid_get_wtile plane cx cy in
      Mapplane.add_feature wt
         (Mapplane.new_feature lx ly radius step 
          Algo.Centre Feature.Stone_circle)
;;

let add_random_feature plane feature_variety =
   let psize = Mapplane.get_planesize plane in
      let coarse_lx1 = Algo.mersenne psize in
      let coarse_ly1 = Algo.mersenne psize in
      let coarse_lx2 = Algo.mersenne psize in
      let coarse_ly2 = Algo.mersenne psize in
         coarse_feature plane coarse_lx1 coarse_ly1
                              coarse_lx2 coarse_ly2 psize
                              feature_variety
;;

let add_road plane =
   let psize = Mapplane.get_planesize plane in
      let coarse_lx1 = Algo.mersenne psize in
      let coarse_ly1 = Algo.mersenne psize in
      let coarse_lx2 = Algo.mersenne psize in
      let coarse_ly2 = Algo.mersenne psize in
         coarse_feature plane coarse_lx1 coarse_ly1
               coarse_lx2 coarse_ly2 psize Feature.Road
;;

let add_river plane =
   let psize = Mapplane.get_planesize plane in
      let coarse_lx1 = Algo.mersenne psize in
      let coarse_ly1 = Algo.mersenne psize in
      let coarse_lx2 = Algo.mersenne psize in
      let coarse_ly2 = Algo.mersenne psize in
         coarse_feature plane coarse_lx1 coarse_ly1
                              coarse_lx2 coarse_ly2 psize
                              Feature.River
;;

let add_stream plane =
   let psize = Mapplane.get_planesize plane in
      let coarse_lx1 = Algo.mersenne psize in
      let coarse_ly1 = Algo.mersenne psize in
      let coarse_lx2 = Algo.mersenne psize in
      let coarse_ly2 = Algo.mersenne psize in
         coarse_feature plane coarse_lx1 coarse_ly1
                              coarse_lx2 coarse_ly2 psize
                              Feature.Stream
;;


let cardinal_coarse_terrains plane x y =
   let wt_north = Mapplane.grid_get_wtile plane x (y - 1) in
   let wt_south = Mapplane.grid_get_wtile plane x (y + 1) in
   let wt_west = Mapplane.grid_get_wtile plane (x - 1) y in
   let wt_east = Mapplane.grid_get_wtile plane (x + 1) y in
      let n = Mapplane.get_wt_terrain wt_north in
      let s = Mapplane.get_wt_terrain wt_south in
      let w = Mapplane.get_wt_terrain wt_west in
      let e = Mapplane.get_wt_terrain wt_east in
         let fin_n =
            if wt_north = Mapplane.wtile then Mapplane.Void
            else n
         in
         let fin_s =
            if wt_south = Mapplane.wtile then Mapplane.Void
            else s
         in
         let fin_w =
            if wt_west = Mapplane.wtile then Mapplane.Void
            else w
         in
         let fin_e =
            if wt_east = Mapplane.wtile then Mapplane.Void
            else e
         in
            (fin_n, fin_s, fin_w, fin_e)
;;



let match_triangle_border_variety t =
   match t with
      Mapplane.Grass -> Feature.Grass_triangle_fade
    | Mapplane.Forest -> Feature.Forest_triangle_fade
    | Mapplane.Sea -> Feature.Sea_triangle_fade
    | Mapplane.Sand -> Feature.Sand_triangle_fade
    | _ -> Feature.Nothing
;;

let north_triangle bsize =
   let x1 = 0 in let y1 = 0 in let x2 = (bsize - 1) in let y2 = 0 in
   let x3 = (Algo.mersenne (x2 - 8)) + 4 in
   let y3 = (Algo.mersenne (x2 / 5)) + 2 in
      x1, y1, x2, y2, x3, y3
;;

let south_triangle bsize =
   let x1 = 0 in let y1 = (bsize - 1) in let x2 = (bsize - 1) in
   let y2 = (bsize - 1) in
   let x3 = (Algo.mersenne (x2 - 8)) + 4 in
   let y3 = (bsize - 1) - (Algo.mersenne (x2 / 5)) - 2 in
      x1, y1, x2, y2, x3, y3
;;

let west_triangle bsize =
   let x1 = 0 in let y1 = 0 in let x2 = 0 in
   let y2 = (bsize - 1) in
   let x3 = Algo.mersenne (y2 / 5) + 2 in
   let y3 = (Algo.mersenne (y2 - 8)) + 4 in
      x1, y1, x2, y2, x3, y3
;;

let east_triangle bsize =
   let x1 = (bsize - 1) in let y1 = 0 in let x2 = (bsize - 1) in
   let y2 = (bsize - 1) in
   let x3 = (bsize - 1) - Algo.mersenne (y2 / 5) - 2 in
   let y3 = (Algo.mersenne (y2 - 8)) + 4 in
      x1, y1, x2, y2, x3, y3
;;

let directional_fade bsize direction =
   match direction with
      Algo.East -> east_triangle bsize
    | Algo.West -> west_triangle bsize
    | Algo.North -> north_triangle bsize
    | _ -> south_triangle bsize
;;

let place_fade plane wtile breadth centre adj direction =
   if adj = centre then ()
   else
      let variety = match_triangle_border_variety adj in
      if not (variety = Feature.Nothing) then
      let from_x, from_y, to_x, to_y, extra_x, extra_y =
         directional_fade breadth direction
      in
         Mapplane.add_feature
            wtile
            (
             Mapplane.new_three_point_feature
                variety direction from_x from_y to_x to_y
                   extra_x extra_y
            )
;;


let step_fades plane wtile (north, south, west, east) =
   if not ((Mapplane.get_wt_terrain wtile) = Mapplane.Sea) then
      let bsizex = Mapplane.get_pblocksize_x plane in
      let bsizey = Mapplane.get_pblocksize_y plane in
      let centre = Mapplane.get_wt_terrain wtile in
         if not (south = Mapplane.Void) then
            place_fade plane wtile bsizex centre south Algo.South;
         if not (north = Mapplane.Void) then
            place_fade plane wtile bsizex centre north Algo.North;
         if not (east = Mapplane.Void) then
            place_fade plane wtile bsizey centre east Algo.East;
         if not (west = Mapplane.Void) then
            place_fade plane wtile bsizey centre west Algo.West;
;;

let add_fades plane =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let terrains = cardinal_coarse_terrains plane countx county
            in
               step_fades plane wtile terrains
      done
   done;
;;


let expanded_insula_pos x y bsizex bsizey =
   (x, y, ((x * bsizex) + (bsizex / 2)), ((y * bsizey) + (bsizey / 2)))
;;

let ten_grid_insula m bsizex bsizey =
   match Algo.mersenne m with
      0 -> expanded_insula_pos 4 1 bsizex bsizey
    | 1 -> expanded_insula_pos 4 8 bsizex bsizey
    | 2 -> expanded_insula_pos 1 4 bsizex bsizey
    | _ -> expanded_insula_pos 8 4 bsizex bsizey
;;


let test_coarse_build_overland plane scenario_variety =
   let psize = Mapplane.get_planesize plane in
   let bsizex = Mapplane.get_pblocksize_x plane in
   let bsizey = Mapplane.get_pblocksize_y plane in
   let (insula_cx, insula_cy, ins_fx, ins_fy) = 
      ten_grid_insula 4 bsizex bsizey
   in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let t_rain =
               if psize < 3 then
                  random_coarse_terrain 2
               else
                  if countx = 0 || county = 0
                  || countx = (psize - 1) || county = (psize - 1)
                  then
                     Mapplane.Sea
                  else if countx = 1 || county = 1
                  || countx = (psize - 2) || county = (psize - 2)
                  then
                    (
                     if not (countx = insula_cx && county = insula_cy)
                        then Mapplane.Sea
                     else
                        Mapplane.Sand
                    )
                  else if countx = 2 || county = 2
                  || countx = (psize - 3) || county = (psize - 3)
                  then
                     Mapplane.Sand
                  else if countx = 3 || county = 3
                  || countx = (psize - 4) || county = (psize - 4)
                  then
                     Mapplane.Grass
                  else
                  (
                     Mapplane.add_feature
                        wtile
                        (Mapplane.new_feature 4 4 
                              (psize - 5) (psize - 5) 
                              Algo.Centre Feature.Ruin_large
                        )
                     ;
                     Mapplane.Ruin_grassy
                  )
            in
              (
               Mapplane.set_wt_terrain wtile t_rain
              )
      done
   done;
   let down_x =
      (bsizex * psize) / 2
   in
   let down_y =
      (bsizey * psize) / 2
   in
   located_coarse_feature
         plane
         ins_fx
         ins_fy
         down_x down_y
         psize bsizex bsizey Feature.Road;
   add_stone_circle_feature plane bsizex bsizey down_x down_y 7 1;
   add_stone_circle_feature plane bsizex bsizey ins_fx ins_fy 5 1;
   add_random_feature plane Feature.Stream;
   add_fades plane;
   (ins_fx, ins_fy)
;;

let coarse_build_overland plane scene_variety =
   match scene_variety with
      Scenario.Ascending_1 -> test_coarse_build_overland plane
            scene_variety
    | _ -> test_coarse_build_overland plane scene_variety
;;

let generic_terrain_build plane =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let t_rain =
                  random_coarse_terrain 2
            in
              (
               Mapplane.set_wt_terrain wtile t_rain
              )
      done
   done;
   add_fades plane;
   (-1, -1)
;;

let banishment_dungeon plane scenario dvar =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
              (
               Mapplane.add_feature wtile
                     (Mapplane.new_feature 0 0 
                           (psize - 1) (psize - 1) Algo.Centre dvar
                     )
               ;
               Mapplane.set_wt_terrain wtile Mapplane.Banishment_dungeon
(* Mapplane.Dungeon *)
              )
      done
   done;
   (-1, -1)
;;

(* WINCE - time to REFACTOR *)

let shop_maze plane scenario dvar =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
              (
               Mapplane.add_feature wtile
                     (Mapplane.new_feature 0 0 
                           (psize - 1) (psize - 1) Algo.Centre dvar
                     )
               ;
               Mapplane.set_wt_terrain wtile Mapplane.Shop_maze
              )
      done
   done;
   (-1, -1)
;;

let perfect_maze plane scenario dvar =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
              (
               Mapplane.add_feature wtile
                     (Mapplane.new_feature 0 0 
                           (psize - 1) (psize - 1) Algo.Centre dvar
                     )
               ;
               Mapplane.set_wt_terrain wtile Mapplane.Perfect_maze
              )
      done
   done;
   (-1, -1)
;;

let coarse_build_dungeon plane scenario dvar =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let t_rain =
               Mapplane.Dungeon
            in
              (
               Mapplane.add_feature
                  wtile
                  (Mapplane.new_feature 0 0 
                        (psize - 1) (psize - 1) Algo.Centre dvar
                  )
               ;
               Mapplane.set_wt_terrain wtile t_rain
              )
      done
   done;
   add_fades plane;
   (-1, -1)
;;

let coarse_build_circleroom plane scenario =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let t_rain =
               Mapplane.Dungeon
            in
              (
               Mapplane.add_feature
                  wtile
                  (Mapplane.new_feature 0 0
                        (psize - 1) (psize - 1)
                        Algo.Centre Feature.Circle_room
                  )
               ;
               Mapplane.set_wt_terrain wtile t_rain
              )
      done
   done;
   (-1, -1)
;;


let coarse_build_rectangleroom plane scenario bx by ex ey =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let t_rain =
               Mapplane.Dungeon
            in
              (
               Mapplane.add_feature
                  wtile
                  (Mapplane.new_feature 0 0 (psize - 1) (psize - 1)
                        Algo.Centre Feature.Rectangle_room
                  )
               ;
               Mapplane.set_wt_terrain wtile t_rain
              )
      done
   done;
   (-1, -1)
;;


let coarse_build_triangleroom plane scenario bx by ex ey =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let t_rain =
               Mapplane.Dungeon
            in
              (
               Mapplane.add_feature
                  wtile
                  (Mapplane.new_feature 0 0 (psize - 1) (psize - 1)
                        Algo.Centre Feature.Triangle_room
                  )
               ;
               Mapplane.set_wt_terrain wtile t_rain
              )
      done
   done;
   (-1, -1)
;;


let coarse_build_holeinrock plane scenario =
   let psize = Mapplane.get_planesize plane in
   for county = 0 to (psize - 1) do
      for countx = 0 to (psize - 1) do
         let wtile = Mapplane.grid_get_wtile plane countx county in
            let t_rain =
               Mapplane.Dungeon
            in
              (
               Mapplane.add_feature
                  wtile
                  (Mapplane.new_feature 0 0 
                        (psize - 1) (psize - 1)
                        Algo.Centre Feature.Hole_in_rock
                  )
               ;
               Mapplane.set_wt_terrain wtile t_rain
              )
      done
   done;
   add_fades plane;
   (-1, -1)
;;


