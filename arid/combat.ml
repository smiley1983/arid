(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    combat.ml

*  What does it do?

**    Combat text and mechanics

*)


type damage =
 | Crush
 | Slash
;;

(*
let throwing_mod pson =

;;
*)

let can_see ms pson time =
   let plane = Mapstruc.get_cplane ms in
      Vismap.can_see_person plane pson time
;;

let kill ms attacker defender msgbank time protolist =
   let mtime = Message.get_last_seen msgbank in
   let see_att = can_see ms attacker mtime in
   let see_def = can_see ms defender mtime in
   let aname = Person.get_pname attacker protolist in
   let dname = Person.get_pname defender protolist in
   let lang = Message.get_language msgbank in
      let msg =
         Language.kill_message lang see_att see_def
            (Person.is_pc attacker)
            (Person.is_pc defender)
            aname dname
      in
      let mtarget = if see_att || see_def then
            Message.Player
         else 
            Message.Hidden
      in
         Message.add_msg msgbank time mtarget msg;
         Person.statuskill defender
;;

let do_hit ms attacker defender msgbank time damage protolist =
   let mtime = Message.get_last_seen msgbank in
   let see_att = can_see ms attacker mtime in
   let see_def = can_see ms defender mtime in
   let aname = Person.get_pname attacker protolist in
   let dname = Person.get_pname defender protolist in
   let lang = Message.get_language msgbank in
      let msg =
         Language.hit_message lang see_att see_def
            (Person.is_pc attacker)
            (Person.is_pc defender)
            aname dname
      in
      let mtarget = if see_att || see_def then
            Message.Player
         else 
            Message.Hidden
      in
         Message.add_msg msgbank time mtarget msg;
      Person.take_damage defender damage;
;;

let rec damage_to defender damagelist =
   match damagelist with
      [] -> 0
    | (amount, dvar) :: tail -> amount + (damage_to defender tail)
;;

let hit ms attacker defender msgbank time damagelist protolist =
   let damage = damage_to defender damagelist in
      if Person.get_health defender < damage then 
         kill ms attacker defender msgbank time protolist
      else do_hit ms attacker defender msgbank time damage protolist
;;

let miss ms attacker defender msgbank time protolist =
   let mtime = Message.get_last_seen msgbank in
   let see_att = can_see ms attacker mtime in
   let see_def = can_see ms defender mtime in
   let aname = Person.get_pname attacker protolist in
   let dname = Person.get_pname defender protolist in
   let lang = Message.get_language msgbank in
      let msg =
         Language.miss_message lang see_att see_def
            (Person.is_pc attacker)
            (Person.is_pc defender)
            aname dname
      in
      let mtarget = if see_att || see_def then
         Message.Player
      else 
         Message.Hidden
      in
         Message.add_msg msgbank time mtarget msg
;;

let roll_bd n =
   match Algo.mersenne n with
      0 -> true
    | _ -> false
;;

let roll_z1 n =
   match Algo.mersenne n with
      0 -> 0
    | _ -> 1
;;

let roll_dn n =
   (Algo.mersenne n) + 1
;;

let defense_success pson =
   let mx = Person.evasion_mod pson in
     roll_dn mx
;;

let attack_success pson =
   let mx = Person.damage_mod pson in
      roll_dn mx
;;

let attack ms attacker defender msgbank time protolist =
   let att_succ = attack_success attacker in
   let def_succ = defense_success defender in
      if def_succ > att_succ then
(*         practice attacker attack_skill 1 *)
         miss ms attacker defender msgbank time protolist
      else
         let dmg_amt = (Algo.mersenne (att_succ - def_succ)) + 1 in
         hit ms attacker defender msgbank time 
            [(dmg_amt, Crush)] protolist
;;

let throw_at ms attacker defender item msgbank time protolist =
   let lang = Message.get_language msgbank in
   let mtime = Message.get_last_seen msgbank in
   let see_def = can_see ms defender mtime in
   let result =
      if (Item.has_attribute item Item.Homing_missile) then
         true
      else
         let hitchance = Algo.mersenne 50 in
            if hitchance < Person.get_statvalue attacker Stat.Muscle_b
            then
               true
         else false
   in
      match result with
         true ->
(* unused
            let aname = Person.get_pname attacker protolist in
*)
            let dname = Person.get_pname defender protolist in
            let iname = Language.get_iname item protolist in
            let attmsg =
               Language.hit_message lang see_def see_def
                  false
                  (Person.is_pc defender)
                  iname dname
            in
               Message.add_msg msgbank time Message.Player
                     attmsg
            ; Person.take_damage defender
                   (Item.get_attribute_strength item Item.Damage)
            ; true
       | false -> 
(* 3.09 warning
            let aname = Person.get_pname attacker in
*)
            let dname = Person.get_pname defender protolist in
            let iname = Language.get_iname item protolist in
            let attmsg =
               Language.miss_message lang see_def see_def
                  false
                  (Person.is_pc defender)
                  iname dname
            in
               Message.add_msg msgbank time Message.Player
                     attmsg
            ; false
;;
