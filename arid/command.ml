(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    command.ml

*  What does it do?

**    Action.ml delegates lots of grunt work to command.ml- hence 
command.ml provides implementations for all the commands the player 
issues during the game.

*)

let compose_inv_keyline uiopt =
   "Previous : " ^ uiopt#get_reverse_key "key_previous" ^
   "  Next : " ^ uiopt#get_reverse_key "key_next" ^ "\n"
;;

let pan_look ui uiopt overmap offsetx offsety width height dx dy mcount 
   time 
=
   for count = 0 to mcount do
      let ox = offsetx + count * dx in
      let oy = offsety + count * dy in
      if (uiopt#get_key "wizard_vision") = "false" then
         ui#dump_timed_vistile_chunk_fs overmap ox oy width height time
      else
         ui#dump_vistile_chunk_fs overmap ox oy width height
      ;
         ui#sync
   done
;;

let direction_from_key uiopt keypress =
   let command = uiopt#get_key keypress in
      match command with
       | "key_north" ->
            Algo.North
       | "key_south" ->
            Algo.South
       | "key_west" ->
            Algo.West
       | "key_east" ->
            Algo.East
       | "key_nw" ->
            Algo.Northwest
       | "key_ne" ->
            Algo.Northeast
       | "key_sw" ->
            Algo.Southwest
       | "key_se" ->
            Algo.Southeast
       | _ -> Algo.Centre
;;

let rec real_look_mode 
 ui uiopt overmap offsetx offsety width height slide_amount time 
=
   let complete = ref 0 in
      if (uiopt#get_key "wizard_vision") = "false" then
(*
      if not uiopt#is_wizard then
*)
            ui#dump_timed_vistile_chunk_fs overmap !offsetx !offsety 
                  width height time
      else
            ui#dump_vistile_chunk_fs overmap !offsetx !offsety 
                  width height
      ;
      ui#sync;
      let keypress = ui#in_key in
      let command = uiopt#get_key keypress in
         match command with
          | "key_longwalk" ->
            let slide_amount = 3 in
               real_look_mode ui uiopt overmap
                  offsetx offsety width height slide_amount time 
          | _ ->
            let direction = direction_from_key uiopt keypress in
               if not (direction = Algo.Centre) then
                  let (dx, dy) = Algo.cardinal_step 0 0 direction in
                  let udx, udy = (dx * 8), (dy * 8) in
                    (pan_look ui uiopt overmap !offsetx !offsety
                           width height udx udy slide_amount time;
                     offsetx := (!offsetx + (slide_amount * udx));
                     offsety := (!offsety + (slide_amount * udy))
                    )
               else (*if keypress = "enter" then*) 
                  (complete := 1);
         (!offsetx,!offsety,!complete)
;;

let get_direction ui uiopt =
   let keypress = ui#in_key in
      direction_from_key uiopt keypress
;;

let try_look_mode ui uiopt omap p offx offy time =
   let width = (*min*) (uiopt#get_screen_chars_wide - 1)
(*                   (Mapstruc.get_total_cpsize omap)*)
   in
   let height = (*min*) (uiopt#get_screen_chars_high - 1)
(*                    (Mapstruc.get_total_cpsize omap)*)
   in
   let offsetx = ref offx in
   let offsety = ref offy in
   let slide_amount = 1 in
   let complete = ref 0 in
      while !complete = 0 do
         let (ox,oy,c) = 
            real_look_mode ui uiopt omap
               offsetx offsety width height slide_amount time
         in
            offsetx := ox ; offsety := oy; complete := c;
      done;
      ui#clear_win "temp";
      ui#sync;
      (!offsetx, !offsety)
;;

let look_mode ui uiopt omap p offx offy time =
   ui#fullscreen "temp";
   let offsetx = ref offx in
   let offsety = ref offy in
   try
      let (ox,oy) = try_look_mode ui uiopt omap p offx offy time in
      offsetx := ox ; offsety := oy;
      (!offsetx, !offsety, 0)
   with
      Not_found -> ui#say 
         ("Error in look_mode- this shouldn't happen.")
      ; (!offsetx, !offsety, 1)
;;

(* 
Like several other functions, fix_vismap is dependant on bigsize 
being odd 
*)

let fix_vismap plane overmap lx ly =
   let (px,py) = Mapplane.which_seg plane lx ly in
   let half_big = (Mapplane.get_bigsize plane) / 2 in
   let tpsx = Mapstruc.get_total_cpsize_x overmap in
   let tpsy = Mapstruc.get_total_cpsize_y overmap in
   let planesize = Mapplane.get_planesize plane in
   let lbx = (Mapstruc.abs_lower_x plane (max 0 (px - half_big)) ) in
   let lby = (Mapstruc.abs_lower_y plane (max 0 (py - half_big)) ) in
   let ubx = 
      ((Mapstruc.abs_upper_x plane (px + half_big) planesize tpsx)) 
   in
   let uby =
      ((Mapstruc.abs_upper_y plane (py + half_big) planesize tpsy))
   in
      Vismap.update_vistile_block overmap lbx lby ubx uby
;;

let prep_fix_vismap mhold actor =
            let omap = Mapstruc.get_map mhold in
            let pl = Mapstruc.get_cplane omap in
               if Mapplane.is_big pl then
                  fix_vismap pl omap
                  (Person.get_locx actor) (Person.get_locy actor)
               else fix_vismap pl omap
                  ((Mapplane.get_tp_size_x pl) / 2)
                  ((Mapplane.get_tp_size_y pl) / 2)
;;

let check_safe_boundaries ui mhold overmap actor lx ly 
    actq gametime msgbank
=
   if not (Mapstruc.is_safe overmap lx ly) then
     (Message.add_msg msgbank gametime Message.Player
           (Language.world_shimmer (Message.get_language msgbank));
     true)
   else false
;;

let display_vismap ui uiopt overmap lx ly gametime =
   let bx = lx - (uiopt#get_map_wide / 2) in
   let by = ly - (uiopt#get_map_high / 2) in
   let wdt = uiopt#get_map_wide - 1 in
   let hgt = uiopt#get_map_high - 1 in
      if (uiopt#get_key "wizard_vision") = "false" then
(*
      if not (uiopt#is_wizard) then
*)
         ui#dump_timed_vistile_chunk overmap bx by wdt hgt gametime
      else
         ui#dump_vistile_chunk overmap bx by wdt hgt
      ;
   ui#sync;
;;

let paint_vismap omap planename actor prevtime protolist =
   let pln = Mapstruc.get_cplane omap in
          Vismap.update_vismap omap planename 
                            (actor ::
                              Mapplane.complete_personlist pln
                            )
                            (Mapplane.complete_itemlist pln) prevtime
                            protolist
;;

let walk ui uiopt mhold planename actor dx dy gametime actq
         prevtime msgbank protolist
=
   let changeblock = ref false in
   let overmap = Mapstruc.get_map mhold in
   let lx = (Person.get_locx actor + dx) in
   let ly = (Person.get_locy actor + dy) in
   let rt = Mapstruc.get_rtile overmap lx ly in
      if not (Mapblock.is_physblocked (rt) ) 
      then
      (
       Action.dirty_tile overmap actq ((lx - dx),(ly - dy));
(*
       Action.dirty_tile overmap actq (lx, ly);
*)
       Person.set_locx actor lx; Person.set_locy actor ly;
       Vismap.paint_dirty overmap planename;
       Vismap.paint_sentient_list overmap [actor] prevtime protolist;
       if Person.is_pc actor then
       (changeblock :=
          (check_safe_boundaries ui mhold overmap 
                                  Message.Player lx ly
                                  actq gametime msgbank
        
          );
(*
        let omap = Mapstruc.get_map mhold in
          (
             paint_vismap omap planename actor prevtime protolist;
             Vismap.paint_dirty omap (Mapstruc.get_cplane omap)
          )
*)
(* *)
(*      ui#display_loc_and_wtile_terrain overmap lx ly *)
(* *)
       )
(* now for if not pc *)
       else
       (
          let plane = Mapstruc.get_cplane (Mapstruc.get_map mhold) in
             if Mapplane.does_cross_boundary plane 
                                             (lx - dx) 
                                             (ly - dy)
                                             lx ly
             then
                (
                    (
                       (* changeblock := true; *)
(* *)
                       Mapplane.pickup_person plane actor
                                              (lx - dx) (ly - dy);
(* *)
                       Mapplane.put_actor plane actor lx ly;
                    )
                )
       )
       ;
       (true, !changeblock)
      )
   else
    ( 
       if Person.is_pc actor then
          (
           let ( _ , _ , _ , _ , bname ) = 
              Mapstruc.get_scb_stat overmap lx ly
           in
           Message.add_msg msgbank prevtime Message.Player
                           ("You cannot walk through the " 
                            ^ bname ^ ".");
          );
       (false, !changeblock)
    )
        
;;

let follow_stair mhold actor down = 
 try
   if Person.is_pc actor then
     (let ms = Mapstruc.get_map mhold in
      let lx = Person.get_locx actor in
      let ly = Person.get_locy actor in
         match down with
            false ->
               if Mapstruc.upstair_on_loc ms lx ly then true 
               else false
          | true ->
               if Mapstruc.downstair_on_loc ms lx ly then true 
               else false
     )
   else false
 with Not_found -> false
;;

let choice_invmenu inv question protolist =
   let ilist = Inventry.get_list inv in
      let options = ref [Menulist.strn_menu_item "heading" 
                                                 question] 
      in
         for count = 0 to (List.length ilist - 1) do
            let (ind, content) = List.nth ilist count in
               options := !options @ 
                  [Menulist.indexed_strn_menu_item "option" 
                   (Language.get_prefixed_iname content protolist) ind]
         done;
         if (List.length ilist) = 0 then
            options := !options @
              [(Menulist.strn_menu_item "option" "nothing")];
      Menulist.make_indexed_menu !options
;;

let choice_statmenu st heading =
   let stats = Stat.get_topstat st in
      let options = ref [Menulist.strn_menu_item "heading" heading]
      in
         for count = 0 to (List.length stats - 1) do
            let (content, (value, marks)) = List.nth stats count in
               options := !options @ 
                  [Menulist.strn_menu_item "option" 
                   ((Stat.get_statname content) ^ "   " ^ 
                   (string_of_int !value)) ]
         done;
         if (List.length stats) = 0 then
            options := !options @
              [(Menulist.strn_menu_item "option" "none")];
      Menulist.make_indexed_menu !options
;;

let msg_target actor =
   match Person.is_pc actor with 
      true -> Message.Player
    | false -> Message.Hidden
;;

let fast_msg msgbank time actor msg =
   let target = msg_target actor in
      Message.add_msg msgbank time target msg
;;

let choice_equipmenu e question protolist =
   let slot = Equip.get_slot e in
      let options = ref [Menulist.strn_menu_item "heading" 
                                                 question] 
      in
         for count = 0 to (List.length slot - 1) do
            let (ind, (bodypart, bpname, content)) = 
               List.nth slot count 
            in
               let istring =
                  if content = Item.proto_item then "nothing"
                  else (Language.get_iname content protolist)
               in
               let optstring =
                  bpname ^ " - " ^ istring
               in
               options := !options @ 
                  [Menulist.indexed_strn_menu_item "option" 
                    optstring ind]
         done;
         if (List.length slot) = 0 then
            options := !options @
              [(Menulist.strn_menu_item "option" 
               "You don't seem to have a body.")];
      Menulist.make_indexed_menu !options
;;

let item_choice_menu itms question protolist =
   let options = ref [Menulist.strn_menu_item "heading" 
                                              question] 
   in
      for count = 0 to (List.length itms - 1) do
         let content = List.nth itms count in
            options := !options @ 
               [Menulist.strn_menu_item "option" 
                (Language.get_iname content protolist)]
      done;
      if (List.length itms) = 0 then
         options := !options @
           [(Menulist.strn_menu_item "option" "nothing")];
   Menulist.make_menu !options
;;

let get_autoindexed indices itms =
   let rslt = ref [] in
      for count = 0 to (List.length indices - 1) do
         let chari = int_of_char (List.nth indices count) in
            let pos =
               if chari < 91 then (chari - 65) + 25  (* caps *)
               else chari - 97 (* lowercase *)
            in
               rslt := !rslt @ [List.nth itms pos]
      done;
   !rslt
;;

let choose_multi_item_msg ui uiopt itms msg protolist =
   let choices = item_choice_menu itms msg protolist in
      let h = ui#box_mimic_ret_height "temp" 
         (uiopt#get_key "big_window")
      in
         let indices, keyp = ui#choose_many_indices uiopt choices h in
            get_autoindexed indices itms
;;

let choose_multi_item ui uiopt itms protolist =
   choose_multi_item_msg ui uiopt itms " > Pick up which items?" 
         protolist
;;

(* *)
let final_pack_item ui uiopt actor msgbank time itm container protolist 
=
   Person.lose_item_by_ref itm actor;
   Item.move_to_into itm container;
   let iname = Language.get_iname itm protolist in
   let cname = Language.get_iname container protolist in
   let msg = "You pack the " ^ iname ^ " into the " ^ cname ^ "." in
      fast_msg msgbank time actor msg;
      true
;;

let verify_pack_item ui uiopt actor msgbank time itm container 
   protolist 
=
   if not (Item.has_attribute container Item.Container) then
      let cname = Language.get_iname container protolist in 
      let msg = cname ^ " is not a container." in
         (fast_msg msgbank time actor msg; false)
   else
      if (Item.is_full container) then
         let cname = Language.get_iname container protolist in
         let msg = cname ^ " is full." in
            (fast_msg msgbank time actor msg; false)
      else
         if itm == container then
            (fast_msg msgbank time actor 
               "You cannot pack an item into itself!"; false)
         else
            final_pack_item ui uiopt actor msgbank time itm 
                  container protolist
;;

let pack_into ui uiopt actor msgbank time index1 protolist =
   let itm1 = Person.show_item index1 actor in
   let itm1_name = Language.get_iname itm1 protolist in
   let invm = 
         choice_invmenu 
            (Person.get_inventory actor)
            ((compose_inv_keyline uiopt) ^ 
             " > Which items do you wish to pack into the " 
             ^ itm1_name ^ "?"
            ) protolist
   in
      let h = ui#box_mimic_ret_height "temp" 
         (uiopt#get_key "big_window")
      in
       try
        (
         let index_mv, keyp = 
            (ui#choose_many_indices uiopt invm h)
         in
         let retv = ref false in
            for count = 0 to (List.length index_mv - 1) do
               let index_2 = (List.nth index_mv count) in
               let itm2 = Person.show_item index_2 actor in
                  retv :=
                     verify_pack_item ui uiopt actor msgbank time
                          itm2 itm1 protolist
            done; 
         !retv
        )
       with Not_found -> false
        | Failure v -> false
;;

let how_to_move ui uiopt actor msgbank time index protolist =
   pack_into ui uiopt actor msgbank time index protolist
;;

let unpack_to_inventory ui uiopt actor msgbank time fromitem indexl 
   protolist 
=
   let fromiteml = Item.get_inventory fromitem in
   let items_to_unpack = get_autoindexed indexl fromiteml in
   for count = 0 to (List.length items_to_unpack - 1) do
      let to_inv = Person.get_inventory actor in
      let this_item = List.nth items_to_unpack count in
         if Inventry.has_index to_inv then
           (
            Item.unpack_item this_item fromitem;
            Person.gain_item this_item actor protolist;
            fast_msg msgbank time actor ("You unpack the " ^
               (Language.get_iname this_item protolist) ^ ".")
           )
         else
            fast_msg msgbank time actor 
                  "No more room in your inventory."
   done
;;

let browse_subcontainer ui uiopt actor msgbank time index protolist =
   let itm = Person.show_item index actor in 
      if Item.has_attribute itm Item.Container then
         let iname = Language.get_iname itm protolist in
         let ilist = Item.get_inventory itm in
         let msg = "choose items to unpack from the " ^ iname ^ ":" ^
            "\n or press " ^ uiopt#get_reverse_key "key_switch" ^ 
            " to put items in"
         in
            let choices = (item_choice_menu ilist msg protolist) in
            let h = ui#box_mimic_ret_height "temp"
               (uiopt#get_key "big_window")
            in
            let indexlist, keyp =
                  ( ui#choose_many_indices uiopt choices h )
            in
               if keyp = uiopt#get_reverse_key "key_switch" then
                  ignore 
                     (how_to_move ui uiopt actor msgbank time index
                      protolist)
               else unpack_to_inventory ui uiopt actor msgbank time
                     itm indexlist protolist
      else
         ()
;;

let show_inventory ui uiopt actor msgbank time protolist =
   let invm = 
         choice_invmenu (Person.get_inventory actor)
         ((compose_inv_keyline uiopt) ^ " > Your inventory:" 
          ^ " (press letter to browse container)" ) protolist
   in
      let h = ui#box_mimic_ret_height "temp" 
            (uiopt#get_key "big_window") 
      in
       try
         let index = (ui#choose_menuitem uiopt invm h)#get_index in
            browse_subcontainer ui uiopt actor msgbank time index 
                  protolist
       with Not_found 
        | Failure _ -> ()
;;

let see_staircase_here ms lx ly =
   try
      let stair = (Mapstruc.stair_on_loc ms lx ly) in
         ("\na " ^ Stair.get_name stair ^ ".");
   with Not_found -> ""
;;

let list_staircase_here ms lx ly =
   try
      let stair = (Mapstruc.stair_on_loc ms lx ly) in
         [( Stair.get_name stair )];
   with Not_found -> []
;;

let list_items_here uiopt ms lx ly protolist =
   let rtile = Mapstruc.get_rtile ms lx ly in
      let nblank = Mapblock.contains_thing rtile in
         if not nblank then []
         else
           let msg = ref [] in
           (
            let cplane = Mapstruc.get_cplane ms in
            let mb = Mapplane.grid_get_mbcontent cplane lx ly in
            let ilist = Mapblock.get_item mb in
            let items = Item.items_on_loc ilist lx ly in
               if not (items = []) then
                   for count = 0 to (List.length items - 1) do
                      let itm = List.nth items count in
                         msg := (!msg @
                            [Language.get_prefixed_iname itm protolist])
                   done;
               !msg
              )
;;

let see_items_here uiopt ms lx ly protolist =
   let separator = match (uiopt#get_key "messageline_style") with
    | "single" -> ", "
    | _ -> "\n"
   in
   let rtile = Mapstruc.get_rtile ms lx ly in
      let nblank = Mapblock.contains_thing rtile in
         if not nblank then ""
         else
           let msg = ref "" in
           (
            let cplane = Mapstruc.get_cplane ms in
            let mb = Mapplane.grid_get_mbcontent cplane lx ly in
            let ilist = Mapblock.get_item mb in
            let items = Item.items_on_loc ilist lx ly in
               if not (items = []) then
                   for count = 0 to (List.length items - 1) do
                      let itm = List.nth items count in
                         msg := (!msg ^ separator
                            ^ Language.get_prefixed_iname itm protolist)
                   done;
               !msg
              )
;;

let see_people_here uiopt ms lx ly protolist =
   let msg = ref "" in
      (
       let cplane = Mapstruc.get_cplane ms in
       let mb = Mapplane.grid_get_mbcontent cplane lx ly in
       let plist = Mapblock.get_person mb in
       let psons = Person.people_on_loc plist lx ly in
          if not (psons = []) then
          if (List.length psons) < (uiopt#get_msg_high / 2)
          then
             for count = 0 to (List.length psons - 1) do
                let pson = List.nth psons count in
                   msg := (!msg ^ "\n" ^ 
                            (Person.get_mname pson
                             (uiopt#is_wizard) protolist)
                          )
             done
          else msg := (!msg ^ " lots of people");
          !msg
         )
;;

let see_terrain_here ms lx ly =
   let ter = Mapstruc.get_terrain ms lx ly in
      ("\n" ^ Terrain.get_name ter)
;;

let see_what_is_here uiopt lx ly actor mhold msgbank gtime protolist =
   let ms = Mapstruc.get_map mhold in
      let msg = 
         Language.you_see_here msgbank ", "
         (list_items_here uiopt ms lx ly protolist)
         (list_staircase_here ms lx ly) 
      in
         Message.add_msg msgbank gtime Message.Player msg
;;

let detailed_see_what_is_here ui uiopt lx ly actor mhold msgbank gtime 
   protolist 
=
   let ms = Mapstruc.get_map mhold in
      let msg = 
         if Mapblock.vis_time (Mapstruc.get_rtile ms lx ly )
            = gtime then
               "You see here :"
               ^ (see_people_here uiopt ms lx ly protolist) 
               ^ (see_items_here uiopt ms lx ly protolist) 
               ^ (see_staircase_here ms lx ly)
               ^ (see_terrain_here ms lx ly)
         else "You cannot see this place."
      in
(*
         Message.add_msg msgbank gtime (Person.get_id actor) msg
*)
         ui#clear_say msg
;;

let wizard_see_what_is_here ui uiopt lx ly actor mhold msgbank gtime 
   protolist 
=
   let ms = Mapstruc.get_map mhold in
      let msg = 
               "You see here :"
               ^ (see_people_here uiopt ms lx ly protolist) 
               ^ (see_items_here uiopt ms lx ly protolist) 
               ^ (see_staircase_here ms lx ly)
               ^ (see_terrain_here ms lx ly)
      in
(*
         Message.add_msg msgbank gtime (Person.get_id actor) msg
*)
         ui#clear_say msg
;;

let msg_detail_see_what_is_here ui uiopt lx ly actor mhold msgbank gtime
      imsg relevant_time protolist
=
   let ms = Mapstruc.get_map mhold in
      let (msg, seen) = 
         if relevant_time (Mapstruc.get_rtile ms lx ly )
            = gtime then
              (
               (imsg 
               ^ (see_people_here uiopt ms lx ly protolist) 
               ^ (see_staircase_here ms lx ly)
               ^ (see_items_here uiopt ms lx ly protolist) 
               ^ (see_terrain_here ms lx ly)),
               true
              )
         else ("You cannot see this place.", false)
      in
(*
         Message.add_msg msgbank gtime (Person.get_id actor) msg
*)
         ui#clear_say msg;
         seen
;;

let step_ontile ui uiopt actor mhold msgbank gtime protolist =
   let ms = Mapstruc.get_map mhold in
   let lx = Person.get_locx actor in
   let ly = Person.get_locy actor in
      let rtile = Mapstruc.get_rtile ms lx ly in
         let nblank = Mapblock.contains_thing rtile in
            if not nblank then ()
            else
              let lx = Person.get_locx actor in
              let ly = Person.get_locy actor in
               see_what_is_here uiopt lx ly actor mhold msgbank gtime 
                     protolist 
(*
               Message.add_msg msgbank gtime (Person.get_id actor)
                  "There is something here."
*)
;;

let single_pickup ms actor msgbank gtime mb itm protolist =
   if
      let inv = Person.get_inventory actor in
         Inventry.has_index inv
   then
      (
       Message.add_msg msgbank gtime Message.Player
          ("You pick up the " ^ Language.get_iname itm protolist ^ "." 
);
       Person.gain_item itm actor protolist;
       let ilist = Mapblock.get_item mb in
          Item.remove_item itm ilist;
       true
      )
    else 
      (Message.add_msg msgbank gtime Message.Player
          ("You cannot pick up the " ^ Language.get_iname itm protolist 
           ^ "." );
        false
      )
;;

let rec multi_pickup ui uiopt ms actor msgbank gtime mb itms_on_loc 
   protolist 
=
   let itms = choose_multi_item ui uiopt itms_on_loc protolist in
      let msg, failed = (
                 part_multi_pickup actor mb itms "You pick up: " true 
                       protolist
                )
      in
            Message.add_msg msgbank gtime Message.Player msg;
            (not failed)
and part_multi_pickup actor mb itms msg firstpass protolist =
   match itms with
      [] -> 
        (if (not firstpass) then (msg ^ "."), firstpass
         else (msg ^ "nothing."), firstpass
        )
    | itm :: tail ->
         (
            if
               let inv = Person.get_inventory actor in
                  Inventry.has_index inv
            then
              (
               Person.gain_item itm actor protolist;
               let ilist = Mapblock.get_item mb in
                  Item.remove_item itm ilist;
               part_multi_pickup actor mb tail 
                                 (msg ^ "\n" ^ 
                                 (Language.get_iname itm protolist)
                                 )
                                 false protolist
              )
            else
               ((msg ^ "\nNo more room in pack."), firstpass)
         )
;;

let realpickup ui uiopt ms actor lx ly msgbank gtime protolist =
   Mapstruc.dirty_tile ms (lx, ly);
   let cplane = Mapstruc.get_cplane ms in
   let mb = Mapplane.grid_get_mbcontent cplane lx ly in
      let ilist = Mapblock.get_item mb in
         try
            let itm = Item.items_on_loc ilist lx ly in
               (if
                   match List.length itm with
                      0 | 1 ->
                         single_pickup ms actor msgbank gtime mb
                               (List.nth itm 0) protolist
                    | _ ->
                         multi_pickup ui uiopt ms actor msgbank gtime
                               mb itm protolist
                then
                   true
                else false
               )
         with Not_found ->
               (Message.add_msg msgbank gtime Message.Player
                  "Can't find anything to pick up.";
               false)
            | (Failure "nth") -> 
               (Message.add_msg msgbank gtime Message.Player
                  "You can't pick that up.";
               false)
;;

let pick_up ui uiopt mhold actor msgbank gtime protolist =
   let ms = Mapstruc.get_map mhold in
   let lx = Person.get_locx actor in
   let ly = Person.get_locy actor in
      let rtile = Mapstruc.get_rtile ms lx ly in
         let nblank = Mapblock.contains_thing rtile in
            if not nblank then
               (Message.add_msg msgbank gtime Message.Player
                   "Nothing to pick up.";
                false
               )
            else
               (realpickup ui uiopt ms actor lx ly msgbank gtime 
                protolist)
;;

let choose_single_inv_item ui uiopt actor question protolist =
   let inv = Person.get_inventory actor in
   let invm = choice_invmenu inv question protolist
   in
      let h = ui#box_mimic_ret_height "temp" 
         (uiopt#get_key "big_window")
      in
         ui#choose_menuindex uiopt invm h
;;

let choose_spell ui uiopt actor question protolist =
   let inv = Person.get_spellist actor in
   let invm = choice_invmenu inv question protolist
   in
      let h = ui#box_mimic_ret_height "temp"
         (uiopt#get_key "big_window")
      in
         ui#choose_menuindex uiopt invm h
;;

let choose_equipslot ui uiopt actor question protolist =
   let equip = Person.get_equipment actor in
   let invm = choice_equipmenu equip question protolist in
      let h = ui#box_mimic_ret_height "temp" 
         (uiopt#get_key "big_window")
      in
         ui#choose_menuindex uiopt invm h
;;

let do_drop mb actor itm lx ly =
   Item.set_locx itm lx;
   Item.set_locy itm ly;
   Mapblock.gain_item mb itm
;;

let silent_loc_drop ui uiopt mhold actor index lx ly =
   let ms = Mapstruc.get_map mhold in
      let cplane = Mapstruc.get_cplane ms in
         let mb = Mapplane.grid_get_mbcontent cplane lx ly in
(* 3.09 warning
            let ilist = Mapblock.get_item mb in
*)
               let itm = Person.lose_item index actor in
                  (
                   do_drop mb actor itm lx ly;
                   Mapstruc.dirty_tile ms (lx, ly)
                  )
;;

let loc_drop ui uiopt mhold actor index msgbank gtime lx ly protolist =
   let ms = Mapstruc.get_map mhold in
      let cplane = Mapstruc.get_cplane ms in
         let mb = Mapplane.grid_get_mbcontent cplane lx ly in
(* 3.09 warning
            let ilist = Mapblock.get_item mb in
*)
               let itm = Person.lose_item index actor in
                  (
                   Message.add_msg msgbank gtime Message.Player
                      ("You drop the " ^ (Language.get_iname itm 
                       protolist));
                   do_drop mb actor itm lx ly
                  )
;;

let realdrop ui uiopt mhold actor index msgbank gtime protolist =
   let lx = Person.get_locx actor in
   let ly = Person.get_locy actor in
      loc_drop ui uiopt mhold actor index msgbank gtime lx ly protolist;
      Mapstruc.dirty_tile (Mapstruc.get_map mhold) (lx, ly)
;;

let drop_it ui uiopt mhold actor msgbank gtime protolist =
   try
      let index = (choose_single_inv_item ui uiopt actor
                   " > Drop which item?" protolist
                  )
      in
         realdrop ui uiopt mhold actor index msgbank gtime protolist ;
         true
   with Not_found | Failure _ -> false
;;

let rec split_multi_item ui itm iidc protolist =
   let rpt_num = (Item.get_repeat itm) in
(*   let s_num = string_of_int rpt_num in *)
      let answer = ui#ask ("How many of the " ^
                          (Language.get_iname itm protolist) ^ 
                          " do you wish to drop?\n")
      in
         try
            let drop_num =
               if answer = "" then rpt_num
               else int_of_string answer
            in
               Item.split_stack itm drop_num iidc
         with Failure "int_of_string" -> 
            split_multi_item ui itm iidc protolist
;;

let sgl_of_multi_drop ui uiopt mhold actor index msgbank gtime iid 
   protolist 
=
   let has_dropped = ref false in
   let ms = Mapstruc.get_map mhold in
   let lx = Person.get_locx actor in
   let ly = Person.get_locy actor in
      Mapstruc.dirty_tile ms (lx, ly);
      let cplane = Mapstruc.get_cplane ms in
         let mb = Mapplane.grid_get_mbcontent cplane lx ly in
(* 3.09 warning
            let ilist = Mapblock.get_item mb in
*)
               let itm = Person.lose_item index actor in
                  (
                   if (Item.get_repeat itm) > 1 then
                      let (keep, drop) = 
                         split_multi_item ui itm iid protolist 
                      in
                         (
                            if not (keep = Item.proto_item) then
                               Person.gain_item keep actor protolist
                            ;
                            if not (drop = Item.proto_item) then
                              (
                               do_drop mb actor drop lx ly;
                               has_dropped := true
                              )
                         )
                   else
                     (
                      do_drop mb actor itm lx ly
                      ; has_dropped := true
                     )
                  );
   !has_dropped
;;

let rec step_multi_drop ui uiopt mhold actor msgbank gtime indices iid 
   protolist
=
   sub_step_multi_drop ui uiopt mhold actor msgbank gtime indices iid
                       (ref false) protolist
and sub_step_multi_drop ui uiopt mhold actor msgbank gtime indices iid
                        has_dropped protolist
=
   match indices with
      [] -> !has_dropped
    | index :: tail ->
         (if
            sgl_of_multi_drop ui uiopt mhold actor index msgbank gtime
                  iid protolist
          then has_dropped := true;
          sub_step_multi_drop ui uiopt mhold actor msgbank gtime tail
                              iid has_dropped protolist
         )
;;

let multi_drop ui uiopt mhold actor msgbank gtime iid protolist =
   try
      let inv = Person.get_inventory actor in
      let invm = choice_invmenu inv " > Drop which items?" protolist
      in
      let h = ui#box_mimic_ret_height "temp" 
         (uiopt#get_key "big_window")
      in
         let indices, keyp = ui#choose_many_indices uiopt invm h in
            step_multi_drop ui uiopt mhold actor msgbank gtime indices
                            iid protolist
   with Not_found -> false
;;

let display_shortlook_vismap ui uiopt overmap lx ly gametime vis =
   let bx = lx - (uiopt#get_map_wide / 2) in
   let by = ly - (uiopt#get_map_high / 2) in
   let wdt = uiopt#get_map_wide - 1 in
   let hgt = uiopt#get_map_high - 1 in
      if vis then
         ui#dump_timed_shortlook_vistile_chunk 
            overmap bx by wdt hgt lx ly gametime
      else
         ui#dump_missile_shortlook_vistile_chunk 
            overmap bx by wdt hgt lx ly gametime
;;

let wizard_shortlook_vismap ui uiopt overmap lx ly gametime vis =
   let bx = lx - (uiopt#get_map_wide / 2) in
   let by = ly - (uiopt#get_map_high / 2) in
   let wdt = uiopt#get_map_wide - 1 in
   let hgt = uiopt#get_map_high - 1 in
      if vis then
         ui#dump_wizard_shortlook_vistile_chunk 
            overmap bx by wdt hgt lx ly
      else
         ui#dump_missile_shortlook_vistile_chunk 
            overmap bx by wdt hgt lx ly gametime
;;

let rec real_short_look ui uiopt lx ly actor mhold overmap msgbank gtime
   vis protolist
=
(* 3.09 warning
   let complete = ref 0 in
*)
      if not uiopt#is_wizard then
      (
         detailed_see_what_is_here ui uiopt lx ly actor mhold msgbank
                                gtime protolist;
         display_shortlook_vismap ui uiopt overmap lx ly gtime vis
      )
      else
      (
         wizard_see_what_is_here ui uiopt lx ly actor mhold msgbank
                                gtime protolist;
         wizard_shortlook_vismap ui uiopt overmap lx ly gtime vis
      );
      ui#sync;
      let direction = get_direction ui uiopt in
      let (nx, ny) = Algo.cardinal_step lx ly direction in
         if not (direction = Algo.Centre) then
            (real_short_look ui uiopt nx ny actor mhold
                             overmap msgbank gtime vis protolist
            )
         else (lx, ly)
;;

let short_look ui uiopt actor mhold msgbank gtime vis protolist =
   let lx = Person.get_locx actor in
   let ly = Person.get_locy actor in
   let overmap = Mapstruc.get_map mhold in
     ignore
     (
      real_short_look ui uiopt lx ly actor mhold overmap msgbank gtime 
            true protolist
     );
   ui#clear_win "msg"(*; ui#clear_win "msg"*)
;;

let rec msg_short_look ui uiopt lx ly actor mhold overmap msgbank gtime
      imsg relevant_time vis protolist
=
      let seen =
         msg_detail_see_what_is_here ui uiopt lx ly actor mhold msgbank
               gtime imsg relevant_time protolist
      in
      display_shortlook_vismap ui uiopt overmap lx ly gtime vis;
      ui#sync;
      let direction = get_direction ui uiopt in
      let (nx, ny) = Algo.cardinal_step lx ly direction in
         if not (direction = Algo.Centre) then
            (msg_short_look ui uiopt nx ny actor mhold
                            overmap msgbank gtime imsg 
                            relevant_time vis protolist
            )
         else (lx, ly, seen)
;;

let throw_look ui uiopt actor itm mhold msgbank gtime protolist =
   let lx = Person.get_locx actor in
   let ly = Person.get_locy actor in
   let overmap = Mapstruc.get_map mhold in
   let visrange = Person.get_vis_range actor in
     ignore (Shdwcast.throw_scan lx ly overmap visrange gtime);
     let (nlx, nly, seen) =
      msg_short_look ui uiopt lx ly actor mhold overmap msgbank gtime
         ("Throw the " ^ (Language.get_iname itm protolist) ^ " at:")
         Mapblock.get_missile_time false protolist
     in
   ui#clear_win "msg"; ui#clear_win "msg"; (nlx, nly, seen)
;;

let identify_item itm protolist =
   let proto = Item.get_prototype itm protolist in
      Item.set_identified proto true;
;;

let display_turns_in_queue ui uiopt actlist =
   ui#set_colour (U_colour.conv_colour_s "grey");
   let len = List.length actlist in
      ui#box_say 
            ("Turns in queue = " ^ (string_of_int len) ^ "     \n") 
            "stat"
;;

let kill_person pson ms plane iid protolist =
   let lx = Person.get_locx pson in
   let ly = Person.get_locy pson in
   let mb = Mapplane.grid_get_mbcontent plane lx ly in
      let plist = Mapblock.get_person mb in
         Person.remove_person pson plist;
(*
         let iid = Mapblock.get_iid mb in
*)
         let corpse = 
            Person.corpse_of_person pson lx ly iid protolist 
         in
            Mapblock.gain_item mb corpse;
         Mapstruc.dirty_tile ms (lx, ly)
;;

let make_attack_msg attacker defender protolist =
   let aname = 
      Person.get_pname attacker protolist
   in
   let pname = Person.get_pname defender protolist in
      aname ^ " attacks the " ^ pname ^ "."
;;

let attack_spot ms plane pc actor msgbank time iid lx ly protolist =
   let plx = Person.get_locx pc in
   let ply = Person.get_locy pc in
   let person = 
      if (plx, ply) = (lx, ly) then
         pc :: (Mapstruc.people_on_loc plane lx ly) 
      else Mapstruc.people_on_loc plane lx ly
   in
      if not (person = []) then
        (
         Combat.attack ms actor (List.nth person 0) 
               msgbank (time + 1) protolist;
         if Person.get_status (List.nth person 0) = Person.Dead then
            kill_person (List.nth person 0) ms plane iid protolist;
         true
        )
      else true
;;

let attack_direction ms plane pc actor msgbank time direction iid 
      protolist 
=
   let lx = Person.get_locx actor in
   let ly = Person.get_locy actor in
      let nlx, nly = Algo.cardinal_step lx ly direction in
         attack_spot ms plane pc actor msgbank time iid nlx nly 
               protolist
;;


let attack ui uiopt ms plane actor msgbank time iid protolist =
false
(*
   let direction =
      match Person.is_pc actor with
         true ->
           (
            ui#clear_say "Attack direction -";
            get_direction ui uiopt
           )
       | _ -> Monstrai.get_attack_dir plane actor
   in 
      attack_direction ms plane actor msgbank time direction iid 
            protolist
*)
;;

let help ui uiopt =
   ui#show_keybindings uiopt
;;

let detect_collision attacker item msgbank time mhold iid lx ly 
      protolist 
=
   let map = Mapstruc.get_map mhold in
   let plane = Mapstruc.get_cplane map in
   let plist = Mapstruc.people_on_loc plane lx ly in
      match plist with
         [] -> false
       | defender :: tail ->
           (
            let tf =
               Combat.throw_at map attacker defender item msgbank 
                     time protolist
            in
               if Person.get_status defender = Person.Dead then
                  kill_person defender map plane iid protolist;
            tf
           )
;;

let choose_target ui uiopt actor itm mhold msgbank gtime protolist =
   throw_look ui uiopt actor itm mhold msgbank gtime protolist
;;

let throw_anim ui uiopt mhold centre_x centre_y gametime lx ly =
   let ms = Mapstruc.get_map mhold in
      Mapstruc.dirty_tile ms (lx, ly);
      let rtile = Mapstruc.get_rtile ms lx ly in
         Mapblock.set_last_vis_symbol rtile Symbol.Magical_special;
         display_vismap ui uiopt ms centre_x centre_y gametime
;;

let throw_it ui uiopt mhold index actor itm msgbank gtime iid protolist 
=
   let (tx, ty, seen) =
      choose_target ui uiopt actor itm mhold msgbank gtime protolist
   in
   let animate = 
      match uiopt#get_key "show_animation" with
       | "normal" | "always" -> true
       | _ -> false
   in
   if seen then
     (
      Message.add_msg msgbank gtime Message.Player
         ("You throw the " ^ (Language.get_iname itm protolist));
      let px = Person.get_locx actor in
      let py = Person.get_locy actor in
(* *)
         let point = Algo.bresenham_line px py tx ty in
         let hit = ref false in
         let count = ref 0 in
         let fx = ref tx in let fy = ref ty in
            while (!count < (List.length point - 1)) && not !hit
            do 
               count := !count + 1;     (* start at 1 to miss player *)
               let (lx, ly) = List.nth point !count in
                  (fx := lx;
                   fy := ly;
                   if animate then 
                         throw_anim ui uiopt mhold px py gtime lx ly;
(* *) 
(* *)
            try
                   if 
                      detect_collision actor itm msgbank gtime mhold 
                         iid lx ly protolist
                   then
                     hit := true
            with Not_found -> raise 
               (Failure "Failed in throw_it")
(* *)
(* *)
                  )
            done;
(* *)
(*
            if hit then throw_hit_square ui uiopt actor index !fx !fy;
*)
            if not (Item.is_spell itm) then
               silent_loc_drop ui uiopt mhold actor index !fx !fy;
            true, !fx, !fy
     )
   else
     (
      Message.add_msg msgbank gtime Message.Player
         ("You cannot see that place.");
      false, 0, 0
     )
;;

let throw ui uiopt mhold actor msgbank time iid protolist =
   try
      let index = (choose_single_inv_item ui uiopt actor
                   " > Throw which item?" protolist
                  )
      in
         let itm = Person.show_item index actor in
            throw_it ui uiopt mhold index actor itm msgbank time iid 
                  protolist
   with Not_found | Failure _ -> false, 0, 0
;;

let cast_spell_index ui uiopt mhold actor msgbank time iid index 
      protolist
=
   let spell = Person.get_spell actor index in
      let spellname = Language.get_iname spell protolist in
         match spellname with
(*
            "magic dart" ->
              (
               Person.take_damage actor 1;
               let pid = Person.get_id actor in
               let sp_itm = Item.spell_item_magic_dart pid protolist in
                  throw_it ui uiopt mhold index actor sp_itm
                        msgbank time iid protolist
              )
          | "acid bolt" ->
              (
               Person.take_damage actor 10;
               let pid = Person.get_id actor in
               let sp_itm = Item.spell_item_acid_bolt pid protolist in
                  throw_it ui uiopt mhold index actor sp_itm
                        msgbank time iid protolist
              )
*)
          | _ -> raise Not_found
;;

let cast_spell ui uiopt mhold actor msgbank time iid protolist =
   try
      let question = " > Cast which spell?" in
      let index = choose_spell ui uiopt actor question protolist in
         cast_spell_index ui uiopt mhold actor msgbank time iid index 
               protolist
   with Not_found | Failure _ -> 
     (
      let target = msg_target actor in
         Message.add_msg msgbank time target
         "You don't have that spell.";
         false, 0, 0
     )
;;

let item_goes_on_slot itm partvariety =
   if partvariety = Body.Hand then true else
   let must_have =
      match partvariety with
         Body.Head -> Item.Hat
       | Body.Neck -> Item.Collar
       | Body.Torso -> Item.Shirt
       | Body.Arm -> Item.Vambrace
       | Body.Hand -> Item.Weapon
       | Body.Glove -> Item.Gloves
       | Body.Legs -> Item.Pants
       | Body.Feet -> Item.Shoes
       | Body.Backpack -> Item.Any
       | Body.Belt -> Item.Any
   in
      Item.has_attribute itm must_have
;;

let adjust_equip_of_index ui uiopt actor equip_index protolist =
   let partvariety, partname, attached_item = 
      Person.show_equip equip_index actor 
   in
   let item_index = (choose_single_inv_item ui uiopt actor
                       (" > Equip which item for your " ^  partname
                        ^ "?") protolist
                    )
   in
      if attached_item = Item.proto_item then
         let check_equip = Person.show_item item_index actor in
            if item_goes_on_slot check_equip partvariety then
               let new_equip = Person.lose_item item_index actor in
                 (
                  ignore (Person.switch_equip equip_index actor 
                          new_equip);
                  true
                 )
            else raise Not_found
      else
        (
         Person.gain_item
               (Person.switch_equip equip_index actor Item.proto_item)
               actor protolist;
         true
        )
;;

let unequip_item actor equip_index protolist =
   Person.gain_item
         (Person.switch_equip equip_index actor Item.proto_item)
         actor protolist
;;

let rec show_equipment ui uiopt actor protolist has_changed =
   try
      let newchange = ref false in
      let question = " > Your equipment: " in
      let index = choose_equipslot ui uiopt actor question protolist in
         let partvariety, partname, attached_item = 
            Person.show_equip index actor
         in
            if attached_item = Item.proto_item then
               newchange :=
                  adjust_equip_of_index ui uiopt actor index protolist
            else
              (newchange := true;
               unequip_item actor index protolist
              );
            show_equipment ui uiopt actor protolist
                  (has_changed || !newchange)
   with Not_found | Failure _ -> has_changed
;;

let show_topstat ui uiopt topstat =
   for count = 0 to (List.length topstat - 1 ) do
      let (variety, score) = List.nth topstat count in
      let vname = Stat.get_statname variety in
         ui#box_say ("\n" ^ vname ^ ": " ^ (string_of_int !score)) 
               "stat"
   done
;;

let update_statwin ui uiopt actor ms =
   ui#set_colour (U_colour.conv_colour_s "grey");
   let health = Person.get_health actor in
      ui#clear_win "stat";
      ui#reset_cursor "stat";
      ui#box_say ("Health: " ^ (string_of_int health)) "stat";
(*
   let topstat = Person.get_topstat actor in
      show_topstat ui uiopt topstat;
*)
   let attack = Person.damage_mod actor in
   let evasion = Person.evasion_mod actor in
   let defense = Person.protection_mod actor in
      ui#box_say ("\nAttack: " ^ (string_of_int attack)) 
                 "stat";
      ui#box_say ("\nEvasion: " ^ (string_of_int evasion)) "stat";
      ui#box_say ("\nProtection: " ^ (string_of_int defense)) "stat";
      let cplane = Mapstruc.get_cplane ms in
      let planeref = Mapplane.get_reference cplane in
      let level = string_of_int ((W_plane.get_number planeref) + 1) in
      let dungeon = W_plane.get_prefix planeref in
      ui#box_say ("\nLocation: " ^ dungeon ^ " level " ^ level) "stat"
   
(*   ui#sync *)
;;

let drink ui uiopt mhold actor msgbank time protolist =
   try
      let index = (choose_single_inv_item ui uiopt actor
                   " > Drink which item?" protolist
                  )
      in
         let itm = Person.show_item index actor in
            let time_passes, msg =
               if Item.has_attribute itm Item.Potion then
                  true, "You drink " ^
                  (
                   if Item.has_attribute itm Item.Healing then
                    (
                     Person.gain_health actor 6;
                     Person.lose_single_item index actor;
                     let msg = ((Language.get_singular_prefixed_iname 
                        itm protolist) ^ 
                        ". You feel better.")
                     in
                        identify_item itm protolist;
                           msg
                    )
                   else Language.get_singular_prefixed_iname itm 
protolist
                  )
               else false, "You can't drink that."
            in fast_msg msgbank time actor msg;
            time_passes
   with Not_found | Failure _ -> false
;;

let read ui uiopt mhold actor msgbank time protolist =
   try
      let index = (choose_single_inv_item ui uiopt actor
                   " > Read which item?" protolist
                  )
      in
         let itm = Person.show_item index actor in
            let msg =
               if Item.has_attribute itm Item.Scroll then
                  "You read the " ^
                  (
                   if Item.has_attribute itm Item.Magic_dart then
                    (
(*
                     Person.gain_spell Item.spell_magic_dart actor;
*)
                     Person.lose_single_item index actor;
                     Language.get_iname itm protolist
                    )
                   else Language.get_iname itm protolist
                  ) ^ " and it crumbles."
               else "You can't read that."
            in
               fast_msg msgbank time actor msg;
            true
   with Not_found | Failure _ -> false
;;

(*
  note that the show_scored_item_characteristics function assumes the 
"temp" window has already been set up
*)

let rec show_scored_item_characteristics ui uiopt il =
   match il with
      [] -> ()
    | (attr, value) :: tail ->
         let atst = Language.attribute_name_qty attr in
            if atst = "" then
               show_scored_item_characteristics ui uiopt tail
            else
            (
               ui#box_say ("\n" ^ atst ^ ": " ^ string_of_int !value) 
                     "temp";
               show_scored_item_characteristics ui uiopt tail
            )
;;

let describe_item ui uiopt itm protolist =
   ui#set_colour (U_colour.conv_colour_s "grey");
   ui#fullscreen "temp";
   ui#clear_win "temp";
   ui#reset_cursor "temp";
   ui#box_say ("examining : " 
         ^ (Language.get_prefixed_iname itm protolist) ^ "\n") "temp";
   ui#box_say ("\n" ^ Item.get_desc itm protolist) "temp";
   if Item.get_identified itm protolist then
      let il = Item.get_itype itm in
         show_scored_item_characteristics ui uiopt il
   else ();
   ui#in_key;
;;

let examine ui uiopt mhold actor msgbank time protolist =
   try
      let index = (choose_single_inv_item ui uiopt actor
                   " > Examine which item?" protolist
                  )
      in
         let itm = Person.show_item index actor in
            let msg =
                  "You examine the " ^
                      Language.get_iname itm protolist
                   ^ "."
            in
               fast_msg msgbank time actor msg;
               describe_item ui uiopt itm protolist;
            true
   with Not_found | Failure _ -> false
;;

let set_vision_distance cplane pc =
   let lightlevel = Mapplane.get_lightlevel cplane in
      Person.set_vis_range pc lightlevel
;;

let display_skills ui uiopt person =
   let stats = Person.get_stat person in
      let sm = choice_statmenu stats "Your statistics: " in
      let h = ui#box_mimic_ret_height "temp" 
         (uiopt#get_key "big_window")
      in
       try
         ignore (ui#choose_menuitem uiopt sm h)
       with Not_found | Failure _ -> ()
;;
