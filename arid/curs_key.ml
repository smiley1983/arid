(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    curs_key.ml

*  What does it do?

**    Provides basic key conversion for low level UI

*)


class keyset =
   object
      method get_key (character:int) =
(*
               print_string ( "key :  " ^
                     (string_of_int character) );
               print_string "\n"; (*DEBUG*)
*)
         match character with
            263 -> "backsp"
          | 1 -> "ctrl+a"
          | 2 -> "ctrl+b"
          | 3 -> "ctrl+c" (* not that you'll receive it, but... *)
          | 4 -> "ctrl+d"
          | 5 -> "ctrl+e"
          | 6 -> "ctrl+f"
          | 7 -> "ctrl+g"
          | 8 -> "ctrl+h"
          | 9 -> "ctrl+i"
          | 10 -> "enter" (* don't ask me, ask ncurses :) *)
          | 11 -> "ctrl+k"
          | 12 -> "ctrl+l"
          | 13 -> "ctrl+m"
          | 14 -> "ctrl+n"
          | 15 -> "ctrl+o"
          | 16 -> "ctrl+p"
          | 17 -> "ctrl+q"
          | 18 -> "ctrl+r"
          | 19 -> "ctrl+s"
          | 20 -> "ctrl+t"
          | 21 -> "ctrl+u"
          | 22 -> "ctrl+v"
          | 23 -> "ctrl+w"
          | 24 -> "ctrl+x"
          | 25 -> "ctrl+y"
          | 26 -> "ctrl+z" (* not receive... *)
          | 61 -> "equals"
          | 260 -> "kp4"
          | 261 -> "kp6"
          | 258 -> "kp2"
          | 259 -> "kp8"
          | 262 -> "kp7"
          | 339 -> "kp9"
          | 360 -> "kp1"
          | 338 -> "kp3"
          | 1001 -> "ctrl+alt+a"
          | 1002 -> "ctrl+alt+b"
          | 1003 -> "ctrl+alt+c"
          | 1004 -> "ctrl+alt+d"
          | 1005 -> "ctrl+alt+e"
          | 1006 -> "ctrl+alt+f"
          | 1007 -> "ctrl+alt+g"
          | 1008 -> "ctrl+alt+h"
          | 1009 -> "ctrl+alt+i"
          | 1010 -> "ctrl+alt+j"
          | 1011 -> "ctrl+alt+k"
          | 1012 -> "ctrl+alt+l"
          | 1013 -> "ctrl+alt+m"
          | 1014 -> "ctrl+alt+n"
          | 1015 -> "ctrl+alt+o"
          | 1016 -> "ctrl+alt+p"
          | 1017 -> "ctrl+alt+q"
          | 1018 -> "ctrl+alt+r"
          | 1019 -> "ctrl+alt+s"
          | 1020 -> "ctrl+alt+t"
          | 1021 -> "ctrl+alt+u"
          | 1022 -> "ctrl+alt+v"
          | 1023 -> "ctrl+alt+w"
          | 1024 -> "ctrl+alt+x"
          | 1025 -> "ctrl+alt+y"
          | 1026 -> "ctrl+alt+z"
          | 1065 -> "alt+A"
          | 1066 -> "alt+B"
          | 1067 -> "alt+C"
          | 1068 -> "alt+D"
          | 1069 -> "alt+E"
          | 1070 -> "alt+F"
          | 1071 -> "alt+G"
          | 1072 -> "alt+H"
          | 1073 -> "alt+I"
          | 1074 -> "alt+J"
          | 1075 -> "alt+K"
          | 1076 -> "alt+L"
          | 1077 -> "alt+M"
          | 1078 -> "alt+N"
          | 1079 -> "alt+O"
          | 1080 -> "alt+P"
          | 1081 -> "alt+Q"
          | 1082 -> "alt+R"
          | 1083 -> "alt+S"
          | 1084 -> "alt+T"
          | 1085 -> "alt+U"
          | 1086 -> "alt+V"
          | 1087 -> "alt+W"
          | 1088 -> "alt+X"
          | 1089 -> "alt+Y"
          | 1090 -> "alt+Z"
          | 1097 -> "alt+a"
          | 1098 -> "alt+b"
          | 1099 -> "alt+c"
          | 1100 -> "alt+d"
          | 1101 -> "alt+e"
          | 1102 -> "alt+f"
          | 1103 -> "alt+g"
          | 1104 -> "alt+h"
          | 1105 -> "alt+i"
          | 1106 -> "alt+j"
          | 1107 -> "alt+k"
          | 1108 -> "alt+l"
          | 1109 -> "alt+m"
          | 1110 -> "alt+n"
          | 1111 -> "alt+o"
          | 1112 -> "alt+p"
          | 1113 -> "alt+q"
          | 1114 -> "alt+r"
          | 1115 -> "alt+s"
          | 1116 -> "alt+t"
          | 1117 -> "alt+u"
          | 1118 -> "alt+v"
          | 1119 -> "alt+w"
          | 1120 -> "alt+x"
          | 1121 -> "alt+y"
          | 1122 -> "alt+z"
          | _ -> 
               if character < 256 then
                  String.make 1 (Char.chr character)
               else ""
               
   end
;;

let new_keyset = new keyset
