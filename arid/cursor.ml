(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    cursor.ml

*  What does it do?

**    Provides, along with boxes.ml, windowing capabilities of a sort.

*)


class cursor (off_x:int) (off_y:int) boundx boundy=
   object (self)
      val off_x = ref off_x
      val off_y = ref off_y
      val boundx = ref boundx
      val boundy = ref boundy
      val xpos = ref 0
      val ypos = ref 0
      method getx = !xpos
      method gety = !ypos
      method getboundx = !boundx
      method getboundy = !boundy
      method newline () =
         xpos := 0;
         ypos := !ypos +1;
         if !ypos >= !boundy then ypos := 0;
         ()
      method lineup () =
         xpos := 0;
         ypos := !ypos - 1;
         if !ypos <= 0 then ypos := 0
      method halfline =
         xpos := (!boundx / 2);
      method lastline () =
         xpos := 0;
         ypos := (!boundy - 1)
      method final_setx (value:int) =
         if value <= !boundx then
            xpos := value
         else self#newline ()
      method setx (value:int) =
         if value >= 0 then
            self#final_setx value
         else 
            (xpos := (!boundx-1);
            self#sety (self#gety -1))
      method final_sety (value:int) =
         if value < !boundy then
            ypos := value
         else if value >= !boundy then ypos := 0
      method sety value =
         if value >= 0 then
            self#final_sety value
         else
            ypos := (!boundy-1);
      method reset =
         self#sety 0; self#setx 0
      method incx = self#setx (!xpos+1)
      method incy = self#sety (!ypos+1)
      method decx = self#setx (!xpos-1)
      method decy = self#sety (!ypos-1)
      method get_x_offset = !off_x
      method get_y_offset = !off_y
      method get_abs_x =
         (self#get_x_offset + self#getx)
      method get_abs_y =
         (self#get_y_offset + self#gety)
      method set_offset_x value =
         off_x := value
      method set_offset_y value =
         off_y := value
      method set_bound_x value =
         boundx := value
      method set_bound_y value =
         boundy := value
      method complete_reset off_x off_y boundx boundy =
         self#set_offset_x off_x;
         self#set_offset_y off_y;
         self#set_bound_x boundx;
         self#set_bound_y boundy
   end
;;

let new_cursor offx offy bndx bndy = new cursor offx offy bndx bndy
