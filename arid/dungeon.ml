(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    dungeon.ml

*  What does it do?

**    Buildmap.ml delegates some building operations to dungeon.ml

*)

let random_tile plane bx1 by1 bx2 by2 =
   let dx, dy = (bx2 - bx1), (by2 - by1) in
      let off_x = (Algo.mersenne (max 1 dx)) in
      let off_y = (Algo.mersenne (max 1 dy)) in
         (bx1 + off_x), (by1 + off_y)
;;

let list_open_tile plane bx1 by1 bx2 by2 =
   let openlist = ref [] in
   for count_y = by1 to by2 do
      for count_x = bx1 to bx2 do
         let rt = Mapplane.grid_get_rtile plane count_x count_y in
         let ter = Terrain.sample_terrain (Mapblock.get_terrain rt) in
            if not (Terrain.get_physblock ter) then
               openlist := (count_x, count_y) :: !openlist
      done
   done;
   !openlist
;;

let random_open_tile plane bx1 by1 bx2 by2 =
   let openlist = list_open_tile plane bx1 by1 bx2 by2 in
   if not (openlist = []) then
      let choose_from = List.length openlist in
         let choice = Algo.mersenne choose_from in
            List.nth openlist choice
   else
      (
         print_string "There are no open tiles.";
         raise Not_found
      )
;;

type dungeon_feature =
   Rectangle
 | Line
 | Random
;;

(*
let kit_mb_with_items mb iid =
   let nstones = (Algo.mersenne 2) + (Algo.mersenne 2) in
   for count = 0 to nstones do
      let itype = Algo.mersenne 5 in
      let (lx, ly) = Mapblock.random_open_loc mb in
(*
         let iid = Mapblock.get_iid mb in
*)
            let id = Uaid.new_aid iid in
               let itm =
                  match itype with
                     0 ->
                        Item.backpack lx ly id
                   | 1 ->
                        Item.stone lx ly id
                   | 2 ->
                        Item.sword lx ly id
                   | 3 ->
                        Item.hat lx ly id
                   | _ ->
                        Item.potion_of_healing lx ly id
               in
                  Mapblock.gain_item mb itm
   done
;;
*)

let random_creature plane danger iid =
   let tbx = Mapplane.get_totalbound_x plane in
   let tby = Mapplane.get_totalbound_y plane in
   let lx, ly = random_open_tile plane 0 0 tbx tby in
   let mb = Mapplane.grid_get_mbcontent plane lx ly in
      if not (mb = Mapblock.mblock) then
(*
         let iid = Mapblock.get_iid mb in
*)
         let id = Uaid.new_aid iid in
         let creature = Person.select_creature danger lx ly id in
            if not (creature = Person.proto_person) then
               let plist = Mapblock.get_person mb in
                  Person.add_person creature plist
;;

let m_random_creature plane creaturesel iid =
   let tbx = Mapplane.get_totalbound_x plane in
   let tby = Mapplane.get_totalbound_y plane in
   let lx, ly = random_open_tile plane 0 0 tbx tby in
   let mb = Mapplane.grid_get_mbcontent plane lx ly in
      if not (mb = Mapblock.mblock) then
(*
         let iid = Mapblock.get_iid mb in
*)
         let id = Uaid.new_aid iid in
         let creature = creaturesel lx ly id in
            if not (creature = Person.proto_person) then
               let plist = Mapblock.get_person mb in
                  Person.add_person creature plist
;;
(*
let always_potion_healing lx ly id =
   Item.potion_of_healing lx ly id
;;

let hh_slimy_d_item lx ly id =
   match Algo.mersenne 8 with
    | 0 -> Item.club lx ly id
    | 1 -> Item.iron_helm lx ly id
    | 2 -> Item.metal_shield lx ly id
    | 3 -> Item.lizardskin_jacket lx ly id
    | 4 -> Item.lizardskin_boots lx ly id
    | 5 -> Item.stone_mace lx ly id
    | 6 -> Item.adze lx ly id
    | _ -> Item.sword lx ly id
;;
*)

let m_random_item plane which_item iid =
   let tbx = Mapplane.get_totalbound_x plane in
   let tby = Mapplane.get_totalbound_y plane in
   let lx, ly = random_open_tile plane 0 0 tbx tby in
(*   let lx, ly = Mapplane.spiralfind_opentile plane sx sy 4096 in *)
   let mb = Mapplane.grid_get_mbcontent plane lx ly in
      if not (mb = Mapblock.mblock) then
(*
         let iid = Mapblock.get_iid mb in
*)
            let id = Uaid.new_aid iid in
               let itm = which_item lx ly id in
                  Mapblock.gain_item mb itm
;;

let add_creatures plane mxm danger iid =
   for count = 0 to (mxm - 1) do
      let dl = Algo.mersenne danger in
         random_creature plane dl iid
   done
;;

let fill_tile plane x1 y1 ter =
   let rt = Mapplane.grid_get_rtile plane x1 y1 in
      Mapblock.set_terrain rt ter
;;

let carve_line_ret_points plane x1 y1 x2 y2 ter =
   let point = Algo.bresenham_line x1 y1 x2 y2 in
      for count = 0 to (List.length point - 1) do
         let lx, ly = List.nth point count in
            let rt = Mapplane.grid_get_rtile plane lx ly in
               Mapblock.set_terrain rt ter
      done;
      point
;;

let carve_line plane x1 y1 x2 y2 ter =
   ignore (carve_line_ret_points plane x1 y1 x2 y2 ter)
;;

let draw_border plane x1 y1 x2 y2 ter =
   carve_line plane x1 y1 x1 y2 ter;
   carve_line plane x1 y2 x2 y2 ter;
   carve_line plane x2 y2 x2 y1 ter;
   carve_line plane x2 y1 x1 y1 ter;
;;

let carve_rectangle_ret_points plane x1 y1 x2 y2 ter =
   let lx, ly = (min x1 x2), (min y1 y2) in
   let gx, gy = (max x1 x2), (max y1 y2) in
   let point = ref [] in
   for county = ly to gy do
      for countx = lx to gx do
         let rt = Mapplane.grid_get_rtile plane countx county in
            Mapblock.set_terrain rt ter;
            point := (countx, county) :: !point
      done
   done;
   !point
;;

let carve_rectangle plane x1 y1 x2 y2 ter =
   ignore (carve_rectangle_ret_points plane x1 y1 x2 y2 ter)
;;

let carve_triangle plane x1 y1 x2 y2 ter =
   let lx, ly = (min x1 x2), (min y1 y2) in
   let gx, gy = (max x1 x2), (max y1 y2) in
   for county = ly to gy do
      for countx = lx to gx do
        if countx >= county then
         let rt = Mapplane.grid_get_rtile plane countx county in
            Mapblock.set_terrain rt ter
      done
   done
;;

let carve_feature plane sub_x1 sub_y1 sub_x2 sub_y2 ter bx1 by1 bx2 by2
   feature 
=
   match feature with
      Line -> 
         carve_line_ret_points plane sub_x1 sub_y1 sub_x2 sub_y2 ter
    | Rectangle ->
         carve_rectangle_ret_points 
            plane sub_x1 sub_y1 sub_x2 sub_y2 ter
    | Random ->
         match Algo.mersenne 5 with
            0 | 1 | 2 -> 
               carve_line_ret_points 
                  plane sub_x1 sub_y1 sub_x2 sub_y2 ter
          | _ ->
               carve_rectangle_ret_points
                  plane sub_x1 sub_y1 sub_x2 sub_y2 ter
;;

let random_dungeon_feature n =
   match Algo.mersenne n with
      0 |
 1 -> Rectangle
    | _ -> Line
;;

let semirandom_cardinal x1 y1 x2 y2 =
   let dx, dy = (x2 - x1), (y2 - y1) in
      Algo.choose_cardinal dx dy
;;

let check_final nvx nvy x2 y2 =
   if nvx = x2 && nvy = y2
      then true
   else false
;;

let check_bounds x1 y1 bx1 by1 bx2 by2 =
   let lx, ly = (min bx1 bx2), (min by1 by2) in
   let ux, uy = (max bx1 bx2), (max by1 by2) in
   let nx1 = 
      if x1 < lx then 
         lx + 1
      else if x1 > ux then
         ux - 1
      else x1
   in
   let ny1 =
      if y1 > uy then
         uy - 1
      else if y1 < ly then
         ly + 1
      else y1
   in
      nx1, ny1
;;

let position_line x1 y1 x2 y2 bx1 by1 bx2 by2 cardinal dv sdv travel =
   let new_x, new_y =
      match cardinal with
         Algo.North | Algo.South ->
               (x1, y1 + (travel * sdv ))
       | _ -> 
               (x1 + (travel * sdv ), y1)
   in
      let bnx, bny = 
         check_bounds new_x new_y bx1 by1 bx2 by2 
      in
      let is_final =
         check_final bnx bny x2 y2
      in
         (bnx, bny, x1, y1, bnx, bny, is_final)
;;

let position_rectangle x1 y1 x2 y2 bx1 by1 bx2 by2 cardinal dv sdv
   travel 
=
   let new_x, new_y, subx1, suby1, subx2, suby2 =
      let rminus = Algo.mersenne 5 in
      let rplus = Algo.mersenne 5 in
      match cardinal with
         Algo.North | Algo.South ->
               (x1, y1 + (travel * sdv ),
                x1 - rminus, y1, x1 + rplus, y1 + (travel * sdv)
               )
       | _ -> 
               (x1 + (travel * sdv ), y1,
                x1, y1 - rminus, x1 + (travel * sdv), y1 - rplus
               )
   in
      let bnx, bny = 
         check_bounds new_x new_y bx1 by1 bx2 by2 
      in
      let is_final =
         check_final bnx bny x2 y2
      in
(* 3.09 found that this is now redundant
      let fx1, fy1 =
         check_bounds subx1 suby1 bx1 by1 bx2 by2
      in
*)
(* now redundant
      let fx2, fy2 =
         check_bounds subx2 suby2 bx1 by1 bx2 by2
      in
*)
(*
         (bnx, bny, fx1, fy1, fx2, fy2, is_final)
*)
         (bnx, bny, subx1, suby1, subx2, suby2, is_final)
;;

let choose_position x1 y1 x2 y2 bx1 by1 bx2 by2 cardinal dv sdv 
   travel variety
=
   match variety with
      Rectangle -> position_rectangle x1 y1 x2 y2 bx1 by1 bx2 by2
                      cardinal dv sdv travel
(*
 position_rectangle x1 y1 x2 y2 bx1 by1 bx2 by2
                      cardinal dv sdv travel
*)
    | _ -> position_line x1 y1 x2 y2 bx1 by1 bx2 by2
                 cardinal dv sdv travel
;;

let pick_feature x1 y1 x2 y2 bx1 by1 bx2 by2 cardinal fvar =
   let dx, dy = (x2 - x1), (y2 - y1) in
      let dv, sdv =
         if (cardinal = Algo.North || cardinal = Algo.South) then 
            if dy = 0 then 0, 0 else
               dy, dy / abs dy
         else 
            if dx = 0 then 0, 0 else
               dx, dx / abs dx
      in
         let travel = 
            if abs dv < 4 then max (abs dv) 1
            else min (3 + Algo.mersenne 3) (abs dv)
         in
         let variety =
            if fvar = Random then random_dungeon_feature 6
            else fvar
         in
            let new_x1, new_y1, sub_x1, sub_y1, sub_x2, sub_y2,
                final 
            =
               choose_position x1 y1 x2 y2 bx1 by1 bx2 by2 cardinal
                  dv sdv travel variety
            in
               let next_card = semirandom_cardinal new_x1 new_y1 x2 y2
               in
                  new_x1, new_y1, sub_x1, sub_y1, sub_x2, sub_y2,
                  variety, final, next_card
;;

let rec step_between plane x1 y1 x2 y2 ter bx1 by1 bx2 by2 cardinal =
   let 
      new_x1, new_y1, sub_x1, sub_y1, sub_x2, sub_y2, feature, final,
      next_cardinal 
   =
      pick_feature x1 y1 x2 y2 bx1 by1 bx2 by2 cardinal Random
   in
      ignore (carve_feature plane sub_x1 sub_y1 sub_x2 sub_y2 ter 
                    bx1 by1 bx2 by2 feature);
      if not final then
         step_between plane new_x1 new_y1 x2 y2 ter bx1 by1 bx2 by2
                      next_cardinal
;;

let rec passage_step_between plane x1 y1 x2 y2 ter bx1 by1 bx2 by2
   cardinal 
=
   let 
      new_x1, new_y1, sub_x1, sub_y1, sub_x2, sub_y2, feature, final,
      next_cardinal 
   =
      pick_feature x1 y1 x2 y2 bx1 by1 bx2 by2 cardinal Line
   in
      let newpoints =
         carve_feature plane sub_x1 sub_y1 sub_x2 sub_y2 ter 
                    bx1 by1 bx2 by2 feature
      in
      if not final then
         newpoints @ passage_step_between plane new_x1 new_y1 x2 y2 ter 
            bx1 by1 bx2 by2 next_cardinal
      else newpoints
;;

let connect_points plane x1 y1 x2 y2 
   bound_x1 bound_y1 bound_x2 bound_y2
=
   let dx, dy = (x2 - x1), (y2 - y1) in
   let cardinal = Algo.choose_cardinal dx dy in
      step_between plane x1 y1 x2 y2 Terrain.Stone_floor
         bound_x1 bound_y1 bound_x2 bound_y2 cardinal
;;

let passageconnect_points_ret_opentiles plane x1 y1 x2 y2 
   bound_x1 bound_y1 bound_x2 bound_y2
=
   let dx, dy = (x2 - x1), (y2 - y1) in
   let cardinal = Algo.choose_cardinal dx dy in
      let open_tiles =
         passage_step_between plane x1 y1 x2 y2 Terrain.Stone_floor
            bound_x1 bound_y1 bound_x2 bound_y2 cardinal
      in
         open_tiles
;;

let passageconnect_points plane x1 y1 x2 y2 
   bound_x1 bound_y1 bound_x2 bound_y2
=
   ignore
      (
         passageconnect_points_ret_opentiles plane x1 y1 x2 y2 
               bound_x1 bound_y1 bound_x2 bound_y2
      )
;;

let bres_connect plane x1 y1 x2 y2 =
   let point = Algo.bresenham_line x1 y1 x2 y2 in
      for count = 0 to (List.length point - 1) do
         let lx, ly = List.nth point count in
            let rt = Mapplane.grid_get_rtile plane lx ly in
               Mapblock.set_terrain rt Terrain.Stone_floor
      done
;;

let rec randomised_portals p bx1 by1 bx2 by2 =
   match p with
      [] -> []
    | head :: tail ->
         (
            let lx, ly = Algo.random_bounded_crd bx1 by1 bx2 by2 in
               Stair.set_lx head lx;
               Stair.set_ly head ly;
                  head :: randomised_portals tail bx1 by1 bx2 by2
         )
;;

let unanchored_connect_portals plane pre_portal bx1 by1 bx2 by2 =
(*
   let portal = randomised_portals pre_portal bx1 by1 bx2 by2 in
*)
   let portal = pre_portal in
   let limit = List.length portal - 1 in
   for count = 1 to limit do
      let from_p = List.nth portal count in
      let to_p = 
       (*  if count = limit then *) List.nth portal 0
(*         else List.nth portal (count + 1)  *)
      in
         let x1 = Stair.get_lx from_p in
         let y1 = Stair.get_ly from_p in
         let x2 = Stair.get_lx to_p in
         let y2 = Stair.get_ly to_p in
            passageconnect_points plane x1 y1 x2 y2 bx1 by1 bx2 by2
   done
;;

let connect_portals plane portal bx1 by1 bx2 by2 =
   unanchored_connect_portals plane portal bx1 by1 bx2 by2;
(*
      let from_p = List.nth portal 0 in
         let x1 = Stair.get_lx from_p in
         let y1 = Stair.get_ly from_p in
         let x2 = (Mapplane.get_tp_size plane) / 2 in
         let y2 = x2 in
            passageconnect_points plane x1 y1 x2 y2 bx1 by1 bx2 by2
*)
;;

let add_random_feature plane bx1 by1 bx2 by2 =
  try
   let start_x, start_y = random_open_tile plane bx1 by1 bx2 by2 in
   let end_x, end_y = random_tile plane bx1 by1 bx2 by2 in
      connect_points plane start_x start_y end_x end_y bx1 by1 bx2 by2
  with Not_found -> ()
(*
print_string "\nAdding random feature failed. \n"
*)
;;

let add_some_random_features plane bx1 by1 bx2 by2 =
   let qmax = (Mapplane.get_tp_size_x plane) / 12 +
         (Mapplane.get_tp_size_y plane / 12)
   in
      let num = Algo.roll_dice 4 qmax in
         for count = 0 to ( num - 1 ) do
            try
               add_random_feature plane bx1 by1 bx2 by2
            with Not_found -> 
               print_string "\nAdding random features failed.\n"
         done
;;

let carve_level plane portal lx1 ly1 gx2 gy2 =
   let bx1, by1 = (lx1 + 1), (ly1 + 1) in
   let bx2, by2 = (gx2 - 1), (gy2 - 1) in
(* *)
      carve_rectangle plane bx1 by1 bx2 by2 Terrain.Floor
(* *)
(*
   let plane_portals = Mapstruc.stairs_on_plane plane portal in
      let present_portals = 
         Stair.inbound_portals plane_portals bx1 by1 bx2 by2 
      in
         connect_portals plane present_portals bx1 by1 bx2 by2;
(*
         add_some_random_features plane bx1 by1 bx2 by2;
*)
(*
         draw_border plane bx1 by1 bx2 by2 Terrain.Earth_wall;
*)
         let psize = Mapplane.get_planesize plane in
()
*)
;;

let carve_circle plane cx cy diameter material =
   let radius = diameter / 2 in
      for countx = (cx - radius) to (cx + radius) do
         for county = (cy - radius) to (cy + radius) do
            if ((countx - cx) * (countx - cx)) +
               ((county - cy) * (county - cy)) < (radius * radius)
            then
            fill_tile plane countx county material
         done
      done
;;

let carve_centre_circle plane bx1 by1 bx2 by2 material =
   let diameter = (min ((bx2 - bx1) / 2) ((by2 - by1) / 2)) in
   let cx = bx1 + ((bx2 - bx1) / 2) in
   let cy = by1 + ((by2 - by1) / 2) in
      carve_circle plane cx cy (diameter - 2) material
;;

let carve_circular_room plane portal bx1 by1 gx2 gy2 =
   let bx2, by2 = (gx2 - 2), (gy2 - 2) in
   draw_border plane bx1 by1 bx2 by2 Terrain.Rock_wall;
   let plane_portals = Mapstruc.stairs_on_plane plane portal in
      let present_portals = 
         Stair.inbound_portals plane_portals bx1 by1 bx2 by2 
      in
         connect_portals plane present_portals bx1 by1 bx2 by2;
         carve_centre_circle plane bx1 by1 bx2 by2 Terrain.Stone_floor;
;;

let carve_rectangular_room plane portal bx1 by1 gx2 gy2 =
   let bx2, by2 = (gx2 - 2), (gy2 - 2) in
   draw_border plane bx1 by1 bx2 by2 Terrain.Rock_wall;
   let plane_portals = Mapstruc.stairs_on_plane plane portal in
      let present_portals = 
         Stair.inbound_portals plane_portals bx1 by1 bx2 by2 
      in
         connect_portals plane present_portals bx1 by1 bx2 by2;
         carve_rectangle plane (bx1 + 1) (by1 + 1) (bx2 - 1) (by2 - 1)
               Terrain.Stone_floor;
;;
