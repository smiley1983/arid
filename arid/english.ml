(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    english.ml

*  What does it do?

**    Provides functions to generate English language strings

*)

let consonants =
   ['b';'c';'d';'f';'g';'h';'j';'k';'l';'m';'n';'p';'q';'r';'s';'t';'v';
    'w';'x';'y';'z'
   ]
;;

let vowels =
   ['a';'e';'i';'o';'u']
;;

let prefixed_singular label =
   let firstletter = Char.lowercase label.[0] in
      if (List.mem firstletter vowels) then
         "an " ^ label
      else
         "a " ^ label
;;

let prefixed_plural label number =
   if number = 1 then
      let firstletter = Char.lowercase label.[0] in
         if (List.mem firstletter vowels) then
            "an " ^ label
         else
            "a " ^ label
   else
      let n = string_of_int number in
         n ^ " " ^ label ^ "s"
;;

let definite_article label number =
   if number = 1 then
      "the " ^ label
   else
      let n = string_of_int number in
         "the " ^ n ^ " " ^ label ^ "s"
;;

let rec you_see_here msgbank separator namelist = 
   "You see here " ^ next_name msgbank separator namelist true
and next_name msgbank separator namelist first =
   if first then
      match namelist with
       | [] -> ""
       | final :: [] -> final
       | head :: tail -> head ^ 
            (next_name msgbank separator tail false)
   else
      match namelist with
       | [] -> ""
       | final :: [] -> " and " ^ final
       | head :: tail -> separator ^ head ^ 
            (next_name msgbank separator tail false)
;;

let n1_verbs_n2 see_n1 see_n2 vf1 vf2 n1_ispc n2_ispc n1name n2name =
  let part1 =
      if n1_ispc then
         "you " ^ vf1 ^ " "
      else if see_n1 then
         (definite_article n1name 1) ^ " " ^ vf2 ^ " "
      else
         "Something " ^ vf2 ^ " "
   in
   let part2 =
      if n2_ispc then
         "you"
      else if see_n2 then
         definite_article n2name 1
      else "something"
   in part1 ^ part2 ^ "."
;;
