(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    equip.ml

*  What does it do?

**    Declares types for equip lists and provides API for command.ml, 
etc... to make use of them.

*)


type equipment =
   {
      mutable equip_index : char list;
      mutable slot : 
         (char * (Body.equip_slot * string * Item.item_flyweight)) list
   }
;;

let proto_equipment =
 {
   equip_index = 
           ['a' ; 'b' ; 'c' ; 'd' ; 'e' ; 'f' ; 'g' ;
            'h' ; 'i' ; 'j' ; 'k' ; 'l' ; 'm' ; 'n' ;
            'o' ; 'p' ; 'q' ; 'r' ; 's' ; 't' ; 'u' ;
            'v' ; 'w' ; 'x' ; 'y' ; 'z'
(*
            ; 'A' ; 'B' ; 'C' ; 'D' ; 'E' ; 'F' ; 'G' ;
            'H' ; 'I' ; 'J' ; 'K' ; 'L' ; 'M' ; 'N' ;
            'O' ; 'P' ; 'Q' ; 'R' ; 'S' ; 'T' ; 'U' ;
            'V' ; 'W' ; 'X' ; 'Y' ; 'Z'
*)
           ];
   slot = []
 }
;;

let get_slot e = e.slot;;

let add_slot e bodypart partname =
   match e.equip_index with
      [] -> raise Not_found
    | head :: tail ->
         (
            e.equip_index <- tail;
            e.slot <- e.slot @ [(head, (bodypart, partname, 
                                Item.proto_item))]
         )
;;

let equipslot_of_index e ind =
   if not (List.mem_assoc ind e.slot) then
      raise Not_found
   else
      List.assoc ind e.slot
;;

let rec switch_and_ret_item e ind itm =
   next_item e e.slot ind [] itm itm
and next_item e eslot ind newslot found_it replace_with =
   match eslot with
      [] ->
         (
            e.slot <- newslot;
            found_it
         )
    | head :: tail ->
         (
            let (test_index , (vala, valb, test_it)) = head in
               if test_index = ind then
                     (
                         let nextslot = newslot @
                           [
                            (test_index, (vala, valb, replace_with))
                           ]
                         in
                            next_item e tail ind nextslot test_it 
                                  replace_with
                     )
               else
                     let nextslot = newslot @ [head] in
                        next_item e tail ind nextslot found_it
                              replace_with
         )
;;

let remove_index e ind =
   if not (List.mem_assoc ind e.slot) then
      raise Not_found
   else
      switch_and_ret_item e ind Item.proto_item
;;

let switch_index_with_item e ind itm =
   if not (List.mem_assoc ind e.slot) then
      raise Not_found
   else
      switch_and_ret_item e ind itm
;;

let has_index e =
   match (List.length e.equip_index) with
      0 -> false
    | _ -> true
;;

let new_equipment (n : int) =
   {proto_equipment with slot = []}
;;

let rec damage_mod e =
   next_damage_mod e.slot 0
and next_damage_mod lst count =
   match lst with
      [] -> count
    | (_, (_, _, itm)) :: tail ->
         let dplus = Item.get_attribute_strength itm Item.Damage in
            if dplus > 0 then next_damage_mod tail (count + dplus)
            else next_damage_mod tail count
;;

let rec attack_mod e =
   next_attack_mod e.slot 0
and next_attack_mod lst count =
   match lst with
      [] -> count
    | (_, (_, _, itm)) :: tail ->
         let dplus = Item.get_attribute_strength itm Item.Attack in
            if dplus > 0 then next_damage_mod tail (count + dplus)
            else next_attack_mod tail count
;;

let rec mod_total e v =
   next_mod_total e.slot v 0
and next_mod_total lst v count =
   match lst with
      [] -> count
    | (_, (_, _, itm)) :: tail ->
         let dplus = Item.get_attribute_strength itm v in
            if dplus > 0 then next_mod_total tail v (count + dplus)
            else next_mod_total tail v count
;;

let protection_mod e =
   mod_total e Item.Protection
;;
