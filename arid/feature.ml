(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    feature.ml

*  What does it do?

**    Provides feature types

*)

type feature_variety =
   Road
 | Startroad
 | River
 | Grass_triangle_fade
 | Forest_triangle_fade
 | Sea_triangle_fade
 | Sand_triangle_fade
 | Stream
 | Road_stub
 | Ruin_large
 | Ruin_small
 | Dungeon_large
 | Dungeon_small
 | Stone_circle
 | Stone_dungeon
 | Steel_dungeon
 | Twisted_tunnel_m
 | Dank_pit
 | Abomination_lair_m
 | Hopeless_cycle_m
 | Hole_in_rock
 | Circle_room
 | Rectangle_room
 | Triangle_room
 | Banishment_dungeon
 | Perfect_maze
 | Shop_maze
 | Nothing
;;

