(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    i_base.ml

*  What does it do?

**    Provides the base item types to be used by all modules.

*)

type base =
 | Proto_item
 | Longsword
 | Corpse
 | Healing_potion
 | Giant_centipede
 | Giant_centipede_corpse
 | Canine_skeleton
 | PC
 | Pile_of_bones
 | Apple
 | Human
 | Human_corpse
 | Rodent
 | Rodent_corpse
;;
