(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    inventry.ml

*  What does it do?

**    Provides types and api for inventory management. Actual management 
is done through action and command.

*)


type inventory =
   {
      mutable index : char list;
      mutable itm : (char * Item.item_flyweight ) list
   }
;;

let proto_inventory =
 {
   index = ['a' ; 'b' ; 'c' ; 'd' ; 'e' ; 'f' ; 'g' ;
            'h' ; 'i' ; 'j' ; 'k' ; 'l' ; 'm' ; 'n' ;
            'o' ; 'p' ; 'q' ; 'r' ; 's' ; 't' ; 'u' ;
            'v' ; 'w' ; 'x' ; 'y' ; 'z' ;
            'A' ; 'B' ; 'C' ; 'D' ; 'E' ; 'F' ; 'G' ;
            'H' ; 'I' ; 'J' ; 'K' ; 'L' ; 'M' ; 'N' ;
            'O' ; 'P' ; 'Q' ; 'R' ; 'S' ; 'T' ; 'U' ;
            'V' ; 'W' ; 'X' ; 'Y' ; 'Z' ];
   itm = []
 }
;;

let get_list inv = inv.itm;;

let is_duplicate_add inv item prototype =
 try
   for count = 0 to (List.length inv.itm - 1) do
      let ( _ , check_item) = (List.nth inv.itm count) in
         if Item.should_stack check_item item prototype then
            (
               Item.stack_on check_item item;
               raise Not_found
            )
         else if item = check_item then raise Not_found
   done;
   false
 with Not_found -> true
;;

let add_item inv item prototype =
   if not (is_duplicate_add inv item prototype) then
      match inv.index with
         [] -> raise Not_found
       | head :: tail ->
            (
               inv.index <- tail;
               inv.itm <- inv.itm @ [(head, item)]
            )
;;

let item_of_index inv ind =
   if not (List.mem_assoc ind inv.itm) then
      raise Not_found
   else
      List.assoc ind inv.itm
;;

let remove_index inv ind =
   if not (List.mem_assoc ind inv.itm) then
      raise Not_found
   else
      let item = List.assoc ind inv.itm in
         (inv.itm <- List.remove_assoc ind inv.itm;
          inv.index <- ind :: inv.index ;
          item
         )
;;

let has_index inv =
   match (List.length inv.index) with
      0 -> false
    | _ -> true
;;

let new_inventory (n : int) =
   {proto_inventory with itm = []}
;;

let rec index_of_item inv itm =
   let lst = inv.itm in
      return_index lst itm
and return_index lst itm =
   match lst with
      [] ->
         raise Not_found
    | (ind, test_itm) :: tail ->
         if test_itm == itm then ind
         else return_index tail itm
;;

let rec lose_item inv itm =
   inv.itm <-
      invlist_without inv.itm itm []
and invlist_without ilist itm newl =
   match ilist with
      [] ->
         newl
    | (ind, test_itm) :: tail ->
         if test_itm == itm then invlist_without tail itm newl
         else invlist_without tail itm ((ind, test_itm) :: newl)
;;

