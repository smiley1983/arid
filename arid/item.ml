(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    item.ml

*  What does it do?

**    Provides types and functions and prototypes for items. Note that a 
Person.person contains an Item.item to describe its item-like 
attributes, and spells are items (both abstract and physical; that is, 
the spell "fireball" is a special item which can go into a person's 
spell inventory, and the ball of fire created by that spell is also an 
Item.item. I was considering whether terrain should be represented by 
items, but I don't think so right now. It would have certain advantages, 
though, so I might move to that approach later.
 
*)


type item_attribute =
 | Any
 | Spell
 | Spell_item
 | Container
 | Stone
 | Hat
 | Collar
 | Shirt
 | Vambrace
 | Weapon
 | Gloves
 | Pants
 | Shoes
 | Homing_missile
 | Magic_dart
 | Acid_bolt
 | Attack
 | Damage
 | Protection
 | Evasion
 | Potion
 | Healing
 | Scroll
 | Food
 | Skeleton
 | Insect
;;

let new_iid item_idc =
   Uaid.new_aid item_idc
;;

type item =
 {
   mutable symbol : Symbol.symbol;
   mutable colour : U_colour.colour;
   mutable iname : string;
   mutable surface_name : string;
   mutable identified : bool;
   desc : string;
   appearance : string;
   stackable : bool;
 }
;;

let proto_item_base =
 {
   symbol = Symbol.Potion;
   colour = U_colour.Magenta;
   iname = "proto-item";
   surface_name = "program bug";
   identified = false;
   desc = "This shouldn't be here. Sorry. Your savefile's integrity"
        ^ "may be less than perfect.";
   appearance = "This is a program bug. Not an easter egg or "
         ^ "elaborate joke, but something which should actually not"
         ^ "appear in the world. Sorry.";
   stackable = false;
 }
;;

(*
 * Not exactly a fly's weight, but here goes...
 * The problem is that all of these things really can vary from one 
 * instance of an item to another, and none of them are required for 
 * generic identification. Note _identified_ being separate from 
 * _attributes_known_; you can find out that copper rings are of 
 * teleportation, but you don't know that this one is cursed.
 * _base_ tells it what prototypical item to use.
 *)

type item_flyweight =
 {
   mutable locx : int;
   mutable locy : int;
   mutable repeat : int;
   mutable inv : item_flyweight list;
   mutable label : string;
   mutable itype : (item_attribute * (int ref)) list;
   mutable losblock : bool;
   mutable physblock : bool;
   mutable attributes_known : bool;
   mutable base : I_base.base;
   id : int;
 }
;;

let proto_item =
 {
   locx = 1;
   locy = 1;
   repeat = 1;
   inv = [];
   label = "";
   itype = [];
   losblock = false;
   physblock = false;
   attributes_known = false;
   base = I_base.Proto_item;
   id = -1;
 }
;;

let get_locx itm = itm.locx;;
let get_locy itm = itm.locy;;

let get_symbol itm protolist = 
   let proto = List.assoc itm.base protolist in
      proto.symbol
;;

let get_colour itm protolist = 
   let proto = List.assoc itm.base protolist in
      proto.colour
;;

let get_id itm = itm.id;;

let get_desc itm protolist =
   let proto = List.assoc itm.base protolist in
      if proto.identified then
         proto.desc
      else
         proto.surface_name
;;

let get_label itm = itm.label;;
let set_label itm v = itm.label <- v;;

let get_itype itm = itm.itype;;
let get_losblock itm = itm.losblock;;
let get_physblock itm = itm.physblock;;
let get_stackable itm = itm.stackable;;
let get_repeat itm = itm.repeat;;

let set_locx itm v = itm.locx <- v ;;
let set_locy itm v = itm.locy <- v ;;

let set_identified itm v = itm.identified <- v;;

let get_identified ifly protolist = 
   let proto = List.assoc ifly.base protolist in
      proto.identified
;;

let should_stack itm1 itm2 protolist =
   if itm1.base = itm2.base then
      let proto = List.assoc itm1.base protolist in
      if proto.stackable then true
      else false
   else false
(*
   if itm1.stackable
   && itm1.symbol = itm2.symbol
   && itm1.colour = itm2.colour
   && itm1.iname = itm2.iname
   && itm1.itype = itm2.itype
   && itm1.losblock = itm2.losblock
   && itm1.physblock = itm2.physblock
   then
      true
   else
      false
*)
;;

let stack_on itm1 itm2 =
   itm1.repeat <- (itm1.repeat + itm2.repeat)
;;

let decrement_stack itm =
   itm.repeat <- (itm.repeat - 1)
;;

let split_stack itm1 number iidc =
   match number with
      0 -> (itm1, proto_item)
    | _ -> 
         if number = itm1.repeat then (proto_item, itm1)
         else if number < itm1.repeat && number > 0 then
              (
               {
                itm1 with
                   repeat = (itm1.repeat - number)
               }
               ,
               {
                itm1 with
                   repeat = number;
                   id = (new_iid iidc)
               }
              )
           else (itm1, proto_item)
;;

type itemlist =
 {
   mutable i : item_flyweight list
 }
;;

let proto_itemlist =
 {
   i = []
 }
;;

let new_ilist b =
   if b then {i = []}
   else proto_itemlist
;;

let get_list il = il.i;;

let add_item itm il =
   il.i <- itm :: il.i
;;

let rec real_remove_item itm lst =
   match lst with
      [] -> []
    | head :: tail ->
        (
         if head = itm then tail
         else head :: real_remove_item itm tail
        )
;;

let unpack_item itm fromitem =
   fromitem.inv <- real_remove_item itm fromitem.inv
;;

let remove_item itm ilist =
   ilist.i <- real_remove_item itm ilist.i
;;

let item_on_loc ilist lx ly =
   let reslt = ref proto_item in
   for count = 0 to (List.length ilist.i - 1) do
      let itm = List.nth ilist.i count in
         if (itm.locx = lx) && (itm.locy = ly) then
            reslt := itm
   done;
   if !reslt = proto_item then raise Not_found
   else !reslt
;;

let items_on_loc ilist lx ly =
   let reslt = ref [] in
   for count = 0 to (List.length ilist.i - 1) do
      let itm = List.nth ilist.i count in
         if (itm.locx = lx) && (itm.locy = ly) then
            reslt := itm :: !reslt
   done;
   !reslt
;;

let get_spell_owner sp = sp.id;;

let is_spell itm =
   List.mem_assoc Spell itm.itype || List.mem_assoc Spell_item itm.itype
;;

let has_attribute itm att =
   List.mem_assoc att itm.itype
;;

let get_attribute_strength itm att =
   if has_attribute itm att then
      !(List.assoc att itm.itype)
   else -1
;;

let proto_person smb clr pname =
 {
   proto_item_base with
      symbol = smb;
      colour = clr;
      iname = pname;
      surface_name = pname;
 }
;;

let new_person_item lx ly iid smb clr pname =
(*
   let nbase = proto_person smb clr pname in
*)
    {
      proto_item with
         locx = lx;
         locy = ly;
         id = iid;
         physblock = true;
         base = pname;
 }
;;

let gain_attribute att itm =
   itm.itype <- (att, (ref 0)) :: itm.itype
;;

let gain_attribute_value a_type a_value itm =
   itm.itype <- (a_type, (ref a_value)) :: itm.itype
;;

let rec itype_without att il =
   next_itype_without att il []
and next_itype_without att il nl =
   match il with
      [] -> nl
    | head :: tail ->
         let (a, v) = head in
            if a = att then next_itype_without att tail nl
            else next_itype_without att tail (head :: nl)
;;

let lose_attribute att itm =
   itm.itype <- (itype_without att itm.itype)
;;

let set_attribute_value a_type a_val itm =
(*
   if has_attribute itm a_type then
*)
   try
      let current_val = List.assoc a_type itm.itype in
         current_val := a_val
   with Not_found ->
      gain_attribute_value a_type a_val itm
;;

let move_to_into subj obj =
   obj.inv <- (subj :: obj.inv)
;;

let move_to_out_of subj obj =
   obj.inv <- real_remove_item subj obj.inv
;;

let is_full (itm:item_flyweight) = false;;

let get_inventory itm = itm.inv;;

let get_prototype itm protolist =
   List.assoc itm.base protolist
;;
