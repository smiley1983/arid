(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    language.ml

*  What does it do?

**    Provides the entry point into the language system. Any and 
every bit of text which is going to be shown to the player should be 
accessed through this file. It controls translation and some text 
formatting issues.

*)

let world_shimmer lang =
   match lang with
    | Message.English ->
         "The world shimmers for a moment"
    | Message.Fictional ->
         "hed twil tar"
;;

let you_see_here msgbank separator itemlist stairlist =
   match Message.get_language msgbank with
    | Message.English ->
         English.you_see_here msgbank separator (itemlist @ stairlist)
    | Message.Fictional ->
         "lom vo "
;;

let template lang =
   match lang with
    | Message.English ->
         ""
    | Message.Fictional ->
         ""
;;

let attribute_name_qty v =
   match v with
 | Item.Any -> ""
 | Item.Spell -> ""
 | Item.Spell_item -> ""
 | Item.Container -> ""
 | Item.Stone -> ""
 | Item.Hat -> ""
 | Item.Collar -> ""
 | Item.Shirt -> ""
 | Item.Vambrace -> ""
 | Item.Weapon -> ""
 | Item.Gloves -> ""
 | Item.Pants -> ""
 | Item.Shoes -> ""
 | Item.Homing_missile -> ""
 | Item.Magic_dart -> ""
 | Item.Acid_bolt -> ""
 | Item.Attack -> "attack"
 | Item.Damage -> "damage"
 | Item.Protection -> "protection"
 | Item.Evasion -> "evasion"
 | Item.Potion -> ""
 | Item.Healing -> "healing"
 | Item.Scroll -> ""
 | Item.Food -> ""
 | Item.Skeleton -> ""
 | Item.Insect -> ""
;;

let get_iname itm protolist =
   let proto = List.assoc itm.Item.base protolist in
   let usename =
      if proto.Item.identified then
         proto.Item.iname
      else
         proto.Item.surface_name
   in
   if itm.Item.repeat < 2 then
      usename
   else ((string_of_int itm.Item.repeat) ^ "x " ^ usename)
;;

let get_prefixed_iname itm protolist =
   let proto = List.assoc itm.Item.base protolist in
   let usename =
      if proto.Item.identified then
         proto.Item.iname
      else
         proto.Item.surface_name
   in
      English.prefixed_plural usename itm.Item.repeat
;;

let get_definite_article itm protolist =
   let proto = List.assoc itm.Item.base protolist in
   let usename =
      if proto.Item.identified then
         proto.Item.iname
      else
         proto.Item.surface_name
   in
      English.definite_article usename itm.Item.repeat
;;

let get_singular_prefixed_iname itm protolist =
   let proto = List.assoc itm.Item.base protolist in
   let usename =
      if proto.Item.identified then
         proto.Item.iname
      else
         proto.Item.surface_name
   in
      English.prefixed_singular usename
;;

let n1_verbs_n2 lang see_n1 see_n2 vf1 vf2 n1_ispc n2_ispc n1name n2name 
=
   match lang with
    | Message.English ->
         English.n1_verbs_n2 see_n1 see_n2 vf1 vf2 n1_ispc n2_ispc 
               n1name n2name
    | Message.Fictional -> "n1 verbs n2"
;;

let kill_message lang see_n1 see_n2 att_ispc def_ispc attname defname =
   match lang with
    | Message.English ->
         English.n1_verbs_n2 see_n1 see_n2 "kill" "kills" 
               att_ispc def_ispc attname defname
    | Message.Fictional -> "something kills something"
;;

let hit_message lang see_n1 see_n2 att_ispc def_ispc attname defname =
   match lang with
    | Message.English ->
         English.n1_verbs_n2 see_n1 see_n2 "hit" "hits" 
               att_ispc def_ispc attname defname
    | Message.Fictional -> "something hits something"
;;

let miss_message lang see_n1 see_n2 att_ispc def_ispc attname defname =
   match lang with
    | Message.English ->
         English.n1_verbs_n2 see_n1 see_n2 "miss" "misses" 
               att_ispc def_ispc attname defname
    | Message.Fictional -> "something misses something"
;;

