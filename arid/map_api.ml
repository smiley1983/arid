(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    map_api.ml

*  What does it do?

*    provides api to arid.ml for map handling

*)

let get_map m = Mapstruc.get_map m;;

let build cplane portal x y cname fid iid protolist =
   let wt = Mapplane.grid_get_wtile cplane x y in
      if wt == Mapplane.wtile then
        (print_string "Attempt to build outside boundaries failed. \n";
         raise Not_found
        )
      else if not (Mapplane.get_wt_built wt) then
         let bsizex = Mapplane.get_pblocksize_x cplane in
         let bsizey = Mapplane.get_pblocksize_y cplane in
         let ter = Mapplane.get_wt_terrain wt in
(* WINCE - This is not good *)
(*
          if not 
             (
                ter = Mapplane.Ruin_grassy 
                || ter = Mapplane.Dungeon
                || ter = Mapplane.Banishment_dungeon
                || ter = Mapplane.Perfect_maze
                || ter = Mapplane.Shop_maze
             )
          then
           (
(* DEBUG
            print_string "Normal block\n";
/DEBUG *)
            let new_block = Mapblock.new_block bsizex bsizey x y in
              (
               Mapblock.init_mapblock new_block;
               Buildmap.individual_block new_block cplane wt iid x y;
               Mapplane.set_wt_built wt true;
               Mapplane.set_wt_loaded wt true;
               Mapplane.set_wtile_content wt new_block;
               let fname = Mapplane.new_filename cname fid in
                  Mapplane.set_filename cplane x y fname;
                  Mapplane.grid_set_wtile cplane x y wt;
              )
           )
          else
*)
            try
             Buildmap.special_feature cplane portal x y wt
                cname fid iid protolist
            with e -> raise (Failure 
                  ("Failed in Buildmap.special_feature " ^
                   "Exception thrown was:   " ^
                   Printexc.to_string e
                  ))
;;

(*
let rec build_all ms unbuilt cplanename fname pc cname fid iid =
   match unbuilt with
      [] -> (Mapstruc.save_mapstruc ms ; ms)
    | (x, y) :: tail ->
         (
           try
           (
            let portal = Mapstruc.get_portal ms in
            let cplane = Mapstruc.get_cplane ms in
               build cplane portal x y cname fid iid;
               Mapplane.save_grid cplane x y
           ) with not_found -> ()
            ; build_all ms tail cplanename fname pc cname fid iid
         )
;;
*)

let build_all ms unbuilt cplanename fname pc cname fid iid protolist =
   let cplane = Mapstruc.get_cplane ms in
   for count = 0 to (List.length unbuilt - 1) do
      let (x, y) = List.nth unbuilt count in
       try
        (
         let portal = Mapstruc.get_portal ms in
         build cplane portal x y cname fid iid protolist;
         Mapplane.save_grid cplane x y
        )
       with e (*Not_found*) ->
          print_string 
          ("block " ^ 
           (string_of_int x) ^ ", " ^ 
           (string_of_int y) ^ " failed to build.\n" ^
           "Exception thrown was: \n" ^
           (Printexc.to_string e) ^ "\n"
          )
   done;
   ms
;;

let load_mapstruc cplanename filename pc cname fid iid protolist =
   let (ms, unbuilt) =
      Mapstruc.load_mapstruc cplanename filename pc cname fid iid
   in
      if not (unbuilt = []) then
        (
         build_all ms unbuilt cplanename filename pc cname fid iid 
            protolist;
        )
      else 
        (
         ms
        )
;;

let physical_switch_planes mhold pre_plane pc from_stair iid =
   Mapstruc.save_mapstruc (get_map mhold);
   let built_new = Mapstruc.prepare_switch mhold pre_plane pc in
   Mapstruc.save_mapstruc (get_map mhold);
   let pname = W_plane.planename pre_plane in
   let ms = get_map mhold in
   let filename = Mapstruc.get_msfilename ms in
      let (new_ms, unbuilt) =
         Mapstruc.load_mapstruc pname filename pc 
            (Mapstruc.get_cname ms)
            (Mapstruc.get_fid mhold) iid
      in
         if built_new then
            (let cplane = Mapstruc.get_cplane ms in
               Buildmap.add_stairs ms cplane)
         ;
         Mapstruc.save_mapstruc ms;
         let (ret_ms, discard_unbuilt) =
            Mapstruc.load_mapstruc pname filename pc 
               (Mapstruc.get_cname ms)
               (Mapstruc.get_fid mhold) iid
         in
(*
            Person.set_locx pc (Stair.get_to_lx from_stair);
            Person.set_locy pc (Stair.get_to_ly from_stair);
*)
            (ret_ms, unbuilt, built_new)
;;

let switch_planes mhold pre_plane filename pc from_stair cname fid iid
   scenario prototypes
=
   let (ms, unbuilt, built_new) =
     (
      physical_switch_planes mhold pre_plane pc from_stair iid
     )
   in
      (
       if built_new then
         let cplane = Mapstruc.get_cplane ms in
            try
               let scene_variety =
                  Scenemap.get_variety scenario
               in
                  ignore (Buildmap.coarse_build cplane scene_variety)
            with Not_found -> 
               print_string "Buildmap.coarse_build failed\n"
      )
      ;
      if not (unbuilt = []) then
         let planename = W_plane.planename pre_plane in
         let new_ms =
            try
               (build_all ms unbuilt planename filename pc cname fid iid
                prototypes)
            with Not_found -> 
               (print_string "Map_api.build_all failed\n"
                ; raise Not_found)
         in
           (
(*
            Person.set_locx pc (Stair.get_to_lx from_stair);
            Person.set_locy pc (Stair.get_to_ly from_stair);
*)
            Mapstruc.set_map mhold new_ms
           )
      else
         Mapstruc.set_map mhold ms
;;

let num_people_in_block_crd ms lx ly =
   List.length 
      ( Person.get_list 
         ( Mapblock.get_person
            ( Mapplane.grid_get_mbcontent
               (Mapstruc.get_cplane (Mapstruc.get_map ms)) lx ly
            )
         )
      )
;;

let new_map w_map bsizex bsizey iid =
   let new_mhold =
      Mapstruc.new_map w_map bsizex bsizey iid
   in
   new_mhold
;;

let mapstruc_init ms mapname bsizex bsizey charname fid scenario link =
   Mapstruc.mapstruc_init ms mapname bsizex bsizey charname fid link;
   let cplane = Mapstruc.get_cplane ms in
      let (lx, ly) =
       let scene_variety =
          Scenemap.get_variety scenario
       in
         Buildmap.coarse_build cplane scene_variety;
      in
         if not (lx = -1) then
            (
               Scenemap.set_start_lx scenario lx;
               Scenemap.set_start_ly scenario ly
            );
         Buildmap.add_stairs ms cplane
;;
