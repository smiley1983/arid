(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    mapblock.ml

*  What does it do?

**    Provides tile-scale map handling

*)


type tile =
   {
      mutable terrain : Terrain.terrain_variety ;
      mutable vis_symbol : Symbol.symbol;
      mutable vis_colour : U_colour.colour;
      mutable vis_time : int;
      mutable special_colour : U_colour.colour;
      mutable losblock : bool;
      mutable physblock : bool;
      mutable blockname : string;
      mutable last_vis_symbol : Symbol.symbol;
      mutable temp_vis_symbol : Symbol.symbol;
      mutable temp_vis_colour : U_colour.colour;
      mutable missile_time : int;
      mutable contains_person : bool;
      mutable contains_thing : bool;
   }
;;

let rtile =
   {
      terrain = Terrain.Void;
      vis_symbol = Symbol.Void;
      vis_colour = U_colour.conv_colour_s "black";
      vis_time = (-1);
      special_colour = U_colour.Darkgrey;
      losblock = false;
      physblock = true;
      blockname = "void";
      last_vis_symbol = Symbol.Void;
      temp_vis_symbol = Symbol.Void;
      temp_vis_colour = U_colour.Black;
      missile_time = -1;
      contains_person = false;
      contains_thing = true;
   }
;;

type map_block =
   {
      mutable grid : tile array array;
      mutable blocksize_x : int;
      mutable blocksize_y : int;
      mutable person : Person.personlist;
      mutable item : Item.itemlist;
      mutable x_off : int;
      mutable y_off : int;
   }
;;

let mblock =
   {
      grid = Array.make_matrix 1 1 rtile;
      blocksize_x = 1;
      blocksize_y = 1;
      person = Person.proto_personlist;
      item = Item.proto_itemlist;
      x_off = 0;
      y_off = 0;
   }
;;

let get_item mb =
   mb.item
;;

let get_person mb =
   mb.person
;;

let get_blocksize_x mb =
   mb.blocksize_x
;;

let get_blocksize_y mb =
   mb.blocksize_y
;;

let new_block bsize_x bsize_y xo yo =
   { mblock with 
     blocksize_x = bsize_x ; blocksize_y = bsize_y ;
     item = Item.new_ilist true ; person = Person.new_plist true ;
     x_off = xo ; y_off = yo
   }
;;

let init_mapblock mapblock =
   mapblock.grid <- Array.make_matrix mapblock.blocksize_x
                                      mapblock.blocksize_y rtile;
   for county = 0 to (mapblock.blocksize_y - 1) do
      for countx = 0 to (mapblock.blocksize_x - 1) do
         mapblock.grid.(countx).(county)
            <- {rtile with 
                terrain=Terrain.Void;
                vis_time = (-1);
                contains_thing = false
               }
      done
   done
;;

let prepare_for_save mapblock =
   for county = 0 to (mapblock.blocksize_y - 1) do
      for countx = 0 to (mapblock.blocksize_x - 1) do
         let thistile = mapblock.grid.(countx).(county) in
            if thistile.vis_time > 0 then
               thistile.vis_time <- 1;
            if thistile.missile_time > 0 then
               thistile.missile_time <- 1
      done
   done
;;

let get_mb_rtile mapblock x y =
   if x >= 0 && x < mapblock.blocksize_x 
             && y >= 0 && y < mapblock.blocksize_y then
      mapblock.grid.(x).(y)
   else rtile
;;

let get_terrain rt =
   rt.terrain
;;

let set_terrain rt t_int =
   rt.terrain <- t_int
;;

let draw_border mapblock ter =
   let limit_x = mapblock.blocksize_x - 1 in
   let limit_y = mapblock.blocksize_y - 1 in
   for count_x = 0 to limit_x do
      for count_y = 0 to limit_y do
         if count_x = 0 || count_x = limit_x
         || count_y = 0 || count_y = limit_y then
            let rt = mapblock.grid.(count_x).(count_y) in
               set_terrain rt ter
      done
   done
;;

let terrain_physblock rt =
   Terrain.get_physblock (Terrain.sample_terrain rt.terrain)
;;

let terrain_losblock rt =
   Terrain.get_losblock (Terrain.sample_terrain rt.terrain)
;;

let vis_symbol rt = rt.vis_symbol;;
let vis_colour rt = rt.vis_colour;;
let vis_time rt = rt.vis_time;;

let set_vis_symbol rt symbol = rt.vis_symbol <- symbol;;
let set_vis_colour rt colour = rt.vis_colour <- colour;;
let set_vis_time rt time = rt.vis_time <- time;;

let is_losblocked rt = rt.losblock;;
let is_physblocked rt = rt.physblock;;

let get_blockname rt = rt.blockname;;
let set_blockname rt v = rt.blockname <- v;;

let get_last_vis_symbol rt = rt.last_vis_symbol;;
let set_last_vis_symbol rt symbol = rt.last_vis_symbol <- symbol;;

let backup_vis_sc rt =
   rt.temp_vis_symbol <- rt.vis_symbol;
   rt.temp_vis_colour <- rt.vis_colour
;;

let restore_vis_sc rt =
   rt.vis_symbol <- rt.temp_vis_symbol;
   rt.vis_colour <- rt.temp_vis_colour
;;

let get_missile_time rt = rt.missile_time;;
let set_missile_time rt t = rt.missile_time <- t;;

let set_losblock rt v = rt.losblock <- v;;
let set_physblock rt v = rt.physblock <- v;;

let get_special_colour rt = rt.special_colour;;
let set_special_colour rt v = rt.special_colour <- v;;

let set_cperson rt v = 
   rt.contains_person <- v
;;

let set_cthing rt v =
   rt.contains_thing <- v
;;

let contains_person rt = rt.contains_person;;

let contains_thing rt = rt.contains_thing;;

let is_blank rt = 
   not (rt.contains_person || rt.contains_thing)
;;

let get_offset mb = (mb.x_off, mb.y_off);;

let rec next_open_xloc mb xcount ycount off_x off_y =
   if xcount < mb.blocksize_x then
      let rt = get_mb_rtile mb xcount ycount in
         let ter = Terrain.sample_terrain (get_terrain rt) in
            if not (Terrain.get_physblock ter) then
               ((xcount + off_x), (ycount + off_y))
               :: next_open_xloc mb (xcount + 1) ycount off_x off_y
            else next_open_xloc mb (xcount + 1) ycount off_x off_y
   else []
;;

let rec list_open_loc mb off_x off_y =
   next_open_yloc mb 0 off_x off_y
and next_open_yloc mb ycount off_x off_y =
   if ycount < mb.blocksize_y then
      next_open_xloc mb 0 ycount off_x off_y 
      @ (next_open_yloc mb (ycount + 1) off_x off_y)
   else []
;;

let random_open_loc mb =
   let offp_x, offp_y = get_offset mb in
   let off_x, off_y = 
      (offp_x * mb.blocksize_x), (offp_y * mb.blocksize_y) 
   in
   let openlist = list_open_loc mb off_x off_y in
   if not (openlist = []) then
      let choice_range = List.length openlist in
         let choice = Algo.mersenne choice_range in
            List.nth openlist choice
   else
   (
      print_string "no open tiles in mapblock";
      raise Not_found
   )
;;

let random_loc mb =
   let (ox, oy) = get_offset mb in
   let tps_x = mb.blocksize_x in
   let tps_y = mb.blocksize_y in
      ( (tps_x * ox) + (Algo.mersenne (tps_x - 1)),
       (tps_y * oy) + (Algo.mersenne (tps_y - 1))
      )
;;

let gain_item mb itm =
   Item.add_item itm mb.item
;;

let lose_item mb itm =
   Item.remove_item itm mb.item
;;
