(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    mapplane.ml

*  What does it do?

**    Provides types and functions for coarse (block-level) map 
operations.

*)

type feature =
 {
   variety : Feature.feature_variety;
   orientation : Algo.cardinal_dir;
   from_x : int;
   from_y : int;
   to_x : int;
   to_y : int;
   extra_x1 : int;
   extra_y1 : int;
   extra_x2 : int;
   extra_y2 : int;
 }
;;

let get_feature_variety f = f.variety;;
let get_feature_x1 f = f.from_x;;
let get_feature_y1 f = f.from_y;;
let get_feature_x2 f = f.to_x;;
let get_feature_y2 f = f.to_y;;

let get_from_x f = f.from_x;;
let get_from_y f = f.from_y;;
let get_to_x f = f.to_x;;
let get_to_y f = f.to_y;;
let get_extra_x f = f.extra_x1;;
let get_extra_y f = f.extra_y1;;
let get_extra_x2 f = f.extra_x2;;
let get_extra_y2 f = f.extra_y2;;

type wtile_what =
   Void
 | Forest
 | Grass
 | Sea
 | Sand
 | Solid_hedge
 | Dungeon
 | Banishment_dungeon
 | Perfect_maze
 | Shop_maze
 | Ruin_grassy
 | Special
;;

let string_of_wtile_terrain wtw =
   match wtw with
      Void -> "void"
    | Grass -> "grass"
    | Forest -> "forest"
    | Sea -> "sea"
    | Sand -> "pebble beach"
    | Solid_hedge -> "dense hedges"
    | Dungeon -> "dungeon"
    | Banishment_dungeon -> "dungeon of banishment"
    | Perfect_maze -> "maze"
    | Shop_maze -> "shopping complex"
    | Ruin_grassy -> "grassy ruins"
    | Special -> "special"
;;

type worldtile =
   {
      mutable loaded:bool;
      mutable built:bool;
      mutable t_rain : wtile_what;
      mutable content: Mapblock.map_block;
      mutable features : feature list;
   }
;;

let wtile =
   {
      loaded = false;
      built = false;
      t_rain = Void;
      content = Mapblock.mblock;
      features = [];
   }
;;

type gridplane =
   {
      mutable psize : int;
      mutable blocksize_x : int;
      mutable blocksize_y : int;
      mutable bigsize : int;
      mutable totalbound_x : int;
      mutable totalbound_y : int;
      mutable filenames : string array array;
      mutable overgrid : worldtile array array;
      mutable fname : string;
      mutable pname : string;
      mutable is_big : bool;
      mutable fallback_startpos : (int * int);
      mutable lightlevel : int;
      reference : W_plane.plane_reference;
      p_variety : W_plane.plane_variety
   }
;;

let gplane =
   {
      psize = 1;
      blocksize_x = 1;
      blocksize_y = 1;
      bigsize = 3;
      totalbound_x = 1;
      totalbound_y = 1;
      filenames = Array.make_matrix 1 1 "";
      overgrid = Array.make_matrix 1 1 wtile;
      fname = "error.sav";
      pname = "error";
      is_big = false;
      fallback_startpos = (1, 1);
      lightlevel = 8;
      reference = (W_plane.new_worldmap 1);
      p_variety = W_plane.Dungeon
   }
;;

let count_loaded gridp =
   let count = ref 0 in
   for countx = 0 to (gridp.psize - 1) do
      for county = 0 to (gridp.psize - 1) do
         if gridp.overgrid.(countx).(county).loaded then
            count := !count + 1
      done
   done;
   !count
;;

let new_road lx1 ly1 lx2 ly2 =
   {
      variety = Feature.Road;
      orientation = Algo.Centre;
      from_x = lx1;
      from_y = ly1;
      to_x = lx2;
      to_y = ly2;
      extra_x1 = -1;
      extra_y1 = -1;
      extra_x2 = -1;
      extra_y2 = -1;
   }
;;

let new_feature lx1 ly1 lx2 ly2 orient fv =
   {
      variety = fv;
      orientation = orient;
      from_x = lx1;
      from_y = ly1;
      to_x = lx2;
      to_y = ly2;
      extra_x1 = -1;
      extra_y1 = -1;
      extra_x2 = -1;
      extra_y2 = -1;
   }
;;

let new_three_point_feature fv orient lx1 ly1 lx2 ly2 lx3 ly3 =
   {
      variety = fv;
      orientation = orient;
      from_x = lx1;
      from_y = ly1;
      to_x = lx2;
      to_y = ly2;
      extra_x1 = lx3;
      extra_y1 = ly3;
      extra_x2 = -1;
      extra_y2 = -1;
   }
;;

let new_four_point_feature fv orient lx1 ly1 lx2 ly2 lx3 ly3 lx4 ly4 =
   {
      variety = fv;
      orientation = orient;
      from_x = lx1;
      from_y = ly1;
      to_x = lx2;
      to_y = ly2;
      extra_x1 = lx3;
      extra_y1 = ly3;
      extra_x2 = lx4;
      extra_y2 = ly4
   }
;;

(*
let new_start_road lx1 ly1 lx2 ly2 =
   {
      variety = Startroad;
      x1 = lx1;
      y1 = ly1;
      x2 = lx2;
      y2 = ly2
   }
;;
*)

(*
let new_river lx1 ly1 lx2 ly2 =
   {
      variety = River;
      x1 = lx1;
      y1 = ly1;
      x2 = lx2;
      y2 = ly2
   }
;;
*)

let add_feature wt feat =
   wt.features <- feat :: wt.features
;;

let get_features wt = wt.features;;

let rec has_oriented_feature_variety wt fv orient =
   test_for_oriented_feature_variety wt.features fv orient
and test_for_oriented_feature_variety fl fv orient =
   match fl with
      [] -> false
    | head :: tail ->
         if (head.variety = fv) 
         && (head.orientation = orient)
            then true
         else test_for_oriented_feature_variety tail fv orient
;;

let rec has_feature_variety wt fv =
   test_for_feature_variety wt.features fv
and test_for_feature_variety fl fv =
   match fl with
      [] -> false
    | head :: tail ->
         if head.variety = fv then true
         else test_for_feature_variety tail fv
;;

let new_filename cname fid =
   cname ^ "." ^ (string_of_int (Uaid.new_aid fid) )
;;

let set_nplane plane bsx bsy =
   plane.blocksize_x <- bsx;
   plane.blocksize_y <- bsy;
   plane.totalbound_x <- (((plane.psize) * bsx) - 1);
   plane.totalbound_y <- (((plane.psize) * bsy) - 1);
   plane.filenames <- (Array.make_matrix plane.psize plane.psize "");
   plane.overgrid <- (Array.make_matrix plane.psize plane.psize wtile)
;;

let new_plane planename ps bsx bsy p_ref =
   let np = {gplane with pname = planename ; psize = ps; 
             blocksize_x = bsx ; blocksize_y = bsy;
             reference = p_ref ;
             p_variety = (W_plane.get_variety p_ref)
            } 
   in
      set_nplane np bsx bsy;
      np
;;

let init_gridplane gridp cname fid =
   gridp.fname <- new_filename cname fid;
   if gridp.psize > gridp.bigsize then gridp.is_big <- true;
   for county = 0 to (gridp.psize - 1) do
      for countx = 0 to (gridp.psize - 1) do
         gridp.overgrid.(countx).(county) <- 
            {wtile with t_rain = Grass;}
      done
   done
;;

let complete_itemlist gridp =
   let reslt = ref [] in
   for county = 0 to (gridp.psize - 1) do
      for countx = 0 to (gridp.psize - 1) do
         reslt := (Item.get_list 
                     (Mapblock.get_item
                      (gridp.overgrid.(countx).(county).content)
                     )
                  ) 
                  @ !reslt
      done
   done;
   !reslt
;;

let complete_personlist gridp =
   let reslt = ref [] in
   for county = 0 to (gridp.psize - 1) do
      for countx = 0 to (gridp.psize - 1) do
         if (gridp.overgrid.(countx).(county).loaded) then
            reslt := (Person.get_list 
                        (Mapblock.get_person
                         (gridp.overgrid.(countx).(county).content)
                        )
                     ) 
                     @ !reslt
      done
   done;
   !reslt
;;

let grid_get_wtile gridp x y = 
   if x < gridp.psize && x >= 0 && y < gridp.psize && y >= 0
      then gridp.overgrid.(x).(y)
   else (
(*
         print_string ("wtile error"
         ^ (string_of_int x) ^ " " ^ (string_of_int y) ^ "   .") 
         ;
         print_string ("\nMax: " ^ (string_of_int gridp.psize) ^ "\n")
         ;
*)
         wtile
        )
;;

let grid_set_wtile gridp x y wt =
   if x >= gridp.psize || x < 0 || y >= gridp.psize || y < 0 then
      raise Not_found
   else
      gridp.overgrid.(x).(y) <- wt
;;

let get_blocksize_x wt =
      Mapblock.get_blocksize_x wt.content
;;

let get_blocksize_y wt =
      Mapblock.get_blocksize_y wt.content
;;

let set_wtile_content wt v = wt.content <- v;;

let save_grid gridp x y =
   let wt = (grid_get_wtile gridp x y) in
      if wt.built && wt.loaded then
         (
          Mapblock.prepare_for_save wt.content;
          let filename = gridp.filenames.(x).(y) in
            let ochan = open_out_bin filename in
               Marshal.to_channel ochan wt.content
                  [];
               close_out ochan
         )
;;

let save_all gridp =
   for county = 0 to (gridp.psize - 1) do
      for countx = 0 to (gridp.psize - 1) do
         save_grid gridp countx county
      done
   done
;;

let save_and_destroy gridp x y =
   save_grid gridp x y;
   let wt = grid_get_wtile gridp x y in
(*
      {wt with 
         content = Mapblock.mblock;
         loaded = false
      }
*)
      wt.content <- Mapblock.mblock;
      wt.loaded <- false
;;

let load_grid gridp x y cname fid iid 
=
   let wt = grid_get_wtile gridp x y in
      if wt.built && (not (gridp.filenames.(x).(y) = "")) then
         (let ichan = open_in_bin gridp.filenames.(x).(y) in
            wt.content <- Marshal.from_channel ichan;
            close_in ichan;
            wt.loaded <- true;
         )
      else if not (wt == wtile) then
         (   (*(*DEBUG*) print_string "building...\n";*)
          raise Not_found
         )
;;

let totalinbound gridp x y =
   if x <= gridp.totalbound_x 
      && x >= 0 
      && y <= gridp.totalbound_y 
      && y >= 0
   then true
   else false
;;

let which_seg cplane x y =
   ((x / cplane.blocksize_x), (y / cplane.blocksize_y))
;;

let does_cross_boundary cplane x1 y1 x2 y2 =
(*   not ((which_seg cplane x1 y1) == (which_seg cplane x2 y2))*)
(*
<Abbreviation of>
*)
   if (which_seg cplane x1 y1) = (which_seg cplane x2 y2) then
      false
   else true
;;

let grid_get_mb gridp x y =
try
   if totalinbound gridp x y then
      let wt = grid_get_wtile gridp (x / gridp.blocksize_x) 
                                    (y / gridp.blocksize_y) in
         if wt.built && wt.loaded then
            wt
         else wtile
   else wtile
with Invalid_argument "Array.get" -> (print_string "bound error\n";
                                      wtile )
;;

let grid_get_mbcontent gridp x y = (grid_get_mb gridp x y).content;;

let pickup_person cplane pson lx ly =
   let mb = (grid_get_mb cplane lx ly).content in
      let plist = Mapblock.get_person mb in
         Person.remove_person pson plist;
;;

let person_to_build pson wt =
   print_string "MeeP"
;;

let put_actor cplane pson lx ly =
   let wt = (grid_get_mb cplane lx ly) in
   let mb = wt.content in
      if wt.loaded then
         let plist = Mapblock.get_person mb in
            (Person.add_person pson plist;
            )
      else person_to_build pson wt
;;

let put_person cplane pson =
   let lx = Person.get_locx pson in
   let ly = Person.get_locy pson in
   let mb = (grid_get_mb cplane lx ly).content in
   let plist = Mapblock.get_person mb in
      Person.add_person pson plist
;;

let grid_get_rtile gridp (x:int) (y:int) =
try
   if totalinbound gridp x y then
      let wt = grid_get_wtile gridp (x / gridp.blocksize_x) 
                                    (y / gridp.blocksize_y) in
         if wt.built
         && wt.loaded then
            Mapblock.get_mb_rtile wt.content (x mod gridp.blocksize_x) 
                                             (y mod gridp.blocksize_y)
         else Mapblock.rtile
   else Mapblock.rtile
with Invalid_argument "Array.get" -> 
   (print_string "bound error\n";
    Mapblock.rtile
   )
;;

let build_spiral_seg x y direction length =
   let rslt = ref [] in
      for count = 0 to length do
         let newx, newy =
            match direction with
               Algo.North ->
                  (x, (y - count))
             | Algo.West ->
                  ((x - count), y)
             | Algo.South ->
                  (x, (y + count))
             | _ (* Algo.East *) ->
                  ((x + count), y)
         in
            rslt := !rslt @ [(newx, newy)]
      done
   ; !rslt
;;

(*
 * The spiral_seg function returns a list of tuples, being the offsets
 * for the nth line of cells in a square spiral path.
 *)

let spiral_seg n =
   let ring = (n / 4) + 1 in
      let x, y, direction =
         match (n mod 4) with
            0 ->
               ( - ring), ( - ring), Algo.East
          | 1 ->
               (ring), ( - ring), Algo.South
          | 2 ->
               (ring), (ring), Algo.West
          | _ ->
               ( - ring), (ring), Algo.North
      in
         build_spiral_seg x y direction ring
;;

let rec segfind_opentile plane x y seg =
   match seg with
      [] -> (-1, -1)
    | (off_x, off_y) :: tail ->
         (
            let cx, cy = (x + off_x), (y + off_y) in
               let rt = grid_get_rtile plane cx cy in
               let ter = Terrain.sample_terrain
                  (Mapblock.get_terrain rt) 
               in
                  if Terrain.get_physblock ter then
                     segfind_opentile plane x y tail
                  else (cx, cy) 
         )
;;

let wtile_of_rtile gridp (x:int) (y:int) =
try
   if totalinbound gridp x y then
      grid_get_wtile gridp 
         (x / gridp.blocksize_x) (y / gridp.blocksize_y)
   else wtile
with Invalid_argument "Array.get" -> 
   (print_string "wt bound error\n";
    wtile
   )
;;

let lower_bound_x plane pv =
   (pv * plane.blocksize_x) + 1
;;

let upper_bound_x plane pv =
   ((pv + 1) * plane.blocksize_x) - 1
;;

let lower_bound_y plane pv =
   (pv * plane.blocksize_y) + 1
;;

let upper_bound_y plane pv =
   ((pv + 1) * plane.blocksize_y) - 1
;;

let vis_symbol rtile = Mapblock.vis_symbol rtile;;

let vis_colour rtile = Mapblock.vis_colour rtile;;

let vis_time rtile = Mapblock.vis_time rtile;;

let grid_get_terrain (gridp:gridplane) (x:int) (y:int) =
   let rt = grid_get_rtile gridp x y in
      Mapblock.get_terrain rt
;;

let get_pname plane = plane.pname;;

let get_plane_variety p = p.p_variety;;

let get_planesize plane = plane.psize;;

let get_planefilename plane = plane.fname;;

let get_pblocksize_x plane = plane.blocksize_x;;

let get_pblocksize_y plane = plane.blocksize_y;;

let get_tp_size_x plane = plane.psize * plane.blocksize_x;;

let get_tp_size_y plane = plane.psize * plane.blocksize_y;;

let is_big plane = plane.is_big;;

let get_lightlevel plane = plane.lightlevel;;

let get_p_ref plane = plane.reference;;

let get_wt_content wt = wt.content;;

let set_wt_built wt v = wt.built <- v;;

let set_wt_loaded wt v = wt.loaded <- v;;

let set_filename gridp x y fname = gridp.filenames.(x).(y) <- fname;;

let set_lightlevel plane v = plane.lightlevel <- v;;

let get_filename gridp x y = gridp.filenames.(x).(y);;

let get_wt_built wt = wt.built;;

let random_bounded_loc plane =
   let tpsx = get_tp_size_x plane in
   let tpsy = get_tp_size_y plane in
      ((Algo.mersenne (tpsx - 3) + 1), (Algo.mersenne (tpsy - 3) + 1))
;;

let middle_loc plane =
   let tpsx = get_tp_size_x plane in
   let tpsy = get_tp_size_y plane in
      ((tpsx / 2), (tpsy / 2))
;;

let set_wt_terrain wt v = wt.t_rain <- v;;

let get_wt_terrain wt = wt.t_rain;;

let delete_plane p =
   for shellc = 0 to (Array.length p.filenames - 1) do
      let namelist = p.filenames.(shellc) in
      for namecount = 0 to (Array.length namelist - 1) do
         try
            Sys.remove (namelist.(namecount))
         with
            Sys_error(": No such file or directory") -> ()
          | _ -> ()
      done
   done;
   try
      Sys.remove p.fname
   with
      Sys_error(": No such file or directory") -> ()
    | _ -> ()
;;

let get_totalbound_x p = p.totalbound_x;;

let get_totalbound_y p = p.totalbound_y;;

let random_tile p =
   let tbx = p.totalbound_x - 1 in
   let tby = p.totalbound_y - 1 in
      let lx = (Algo.mersenne tbx) + 1 in
      let ly = (Algo.mersenne tby) + 1 in
         lx, ly
;;

let is_blocked plane cx cy =
   let rt = grid_get_rtile plane cx cy in
      let ter = Terrain.sample_terrain (Mapblock.get_terrain rt) in
         if Terrain.get_physblock ter then
            false
         else true
;;

let rec randomfind_opentile plane x y =
   next_opentile plane x y 0
and next_opentile plane x y count =
   if count > (plane.totalbound_x * plane.totalbound_y * 2) then 
      (print_string "Mapplane.randomfind_opetile failed"; x, y)
   else
      let lx, ly = random_tile plane in
         if (is_blocked plane lx ly) then
            next_opentile plane lx ly (count + 1)
         else lx, ly
;;

(*
let rec spiralfind_opentile plane x y limit =
   next_spiral_step_opentile plane x y 1 limit 0 (Algo.mersenne 6) x y
and next_spiral_step_opentile plane x y count limit countsucc csl tx ty
=
   if count < limit then
      let ox, oy = Algo.spiral_nth count in
         let lx, ly = (ox + x), (oy + y) in
            let rt = grid_get_rtile plane lx ly in
               let ter = Terrain.sample_terrain
                  (Mapblock.get_terrain rt) 
               in
                  if not (Terrain.get_physblock ter) then
                  (
                     if countsucc < csl then
                        next_spiral_step_opentile plane x y 
                        (count + 1) limit (countsucc + 1) csl lx ly
                     else lx, ly
                  )
                  else next_spiral_step_opentile plane x y
                        (count + 1) limit countsucc csl tx ty
   else tx, ty
;;
*)

let get_bigsize gridp =
   gridp.bigsize
;;

let get_reference gridp = gridp.reference;;
