(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    mapstruc.ml

*  What does it do?

**    Provides plane-scale map operations; map_api is above mapstruc in 
the hierarchy, but mapstruc does some of the top-level operations also, 
and map_api wraps them. There are some... legacies, where modules higher 
in the hierarchy call mapstruc directly instead of going through 
map_api.

It's a bit messy. Cleaning is a work in progress.

*)


type safe_bound =
 {
   mutable bx : int;
   mutable by : int;
   mutable ex : int;
   mutable ey : int
 }
;;

let proto_safe_bound = 
 {
   bx = 0;
   by = 0;
   ex = 2;
   ey = 2;
 }
;;

(*# 
  # Planes could either be referenced as associative lists, in which
  # the planes may or may not be loaded at any one time, or there may
  # simply be one "currently loaded" plane. The latter is more
  # streamlined and memory-efficient, and I don't intend on having more
  # than one plane loaded at a time at this stage. I guess I should go
  # with that. The type definition includes the following in place of
  # cplane
  # if the other option is to be pursued:
      planelist : (string * Mapplane.gridplane) list;
  #*)

type mapstruc =
   {
      mutable planeref : W_plane.plane_reference list;
      mutable fnamelist : (string * string) list;
      mutable cplane : Mapplane.gridplane;
      mutable cname : string;
      mutable ownfilename : string;
      mutable dirty : (int * int) list;
      mutable portal : Stair.portal list;
      mutable link : W_plane.plane_link;
      safe_boundary : safe_bound
   }
;;

let m_struct =
   {
(*      terraintype = [tvoid]; *)
      planeref = [ (W_plane.new_worldmap 1) ];
      fnamelist = [ ("0" , "error.sav") ];
      cplane = Mapplane.new_plane "error" 1 1 1 
            (W_plane.new_worldmap 1);
      cname = "";
      ownfilename = "error.sav";
      dirty = ([] : (int * int) list);
      portal = ([] : Stair.portal list);
      link = W_plane.proto_link;
(*      built_plane = [(W_plane.worldmap, true)];*)
      safe_boundary = proto_safe_bound;
   }
;;

let plist ms = Mapplane.complete_personlist ms.cplane;;

let person_of_id ms id =
  let reslt = ref Person.proto_person in
  (
   let plist = Mapplane.complete_personlist ms.cplane in
      for count = 0 to (List.length plist - 1) do
         let pson = List.nth plist count in
            if Person.get_id pson = id then reslt := pson
      done;
(*
      if (!reslt = Person.proto_person) && (not (id = 0)) then 
         (
          print_string (string_of_int id);
          raise Not_found
         )
      else
*)
      !reslt
  )
;;

let unbuilt_plane_reference ms pre_plane =
   if not (List.mem pre_plane ms.planeref) then
      ms.planeref <- pre_plane :: ms.planeref
;;

let plane_exist ms pre_plane =
   let reslt = ref (-1) in
   for count = 0 to (List.length ms.planeref - 1) do
      let epref = List.nth ms.planeref count in
         if epref = pre_plane then reslt := count
   done;
   !reslt
;;

let get_portal ms = ms.portal;;

let set_portal ms portal = ms.portal <- portal;;

let rec find_equiv_stair ms stair =
   next_check_stair ms.portal stair
and next_check_stair lst stair =
   match lst with
      [] -> raise Not_found
    | head :: tail ->
         if head = stair then head else next_check_stair tail stair
;;

let is_safe ms x y =
   if x < ms.safe_boundary.bx then false
   else if x > ms.safe_boundary.ex then false
   else if y < ms.safe_boundary.by then false
   else if y > ms.safe_boundary.ey then false
   else true
;;

let abs_lower_x plane cpv =
   if cpv < 1 then 0
   else (Mapplane.lower_bound_x plane cpv)
;;

let abs_upper_x plane cpv psize tpsize =
   if cpv >= psize then (tpsize)
   else (Mapplane.upper_bound_x plane cpv)
;;

let abs_lower_y plane cpv =
   if cpv < 1 then 0
   else (Mapplane.lower_bound_y plane cpv)
;;

let abs_upper_y plane cpv psize tpsize =
   if cpv >= psize then (tpsize)
   else (Mapplane.upper_bound_y plane cpv)
;;

let safe_lower_boundary_x plane cpv bsize =
   if cpv < 1 then 0
   else (Mapplane.lower_bound_x plane cpv) + (bsize / 2)
;;

let safe_upper_boundary_x plane cpv psize tpsize bsize =
   if cpv >= psize then tpsize
   else (Mapplane.upper_bound_x plane cpv) - (bsize / 2)
;;

let safe_lower_boundary_y plane cpv bsize =
   if cpv < 1 then 0
   else (Mapplane.lower_bound_y plane cpv) + (bsize / 2)
;;

let safe_upper_boundary_y plane cpv psize tpsize bsize =
   if cpv >= psize then tpsize
   else (Mapplane.upper_bound_y plane cpv) - (bsize / 2)
;;

(* Note that for the safe_boundary to work, big_size must be odd;
half_big, which is bigsize / 2, will discard the remainder. That 
remainder represents the block in which the PC resides.
*)

let set_safe_boundary ms pc =
   let plane = ms.cplane in 
   let tpsx = Mapplane.get_tp_size_x plane in
   let tpsy = Mapplane.get_tp_size_y plane in
   if Mapplane.is_big plane then
      let half_big = (Mapplane.get_bigsize plane) / 2 in
      let lx = Person.get_locx pc in
      let ly = Person.get_locy pc in
      let (px,py) = Mapplane.which_seg plane lx ly in
      let bsx = Mapplane.get_pblocksize_x plane in
      let bsy = Mapplane.get_pblocksize_y plane in
      let ps = Mapplane.get_planesize plane in
        (
         ms.safe_boundary.bx <- 
            safe_lower_boundary_x plane (px - half_big) bsx;
         ms.safe_boundary.by <- 
            safe_lower_boundary_y plane (py - half_big) bsy;
         ms.safe_boundary.ex <- 
            safe_upper_boundary_x plane (px + half_big) ps tpsx bsx;
         ms.safe_boundary.ey <- 
            safe_upper_boundary_y plane (py + half_big) ps tpsy bsy
        )
   else 
      (
       ms.safe_boundary.bx <- 0;
       ms.safe_boundary.by <- 0;
       ms.safe_boundary.ex <- tpsx;
       ms.safe_boundary.ey <- tpsy
      )      
;;

let dirty_tile mstruct tup = mstruct.dirty <- (tup :: mstruct.dirty);;

let get_dirty mstruct = mstruct.dirty;;

let clear_all_dirty mstruct = mstruct.dirty <- [];;

let clean_dirty mstruct x y =
   let newlist = ref [] in
   for count = 0 to (List.length mstruct.dirty - 1) do
      let crd = List.nth mstruct.dirty count in
         if crd = (x, y) then ()
      else newlist := crd :: !newlist
   done;
   mstruct.dirty <- !newlist
;;

let ms_getblock ms x y =
   let map_plane = ms.cplane in
      Mapplane.grid_get_wtile map_plane x y
;;

let get_cplane ms = ms.cplane
(*   if not (ms.cplane == Mapplane.gplane) then ms.cplane *)
(*   else raise Not_found *)
;;

let get_total_cpsize_x ms = Mapplane.get_tp_size_x ms.cplane;;

let get_total_cpsize_y ms = Mapplane.get_tp_size_y ms.cplane;;

let blocksize_x ms p x y = 
   let wt = ms_getblock ms x y in
      Mapplane.get_blocksize_x wt
;;

let blocksize_y ms p x y = 
   let wt = ms_getblock ms x y in
      Mapplane.get_blocksize_y wt
;;

let get_rtile ms (x:int) (y:int) =
   let plane = ms.cplane in
      Mapplane.grid_get_rtile plane x y
;;

let vis_symbol rtile = Mapplane.vis_symbol rtile;;

let vis_colour rtile = Mapplane.vis_colour rtile;;

let vis_time rtile = Mapplane.vis_time rtile;;

let get_terrain ms x y =
   let tv =
      Mapplane.grid_get_terrain ms.cplane x y
   in
      Terrain.sample_terrain tv
;;

let get_symb_col ms p x y =
(*
   let plane = ms.cplane in
   let terrain = Mapplane.grid_get_terrain plane x y in
   let ter = List.nth ms.terraintype terrain in
*)
   let ter = get_terrain ms x y in
      (Terrain.get_symbol ter, Terrain.get_colour ter)
;;

let get_scb_stat ms x y =
   let plane = ms.cplane in
   let rtile = Mapplane.grid_get_rtile ms.cplane x y in
   let ter_v = Mapplane.grid_get_terrain plane x y in
   let ter = Terrain.sample_terrain ter_v in
      ((Terrain.get_symbol ter),
       (Terrain.get_colour ter),
       (Terrain.get_physblock ter),
       (Terrain.get_losblock ter),
       (Mapblock.get_blockname rtile)
      )
;;

let stair_on_plane_loc ms cplane lx ly =
   let reslt = ref Stair.proto_portal in
      for count = 0 to ((List.length ms.portal) - 1) do
         let portl = List.nth ms.portal count in
            if Stair.get_from_plane portl = 
              (Mapplane.get_p_ref cplane)
            then
            if (Stair.get_lx portl = lx)
            && (Stair.get_ly portl = ly) then
               reslt := portl
      done;
   if !reslt == Stair.proto_portal then raise Not_found
(* DEBUG
      (print_string ("\nno portal here - " ^ (string_of_int lx) 
             ^ ", " ^ (string_of_int ly))
       ; raise Not_found)
/ DEBUG *)

   ;
   !reslt
;;

let stair_on_loc ms x y =
   let cplane = get_cplane ms in
      try
         stair_on_plane_loc ms cplane x y
      with Not_found -> 
(* DEBUG
         print_string "failed through stair_on_loc";
/DEBUG *)
         raise Not_found
;;

let downstair_on_loc ms x y =
   let reslt = ref false in
      for count = 0 to ((List.length ms.portal) - 1) do
         let portl = List.nth ms.portal count in
            if Stair.get_from_plane portl = 
               (Mapplane.get_p_ref (get_cplane ms))
            then
            if Stair.leads_down portl 
            && (Stair.get_lx portl = x)
            && (Stair.get_ly portl = y) then
               reslt := true
      done;
   !reslt
;;

let upstair_on_loc ms x y =
   let reslt = ref false in
      for count = 0 to ((List.length ms.portal) - 1) do
         let portl = List.nth ms.portal count in
            if Stair.get_from_plane portl = 
              (Mapplane.get_p_ref (get_cplane ms))
            then
            if not (Stair.leads_down portl) then
            if (Stair.get_lx portl = x)
            && (Stair.get_ly portl = y) then
               reslt := true
      done;
   !reslt
;;

(*
let stairs_on_plane plane portal =
   let rslt = ref [] in
   for count = 0 to (List.length portal - 1) do
      let cport = List.nth portal count in
         if Stair.get_from_plane cport = Mapplane.get_p_ref plane then
            rslt := cport :: !rslt
   done;
   !rslt
;;
*)

let rec stairs_on_plane plane portal =
   match portal with
      [] -> []
    | head :: tail ->
         if Stair.get_from_plane head = Mapplane.get_p_ref plane then
            head :: stairs_on_plane plane tail
         else stairs_on_plane plane tail
;;

let rec first_downstair_on_plane plane portal =
   let portals = stairs_on_plane plane portal in
      next_dstair_on_plane plane portals
and next_dstair_on_plane plane pl =
   match pl with
      [] -> raise Not_found
    | head :: tail ->
         if Stair.leads_down head then head
         else next_dstair_on_plane plane tail
;;

let add_portal ms portal =
   ms.portal <- portal :: ms.portal
;;

let add_new_plane ms pname psize bsize_x bsize_y w_map fid =
   let np = Mapplane.new_plane pname psize bsize_x bsize_y w_map in
(*      Mapplane.set_nplane np bsize; *)
      Mapplane.init_gridplane np ms.cname fid;
      if not (List.mem w_map ms.planeref) then
         ms.planeref <- w_map :: ms.planeref;
      ms.fnamelist <- 
         (pname , (Mapplane.get_planefilename np)) :: ms.fnamelist;
   np
;;

let prep_new_plane ms w_map fid =
   let pname = W_plane.planename w_map in
   let psize = W_plane.get_size w_map in
   let bsizex = W_plane.get_bsize_x w_map in
   let bsizey = W_plane.get_bsize_y w_map in
      add_new_plane ms pname psize bsizex bsizey w_map fid
;;

let new_map_structure obsizex obsizey w_map =
   let pname = W_plane.planename w_map in
   let opsize = W_plane.get_size w_map in
   let ms = 
      {m_struct with
         planeref = [ w_map ];
         cplane = Mapplane.new_plane pname opsize obsizex obsizey w_map;
      }
   in
(*      add_stairs ms ms.cplane; *)
      ms
;;

let ms_plane ms p =
   if Mapplane.get_pname ms.cplane = p then 
      ms.cplane 
   else m_struct.cplane
;;

let prep_save_plane sp =
      for cy = 0 to ((Mapplane.get_planesize sp) - 1) do
         for cx = 0 to ((Mapplane.get_planesize sp) - 1) do
            Mapplane.save_and_destroy sp cx cy
         done
      done
;;

let save_plane ms =
   (
      prep_save_plane ms.cplane;
      let filename = Mapplane.get_planefilename ms.cplane in
         let ochan = open_out_bin filename in
            (
               Marshal.to_channel ochan ms.cplane [];
               close_out ochan
            )
   )
;;

let save_any_plane sp =
      (prep_save_plane sp;
       let filename = Mapplane.get_planefilename sp in
          let ochan = open_out_bin filename in
             (Marshal.to_channel ochan sp [];
             close_out ochan)
      );
;;

let fill_load_segs ms bx by width height cname fid iid =
   let unbuilt = ref [] in
   for cy = by to (by + (height - 1)) do
      for cx = bx to (bx + (width - 1)) do
        try
        (
(* DEBUG
         print_string ((string_of_int cx) ^ ", " ^
            (string_of_int cy) ^ "\n");
/DEBUG *)
         Mapplane.load_grid ms.cplane cx cy cname fid iid
        )
        with Not_found ->
            unbuilt := (cx, cy) :: !unbuilt
      done
   done;
   !unbuilt
;;

let which_segs_to_load ms p pc =
   if Mapplane.is_big ms.cplane then
      let bigsize = (Mapplane.get_bigsize ms.cplane) in
      let planesize = Mapplane.get_planesize ms.cplane in
      let half_big = bigsize / 2 in
      let (sx,sy) =
         Mapplane.which_seg ms.cplane 
          (Person.get_locx pc) (Person.get_locy pc)
      in 
      let (fsx,fsy) =
         (max 0 (min (sx - half_big) 
                 (planesize - (bigsize))
                ),
          max 0 (min (sy - half_big) 
                 (planesize - (bigsize))
                )
         ) 
      in
         (fsx , fsy , bigsize , bigsize)
   else (0, 0, ((Mapplane.get_planesize ms.cplane) ),
               ((Mapplane.get_planesize ms.cplane) )
        )
;;

let load_plane_seg ms p pc cname fid iid =
   let filename = List.assoc p ms.fnamelist in
      let ichan = open_in_bin filename in
         ms.cplane <- Marshal.from_channel ichan;
         close_in ichan;
         let (bx, by, width, height) = which_segs_to_load ms p pc in
            fill_load_segs ms bx by width height cname fid iid
;;

let mapstruc_init ms plane bsizex bsizey cnm fid link =
   ms.cname <- cnm;
   ms.ownfilename <- (Mapplane.new_filename cnm fid);
   ms.link <- link;
(*   terrain_init ms; *)
   let omap_plane = ms.cplane in
   (
      Mapplane.set_nplane omap_plane bsizex bsizey;
      Mapplane.init_gridplane omap_plane cnm fid;
      ms.fnamelist <- 
         [ (plane , (Mapplane.get_planefilename omap_plane)) ]
   )
;;

let save_mapstruc ms =
   save_plane ms;
   let filename = ms.ownfilename in
      let ochan = open_out_bin filename in
         (Marshal.to_channel ochan ms [];
         close_out ochan)
;;

let load_mapstruc cplanename filename pc cname fid iid =
   let ichan = open_in_bin filename in
      let ms = Marshal.from_channel ichan in
      (close_in ichan;
       let unbuilt =
          load_plane_seg ms cplanename pc cname fid iid
       in
         (
          set_safe_boundary ms pc;
          (ms, unbuilt)
         )
      )
;;

let get_msfilename ms = ms.ownfilename;;

type mhold =
 {
   iid : Uaid.uaid ref;
   fid : Uaid.uaid;
   mutable overmap : mapstruc
 }
;;

let proto_mhold =
 {
   iid = ref (Uaid.get_new_uaid (-1));
   fid = Uaid.get_new_uaid (-1);
   overmap = m_struct
 }
;;

let get_cname ms = ms.cname;;

let get_fid m = m.fid;;
let get_iid m = !(m.iid);;

let get_map m = m.overmap;;

let set_map m ms = m.overmap <- ms;;

let new_map w_map bsizex bsizey iid = 
   {proto_mhold with 
      overmap =
         (new_map_structure 
             bsizex bsizey w_map);
      iid = ref iid
   }

let get_safe_bx ms = ms.safe_boundary.bx ;;
let get_safe_by ms = ms.safe_boundary.by ;;
let get_safe_ex ms = ms.safe_boundary.ex ;;
let get_safe_ey ms = ms.safe_boundary.ey ;;

let get_planeref ms = ms.planeref;;

let get_link ms = ms.link;;

let set_links ms v = ms.link <- v;;

let current_mapname ms =
   let pre_plane = Mapplane.get_p_ref ms.cplane in
      W_plane.planename pre_plane
;;

let prepare_switch mhold pre_plane pc =
   let ms = mhold.overmap in
   let pname = W_plane.planename pre_plane in
      if (List.mem_assoc pname ms.fnamelist) then false
      else
         let newplane = prep_new_plane ms pre_plane mhold.fid in
            (
             ms.cplane <- newplane;
             save_any_plane newplane;
             true
            )
;;

(*
let switch_planes mhold pre_plane pc from_stair iid =
   save_mapstruc (get_map mhold);
   let built_new = prepare_switch mhold pre_plane pc in
   save_mapstruc (get_map mhold);
   Person.set_locx pc (Stair.get_to_lx from_stair);
   Person.set_locy pc (Stair.get_to_ly from_stair);
   let pname = W_plane.planename pre_plane in
   let ms = get_map mhold in
   let filename = ms.ownfilename in
      let (new_ms, unbuilt) =
         load_mapstruc pname filename pc ms.cname mhold.fid iid
      in
(*         save_mapstruc new_ms; *)
         (new_ms, unbuilt, built_new)
;;
*)

let people_on_loc plane lx ly =
   let mb = Mapplane.grid_get_mbcontent plane lx ly in
      let plist = Mapblock.get_person mb in
         Person.people_on_loc plist lx ly
;;

let delete_mapstruc mhold =
   let ms = get_map mhold in
   for count = 0 to (List.length ms.fnamelist - 1) do
      let (_, filename) = List.nth ms.fnamelist count in
         let ichan = open_in_bin filename in
            let p = Marshal.from_channel ichan in
               Mapplane.delete_plane p
   done;
   try
      Sys.remove ms.ownfilename
   with Sys_error(": No such file or directory") -> ()
    | _ -> ()
;;

let is_blocked ms lx ly =
   Mapplane.is_blocked ms.cplane lx ly
;;
