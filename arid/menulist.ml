(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    menulist.ml

*  What does it do?

**    UI for handling menus

*)


let rec real_count_to_item l item count =
   match l with
      [] -> raise Not_found
    | x :: l ->
         if x = item then count else
            real_count_to_item l item (count+1)
;;

let count_to_item l item =
   try 
      real_count_to_item l item 0
   with Not_found -> -1
;;

let rec real_remove_nth l n count =
   match l with
      [] -> []
    | elem :: l ->
         if count = n then l
         else elem :: real_remove_nth l n (count+1)
;;            

let remove_nth l n =
   if n > 0 then 
         real_remove_nth l n 0
   else l
;;

let rec real_insert_nth c l n count =
   match l with [] -> []
    | elem :: l ->
         if count = (n-1) then elem :: c :: l
         else elem :: real_insert_nth c l n (count + 1)
;;

let insert_nth (c:char) l n =
   if n > 0 then
      if n <= List.length l then
         real_insert_nth c l n 0
      else l
   else l
;;

class string_menu_item itemtype content =
   object
      val itemtype = (itemtype:string)
      val content = (content:string)
      val index = ref ' '
      val lines = ref 0
      val start_line = ref 0
      val page = ref 0
      val page_start = ref 0
      val selected = ref false
      method get_itemtype = itemtype
      method get_content = content
      method set_index c = index := c
      method get_index = !index
      method set_num_lines boundx =
         lines := (((String.length content) / boundx ) + 2)
      method set_start_line v = 
         start_line := v
      method get_start_line = !start_line
      method get_numlines = !lines
      method set_page_start v = page_start := v
      method get_page_start = !page_start
      method set_page v = page := v
      method get_page = !page
      method get_selected = !selected
      method set_selected v = selected := v
   end
;;

let strn_menu_item itemtype content =
   new string_menu_item itemtype content
;;

let indexed_strn_menu_item itemtype content index =
   let newitem = new string_menu_item itemtype content in
      (newitem#set_index index;
       newitem)
;;

class menulist =
   object (self)
      val avail_index = ref ['a' ; 'b' ; 'c' ; 'd' ; 'e' ; 'f' ; 'g' ;
                             'h' ; 'i' ; 'j' ; 'k' ; 'l' ; 'm' ; 'n' ;
                             'o' ; 'p' ; 'q' ; 'r' ; 's' ; 't' ; 'u'
                             ; 'v' ; 'w' ; 'x' ; 'y' ; 'z' ;
                             'A' ; 'B' ; 'C' ; 'D' ; 'E' ; 'F' ; 'G' ;
                             'H' ; 'I' ; 'J' ; 'K' ; 'L' ; 'M' ; 'N' ;
                             'O' ; 'P' ; 'Q' ; 'R' ; 'S' ; 'T' ; 'U' ;
                             'V' ; 'W' ; 'X' ; 'Y' ; 'Z']
      val menuindex = ref []
      val menuitem = ref []
      method add_avail_index i = 
         avail_index := List.concat [!avail_index ; [i]]
      method is_avail index =
         List.mem index !avail_index
      method get_avail_index =
         match !avail_index with
            [] -> ' '
          | elem :: l ->
               let value = elem in
                  avail_index := l;
                  value
      method add_item (item:string_menu_item) =
         if item#get_itemtype = "option" then
            let new_index = 
               if item#get_index = ' ' then
                  self#get_avail_index
               else
                 (
                  self#remove_avail_index item#get_index;
                  item#get_index
                 )
            in
               (menuindex := List.append !menuindex [new_index];
                menuitem := List.append !menuitem [item];
                item#set_index new_index
               )
         else let new_index = ' ' in
               (menuindex := List.append !menuindex [new_index];
                menuitem := List.append !menuitem [item])
(*
      method add_indexed_item item index =
         if item#get_itemtype = "option" then
            let new_index = self#get_avail_index in
               (menuindex := List.append !menuindex [new_index];
                menuitem := List.append !menuitem [item];
                item#set_index new_index)
         else let new_index = ' ' in
               (menuindex := List.append !menuindex [new_index];
                menuitem := List.append !menuitem [item])
*)
      method get_index = !menuindex
      method get_item (index:char) =
         let count = count_to_item !menuindex index in
            if count = -1 then
               strn_menu_item "heading" "Did not find item" 
            else
               (List.nth !menuitem count)
      method nth (count:int) =
         List.nth !menuitem count
      method nth_index (count:int) =
         List.nth !menuindex count
      method remove_avail_index (index:char) =
         let count = count_to_item !menuindex index in
             avail_index := remove_nth !avail_index count
      method remove_item (index:char) =
         let count = count_to_item !menuindex index in
            let item = List.nth !menuitem count in
            (self#add_avail_index (List.nth !menuindex count);
             menuindex := remove_nth !menuindex count;
             menuitem := remove_nth !menuitem count;
             item#set_index ' '
            );
            item
      method change_index (fromc:char) (toc:char) =
         if self#is_avail toc then
            (self#remove_avail_index toc;
            self#add_avail_index fromc;
            let count = count_to_item !menuindex fromc in
               menuindex := remove_nth !menuindex count;
               menuindex := insert_nth toc !menuindex count;
               (self#nth count)#set_index toc
            )
      method change_final_index toc =
         let lastitem = List.nth !menuitem (List.length !menuitem - 1)
         in
            let fromc = lastitem#get_index in
               self#change_index fromc toc
   end
;;

let new_menulist = new menulist;;

let make_indexed_menu (choices:string_menu_item list) =
   let tempmenu = new menulist in
      for count = 0 to (List.length choices) - 1 do
         tempmenu#add_item (List.nth choices count);
         tempmenu#change_final_index (List.nth choices count)#get_index
      done;
   tempmenu
;;

let make_menu (choices:string_menu_item list) =
   let tempmenu = new menulist in
      for count = 0 to (List.length choices) - 1 do
         tempmenu#add_item (List.nth choices count)
      done;
   tempmenu
;;

(*
let menu_of_assoc choices =
   let tempmenu = new menulist in
     if List.length choices = 0 then
        tempmenu#add_item (strn_menu_item)
     else
     (
      for count = 0 to (List.length choices) - 1 do
         let (index, content) = (List.nth choices count) in
            (tempmenu#add_item content;
             tempmenu#change_final_index index
            )
      done
     );
   tempmenu
;;
*)

let window_size menulist maxw maxh =
   let ydim = min ((2 * (List.length menulist#get_index) ) + 1) maxh in
      let xdim = maxw in
         (0, 0, xdim, ydim)
;;
