(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    message.ml

*  What does it do?

**    Provides message handling to arid.ml

*)


type msg_variety =
   Player
 | Hidden
 | Debug
;;

type msg =
 {
   mutable time : int;
   colour : U_colour.colour;
   target : msg_variety;
   mutable repeat : int;
   content : string;
   mutable seen : bool
 }
;;

type lang =
 | English
 | Fictional
;;

type msgbank =
 {
   mutable last_seen : int;
   mutable message : msg list;
   mutable language : lang
 }
;;

let proto_msg =
 {
   time = 0;
   colour = U_colour.conv_colour_s "grey";
   target = Hidden;
   repeat = 0;
   content = "";
   seen = false
 }
;;

let proto_msgbank =
 {
   last_seen = -1;
   message = [proto_msg];
   language = English
 }
;;

let count_sub_mlines subm width =
   ((String.length subm) / (width - 4) ) + 1
;;

let rec count_mlines msg width =
   real_count_mlines msg 0 width
and real_count_mlines strn counted width =
   try
      let nlpos = (String.index strn '\n') + 1 in
         let next_part = String.sub strn 
                                    (nlpos)
                                    ((String.length strn) - nlpos)
         in
          if String.length next_part > 0 then
            real_count_mlines 
               next_part
               (counted + count_sub_mlines (String.sub strn 0 nlpos)
                                           width
               )
               width
          else counted
   with Not_found -> counted + count_sub_mlines strn width
;;

let add_msg mbank mtime mtarget mcontent =
   let prev_msg = List.nth mbank.message (List.length mbank.message - 1)
   in
      if prev_msg.target = mtarget
      && prev_msg.content = mcontent && false then
         (prev_msg.repeat <- prev_msg.repeat + 1;
          prev_msg.time <- mtime;
          prev_msg.seen <- false
         )
      else
         let new_msg =
            {proto_msg with
                time = mtime;
                colour = U_colour.conv_colour_s "grey";
                target = mtarget;
                repeat = 0;
                content = mcontent
            }
         in 
         if not (mtarget = Hidden) then
            mbank.message <- mbank.message @ [new_msg]
;;

let display_message ui uiopt msg time is_seen =
   if (* msg.time < time || *) msg.seen then
     (
      ui#colour_say msg.content (U_colour.conv_colour_s "darkgrey");
      if msg.repeat > 0 then ui#colour_say 
            ("(" ^ (string_of_int (msg.repeat + 1)) ^ ")")
            U_colour.Darkgrey;
      ui#say "\n"
     )
   else
     (
      msg.seen <- is_seen;
      ui#colour_say msg.content msg.colour;
      if msg.repeat > 0 then ui#colour_say 
            ("(repeated x" ^ (string_of_int (msg.repeat + 1)) ^ ")")
            msg.colour;
      ui#say "\n"
     )
;;

let pared_msg_bank mbank =
   let length = List.length mbank.message in
      if length < 10 then mbank
      else
         let new_message = ref [] in
            for count = (length - 10) to (length - 1) do
               (new_message := 
                    (
                     !new_message @ 
                     [(List.nth mbank.message count)]
                    )
               )
            done;
            {mbank with message = !new_message}
;;

let display_forward_msgbank ui uiopt mbank time is_seen =
 ui#clear_win "msg";
 ui#reset_cursor "msg";
 if List.length mbank.message > 0 then
  (
   mbank.last_seen <- time;
   let height = uiopt#get_msg_high in
   let width = uiopt#get_msg_wide + 1 in
   let count = ref 0 in
   let mlines = ref 0 in
   let mbanklength = List.length mbank.message - 1 in
   let finished = ref false in
      while not !finished do
         let cmsg = List.nth mbank.message (mbanklength - !count) in
            let cmlines = count_mlines cmsg.content width in
               if cmlines + !mlines > height then
                  finished := true
               else if (cmsg.seen && 
                  (not uiopt#get_old_messages)) then
                     finished := true
               else if cmsg.target = Player 
                  || (uiopt#get_show_debug && (cmsg.target = Debug))
               then
                  (
                   display_message ui uiopt cmsg time is_seen;
                   mlines := !mlines + cmlines;
                   count := !count + 1;
                   if !count > mbanklength then
                      finished := true
                  )
              else 
                (
                 count := !count + 1;
                 if !count > mbanklength then
                    finished := true
                )
      done
  )
;;

let collect_reverse_msgbank ui uiopt mbank time =
 let new_mbank = {proto_msgbank with message = []} in
 if List.length mbank.message > 0 then
  (
   mbank.last_seen <- time;
   let height = uiopt#get_msg_high in
   let width = uiopt#get_msg_wide + 1 in
   let count = ref 0 in
   let mlines = ref 0 in
   let mbanklength = List.length mbank.message - 1 in
   let finished = ref false in
      while not !finished do
         let cmsg = List.nth mbank.message (mbanklength - !count) in
            let cmlines = count_mlines cmsg.content width in
               if cmlines + !mlines > height then
                  finished := true
               else if (cmsg.seen && 
                  (not uiopt#get_old_messages)) then
                     finished := true
               else if cmsg.target = Player 
                  || (uiopt#get_show_debug && (cmsg.target = Debug))
               then
                  (
                   new_mbank.message <- (new_mbank.message @ [cmsg]);
                   mlines := !mlines + cmlines;
                   count := !count + 1;
                   if !count > mbanklength then
                      finished := true
                  )
              else 
                (
                 count := !count + 1;
                 if !count > mbanklength then
                    finished := true
                )
      done
  ); new_mbank
;;

let display_msgbank ui uiopt mbank time is_seen =
   if uiopt#get_message_reverse then
      display_forward_msgbank ui uiopt 
            (collect_reverse_msgbank ui uiopt mbank time) time is_seen
   else
      display_forward_msgbank ui uiopt mbank time is_seen
;;

let get_last_seen mb = mb.last_seen;;

let rec time_of_most_recent mb =
   let mblength = (List.length mb.message) - 1 in
      next_most_recent mb mblength
and next_most_recent mb count =
   if count < 0 then 0 else
   let most_recent = List.nth mb.message count in
      if most_recent.target = Player then
         most_recent.time
      else next_most_recent mb (count - 1)
;;

let get_language msgbank = msgbank.language;;
