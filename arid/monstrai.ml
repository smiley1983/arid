(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    monstrai.ml

*  What does it do?

**    Provides monster AI. At this stage it's a "bump toward player" ai 
which takes NO account of its surroundings whatsoever.

*)


let bump_toward_person mon pc =
   let mx = Person.get_locx mon in
   let my = Person.get_locy mon in
   let px = Person.get_locx pc in
   let py = Person.get_locy pc in
      let dx, dy = (px - mx), (py - my) in
         let crdnl = Algo.fixed_cardinal dx dy in
            match crdnl with
               Algo.North -> Actpit.Bump_n
             | Algo.South -> Actpit.Bump_s
             | Algo.West -> Actpit.Bump_w
             | Algo.East -> Actpit.Bump_e
             | Algo.Northwest -> Actpit.Bump_nw
             | Algo.Northeast -> Actpit.Bump_ne
             | Algo.Southwest -> Actpit.Bump_sw
             | Algo.Southeast -> Actpit.Bump_se
             | _ -> Actpit.Pass
;;

let get_random_move n =
   let choice = Algo.mersenne n in
      match choice with
         0 -> "bump_n"
       | 1 -> "bump_e"
       | 2 -> "bump_s"
       | _ -> "bump_w"
;;

let get_attack_dir plane actor =
   let choice = Algo.mersenne 4 in
      match choice with
         0 -> Algo.North
       | 1 -> Algo.West
       | 2 -> Algo.South
       | _ -> Algo.East
;;

(* DEBUG
let get_random_move n =
   let choice = Algo.mersenne n in
      match choice with
         0 -> "walk_n"
       | 1 -> "walk_n"
       | 2 -> "walk_w"
       | _ -> "walk_w"
;;
*)
