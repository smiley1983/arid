(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    person.ml

*  What does it do?

**    provides types and functions and prototypes for sentient beings to 
arid.ml

*)


type sentience =
   PC
 | NPC
 | Party
 | Goblin_tribe (*example*)
;;

type tstatus =
   Sleeping
 | Awake
 | Dead
;;

type species =
   Mammal
 | Reptile
 | Monster
 | Nothing
;;

type person =
 {
   mutable person_item : Item.item_flyweight;
   mutable ptype : sentience;
   inv : Inventry.inventory;
   spell : Inventry.inventory;
   equip : Equip.equipment;
   mutable status : tstatus;
   mutable vis_range : int;
   mutable statistic : Stat.stat_table;
   mutable next_act_time : int;
   mutable walk_speed : int;
   mutable act_speed : int;
   mutable pspecies : species;
 }
;;

let proto_person =
 {
   person_item = Item.proto_item;
   ptype = NPC;
   inv = (Inventry.new_inventory (-1));
   spell = Inventry.new_inventory (-1);
   equip = Equip.new_equipment (-1);
   status = Awake;
   vis_range = 8;
   statistic = Stat.proto_stat_table;
   next_act_time = 0;
   walk_speed = 100;
   act_speed = 100;
   pspecies = Nothing;
 }
;;

let new_human lx ly iid =
 let pi = Item.new_person_item lx ly iid Symbol.Humanoid U_colour.White 
      I_base.Human
 in
 let ns =
   {proto_person with
       person_item = pi;
       ptype = NPC;
       inv = (Inventry.new_inventory iid);
       spell = (Inventry.new_inventory iid);
       equip = Equip.new_equipment (-1);
       statistic = Stat.new_stat_table (50);
   }
 in
   Stat.grant_stat ns.statistic Stat.Intrinsic_armour 9;
   ns
;;

let new_person lx ly iid symbol colour basename =
 let pi = Item.new_person_item lx ly iid symbol colour basename in
   {proto_person with
       person_item = pi;
       ptype = NPC;
       inv = (Inventry.new_inventory iid);
       spell = (Inventry.new_inventory iid);
       equip = Equip.new_equipment (-1);
       statistic = Stat.new_stat_table (10);
   }
;;

let new_rat lx ly iid =
 let pi = Item.new_person_item lx ly iid Symbol.Rodent U_colour.Brown 
      I_base.Rodent
 in
   {proto_person with
       person_item = pi;
       ptype = NPC;
       inv = (Inventry.new_inventory iid);
       spell = (Inventry.new_inventory iid);
       equip = Equip.new_equipment (-1);
       statistic = Stat.new_stat_table (10);
   }
;;

let set_locx pson v = pson.person_item.Item.locx <- v;;
let set_locy pson v = pson.person_item.Item.locy <- v;;

let get_loc pson = (pson.person_item.Item.locx,
                    pson.person_item.Item.locy);;
let get_locx pson = pson.person_item.Item.locx;;
let get_locy pson = pson.person_item.Item.locy;;
(*let get_plane pson = pson.plane;;*)

let get_symbol pson protolist = 
(*
   let proto = Item.get_prototype pson.person_item prototype in
*)
   Item.get_symbol pson.person_item protolist
;;

let get_colour pson protolist = 
(*
   let proto = Item.get_prototype pson.person_item prototype in
*)
      Item.get_colour pson.person_item protolist
;;

let get_id pson = pson.person_item.Item.id;;

let get_pname pson protolist =
(*
   let proto = Item.get_prototype pson.person_item protolist in
*)
   Language.get_iname pson.person_item protolist
;;

let get_ptype pson = pson.ptype;;

let get_desc pson protolist =
   Item.get_desc pson.person_item protolist
;;

let get_losblock pson = pson.person_item.Item.losblock;;
let get_physblock pson = pson.person_item.Item.physblock;;
let get_inventory pson = pson.inv;;

let get_mname pson wizard prototype =
   if wizard then
      (get_pname pson prototype) ^ (string_of_int (get_id pson))
   else get_pname pson prototype
;;

let new_player cnm item_id =
 let iid = Item.new_iid item_id in
 let pi = Item.new_person_item 0 0 iid Symbol.Person_unique 
      U_colour.White I_base.PC
 in
   {proto_person with
      person_item = pi;
      ptype = PC;
      inv = (Inventry.new_inventory 0);
      spell = (Inventry.new_inventory 0);
      equip = Equip.new_equipment (-1);
      statistic = Stat.new_stat_table (100);
      vis_range = 12;
   }
;;

type personlist =
 {
   mutable p : person list
 }
;;

let proto_personlist =
 {
   p = []
 }
;;

let new_plist b = 
   if b then {p = []}
   else proto_personlist
;;

let get_list pl =
   pl.p
;;

let add_person pson plist =
   if not (plist == proto_personlist) then
      plist.p <- plist.p @ [pson]
   else raise (Failure ("Trying to add person to proto_personlist"))
;;

let remove_person pson plist =
   let newlist = ref [] in
   for count = 0 to (List.length plist.p - 1) do
      let cpson = List.nth plist.p count in
         if not (cpson == pson) then
            newlist := !newlist @ [cpson]
   done;
   plist.p <- !newlist;
(* DEBUG   print_string (string_of_int (List.length plist.p)) *)
;;

let person_in_list pnumber plist =
   let rslt = ref proto_person in
   for count = 0 to (List.length plist - 1) do
      let pson = List.nth plist count in
         if pson.person_item.Item.id = pnumber then rslt := pson
   done;
   if !rslt == proto_person then raise Not_found else
      !rslt
;;

let list_has_person plist pson =
   let rslt = ref false in
   for count = 0 to (List.length plist - 1) do
      let check_pson = List.nth plist count in
         if check_pson.person_item.Item.id = 
            pson.person_item.Item.id 
         then rslt := true
   done;
   !rslt
;;

let gain_item itm actor protolist =
   Inventry.add_item actor.inv itm protolist
;;

let gain_spell itm actor =
   Inventry.add_item actor.spell itm
;;

let lose_item index actor =
   Inventry.remove_index actor.inv index
;;

let show_item index actor =
   Inventry.item_of_index actor.inv index
;;

let lose_single_item index actor =
   let itm = show_item index actor in
      if Item.get_repeat itm < 2 then
         ignore (Inventry.remove_index actor.inv index)
      else Item.decrement_stack itm
;;

let show_equip index actor =
   Equip.equipslot_of_index actor.equip index
;;

let switch_equip index actor itm =
   Equip.switch_index_with_item actor.equip index itm
;;

let remove_equip index actor =
   Equip.remove_index actor.equip index
;;

let rec id_list plist =
   next_id plist []
and next_id plist total =
   match plist with 
      [] -> total
    | head :: tail ->
         (get_id head) :: total @ (next_id tail total)
;;

let people_on_loc plist lx ly =
   let rslt = ref [] in
      for count = 0 to (List.length plist.p - 1) do
         let pson = List.nth plist.p count in
            if (pson.person_item.Item.locx = lx) && 
               (pson.person_item.Item.locy = ly) 
            then
               rslt := pson :: !rslt
      done;
   !rslt
;;

let select_creature danger lx ly iid =
   match danger with _ ->
      match Algo.mersenne 4 with
         0 -> new_human lx ly iid
       | _ -> new_rat lx ly iid
(*
       | 2 -> new_lizard lx ly iid
       | _ -> new_snake lx ly iid
*)
;;

let is_dead pson =
   if pson.status = Dead then true else false
;;

let statuskill pson =
(*
   if not (pson.ptype = PC) then
*)
      pson.status <- Dead
(*
   else raise (Failure ("PC_DIED"))
*)
;;

let get_status pson = pson.status;;

let is_pc pson = (pson.ptype = PC);;

let get_spellist pson = pson.spell;;

let get_spell pson index = Inventry.item_of_index pson.spell index;;

let get_equipment pson = pson.equip;;

(*
let get_equipslot pson index =
   Inventry.item_of_index pson.equip index
;;
*)

let add_head pson headname =
   Equip.add_slot pson.equip Body.Head headname
;;

let add_neck pson partname =
   Equip.add_slot pson.equip Body.Neck partname
;;

let add_torso pson partname =
   Equip.add_slot pson.equip Body.Torso partname
;;

let add_arm pson partname =
   Equip.add_slot pson.equip Body.Arm partname
;;

let add_hand pson partname =
   Equip.add_slot pson.equip Body.Hand partname
;;

let add_glove pson partname =
   Equip.add_slot pson.equip Body.Glove partname
;;

let add_legs pson partname =
   Equip.add_slot pson.equip Body.Legs partname
;;

let add_feet pson partname =
   Equip.add_slot pson.equip Body.Feet partname
;;

let get_vis_range pson = pson.vis_range;;

let get_walk_speed pson = pson.walk_speed;;

let get_act_speed pson = pson.act_speed;;

let set_walk_speed pson v = pson.walk_speed <- v;;

let set_act_speed pson v = pson.act_speed <- v;;

let get_stat pson = pson.statistic;;

let get_health pson = Stat.get_health pson.statistic;;

let get_max_health pson = Stat.get_max_health pson.statistic;;

let get_topstat pson = Stat.get_topstat pson.statistic;;

let attack_mod pson = 
   (Stat.get_statvalue pson.statistic Stat.Muscle_b)
 + (Equip.attack_mod pson.equip)
;;

let damage_mod pson = 
   (Stat.get_statvalue pson.statistic Stat.Muscle_b)
 + (Equip.damage_mod pson.equip)
;;

let evasion_mod pson =
   (Stat.get_statvalue pson.statistic Stat.Muscle_b)
;;

let protection_mod pson = 
   (Stat.get_statvalue pson.statistic Stat.Intrinsic_armour)
 + (Equip.protection_mod pson.equip)
;;

let get_statvalue pson v = Stat.get_statvalue pson.statistic v;;

let take_damage pson v =
   if v > get_health pson then statuskill pson else
      Stat.reduce_health pson.statistic v
;;

let gain_health pson v =
   let chp = get_health pson in
      let mhp = get_max_health pson in
         if chp + v > mhp then
            Stat.set_health pson.statistic mhp
         else Stat.set_health pson.statistic (chp + v)
;;

let lose_item_by_ref itm pson =
   Inventry.lose_item pson.inv itm
;;

let get_next_action_time pson =
   pson.next_act_time
;;

let set_next_action_time pson v =
(*
   Item.set_attribute_value Item.Next_act_time v pson.person_item
*)
   pson.next_act_time <- v
;;

let set_vis_range pc v =
   pc.vis_range <- v
;;

let colour_and_desc_of_corpse pson protolist =
   let c =
      if Item.has_attribute pson.person_item Item.Insect then
         U_colour.Brown
      else U_colour.Red
   in
   let desc = 
      "a " ^ Language.get_iname pson.person_item protolist ^ " corpse."
   in
      (c, desc)
;;

let corpse_of_person pson lx ly iid protolist =
   let id = Uaid.new_aid iid in
   let symbol, colour, nm, desc =
      if Item.has_attribute pson.person_item Item.Skeleton then
         Symbol.Food, U_colour.White, I_base.Pile_of_bones,
         "A pile of bones"
      else
         let c, desc = colour_and_desc_of_corpse pson protolist in
            Symbol.Food, c, 
            I_base.Corpse,
(*
            ((Language.get_iname pson.person_item protolist ) ^ " 
            corpse"),
*)
            desc
   in
      B_item.special_corpse lx ly symbol colour id nm desc protolist
;;

let gain_attribute pson att =
   Item.gain_attribute att pson.person_item
;;

let get_species pson = pson.pspecies;;

let set_species pson v = pson.pspecies <- v;;
