(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    plot.ml

*  What does it do?

**    Provides types and functions for the plot system. Note that this 
is highly game-dependent. A single sample plot event is provided, in 
which the player wins upon following a particular staircase. A new file 
should be created above this one for the specific plot of every game. 
new types may be added to the file plotlink.ml if needed, but existing 
types may also be reused between games.

*)


type plot_event =
 {
   variety : Plotlink.event_variety;
   completed : bool;
   sequence : int;
 }
;;

let proto_plot_event =
 {
   variety = Plotlink.Loss;
   completed = false;
   sequence = -1;
 }
;;

type plot =
 {
   plot_event_link : (Plotlink.plotlink * plot_event) list
 }
;;

let proto_plot = {plot_event_link = []};;

let ascending1_plot = 
 {
   plot_event_link = [(Plotlink.Ascending1_exit, 
                      {proto_plot_event with variety = Plotlink.Victory}
                     )]
 }
;;

let rec check_stair plot plink =
   match plink with
      [] -> ()
    | head :: tail ->
         if not (List.mem_assoc head plot.plot_event_link) then
            check_stair plot tail
         else
            if (List.assoc head plot.plot_event_link ).variety = 
                  Plotlink.Victory 
            then
               raise (Failure ("Victory"))
;;
