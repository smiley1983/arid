(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*)

(*

*  What is this file?

**    s_ahandl.ml

*  What does it do?

**    Provides a string of action handlers. You could take all actions 
in one handler, but this is more flexible if you want to make a game in 
which a single command works differently, or you want to make piecemeal 
changes to the action system. Note that you should make a copy of this 
file, under another name,
for each game where you want a modified command set; this is the 
standard origianal, and should be left as such. Look at arid.ml to see 
how it links to S_ahandl (or another action handler you create).

*)

let resolve_ispc_and_prevtime actor actq =
   if Person.is_pc actor then
      (
       let prior = Action.get_prevtime actq in
          Action.set_prior_prevtime actq prior;
       Action.set_prevtime actq (Action.get_gametime actq);
       true
      )
   else false
;;

let update_visuals ui uiopt overmap planename pc iactor act actq 
      msgbank is_seen protolist
=
   if uiopt#get_show_debug then
      print_string 
         ("\n" ^ (string_of_int 
                  (Mapplane.count_loaded 
                   (Mapstruc.get_cplane overmap))) ^ "\n"
         )
   ;
   let acttime = Action.get_time act in
   let prevtime = 
      if Action.get_variety act = Actpit.Pass then
         Action.get_subinfo1 act
      else
         Action.get_prevtime actq 
   in
(*   Vismap.paint_dirty overmap (Mapstruc.get_cplane overmap); *)
   let message_interrupt = 
      (Message.time_of_most_recent msgbank > prevtime)
   in
   Command.paint_vismap overmap planename iactor
         (Action.get_prevtime actq) protolist;
   let interrupt = Shdwcast.vision_scan (Person.get_locx iactor)
         (Person.get_locy iactor) overmap (Person.get_vis_range iactor)
         acttime
   in
      if (interrupt || message_interrupt) then
         Action.set_interrupt_activity actq true;
      if message_interrupt then Action.set_interrupt_pass actq true;
   if is_seen then
      (
      Message.display_msgbank ui uiopt msgbank 
            (Action.get_gametime actq) is_seen;
      Command.update_statwin ui uiopt iactor overmap;
      Command.display_vismap ui uiopt overmap 
         (Person.get_locx iactor) (Person.get_locy iactor) acttime
      )
(*
   else
      uiopt#set_c_colour U_colour.White;
*)
;;

let get_dirkey_int ui uiopt q =
   ui#clear_say q; ui#sync;
   let keypress = ui#in_key in
   let command = uiopt#get_key keypress in
      match command with
       | "key_north" -> 8
       | "key_south" -> 2
       | "key_east"  -> 6
       | "key_west"  -> 4
       | "key_ne"    -> 9
       | "key_nw"    -> 7
       | "key_se"    -> 3
       | "key_sw"    -> 1
       | _           -> 0
;;

let convert_keypress_to_command ui uiopt overmap planename pc iactor act 
   actq msgbank keypress
=
   let acttime = Action.get_time act in
   let command = 
     try
      uiopt#get_key keypress 
     with Not_found ->
      (
(*         print_string ("keypress " ^ keypress ^ " not found.\n"); *)
         ""
      )
   in
      match command with
       | "key_viewmap" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Look_mode
            }
       | "key_help" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Help
            }
       | "key_look" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Short_look
            }
       | "key_quit" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Act_quit
            }
       | "key_north" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_n
            }
       | "key_east" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_e
            }
       | "key_south" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_s
            }
       | "key_west" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_w
            }
       | "key_nw" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_nw
            }
       | "key_ne" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_ne
            }
       | "key_sw" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_sw
            }
       | "key_se" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.variety = Actpit.Bump_se
            }
       | "key_downstair" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Follow_downstair
            }
       | "key_upstair" ->
            {act with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Follow_upstair
            }
       | "key_inventory" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Show_inventory
            }
       | "key_get" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Pick_up
            }
       | "key_drop" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Drop_it
            }
       | "key_multidrop" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Multi_drop
            }
       | "key_attack" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Attack
            }
       | "key_throw" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Throw
            }
       | "key_cast_spell" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Cast_spell
            }
       | "key_equipment" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Show_equipment
            }
       | "key_drink" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Drink
            }
       | "key_read" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Read
            }
       | "key_examine" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Examine
            }
       | "key_skills" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Show_skills;
                  Action.subinfo1 = acttime
            }
       | "key_pass" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Pass;
                  Action.subinfo1 = acttime
            }
       | "key_longwalk" ->
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Follow;
                  Action.subinfo1 = (get_dirkey_int ui uiopt 
                     "walk in which direction?")
            }
       | "wizard_show_actionq" ->
            if uiopt#is_wizard then
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Wizard_show_actionq;
            }
            else
            {Action.proto_action with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Request
            }
       | "key_wizard_vision" ->
            if uiopt#is_wizard then
            {
               act with
                  Action.actor = Person.get_id iactor;
                  Action.time = acttime;
                  Action.variety = Actpit.Wizard_vision;
            }
            else
            {Action.proto_action with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Request
            }
       | _ ->
            {Action.proto_action with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = Actpit.Request
            }
;;

let player_input_act ui uiopt overmap planename pc iactor act actq 
      msgbank protolist
=
   (
      Action.set_interrupt_activity actq false;
      Action.set_interrupt_pass actq false;
      update_visuals ui uiopt overmap planename pc iactor act actq 
            msgbank true protolist;
      if uiopt#get_show_debug then
        (
         Message.add_msg msgbank (Action.get_gametime actq) 
               Message.Debug
            ("People in memory: " ^ 
             (string_of_int (List.length (Mapstruc.plist overmap))) 
            );
         Command.display_turns_in_queue ui uiopt 
               (Action.get_actlist actq)
        );
      ui#sync;
      let keypress = ui#in_key in
         convert_keypress_to_command ui uiopt overmap planename pc 
               iactor act actq msgbank keypress
   )
;;

let get_action ui uiopt overmap planename pc iactor act actq msgbank 
      protolist
=
   if Person.get_status iactor = Person.Dead then
      (Action.remove_person actq iactor; raise Not_found)
   else
   match Person.is_pc iactor with 
   true ->
      player_input_act ui uiopt overmap planename pc iactor act actq 
            msgbank protolist
    | _ -> 
      (
        let anm = Monstrai.bump_toward_person iactor pc in
        let acttime = Action.get_time act in
           {Action.proto_action with
               Action.actor = Person.get_id iactor;
               Action.time = acttime;
               Action.variety = anm
           }
      )
;;

let part_walk ui uiopt mhold planename actor act actq dx dy msgbank 
   protolist 
=
   let ispc = resolve_ispc_and_prevtime actor actq in
   let acttime = Action.get_time act in
   let delay = Action.act_time actor act in
   let (success, changeblock) =
      (Command.walk ui uiopt mhold planename actor dx dy acttime
                    actq (Action.get_gametime actq) msgbank protolist)
   in
   if success then
       (
        if ispc then
          (
           Command.step_ontile ui uiopt actor mhold msgbank
                               (acttime + delay) protolist;
          );
        Action.set_time act (acttime + delay)
       )
    else if not ispc then
       Action.set_time act (acttime + delay);
   success, changeblock
;;

let do_walk ui uiopt mhold planename actor act actq dx dy msgbank 
   protolist 
=
   let success, changeblock =
      part_walk ui uiopt mhold planename actor act actq dx dy msgbank 
            protolist
   in
   Action.next_act actq actor (Action.get_time act)
         (Person.is_pc actor);
   if changeblock then
      Action.Savecontinue
   else Action.Continue
;;

let do_bump ui uiopt mhold planename pc actor act actq dx dy iid msgbank 
   protolist
=
   let acttime = Action.get_time act in
   let gtime = Action.get_gametime actq in
   let ax, ay = Person.get_loc actor in
   let px, py = Person.get_loc pc in
   let ms = Map_api.get_map mhold in
   let plane = Mapstruc.get_cplane ms in
      let (lx, ly) = (ax + dx), (ay + dy) in
         let ppl = Mapstruc.people_on_loc plane lx ly in
            if ppl = [] && not ((lx, ly) = (px, py)) then
               do_walk ui uiopt mhold planename actor act actq
                     dx dy msgbank protolist
            else if
               Command.attack_spot ms plane pc actor msgbank
                     gtime iid lx ly protolist
            then
                  ( 
                    Action.set_variety act Actpit.Attack;
                    let delay = Action.act_time actor act in
                    Action.set_time act (acttime + delay);
                    Action.next_act actq actor
                          (acttime + delay) (Person.is_pc actor);
                   Action.Continue
                  )
            else
                  (
                   Action.next_act actq actor acttime 
                         (Person.is_pc actor);
                   Action.Continue
                  )
;;

let step_follow ui uiopt mhold planename actor act actq dx dy msgbank 
   subinfo protolist
=
   let success, changeblock =
      part_walk ui uiopt mhold planename actor act actq dx dy msgbank 
            protolist
   in
      success, subinfo, changeblock
;;

let do_follow ui uiopt mhold planename actor act actq msgbank acttime 
      subinfo protolist
=
   let ms = Mapstruc.get_map mhold in
   let cx, cy = Person.get_loc actor in
      try
         let direction = Algo.number_cardinal subinfo in
         let dx, dy = Algo.cardinal_step 0 0 direction in
         let nx, ny = (dx + cx), (dy + cy) in
            let n_rt = Mapstruc.get_rtile ms nx ny in
               if (Mapblock.is_blank n_rt) && 
                  (not (Mapblock.is_physblocked n_rt))
               then
                  step_follow ui uiopt mhold planename actor act actq 
                        dx dy msgbank subinfo protolist
               else raise Not_found
      with Not_found ->
         false, 0, false
;;

let proc_act_template ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Template_action -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         () (* Command.do_something ui uiopt*) ;
         Action.next_act actq actor acttime ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_request ui uiopt mhold planename pc actor act actq iid  
      msgbank protolist
=
   match act.Action.variety with
      Actpit.Request -> 
        (
         let _ (* ispc *) = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in
          if Person.is_pc actor then
           try
            Action.insert_action
               (get_action ui uiopt overmap planename pc actor act actq
                           msgbank protolist
               )
               actq true
           with Not_found -> print_string "Insert PC action failed\n"
          else
            try
             Action.insert_action 
               (get_action ui uiopt overmap planename pc actor act actq
                           msgbank protolist
               )
               actq false
            with Not_found -> print_string "Insert AI action failed\n"
        )
        ;
        Action.Continue
    | _ -> Action.Unprocessed
;;

let proc_act_help ui uiopt mhold planename pc actor act actq iid msgbank
      protolist
=
   match act.Action.variety with
    | Actpit.Help -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         Command.help ui uiopt;
         Action.next_act actq actor acttime ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_short_look ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Short_look -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         Command.short_look ui uiopt actor mhold msgbank 
               (Action.get_time act) true protolist;
         Action.next_act actq actor acttime ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_look_mode ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Look_mode -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in
         ignore 
         (
            Command.look_mode ui uiopt overmap planename
      ((Person.get_locx actor) - (uiopt#get_screen_chars_wide / 2))
      ((Person.get_locy actor) - (uiopt#get_screen_chars_high / 2))
                              (Action.get_gametime actq)
         );
         Action.next_act actq actor acttime ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_n ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_n -> 
        (
         do_bump ui uiopt mhold planename pc actor act actq 0 (-1) 
               iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_e ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_e -> 
        (
            do_bump ui uiopt mhold planename pc actor
                    act actq 1 0 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_s ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_s ->
        (
            do_bump ui uiopt mhold planename pc actor
                    act actq 0 1 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_w ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_w -> 
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq (-1) 0 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_ne ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_ne -> 
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq 1 (-1) iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_nw ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_nw -> 
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq (-1) (-1) iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_se ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_se -> 
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq 1 1 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_bump_sw ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Bump_sw -> 
        (
                  do_bump ui uiopt mhold planename pc actor
                          act actq (-1) 1 iid msgbank protolist
        )
    | _ -> Action.Unprocessed
;;

let proc_act_follow_downstair ui uiopt mhold planename pc actor act actq 
      iid msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Follow_downstair -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if (Command.follow_stair mhold actor true)
           then
             (
              Action.set_time act (acttime + delay);
              Action.next_act actq actor (acttime + delay) 
                    ispc;
              Action.Switchcontinue
             )
           else
             (
              Action.next_act actq actor acttime ispc;
              Action.Continue
             )
        )
    | _ -> Action.Unprocessed
;;

let proc_act_follow_upstair ui uiopt mhold planename pc actor act actq 
      iid msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Follow_upstair -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if (Command.follow_stair mhold actor false)
           then
             (
              Action.set_time act (acttime + delay);
              Action.next_act actq actor (acttime + delay) 
                    ispc;
              Action.Switchcontinue
             )
           else
             (
              Action.next_act actq actor acttime ispc;
              Action.Continue
             )
        )
    | _ -> Action.Unprocessed
;;


let proc_act_show_inventory ui uiopt mhold planename pc actor act actq 
      iid msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Show_inventory -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if ispc then 
            Command.show_inventory ui uiopt actor msgbank acttime 
               protolist;
         Action.next_act actq actor acttime ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_show_equipment ui uiopt mhold planename pc actor act actq 
      iid msgbank protolist
=
   match act.Action.variety with
    | Actpit.Show_equipment -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         let new_acttime =
            if ispc then
              (
               if Command.show_equipment ui uiopt actor protolist false
               then
                 (Message.add_msg msgbank acttime Message.Player
                     "You alter your equipment.";
                  acttime + delay
                 )
               else acttime
              )
            else (acttime + delay)
         in
         Action.set_time act new_acttime;
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_pick_up ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Pick_up -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if Command.pick_up ui uiopt mhold actor msgbank 
               (acttime + delay) protolist
         then
            Action.set_time act (acttime + delay);
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_drop_it ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Drop_it -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if Command.drop_it ui uiopt mhold actor msgbank 
               (acttime + delay) protolist
         then
            Action.set_time act (acttime + delay);
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;


let proc_act_multi_drop ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Multi_drop -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if Command.multi_drop ui uiopt mhold actor msgbank
                               (acttime + delay) iid protolist
         then
            Action.set_time act (acttime + delay);
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_attack ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Attack -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
         let overmap = Mapstruc.get_map mhold in
         if Command.attack
               ui uiopt (Map_api.get_map mhold)
               (Mapstruc.get_cplane overmap)
               actor msgbank (acttime + delay) iid protolist
            then 
               Action.set_time act (acttime + delay);
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let setup_missile_trail actq actor time ispc to_x to_y 
=
   let px, py = Person.get_loc actor in
   let points = Algo.bresenham_line to_x to_y px py in
      for count = 0 to ((List.length points) - 1) do
         let cx, cy = List.nth points count in
            Action.remove_missile_trail actq actor time ispc cx cy
      done;
      for count = 0 to ((List.length points) - 1) do
         let cx, cy = List.nth points count in
            Action.missile_trail actq actor time ispc cx cy
      done
;;

let proc_act_throw ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Throw -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         let success, to_x, to_y =
            Command.throw
               ui uiopt mhold actor msgbank acttime iid protolist
         in
         if success then
           (
            setup_missile_trail actq actor (Action.get_time act) ispc 
                  to_x to_y;
            Action.set_time act (acttime + delay)
           );
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_cast_spell ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Cast_spell -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         let success, to_x, to_y =
            Command.cast_spell
               ui uiopt mhold actor msgbank acttime iid protolist
         in
         if success then
            Action.set_time act (acttime + delay);
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_drink ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Drink -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if Command.drink
               ui uiopt mhold actor msgbank acttime protolist
         then
            Action.set_time act (acttime + delay);
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_read ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Read -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if Command.read
               ui uiopt mhold actor msgbank acttime protolist
         then
            Action.set_time act (acttime + delay);
         Action.next_act actq actor (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_examine ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Examine -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let delay = Action.act_time actor act in
(* 3.09 warning
         let overmap = Mapstruc.get_map mhold in
*)
         if Command.examine
               ui uiopt mhold actor msgbank acttime protolist
         then
            Action.set_time act (acttime + delay);
         Action.next_act actq actor
               (Action.get_time act) ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_pass ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Pass -> 
        (
         let overmap = Mapstruc.get_map mhold in
         update_visuals ui uiopt overmap planename pc actor act 
               actq msgbank false protolist;
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let nexttime = (Action.time_of_next_act actq) + 1 in
         Action.set_time act (nexttime);
         let act_interrupt = Action.interrupt_pass actq in
         let starttime = Action.get_subinfo1 act in
         let turn_passed = ((acttime - starttime) > 450) in
         if ispc && (act_interrupt || turn_passed) then
            Action.next_act actq actor (*nexttime*) acttime ispc
         else
            Action.next_pass actq actor starttime nexttime ispc;
         Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_follow ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   match act.Action.variety with
    | Actpit.Follow -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in
         let k_interrupted, keypress = 
          try
            if ispc then
              (
               update_visuals ui uiopt overmap planename pc actor act 
                     actq msgbank true protolist;
               ui#unblocked_key
              )
            else false, ""
          with Not_found -> false, ""
         in
         let act_interrupt = Action.should_interrupt actq in
         if k_interrupted then
              (
               let newact = convert_keypress_to_command ui uiopt overmap 
                     planename pc actor act actq msgbank keypress
               in
                  Action.insert_action newact actq true;
               Action.Continue
              )
         else if (act_interrupt && ispc) then
           (
               Action.next_act actq actor
                     acttime ispc;
               Action.Continue
           )
         else
           (
            let (completed, nsubdat, changeblock) = do_follow
               ui uiopt mhold planename actor act actq msgbank acttime 
               act.Action.subinfo1 protolist
            in
            let delay = Action.act_time actor act in
            let new_acttime =
               if completed then (acttime + delay)
               else acttime
            in
                  Action.set_time act new_acttime;
            if (nsubdat = 0) (* || changeblock *) then
               Action.next_act actq actor
                     new_acttime ispc
            else
               Action.next_follow actq (Person.get_id actor)
                    (Action.get_time act) ispc nsubdat;
            if changeblock then
               Action.Savecontinue
            else
               Action.Continue
           )
        )
    | _ -> Action.Unprocessed
;;

let proc_act_missile_trail ui uiopt mhold planename pc actor act actq 
      iid msgbank protolist
=
   match act.Action.variety with
    | Actpit.Missile_trail -> 
        (
         let _ (* ispc *) = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in
         let lx, ly = Action.get_subdatpair act in
            let rtile = Mapstruc.get_rtile overmap lx ly in
               Mapblock.backup_vis_sc rtile;
               Mapblock.set_vis_colour rtile U_colour.White;
               Mapblock.set_vis_symbol rtile 
                     Symbol.Magical_special;
               Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_remove_missile_trail ui uiopt mhold planename pc actor act 
      actq iid msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Remove_missile_trail -> 
        (
         let _ (* ispc *) = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in
         let lx, ly = Action.get_subdatpair act in
            let rtile = Mapstruc.get_rtile overmap lx ly in
               Mapblock.restore_vis_sc rtile;
               Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_wizard_show_actionq ui uiopt mhold planename pc actor 
      act actq iid msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Wizard_show_actionq -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
            ui#show_actionq_plus actq act;
            Action.next_act actq actor acttime ispc;
            Action.Continue
        )
    | _ -> Action.Unprocessed
;;

let proc_act_switch_wizard_vision ui uiopt mhold planename pc actor 
      act actq iid msgbank  protolist
=
   match act.Action.variety with
    | Actpit.Wizard_vision -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
            let newval =
               match uiopt#get_key "wizard_vision" with
                  "true" -> "false"
                | _ -> "true"
            in
            uiopt#set_opt "wizard_vision" newval;
            Action.next_act actq actor acttime ispc;
            Action.Continue
        )
    | _ -> Action.Unprocessed
;;

(*
let proc_act_ ui uiopt mhold planename pc actor act actq iid 
      msgbank  protolist protolist
=
   match act.Action.variety with
    | Actpit. -> 
        (
         let ispc = resolve_ispc_and_prevtime actor actq in
         let acttime = Action.get_time act in
            Action.set_gametime actq acttime;
         let overmap = Mapstruc.get_map mhold in

        )
    | _ -> Action.Unprocessed
;;
*)

let proc_act_other ui uiopt mhold planename pc actor act actq iid 
      msgbank protolist
=
   let ispc = resolve_ispc_and_prevtime actor actq in
   let acttime = Action.get_time act in
      Action.set_gametime actq acttime;
(* 3.09 warning
   let overmap = Mapstruc.get_map mhold in
*)
   match act.Action.variety with
    | _ ->  (* error: SAVE and QUIT if pc or drop action if monster *)
            if (Person.is_pc actor) then 
               (Action.next_act actq actor acttime ispc;
                Action.Save)
            else Action.Continue
;;

let processor_chain_a = 
[
      proc_act_request;
      proc_act_help;
      proc_act_short_look;
      proc_act_look_mode;
      proc_act_bump_n;
      proc_act_bump_e;
      proc_act_bump_s;
      proc_act_bump_w;
      proc_act_bump_ne;
      proc_act_bump_nw;
      proc_act_bump_se;
      proc_act_bump_sw;
      proc_act_follow_downstair;
      proc_act_follow_upstair;
      proc_act_show_inventory;
      proc_act_show_equipment;
      proc_act_pick_up;
      proc_act_drop_it;
      proc_act_multi_drop;
      proc_act_attack;
      proc_act_throw;
      proc_act_cast_spell;
      proc_act_drink;
      proc_act_read;
      proc_act_examine;
      proc_act_pass;
      proc_act_follow;
      proc_act_missile_trail;
      proc_act_remove_missile_trail;
      proc_act_wizard_show_actionq;
      proc_act_switch_wizard_vision;
      proc_act_other;
]
;;

(*

This code is currently redundant, but is being left here in case it's 
needed in the future.

let check_animations ui uiopt overmap planename pc actor act actq iid 
      msgbank protolist
=
   let could_anim = Actpit.could_animate (Action.get_variety act) in
   let show_anim = uiopt#get_key "show_animation" in
      match (show_anim, could_anim) with
         "always", Actpit.Could
       | "always", Actpit.Should
       | "normal", Actpit.Should ->
             update_visuals ui uiopt overmap planename pc pc act 
                   actq msgbank false protolist
       | _ -> ()
;;
*)

let chain_process_action ui uiopt mhold planename pc actor act actq iid
                        msgbank processor_chain protolist
=
   let ispc = resolve_ispc_and_prevtime actor actq in
(* DEBUG *)
(* used when there are problems with PC's location
   if ispc then 
   (let lx, ly = Person.get_loc pc in
      print_string ((string_of_int lx) ^ ", " ^ (string_of_int ly) ^ 
      "\n")
   );
*)
(* /DEBUG *)
   let acttime = Action.get_time act in
      Action.set_gametime actq acttime;
(* 3.09 warning
   let overmap = Mapstruc.get_map mhold in
*)
   let last_chain_el = (List.length processor_chain) - 1 in
   let status = ref Action.Unprocessed in
   let count = ref 0 in
      while (!count <= last_chain_el) && (!status = Action.Unprocessed)
      do
         let actproc = List.nth processor_chain !count in
            status := 
               actproc ui uiopt mhold planename pc actor act actq iid 
                     msgbank protolist
            ;
            count := !count + 1;
      done;
   if (!status = Action.Unprocessed) && ispc
   then 
      Action.Save 
   else if (!status = Action.Unprocessed) && ( not ispc ) then
      Action.Continue
   else !status
;;

let process_action ui uiopt overmap planename pc actq iid msgbank 
      protolist
=
 try
   match Action.get_actlist actq with
      [] -> (print_string "action q empty!" ; Action.Quit)
    | head :: tail ->
        (
         let actorid = Action.get_actorid head in
         Action.set_actlist actq tail;
         let actor = ref
            (Mapstruc.person_of_id (Mapstruc.get_map overmap)
                                   actorid
            )
         in (* FIXME - this should be handled by the above function*)
           if actorid = (Person.get_id pc) then
               (
                actor := pc
               )
           ;
           if not (Person.get_id !actor = - 1) then
             (
              chain_process_action ui uiopt overmap planename pc
                    !actor head actq iid msgbank processor_chain_a []

             )
           else (
              Action.Continue)
        )
 with Failure ("PC_DIED") -> raise (Failure ("PC_DIED"))
    | Not_found -> raise (Failure ("Failed to process action"))
;;

