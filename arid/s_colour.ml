(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    s_colour.ml

*  What does it do?

**    Provides colour translation for the SDL interface

*)


class colourset option =
   object
      val option = option
      val colourset =
         [(U_colour.Black, Sdlvideo.black);
          (U_colour.Red,Sdlvideo.red);
          (U_colour.Green,Sdlvideo.green);
          (U_colour.Blue,Sdlvideo.blue);
          (U_colour.White, (192, 192, 192));
          (U_colour.Brown , (192, 96, 48));
          (U_colour.Cyan , Sdlvideo.cyan);
          (U_colour.Magenta, Sdlvideo.magenta);
          (U_colour.Darkgrey, (80, 80, 80));
          (U_colour.Brightyellow, Sdlvideo.yellow);
          ]
      method get_colour cstr =
         try
            List.assoc cstr colourset
         with Not_found ->
            (80, 80, 80)
   end
;;

let get_colourset = new colourset
