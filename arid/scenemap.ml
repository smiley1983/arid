(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    scenemap.ml

*  What does it do?

**    links the scenario type with the maps needed to make the world.

*)


type scenario =
 {
   variety : Scenario.game_module_variety;
   start_plane : W_plane.plane_reference;
(*
   first_exit : W_plane.exit_ref;
*)
   mutable start_lx : int;
   mutable start_ly : int;
 }
;;

let proto_scenario =
 {
   variety = Scenario.Test;
   start_plane = W_plane.worldmap;
(*
   first_exit = W_plane.worldmap_to_dungeon_exit;
*)
   start_lx = -1;
   start_ly = -1;
 }
;;

let get_start_plane s = s.start_plane;;
let get_start_lx s = s.start_lx;;
let get_start_ly s = s.start_ly;;

let set_start_lx s v = s.start_lx <- v ;;
let set_start_ly s v = s.start_ly <- v ;;

let get_variety s = s.variety;;

let new_scenario v wplane =
   let slx, sly =
      match v with
         Scenario.Test -> (104, 42)
       | Scenario.Ascending_1 -> (-1, -1)
       | Scenario.Tiny_rl -> (13, 13)
       | _ -> (-1, -1)
   in
   { (* proto_scenario with *)
       variety = v;
       start_plane = wplane;
       start_lx = slx;
       start_ly = sly
   }
;;
