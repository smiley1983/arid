(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)

(*

*  What is this file?

**    shdwcast.ml

*  What does it do?

**    Recursive shadowcasting.

*)


type scandat =
 {
   mutable start_slope : float;
   mutable end_slope : float;
 }
;;

let proto_scandat =
 {
   start_slope = 1.0;
   end_slope = 0.0;
 }
;;

let get_rotated_pos cx cy dx dy xfirst depth count =
   let (px, py) =
      if xfirst then
         (
          (cx + (dx * count)),
          (cy + (dy * depth))
         )
      else
         (
          (cx + (dx * depth)),
          (cy + (dy * count))
         )
   in (px, py)
;;

let slope x1 y1 x2 y2 =
   if (y2 -. y1) = 0.0 then 0.0
   else
      (*abs_float*) ( ((x2 -. x1) +. 1.0 ) /. ((y2 -. y1) +. 1.0) )
;;

let new_startslope x y =
   slope 0.5 0.5 ((float_of_int x) (* -. 0.0000000001 *) ) 
         ((float_of_int y) (* +. 0.9999999999 *) )
;;

let new_endslope x y =
   slope 0.5 0.5 ((float_of_int x) 
            +. 1.0 (* 0.9999999999 *) ) 
         ((float_of_int y) (* -. 0.0000000001 *) )
;;

let see_tile tile gametime =
   Mapblock.set_vis_time tile gametime;
   Mapblock.set_last_vis_symbol tile (Mapblock.vis_symbol tile);
   Mapblock.contains_person tile
;;

let throw_access tile gametime =
   Mapblock.set_missile_time tile gametime
;;

let in_circle x y d =
 ( (x * x) + (y * y) ) <= (d * d)
;;

let vision_block rtile =
   (Mapblock.is_losblocked rtile), true
;;

let physical_block rtile =
   (Mapblock.terrain_physblock rtile), 
   false
;;

let rec scan_octant cx cy ms sdat dx dy xfirst depth limit gametime
      test_blocked (should_interrupt:bool ref) people_seen
=
   if depth < limit then
     (
      let begin_see = true in
      let count_start = 
         (int_of_float 
            ((
              ( (sdat.start_slope) ) *. (float_of_int depth) 
             ) +. 0.9
            )
         )
      in
         match
            step_octant_line cx cy ms sdat dx dy xfirst depth limit
                  gametime begin_see count_start count_start test_blocked
                  should_interrupt people_seen
         with (* was the last scan unblocked at the end? *)
            true ->
               next_octant_line cx cy ms sdat dx dy xfirst depth 
                     limit gametime test_blocked should_interrupt 
                     people_seen
          | _ -> !should_interrupt
     )
   else !should_interrupt
and step_octant_line cx cy ms sdat dx dy xfirst depth limit gametime
      could_see count count_start test_blocked should_interrupt 
      people_seen
=
   let can_see = ref could_see in
         if count >= 
            (int_of_float 
               ( (sdat.end_slope) *. (float_of_int depth) )
            )
         then
            real_step_octant_line cx cy ms sdat dx dy xfirst depth limit
                  gametime can_see count count_start test_blocked
                  should_interrupt people_seen
            else !can_see
and real_step_octant_line cx cy ms sdat dx dy xfirst depth limit
      gametime can_see count count_start test_blocked should_interrupt 
      people_seen
=
           (let (px, py) = 
               get_rotated_pos cx cy dx dy xfirst depth count 
            in
               let rtile = Mapstruc.get_rtile ms px py in
                  if in_circle count depth limit then
                  (
                   should_interrupt := 
                      (see_tile rtile gametime 
                       || !should_interrupt);
                   match test_blocked rtile with
                      (true, pb) ->
                        should_interrupt :=
                         case_blocked can_see count count_start sdat
                            new_endslope cx cy ms sdat dx dy xfirst
                            depth limit gametime test_blocked
                            should_interrupt people_seen
                    | (false, pb) ->
                        (
                         if not pb then throw_access rtile gametime;
                         case_unblocked can_see sdat count depth
                               should_interrupt people_seen
                        )
                  );
                  step_octant_line cx cy ms sdat dx dy xfirst depth
                        limit gametime !can_see (count - 1) count_start
                        test_blocked should_interrupt people_seen
           )
and case_blocked can_see count count_start sdat new_endslope
      cx cy ms sdat dx dy xfirst depth limit gametime test_blocked 
      (should_interrupt:bool ref) people_seen
=
   if !can_see = false then !should_interrupt
   else
   (
      can_see := false;
      if not (count = count_start) then
      (
         let new_sdat =
            {sdat with end_slope =
               new_endslope count depth
            } in
               (
                (scan_octant cx cy ms new_sdat dx dy xfirst (depth + 1)
                     limit gametime test_blocked should_interrupt 
                     people_seen)
                || !should_interrupt
               )
      )
      else !should_interrupt
   )
and case_unblocked can_see sdat count depth should_interrupt 
      people_seen 
=
   if !can_see = true then ()
   else
     (
      can_see := true;
      sdat.start_slope <- new_startslope count depth
     )
and next_octant_line cx cy ms sdat dx dy xfirst depth limit gametime
      test_blocked should_interrupt people_seen
=
   let new_sdat = {sdat with start_slope = sdat.start_slope } in
      scan_octant cx cy ms new_sdat dx dy xfirst 
            (depth + 1) limit gametime test_blocked should_interrupt 
             people_seen
;;
   
let octant_1 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat 1 1 true 1 limit gametime fun_block
             (ref false) [];
;;

let octant_2 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat 1 1 false 1 limit gametime
             fun_block (ref false) [];
;;

let octant_3 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat (-1) 1 true 1 limit gametime
             fun_block (ref false) [];
;;

let octant_4 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat (-1) 1 false 1 limit gametime
             fun_block (ref false) [];
;;

let octant_5 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat 1 (-1) true 1 limit gametime
             fun_block (ref false) [];
;;

let octant_6 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat 1 (-1) false 1 limit gametime
             fun_block (ref false) [];
;;

let octant_7 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat (-1) (-1) true 1 limit gametime
             fun_block (ref false) [];
;;

let octant_8 cx cy ms limit gametime fun_block =
      let sdat = {proto_scandat with end_slope = 0.0} in
       scan_octant cx cy ms sdat (-1) (-1) false 1 limit gametime
             fun_block (ref false) [];
;;

let complete_scan cx cy ms limit gametime fun_block =
      let tile = Mapstruc.get_rtile ms cx cy in
         ignore (see_tile tile gametime);
   let b1 = (octant_1 cx cy ms limit gametime fun_block) in
   let b2 = (octant_2 cx cy ms limit gametime fun_block) in
   let b3 = (octant_3 cx cy ms limit gametime fun_block) in
   let b4 = (octant_4 cx cy ms limit gametime fun_block) in
   let b5 = (octant_5 cx cy ms limit gametime fun_block) in
   let b6 = (octant_6 cx cy ms limit gametime fun_block) in
   let b7 = (octant_7 cx cy ms limit gametime fun_block) in
   let b8 = (octant_8 cx cy ms limit gametime fun_block) in
      b1 || b2 || b3 || b4 || b5 || b6 || b7 || b8
;;

let vision_scan cx cy ms limit gametime =
   complete_scan cx cy ms limit gametime vision_block
;;

let throw_scan cx cy ms limit gametime =
   complete_scan cx cy ms limit gametime physical_block
;;

