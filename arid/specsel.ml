(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    specsel.ml

*  What does it do?

**    Selects special player attributes; charater creation, etc...

*)


let rec pick_species ui uiopt =
   let options = [
      Menulist.strn_menu_item "heading" "Please choose your species:";
      Menulist.strn_menu_item "option" "Mammal";
      Menulist.strn_menu_item "option" "Insectoid";
      Menulist.strn_menu_item "option" "Reptile";
   ] in
      let menu = Menulist.make_menu options in
         let h = ui#box_mimic_ret_height "temp" 
            (uiopt#get_key "big_window")
         in
         try
            (ui#choose_menuitem uiopt menu h)#get_content
         with Not_found | (Failure _ ) ->
           (ui#say "bad selection\n";
            pick_species ui uiopt
           )
;;

let rec pick_win_lose ui uiopt =
   let options = [
      Menulist.strn_menu_item "heading" "Would you like to win or \
                                        lose?";
      Menulist.strn_menu_item "option" "win";
      Menulist.strn_menu_item "option" "lose";
   ] in
      let menu = Menulist.make_menu options in
         menu#change_index 'a' 'w';
         menu#change_index 'b' 'l';
         let h = ui#box_mimic_ret_height "temp"
            (uiopt#get_key "big_window")
         in
         try
            (ui#choose_menuitem uiopt menu h)#get_content
         with Not_found
          | Failure _ ->
           (ui#say "bad selection\n";
            pick_win_lose ui uiopt
           )
;;

let rec pick_banishment_species ui uiopt =
   let bwin = (uiopt#get_key "big_window") in 
   let _ = ui#box_mimic_ret_height "temp" bwin in
      ui#clear_win bwin; 
      ui#reset_cursor bwin;
      ui#box_say ("Reptiles are very strong, and their young are "
         ^ "not inclined to use magic, though they can learn.\n\n")
         bwin;
      ui#box_say "Mammals are weak, and predisposed to magic.\n\n" 
         bwin;
      ui#box_say ("Monsters are weaker than reptiles, stronger than "
      ^ "mammals, and have a weak aptitude for magic.") bwin;
   let options = [
      Menulist.strn_menu_item "heading" 
         "What sort of humanoid are you?";
      Menulist.strn_menu_item "option" "Reptile";
      Menulist.strn_menu_item "option" "Mammal";
      Menulist.strn_menu_item "option" "Monster";
   ] in
      let menu = Menulist.make_menu options in
         let h = ui#box_mimic_ret_height "temp" bwin in
         try
            match (ui#choose_menuitem uiopt menu h)#get_content with
             | "Mammal" -> Person.Mammal
             | "Reptile" -> Person.Reptile
             | _ -> Person.Monster
         with Not_found | (Failure _ ) ->
           (ui#say "bad selection\n";
            pick_banishment_species ui uiopt
           )
;;

let generate_character_b ui ui_opt species pc =
   Person.set_species pc species;
   match species with
    | Person.Mammal ->
         let stat = Person.get_stat pc in
           (
            Stat.set_stat_value stat Stat.Muscle_b 7;
            Stat.set_stat_value stat Stat.Magic_b 6;
           )
    | Person.Reptile -> 
         let stat = Person.get_stat pc in
           (
            Stat.set_stat_value stat Stat.Muscle_b 10;
            Stat.set_stat_value stat Stat.Magic_b 0;
           )
    | Person.Monster -> 
         let stat = Person.get_stat pc in
           (
            Stat.set_stat_value stat Stat.Muscle_b 8;
            Stat.set_stat_value stat Stat.Magic_b 2;
           )
    | Person.Nothing -> print_string "Oops, Nothing person in chargen\n"
;;

