(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    stair.ml

*  What does it do?

**    Provides types and functions for handling portals between planes.

*)


type portal =
 {
   symbol : Symbol.symbol;
   mutable colour : U_colour.colour;
   mutable from_plane : W_plane.plane_reference;
   mutable to_plane : W_plane.plane_reference;
   lx : int ref;
   ly : int ref;
   mutable to_lx : int ref ref;
   mutable to_ly : int ref ref;
   two_way : bool;
   leads_down : bool;
   is_open : bool;
   name : string;
   is_special_link : bool;
   mutable plotlink : Plotlink.plotlink list;
 }
;;

let proto_portal =
 {
   symbol = Symbol.Portal_downward;
   colour = U_colour.conv_colour_s "grey";
   from_plane = W_plane.worldmap;
   to_plane = W_plane.worldmap;
   lx = ref 1;
   ly = ref 1;
   to_lx = ref (ref (-1));
   to_ly = ref (ref (-1));
   two_way = true;
   leads_down = true;
   is_open = true;
   name = "";
   is_special_link = false;
   plotlink = [];
 }
;;

let get_from_plane p = p.from_plane;;
let get_to_plane p = p.to_plane;;
let get_lx p = !(p.lx);;
let get_ly p = !(p.ly);;
let get_to_lx p = !(!(p.to_lx));;
let get_to_ly p = !(!(p.to_ly));;
let get_symbol p = p.symbol;;
let get_colour p = p.colour;;

let get_name p = 
   if p.is_special_link then
      p.name
      ^ " to the " ^ (W_plane.get_prefix p.to_plane)
(* DEBUG
      p.name ^ " from the " ^ (W_plane.get_prefix p.from_plane)
      ^ (string_of_int (W_plane.get_number p.from_plane))
      ^ " to the " ^ (W_plane.get_prefix p.to_plane)
/DEBUG *)
   else
      p.name 
(* DEBUG
      p.name ^ " from the " ^ (W_plane.get_prefix p.from_plane)
      ^ (string_of_int (W_plane.get_number p.from_plane))
/ DEBUG *)
;;

let set_lx p v = p.lx := v ;;
let set_ly p v = p.ly := v ;;
let set_to_lx p v = p.to_lx := v ;;
let set_to_ly p v = p.to_ly := v ;;
let set_to_lx_value p v = p.to_lx := ref v
let set_to_ly_value p v = p.to_ly := ref v
let set_colour p v = p.colour <- v;;

let get_plotlink p = p.plotlink;;
let add_plotlink p v = p.plotlink <- p.plotlink @ [v];;
let add_plotlink_lots p v = p.plotlink <- p.plotlink @ v;;

let inbound_portals plist x1 y1 x2 y2 =
   let rslt = ref [] in
      for count = 0 to (List.length plist -1) do
         let cport = List.nth plist count in
            if !(cport.lx) >= x1 && !(cport.lx) <= x2 
            && !(cport.ly) >= y1 && !(cport.ly) <= y2
            then
               rslt := cport :: !rslt
      done
   ; !rslt
;;

let up_staircase = 
 { proto_portal with
      symbol = Symbol.Portal_upward;
      name = "staircase leading up";
      leads_down = false
 }
;;

let down_staircase = 
 { proto_portal with
      symbol = Symbol.Portal_downward;
      name = "staircase leading down"
 }
;;

let banishing_gate = 
 { proto_portal with
      symbol = Symbol.Portal_upward;
      name = "banishing gate";
      colour = U_colour.conv_colour_s "grey";
      is_open = false;
      leads_down = false
 }
;;

let new_spec_stair from toplane tolx toly nlx nly is_down =
   let links_back = W_plane.does_link_back toplane in
     if is_down then
      {down_staircase with
         colour = U_colour.conv_colour_s "brightyellow";
         from_plane = from;
         to_plane = toplane;
         lx = ref nlx;
         ly = ref nly;
         to_lx = ref tolx;
         to_ly = ref toly;
         two_way = links_back;
         is_special_link = true;
      }
     else
      {up_staircase with
         colour = U_colour.conv_colour_s "brightyellow";
         from_plane = from;
         to_plane = toplane;
         lx = ref nlx;
         ly = ref nly;
         to_lx = ref tolx;
         to_ly = ref toly;
         two_way = links_back;
         is_special_link = true;
      }
;;

let new_up_stair from toplane tolx toly nlx nly colour =
      {up_staircase with
         from_plane = from;
         to_plane = toplane;
         lx = ref nlx;
         ly = ref nly;
         to_lx = ref tolx;
         to_ly = ref toly;
         colour = colour
      }
;;

let new_down_stair from toplane tolx toly nlx nly colour =
      {down_staircase with
         from_plane = from;
         to_plane = toplane;
         lx = ref nlx;
         ly = ref nly;
         to_lx = ref tolx;
         to_ly = ref toly;
         colour = colour
      }
;;

let is_two_way pr = pr.two_way;;

let leads_down pr = pr.leads_down;;

let redirected_stair st lx ly =
   {st with to_lx = lx; to_ly = ly}
;;

let is_open st = st.is_open;;

let direction_name b =
   match b with
      true -> "down"
    | false -> "up"
;;

let partner_of port from nlx nly =
   let new_symbol =
      match port.symbol with
         Symbol.Portal_upward -> Symbol.Portal_downward
       | _ -> Symbol.Portal_upward
   in
   let new_name = ("staircase leading " ^ 
         (direction_name (not port.leads_down))) 
   in
   let new_portal =
   {port with
      symbol=new_symbol;
      from_plane = from;
      to_plane = port.from_plane;
      lx = ref nlx;
      ly = ref nly;
      to_lx = ref port.lx;
      to_ly = ref port.ly;
      leads_down = (not port.leads_down);
      name = new_name;
   }
   in
      set_to_lx port new_portal.lx;
      set_to_ly port new_portal.ly;
      new_portal
;;

let special_colour p =
   if p.is_special_link then U_colour.Green
   else if p.leads_down then U_colour.Red
   else U_colour.Blue
;;

let get_is_special_link p = p.is_special_link;;
