(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    stat.ml

*  What does it do?

**    Provides types and api to arid.ml for player stat handling

*)


type stat_variety =
 | Skill_combat
 | Skill_magic
 | Intrinsic_armour
 | Magic_b
 | Muscle_b
;;

(*
type statistic =
  {
   variety : stat_variety;
   score : int;
  }
;;
*)

type stat_table =
   {
      mutable max_health : int;
      mutable cur_health : int;
      mutable topstat : 
         (stat_variety * ( (int ref) * (int ref) ) ) list;
   }
;;

let proto_stat_table =
   {
      max_health = 100;
      cur_health = 100;
      topstat = [];
   }
;;

let new_stat_table (i:int) =
   {
      max_health = i;
      cur_health = i;
      topstat = [];
   }
;;

let get_health table = table.cur_health;;

let set_health table v = table.cur_health <- v;;

let set_max_health table v = table.max_health <- v;;

let get_max_health table = table.max_health;;

let get_topstat table = table.topstat;;

let grant_stat st variety score =
   st.topstat <- (variety , (ref score, ref 0)) :: st.topstat
;;

let set_stat_value st variety value =
   if List.mem_assoc variety st.topstat then
      let score, marks = List.assoc variety st.topstat in
         score := value
   else grant_stat st variety value
;;

let reduce_health st v =
   st.cur_health <- st.cur_health - v
;;

let increment_stat st variety =
   if List.mem_assoc variety st.topstat then
      let score, marks = List.assoc variety st.topstat in
         score := !score + 1
;;

let get_statvalue st variety =
   if List.mem_assoc variety st.topstat then
      let score, marks =
         (List.assoc variety st.topstat)
      in !score
   else 0
;;

let get_statname variety =
   match variety with
    | Skill_combat -> "Melee"
    | Skill_magic -> "Magic"
    | Intrinsic_armour -> "Intrinsic armour"
    | Magic_b -> "magic"
    | Muscle_b -> "muscle"
;;


