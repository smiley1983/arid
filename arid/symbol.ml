(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    symbol.ml

*  What does it do?

**    Provides symbol types, and conversion to characters.

*)


type symbol =
 | Void
 | Potion
 | Amulet 
 | Terrain_pb_lb
 | Terrain_pb_nonlb
 | Floor
 | Terrain_nonpb_lb
 | Terrain_liquid
 | Money
 | Container
 | Open_door
 | Closed_door
 | Sharp_weapon
 | Blunt_weapon
 | Magical_special
 | Book
 | Food
 | Wand
 | Bridge
 | Rubble
 | Portal_upward
 | Portal_downward
 | Ring
 | Scroll
 | Person_unique
 | Heavy_armour
 | Light_armour
 | Ranged_weapon
 | Ammunition
 | Humanoid
 | Crawler
 | Jackal
 | Skeleton
 | Rodent
 | Rubbish
;;

let acs_char s =
 match s with
 | Void -> ' '
 | Potion -> '!'
 | Amulet -> (Char.chr 34)
 | Terrain_pb_lb -> (Char.chr 177)
 | Terrain_pb_nonlb -> (Char.chr 176)
 | Floor -> '.'
 | Terrain_nonpb_lb -> ';'
 | Terrain_liquid -> '~'
 | Money -> '$'
 | Container -> '&'
 | Open_door -> '\''
 | Closed_door -> '\\'
 | Sharp_weapon -> '('
 | Blunt_weapon -> ')'
 | Magical_special -> '*'
 | Book -> '+'
 | Food -> '%'
 | Wand -> '-'
 | Bridge -> ':'
 | Rubble -> ';'
 | Portal_upward -> '<'
 | Portal_downward -> '>'
 | Ring -> '='
 | Scroll -> '?'
 | Person_unique -> '@'
 | Heavy_armour -> '['
 | Light_armour -> ']'
 | Ranged_weapon -> '{'
 | Ammunition -> '}'
 | Humanoid -> 'h'
 | Crawler -> 'c'
 | Jackal -> 'j'
 | Skeleton -> 'z'
 | Rodent -> 'r'
 | Rubbish -> ','
;;

let base_char s =
 match s with
 | Void -> ' '
 | Potion -> '!'
 | Amulet -> (Char.chr 34)
 | Terrain_pb_lb -> '#'
 | Terrain_pb_nonlb -> ';'
 | Floor -> '.'
 | Terrain_nonpb_lb -> '#'
 | Terrain_liquid -> '~'
 | Money -> '$'
 | Container -> '&'
 | Open_door -> '\''
 | Closed_door -> '\\'
 | Sharp_weapon -> '('
 | Blunt_weapon -> ')'
 | Magical_special -> '*'
 | Book -> '+'
 | Food -> '%'
 | Wand -> '-'
 | Bridge -> ':'
 | Rubble -> ';'
 | Portal_upward -> '<'
 | Portal_downward -> '>'
 | Ring -> '='
 | Scroll -> '?'
 | Person_unique -> '@'
 | Heavy_armour -> '['
 | Light_armour -> ']'
 | Ranged_weapon -> '{'
 | Ammunition -> '}'
 | Humanoid -> 'h'
 | Crawler -> 'c'
 | Jackal -> 'j'
 | Skeleton -> 'z'
 | Rodent -> 'r'
 | Rubbish -> ','
;;

let to_char s which_set =
   match which_set with
      "acs" | "ACS" ->
         acs_char s
    | _ -> base_char s
;;

