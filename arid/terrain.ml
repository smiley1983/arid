(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    terrain.ml

*  What does it do?

**    Provides types, prototypes and functions for terrains.

*)


type terrain_variety =
   Void
 | Small_tree
 | Grass
 | Floor
 | Stone_floor
 | Sand
 | Big_tree
 | Dense_shrub
 | Earth_wall
 | Rock_wall
 | Steel_wall
 | Slimy_wall
 | Stream
 | Path
 | Road
 | Sea
 | River
 | Starting_road
 | Stone_bridge
;;

let sort_pair ea eb =
   if ea < eb then (ea, eb)
   else (eb, ea)
;;

let mix_terrain ta tb =
   let (tfirst, tsecond) = sort_pair ta tb in
      match tfirst, tsecond with
         River, Road -> Stone_bridge
       | Stream, Road -> Stone_bridge
       | _ -> max tfirst tsecond
;;

type obstruction_type =
   Complete | Partial | Deadly | Hazardous | Unblocked
;;

type terrain =
 {
   tname : string;
   colour : U_colour.colour;
   symbol : Symbol.symbol;
   physblock : obstruction_type;
   losblock : obstruction_type;
 }
;;

let tvoid =
   { tname = "void";
     colour = U_colour.conv_colour_s "grey";
     symbol = Symbol.Void;
     physblock = Complete;
     losblock = Unblocked
   }
;;

let paved_road =
   {
     tname = "paved road";
     colour = U_colour.conv_colour_s "white";
     symbol = Symbol.Floor;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let path =
   {
     tname = "path";
     colour = U_colour.conv_colour_s "yellow";
     symbol = Symbol.Floor;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let stone_bridge =
   {
     tname = "stone bridge";
     colour = U_colour.conv_colour_s "grey";
     symbol = Symbol.Bridge;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let stone_floor =
   {
     tname = "stone floor";
     colour = U_colour.conv_colour_s "grey";
     symbol = Symbol.Floor;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let purple_road =
   {
     tname = "purple road";
     colour = U_colour.conv_colour_s "magenta";
     symbol = Symbol.Rubble;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let river =
   {
     tname = "river";
     colour = U_colour.conv_colour_s "blue";
     symbol = Symbol.Terrain_liquid;
     physblock = Deadly;
     losblock = Unblocked
   }
;;

let stream =
   {
     tname = "stream";
     colour = U_colour.conv_colour_s "blue";
     symbol = Symbol.Terrain_liquid;
     physblock = Hazardous;
     losblock = Unblocked
   }
;;

let sea =
   {
     tname = "sea";
     colour = U_colour.conv_colour_s "blue";
     symbol = Symbol.Terrain_liquid;
     physblock = Deadly;
     losblock = Unblocked
   }
;;

let plain_floor =
   { tname = "dirt";
     colour = U_colour.conv_colour_s "yellow";
     symbol = Symbol.Floor;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let sand =
   { tname = "pebbles";
     colour = U_colour.conv_colour_s "yellow";
     symbol = Symbol.Floor;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let plain_grass =
   { tname = "grass";
     colour = U_colour.conv_colour_s "green";
     symbol = Symbol.Floor;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let earth_wall =
   { tname = "earth wall";
     colour = U_colour.conv_colour_s "yellow";
     symbol = Symbol.Terrain_pb_lb;
     physblock = Complete;
     losblock = Complete
   }
;;

let rock_wall =
   { tname = "rock wall";
     colour = U_colour.conv_colour_s "grey";
     symbol = Symbol.Terrain_pb_lb;
     physblock = Complete;
     losblock = Complete
   }
;;

let steel_wall =
   { tname = "steel wall";
     colour = U_colour.conv_colour_s "cyan";
     symbol = Symbol.Terrain_pb_lb;
     physblock = Complete;
     losblock = Complete
   }
;;

let slimy_wall =
   { tname = "slimy wall";
     colour = U_colour.conv_colour_s "green";
     symbol = Symbol.Terrain_pb_lb;
     physblock = Complete;
     losblock = Complete
   }
;;

let big_tree =
   { tname = "large tree";
     colour = U_colour.conv_colour_s "green";
     symbol = Symbol.Terrain_pb_nonlb;
     physblock = Partial;
     losblock = Unblocked
   }
;;

let small_tree =
   { tname = "small tree";
     colour = U_colour.conv_colour_s "green";
     symbol = Symbol.Floor;
     physblock = Unblocked;
     losblock = Unblocked
   }
;;

let dense_shrub =
   { tname = "dense shrub";
     colour = U_colour.conv_colour_s "green";
     symbol = Symbol.Terrain_pb_lb;
     physblock = Complete;
     losblock = Complete
   }
;;

let get_symbol t = t.symbol;;
let get_colour t = t.colour;;
let get_name t = t.tname;;


let get_physblock t = 
   match t.physblock with
      Complete
    | Partial
    | Deadly ->
         true
    | Hazardous
    | Unblocked ->
         false
;;

let get_terrain_passable t = not (get_physblock t);;

let get_losblock t = 
   match t.losblock with
      Complete | Partial ->
         true
    | Deadly | Hazardous | Unblocked ->
         false
;;

let sample_terrain v =
   match v with
      Void -> tvoid
    | Floor -> plain_floor
    | Stone_floor -> stone_floor
    | Sand -> sand
    | Stream -> stream
    | Grass -> plain_grass
    | Earth_wall -> earth_wall
    | Rock_wall -> rock_wall
    | Steel_wall -> steel_wall
    | Slimy_wall -> slimy_wall
    | Big_tree -> big_tree
    | Road -> paved_road
    | River -> river
    | Sea -> sea
    | Small_tree -> small_tree
    | Dense_shrub -> dense_shrub
    | Path -> path
    | Starting_road -> purple_road
    | Stone_bridge -> stone_bridge
;;
