(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    u_colour.ml

*  What does it do?

**    Universal colour handling. It needs a serious overhaul.

*)

type colour =
 | Red
 | Green
 | Brown
 | Blue
 | Magenta
 | Cyan
 | Black
 | Darkgrey
 | Brightyellow
 | White
;;

let conv_colour colour_int =
   match colour_int with
      Red -> "red"
    | Green -> "green"
    | Brown -> "brown"
    | Blue -> "blue"
    | Magenta -> "magenta"
    | Cyan -> "cyan"
    | Black -> "black"
    | Darkgrey -> "darkgrey"
    | Brightyellow -> "brightyellow"
    | _ -> "white"
;;

let conv_colour_s colourstring =
   match colourstring with
     "red" -> Red
    | "green" -> Green
    | "brown" -> Brown
    | "yellow" -> Brown
    | "blue" -> Blue
    | "magenta" -> Magenta
    | "cyan" -> Cyan
    | "black" -> Black
    | "darkgrey" -> Darkgrey
    | "brightyellow" -> Brightyellow
    | _ -> White (*white*)
;;

let rgb_colour colour_int =
   match colour_int with
      Red -> (192, 0, 0)
    | Green -> (0, 192, 0)
    | Brown -> (128, 64, 32)
    | Blue -> (0, 0, 192)
    | Magenta -> (192, 32, 64)
    | Cyan -> (64, 64, 192)
    | Black -> (0, 0, 0)
    | Darkgrey -> (96, 96, 96)
    | Brightyellow -> (0, 192, 192)
    | (* White *) _ -> (192, 192, 192)
;;


