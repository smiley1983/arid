(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    uaid.ml

*  What does it do?

**    Unique Arid Identity Documentation.

*)


type uaid =
   {
      mutable count : int
   }
;;

let proto_uaid =
   {
      count = 2000000
   }
;;

let get_new_uaid n = {count = n};;

let new_aid c =
   c.count <- (c.count + 1);
   c.count
;;

let set_idcount c n =
   c.count <- n
;;
