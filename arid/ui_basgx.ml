(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    ui_basgx.ml

*  What does it do?

**    Deprecated UI module
No longer really deprecated, because Windoze requires it. :(
More to the point, Windoze is too braindead to use the curses or sdl 
modules.

*)

let load_font_template fname =
   let ichan = open_in_bin fname in
      let template = Marshal.from_channel ichan in
      close_in ichan;
      template   
;;

let image_of c bg fg =
   let new_letter = Array.make_matrix 16 8 bg in
      for county = 0 to 15 do
         for countx = 0 to 7 do
            if c.(county).(countx) then
               new_letter.(county).(countx) <- fg
         done
      done;
      Graphics.make_image new_letter
;;

let make_colour_font template colour =
   let proto_image = Graphics.make_image [| [| Graphics.transp |] |] in
   let new_font = Array.make 256 proto_image in
   let bg = Graphics.transp in
   let fgr, fgg, fgb = (U_colour.rgb_colour colour) in
   let fg = Graphics.rgb fgr fgg fgb in
      for count = 0 to 255 do
         let this_char = template.(count) in
            new_font.(count) <- image_of this_char bg fg
      done;
      new_font
;;

let try_get_font (b: bool) =
   let template = load_font_template "font.dat" in
      try
         [
          (U_colour.Red, (make_colour_font template U_colour.Red));
          (U_colour.Green, (make_colour_font template U_colour.Green));
          (U_colour.Brown, (make_colour_font template U_colour.Brown));
          (U_colour.Blue, (make_colour_font template U_colour.Blue));
          (U_colour.Magenta, (make_colour_font template 
                U_colour.Magenta));
          (U_colour.Cyan, (make_colour_font template U_colour.Cyan));
          (U_colour.Black, (make_colour_font template U_colour.Black));
          (U_colour.Darkgrey, (make_colour_font template 
                U_colour.Darkgrey));
          (U_colour.Brightyellow, (make_colour_font template 
                U_colour.Brightyellow));
          (U_colour.White, (make_colour_font template U_colour.White))
         ]
      with Not_found -> []
;;

let rec list_backspaced list =
   match list with
      [] -> []
    | element :: [] -> []
    | element :: l -> element :: list_backspaced l
;;

let rec pack_string count strlist newstring =
   match count with
      0 -> newstring
    | _ -> newstring.[count-1] <- List.nth strlist (count-1);
         pack_string (count-1) strlist newstring
;;

let string_of_list str =
   let len = List.length str in
      let newstring = String.create len in
         pack_string len str newstring
;;

let draw_character c font colour =
   if font = [] then
      Graphics.draw_char (Char.chr c)
   else
      let img = (List.assoc colour font).(c) in
      let cx, cy = Graphics.current_point () in
         Graphics.draw_image img cx cy
;;

class terminal opts =
   object (self)
      val option = opts
      val colour = G_colour.get_colourset opts
      val keyset = Bgx_keys.new_keyset
      val font = ref []
      method cell_to_pixel xcell ycell =
         let xpixel = xcell * option#get_font_width in
            let ypixel = option#get_screen_height 
                       - ((ycell * option#get_font_height) + 45 )
                       in
               (xpixel, ypixel)
      method move_to_curs (cursor:Cursor.cursor) =
         let x = cursor#getx + cursor#get_x_offset in
            let y = cursor#gety + cursor#get_y_offset in
               let dx, dy = self#cell_to_pixel x y in
               Graphics.moveto dx dy
      method clear_cursor (cursor:Cursor.cursor) = ()
      method print_ccodechr (character:int) (cursor:Cursor.cursor)=
         match (Char.chr character) with
            '\n' -> cursor#newline ()
          | _ ->
            self#move_to_curs cursor;
            self#clear_cursorcell cursor;
            let clr = option#get_c_colour in
               draw_character character !font clr;
            cursor#incx
      method print_ccodechr_up (character:int) (cursor:Cursor.cursor)=
         match (Char.chr character) with
            '\n' -> cursor#lineup ()
          | _ ->
            self#move_to_curs cursor;
            self#clear_cursorcell cursor;
            let clr = option#get_c_colour in
               draw_character character !font clr;
            cursor#incx
      method print_codechr (character:int) (cursor:Cursor.cursor)= ()
      method print_str (string:string) cursor =
         for count = 0 to ((String.length string) -1) do
            self#print_ccodechr (Char.code string.[count]) cursor
         done;
      method putsymbol symbol cursor =
         let character_set = option#get_key "graphics" in
            let s = String.create 1 in
               (
                  s.[0] <- Symbol.to_char symbol character_set;
                  self#print_str s cursor
               )
      method print_str_up (string:string) cursor =
         for count = 0 to ((String.length string) -1) do
            self#print_ccodechr_up (Char.code string.[count]) cursor
         done;
      method get_string cursor=
         let str = ref [] in
            let character = ref (Char.chr 0) in
               (while not (!character = Char.chr 13) do
                  Graphics.synchronize ();
                  (character := Graphics.read_key ();
                  if not (!character = Char.chr 13) then
                     match !character with
(*                        (Char.chr 13) -> () *)
                        '\008' ->
                        if List.length !str > 0 then
                           (
                            str := list_backspaced !str;
                            cursor#decx;
                            self#clear_cursorcell cursor;
                            ()
                           )
                      | _ ->
                        let c = Char.code !character in
                           if c < 256 then
                              (str := !str @ [!character];
                              self#print_ccodechr c cursor;
                              ()
                              )
                  )
               done;
               string_of_list !str)
      method set_colour colouri =
         option#set_c_colour colouri;
         Graphics.set_color (colour#get_colour colouri); 
         ()
      method internal_set_colour (colouri:U_colour.colour) = ()
      method internal_unset_colour (colouri:U_colour.colour) = () 
      method imp_setup_screen =
        (
         try
               (Graphics.open_graph("640x480"))
         with _ -> try ((Graphics.open_graph ("640 480")))
         with _ -> try ((Graphics.open_graph ("")) ;
                        Graphics.fill_rect 0 0 640 480)
         with _ -> print_string 
               "\nCould not open graphical interface\n\n"
        );
(*         Graphics.resize_window 640 480 *)
         self#set_colour option#get_b_colour ;
         Graphics.fill_rect 0 0 640 480;
         self#set_colour option#get_f_colour;
         Graphics.auto_synchronize false;
         try
            font := try_get_font true;
            Graphics.set_window_title "ARID";
            Graphics.set_text_size 16;
            Graphics.set_font "vtsr____.ttf";
         with _ -> ();
      method imp_ask q cursor = 
         (self#print_str q cursor;
         let a = self#get_string cursor in
            cursor#newline () ;
            a)
      method clear_cursorcell (cursor:Cursor.cursor) =
         let x,y = self#cell_to_pixel cursor#get_abs_x cursor#get_abs_y
         in
            let prevcolour = option#get_c_colour in
               (
                self#set_colour option#get_b_colour;
                Graphics.fill_rect x y option#get_font_width
                                   option#get_font_height;
                self#set_colour prevcolour;
                ()
               )
      method clear_cursorwindow (cursor:Cursor.cursor) =
         let prevcolour = option#get_c_colour in
            let x,y = self#cell_to_pixel cursor#get_x_offset
                                         cursor#get_y_offset in
               (
                self#set_colour option#get_b_colour;
                let y_offset = (option#get_font_height *
                 cursor#getboundy)
                 in
                   Graphics.fill_rect x (y - y_offset)
                                   (option#get_font_width *
                                      cursor#getboundx)
                                   (y_offset + option#get_font_height) ;
                self#set_colour prevcolour;
                ()
               )
      method in_key = keyset#get_key (Graphics.read_key ())
      method unblocked_key = 
         if Graphics.key_pressed () then
            true, self#in_key
         else
            false, ""
      method sync_screen = Graphics.synchronize ()
      method end_ui = ()
   end
;;

let new_terminal opts =
   new terminal opts

