(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    ui_curs.ml

*  What does it do?

**    Curses UI

*)

let conv_colour colouri =
   match colouri with
     U_colour.Red -> Curses.A.color_pair 2 (*red*)
    | U_colour.Green -> Curses.A.color_pair 3 (*green*) (*3*)
    | U_colour.Brown -> Curses.A.color_pair 4 (*yellow*)
    | U_colour.Blue -> Curses.A.color_pair 5 (*blue*)
    | U_colour.Magenta -> Curses.A.color_pair 6 (*magenta*)
    | U_colour.Cyan -> Curses.A.color_pair 7 (*cyan*)
    | U_colour.Black -> Curses.A.color_pair 1 (*black*)
    | U_colour.Darkgrey -> Curses.A.color_pair 1 (*darkgrey*)
    | U_colour.Brightyellow -> Curses.A.color_pair 4 (*brightyellow*)
    | _ -> Curses.A.color_pair 8 (*white / grey*)

let re_fresh () =
   let _ = Curses.refresh () in	 ()
;;

let rec list_backspaced list =
   match list with
      [] -> []
    | x :: [] -> []
    | x :: l -> x :: list_backspaced l
;;

let rec pack_string count strlist newstring =
   match count with
      0 -> newstring
    | _ -> newstring.[count-1] <- List.nth strlist (count-1);
         pack_string (count-1) strlist newstring
;;

let string_of_list str =
   let len = List.length str in
      let newstring = String.create len in
         pack_string len str newstring
;;

class terminal opts =
   object (self)
      val option = opts
      val screen_obj = Curses.initscr ()
      val keyset = Curs_key.new_keyset
      method cell_to_pixel (xcell:int) (ycell:int) = (xcell, ycell)
      method move_to_curs (cursor:Cursor.cursor) =
         ignore (Curses.mvaddstr
                 cursor#get_abs_y,
                 cursor#get_abs_x,
                 ""
                )
      method print_ccodechr character (cursor:Cursor.cursor) =
         match Char.chr(character) with 
            '\n' -> cursor#newline ()
          | _ -> 
            self#internal_set_colour option#get_c_colour;
            ignore (Curses.mvaddstr cursor#get_abs_y cursor#get_abs_x
                    (String.make 1 (Char.chr character))) ;
            self#internal_unset_colour option#get_c_colour;
            cursor#incx;
            ()
      method print_ccodechr_up character (cursor:Cursor.cursor) =
         match Char.chr(character) with 
            '\n' -> cursor#lineup ()
          | _ -> 
            self#internal_set_colour option#get_c_colour;
            ignore (Curses.mvaddstr cursor#get_abs_y cursor#get_abs_x
                    (String.make 1 (Char.chr character))) ;
            self#internal_unset_colour option#get_c_colour;
            cursor#incx;
            ()
      method print_codechr character (cursor:Cursor.cursor)=
         let _ = assert (Curses.mvaddstr cursor#get_abs_y
            cursor#get_abs_x (String.make 1 (Char.chr(character)))) in
();
         cursor#incx
      method clear_cursor (cursor:Cursor.cursor)=
         ignore (Curses.mvaddstr cursor#get_abs_y
            cursor#get_abs_x (String.make 1 ' '))
      method print_str string cursor =
         for count = 0 to (String.length (string) -1) do
            self#print_ccodechr (Char.code (string.[count])) cursor
         done
      method putsymbol symbol cursor =
         let character_set = option#get_key "graphics" in
            let s = String.create 1 in
              (
               s.[0] <- Symbol.to_char symbol character_set;
               self#print_str s cursor
              )
      method print_str_up string cursor =
         for count = 0 to (String.length (string) -1) do
            self#print_ccodechr_up (Char.code (string.[count])) cursor
         done
      method get_string (cursor:Cursor.cursor)=
         let str = ref [] in
            let character = ref 0 in
               (while not (!character = 10) do
                  (character := Curses.getch ();
                  match !character with
                     10 -> ()
                   | 263 ->
                        if (List.length !str) > 0 then
                        (str := list_backspaced !str; 
                        cursor#decx; 
                        self#print_ccodechr (Char.code ' ') cursor;
                        cursor#decx;
                        ignore
                           (Curses.move 
                           (cursor#get_abs_y) (cursor#get_abs_x)
                           );
                        )
                   | _ -> if !character < 256 then
                     (
                     str := !str @ [Char.chr(!character)];
                     self#print_ccodechr !character cursor;
                     re_fresh ()
                     ) 
                     else ()
                  );
               done;
               string_of_list !str)
      method set_colour (colouri:U_colour.colour) = 
         option#set_c_colour colouri; ()
      method internal_set_colour (clr) =
         if not ((clr = U_colour.Darkgrey) 
                 || (clr = U_colour.Brightyellow)
                )
         then
            ignore (Curses.attron (conv_colour clr))
         else
            (
               ignore (Curses.attron (conv_colour clr));
               ignore (Curses.attron Curses.A.bold)
            )
      method internal_unset_colour (clr) =
         if not ((clr = U_colour.Darkgrey) 
                 || (clr = U_colour.Brightyellow)
                )
         then
            ignore (Curses.attroff (conv_colour clr))
         else
            (
               ignore (Curses.attroff (conv_colour clr));
               ignore (Curses.attroff Curses.A.bold)
            )
      method imp_setup_screen =
(*
         ignore (Curses.cbreak() );
*)
         ignore (Curses.raw() );
         ignore (Curses.noecho() );
         ignore (Curses.intrflush screen_obj false);
         ignore (Curses.keypad screen_obj true);
         ignore (Curses.start_color () );
         ignore (Curses.init_pair 1 Curses.Color.black
                 Curses.Color.black);
(*
         ignore (Curses.init_color Curses.Color.black 0 500 0);
*)
         ignore (Curses.init_pair 2 Curses.Color.red
                 Curses.Color.black);
         ignore (Curses.init_pair 3 Curses.Color.green
                 Curses.Color.black);
         ignore (Curses.init_pair 4 Curses.Color.yellow
                 Curses.Color.black);
(*
         ignore (Curses.init_color Curses.Color.blue 0 0 999);
*)
         ignore (Curses.init_pair 5 Curses.Color.blue
                 Curses.Color.black);
         ignore (Curses.init_pair 6 Curses.Color.magenta
                 Curses.Color.black);
         ignore (Curses.init_pair 7 Curses.Color.cyan
                 Curses.Color.black);
         ignore (Curses.init_pair 8 Curses.Color.white
                 Curses.Color.black);
         ()
      method imp_ask q cursor = 
         (self#print_str q cursor;
         re_fresh ();
         let a = self#get_string cursor in
(*            cursor#newline ();
            self#print_str a cursor;
*)
            cursor#newline ();
            a)
      method clear_cursorcell (cursor:Cursor.cursor) =
         self#clear_cursor cursor
      method clear_cursorwindow (cursor:Cursor.cursor) =
         for count = 0 to ((cursor#getboundx + 1) 
                           * (cursor#getboundy + 1)
                          ) 
         do
            self#clear_cursorcell cursor;
            cursor#incx; 
            ()
         done
      method sync_screen = re_fresh ()
      method in_key = 
         let in_char = (Curses.getch ()) in
            if in_char = 27 then
               let in_char2 = (Curses.getch ()) in
                  keyset#get_key (in_char2 + 1000)
            else
               keyset#get_key (in_char)
      method unblocked_key =
         ignore (Curses.nodelay (Curses.stdscr ()) true) ;
         try
            let retv = keyset#get_key (Curses.getch ()) in
               (ignore (Curses.nodelay (Curses.stdscr ()) false);
                (true, retv)
               )
         with _ -> 
           (ignore (Curses.nodelay (Curses.stdscr ()) false);
            (false, "")
           )
      method end_ui =
         Curses.endwin ()
   end
;;

let new_terminal opts = 
   new terminal opts
