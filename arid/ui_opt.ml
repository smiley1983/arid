(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    ui_opt.ml

*  What does it do?

**    UI option handling

*)

let check_special s =
   match s with
      "hash" -> "#"
    | _ -> s
;;

let default_opt v =
   match v with
    | "terminaltype" -> "curses"
    | "f_colour" -> "grey"
    | "b_colour" -> "black"
    | "c_colour" -> "grey"
    | "font_width" -> "8"
    | "font_height" -> "16"
    | "screen_height" -> "480"
    | "screen_chars_high" -> "25"
    | "screen_chars_wide" -> "79"
    | "is_curses_compile" -> "false"
    | "map_x" -> "0"
    | "map_y" -> "0"
    | "map_wide" -> "25"
    | "map_high" -> "25"
    | "stat_x" -> "25"
    | "stat_y" -> "17"
    | "stat_wide" -> "54"
    | "stat_high" -> "8"
    | "msg_x" -> "25"
    | "msg_y" -> "0"
    | "msg_wide" -> "54"
    | "msg_high" -> "16"
    | "menu_forecolour" -> "grey"
    | "menu_backcolour" -> "grey"
    | "menu_headingcolour" -> "cyan"
    | "shift_bq" -> "~"
    | "shift_1" -> "!"
    | "shift_2" -> "@"
    | "shift_3" -> "hash"
    | "shift_4" -> "$"
    | "shift_5" -> "%"
    | "shift_6" -> "^"
    | "shift_7" -> "&"
    | "shift_8" -> "*"
    | "shift_9" -> "("
    | "shift_0" -> ")"
    | "shift_minus" -> "_"
    | "shift_equals" -> "+"
    | "shift_bs" -> "|"
    | "shift_[" -> "{"
    | "shift_]" -> "}"
    | "shift_;" -> ":"
    | "shift_'" -> "\""
    | "shift_," -> "<"
    | "shift_." -> ">"
    | "shift_/" -> "?"
    | "," -> "key_get" 
    | "?" -> "key_help" 
    | "kp8" -> "key_north" 
    | "kp2" -> "key_south"
    | "h" -> "key_west"
    | "kp6" -> "key_east"
    | "kp7" -> "key_nw"
    | "kp9" -> "key_ne"
    | "kp1" -> "key_sw"
    | "kp3" -> "key_se"
    | "w" -> "key_longwalk"
    | "M" -> "key_viewmap" 
    | "l" -> "key_look" 
    | "ctrl+q" -> "key_quit" 
    | ">" -> "key_downstair" 
    | "<" -> "key_upstair" 
    | "i" -> "key_inventory" 
    | "d" -> "key_drop" 
    | "D" -> "key_multidrop" 
    | "a" -> "key_attack" 
    | "-" -> "key_previous" 
    | "=" -> "key_next" 
    | "/" -> "key_switch" 
    | "m" -> "key_skills" 
    | "t" -> "key_throw" 
    | "." -> "key_enter"
    | "Z" -> "key_cast_spell" 
    | "e" -> "key_equipment" 
    | "q" -> "key_drink" 
    | "r" -> "key_read" 
    | "v" -> "key_examine" 
    | "p" -> "key_pass" 
    | "alt+a" -> "wizard_show_actionq"
    | "alt+v" -> "key_wizard_vision"
    | "fast_menu" -> "true"
    | "is_wizard" -> "false"
    | "wizard_vision" -> "false"
    | "show_debug" -> "false"
    | "message_reverse" -> "true"
    | "stack_messages" -> "false"
    | "old_messages" -> "true"
    | "scenario" -> "Banishment"
    | "fontfile" -> "vtsr____.ttf"
    | "font_points" -> "16"
    | "fullscreen" -> "false"
    | "show_animation" -> "normal"
    | "graphics" -> "base"
    | "mapmode" -> "scroll"
    | "big_window" -> "msg"
    | _ -> "option_error"
;;

class option =
   object (self)
      val oset = Hashtbl.create 90
      val rev_oset = Hashtbl.create 90
      method add_option v1 v2 =
        (
         Hashtbl.replace oset v1 v2;
         Hashtbl.replace rev_oset v2 v1
        )
      method hard_init =
         self#add_option "terminaltype" "curses"(*"basic_graph"*);
         self#add_option "f_colour" "grey";
         self#add_option "b_colour" "black";
         self#add_option "c_colour" "grey";
         self#add_option "font_width" "8";
         self#add_option "font_height" "16";
         self#add_option "screen_height" "480";
         self#add_option "screen_chars_high" "25";
         self#add_option "screen_chars_wide" "79";
         self#add_option "is_curses_compile" "false";
         self#add_option "map_x" "0";
         self#add_option "map_y" "0";
         self#add_option "map_wide" "25";
         self#add_option "map_high" "25";
         self#add_option "stat_x" "25";
         self#add_option "stat_y" "17";
         self#add_option "stat_wide" "54";
         self#add_option "stat_high" "8";
         self#add_option "msg_x" "25";
         self#add_option "msg_y" "0";
         self#add_option "msg_wide" "54";
         self#add_option "msg_high" "16";
         self#add_option "menu_forecolour" ("grey");
         self#add_option "menu_backcolour" ("grey");
         self#add_option "menu_headingcolour" ("cyan");
         self#add_option "shift_bq" "~";
         self#add_option "shift_1" "!";
         self#add_option "shift_2" "@";
         self#add_option "shift_3" "hash";
         self#add_option "shift_4" "$";
         self#add_option "shift_5" "%";
         self#add_option "shift_6" "^";
         self#add_option "shift_7" "&";
         self#add_option "shift_8" "*";
         self#add_option "shift_9" "(";
         self#add_option "shift_0" ")";
         self#add_option "shift_minus" "_";
         self#add_option "shift_equals" "+";
         self#add_option "shift_bs" "|";
         self#add_option "shift_[" "{";
         self#add_option "shift_]" "}";
         self#add_option "shift_;" ":";
         self#add_option "shift_'" "\"";
         self#add_option "shift_," "<";
         self#add_option "shift_." ">";
         self#add_option "shift_/" "?";
         self#add_option "," "key_get" ;
         self#add_option "?" "key_help" ;
         self#add_option "kp8" "key_north" ;
         self#add_option "kp2" "key_south";
         self#add_option "h" "key_west";
         self#add_option "kp6" "key_east";
         self#add_option "kp7" "key_nw";
         self#add_option "kp9" "key_ne";
         self#add_option "kp1" "key_sw";
         self#add_option "kp3" "key_se";
         self#add_option "w" "key_longwalk";
         self#add_option "M" "key_viewmap" ;
         self#add_option "l" "key_look" ;
         self#add_option "Q" "key_quit" ;
         self#add_option ">" "key_downstair" ;
         self#add_option "<" "key_upstair" ;
         self#add_option "i" "key_inventory" ;
         self#add_option "d" "key_drop" ;
         self#add_option "D" "key_multidrop" ;
         self#add_option "a" "key_attack" ;
         self#add_option "-" "key_previous" ;
         self#add_option "=" "key_next" ;
         self#add_option "/" "key_switch" ;
         self#add_option "m" "key_skills" ;
         self#add_option "alt+a" "wizard_show_actionq" ;
         self#add_option "t" "key_throw" ;
         self#add_option "." "key_enter";
         self#add_option "Z" "key_cast_spell" ;
         self#add_option "e" "key_equipment" ;
         self#add_option "q" "key_drink" ;
         self#add_option "r" "key_read" ;
         self#add_option "v" "key_examine" ;
         self#add_option "p" "key_pass" ;
         self#add_option "m" "key_move" ;
         self#add_option "fast_menu" "true";
         self#add_option "is_wizard" "false";
         self#add_option "show_debug" "false";
         self#add_option "message_reverse" "true";
         self#add_option "stack_messages" "false";
         self#add_option "old_messages" "true";
         self#add_option "scenario" "Banishment";
         self#add_option "fontfile" "vtsr____.ttf";
         self#add_option "font_points" "16";
         self#add_option "fullscreen" "false";
         self#add_option "show_animation" "normal";
         self#add_option "graphics" "base";
         self#add_option "mapmode" "scroll";
         self#add_option "big_window" "msg";
      method get_key v =
         try
            Hashtbl.find oset v
         with Not_found ->
            default_opt v
      method get_reverse_key v =
         try
            Hashtbl.find rev_oset v
         with Not_found ->
            "reverse option error"
      method get_terminaltype =
         self#get_key "terminaltype"
      method get_menu_fc =
         U_colour.conv_colour_s
            (self#get_key "menu_forecolour")
      method get_menu_bc = 
         U_colour.conv_colour_s
            (self#get_key "menu_backcolour")
      method get_menu_hc =
         U_colour.conv_colour_s
            (self#get_key "menu_headingcolour")
      method get_f_colour =
         U_colour.conv_colour_s
            (self#get_key "f_colour")
      method get_b_colour =
         U_colour.conv_colour_s
            (self#get_key "b_colour")
      method get_c_colour =
         U_colour.conv_colour_s
            (self#get_key "c_colour")
      method get_font_width =
         int_of_string (self#get_key "font_width")
      method get_font_height =
         int_of_string (self#get_key "font_height")
      method get_screen_height =
         int_of_string (self#get_key "screen_height")
      method get_screen_chars_high =
         int_of_string (self#get_key "screen_chars_high")
      method get_screen_chars_wide =
         int_of_string (self#get_key "screen_chars_wide")
      method get_is_curses_compile =
         match self#get_key "is_curses_compile" with
            "true" -> true
          | _ -> false
      method set_c_colour colour = 
         Hashtbl.replace oset "c_colour" (U_colour.conv_colour colour)
      method get_map_x =
         int_of_string (self#get_key "map_x")
      method get_map_y =
         int_of_string (self#get_key "map_y")
      method get_map_wide =
         int_of_string (self#get_key "map_wide")
      method get_map_high =
         int_of_string (self#get_key "map_high")
      method get_stat_x =
         int_of_string (self#get_key "stat_x")
      method get_stat_y =
         int_of_string (self#get_key "stat_y")
      method get_stat_wide =
         int_of_string (self#get_key "stat_wide")
      method get_stat_high =
         int_of_string (self#get_key "stat_high")
      method get_msg_x =
         int_of_string (self#get_key "msg_x")
      method get_msg_y =
         int_of_string (self#get_key "msg_y")
      method get_msg_wide =
         int_of_string (self#get_key "msg_wide")
      method get_msg_high =
         int_of_string (self#get_key "msg_high")
      method has_opt o =
         try
            ignore ( self#get_key o );
            true
         with Not_found -> false
      method set_opt oname ovalue =
         Hashtbl.replace oset oname ovalue
      method get_shift_bq = check_special (self#get_key "shift_bq")
      method get_shift_1 = check_special (self#get_key "shift_1")
      method get_shift_2 = check_special (self#get_key "shift_2")
      method get_shift_3 = check_special (self#get_key "shift_3")
      method get_shift_4 = check_special (self#get_key "shift_4")
      method get_shift_5 = check_special (self#get_key "shift_5")
      method get_shift_6 = check_special (self#get_key "shift_6")
      method get_shift_7 = check_special (self#get_key "shift_7")
      method get_shift_8 = check_special (self#get_key "shift_8")
      method get_shift_9 = check_special (self#get_key "shift_9")
      method get_shift_0 = check_special (self#get_key "shift_0")
      method get_shift_minus = check_special (self#get_key 
                                              "shift_minus")
      method get_shift_equals = check_special (self#get_key 
                                               "shift_equals")
      method get_shift_bs = check_special (self#get_key "shift_bs")
      method get_shift_lsb = check_special (self#get_key "shift_[")
      method get_shift_rsb = check_special (self#get_key "shift_]")
      method get_shift_sc = check_special (self#get_key "shift_;")
      method get_shift_ap = check_special (self#get_key "shift_'")
      method get_shift_com = check_special (self#get_key "shift_,")
      method get_shift_dot = check_special (self#get_key "shift_.")
      method get_shift_fs = check_special (self#get_key "shift_/")
      method get_scenario = self#get_key "scenario"
      method get_fontfile = self#get_key "fontfile"
      method get_font_points = self#get_key "font_points"
      method is_fast_menu = 
         match (self#get_key "fast_menu") with
            "true" -> true
          | _ -> false
      method is_wizard =
         match (self#get_key "is_wizard") with
            "true" -> true
          | _ -> false
      method get_show_debug =
         match (self#get_key "show_debug") with
            "true" -> true
          | _ -> false
      method get_message_reverse =
         match (self#get_key "message_reverse") with
            "true" -> true
          | _ -> false
      method get_stack_messages =
         match (self#get_key "stack_messages") with
            "true" -> true
          | _ -> false
      method get_fullscreen =
         match (self#get_key "fullscreen") with
            "true" -> true
          | _ -> false
      method get_old_messages =
         match (self#get_key "old_messages") with
            "true" -> true
          | _ -> false
   end
;;

let get_option = 
   let topt = new option in
      if Sys.file_exists "arid.rc" then
         Uiopt_f.load_uiopt_file "arid.rc" topt "0.17.77"
      else
         topt#hard_init
   ;
   topt
;;
