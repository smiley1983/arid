(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    ui_plain.ml

*  What does it do?

**    The beginnings of a plainterm UI module

*)


class terminal opts offx offy charsx charsy=
   object (self)
      val option = opts
      method print_ccodechr (chr:int) = ()
      method print_codechr (chr:int)= ()
      method print_str (string:string)= ()
      method cell_to_pixel (xcell:int) (ycell:int) = (xcell, ycell)
      method get_string =
         input_line stdin
      method imp_setup_screen = ()
      method imp_ask q =
         (Printf.printf "%s> " q;
         flush stdout;
         input_line stdin)
      method end_ui = ()
   end
;;

let new_terminal opts offx offy charsx charsy = 
   new terminal opts offx offy charsx charsy
