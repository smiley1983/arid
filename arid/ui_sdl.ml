(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    ui_sdl.ml

*  What does it do?

**    Wraps ocamlsdl for uilink

*)

let uppercase option c =
   match c with
    | '`' -> option#get_shift_bq.[0]
    | '1' -> option#get_shift_1.[0]
    | '2' -> option#get_shift_2.[0]
    | '3' -> option#get_shift_3.[0]
    | '4' -> option#get_shift_4.[0]
    | '5' -> option#get_shift_5.[0]
    | '6' -> option#get_shift_6.[0]
    | '7' -> option#get_shift_7.[0]
    | '8' -> option#get_shift_8.[0]
    | '9' -> option#get_shift_9.[0]
    | '0' -> option#get_shift_0.[0]
    | '-' -> option#get_shift_minus.[0]
    | '=' -> option#get_shift_equals.[0]
    | '\\' -> option#get_shift_bs.[0]
    | '[' -> option#get_shift_lsb.[0]
    | ']' -> option#get_shift_rsb.[0]
    | ';' -> option#get_shift_sc.[0]
    | '\'' -> option#get_shift_ap.[0]
    | ',' -> option#get_shift_com.[0]
    | '.' -> option#get_shift_dot.[0]
    | '/' -> option#get_shift_fs.[0]
    | _ -> Char.uppercase c
;;

let make_rect (x, y, x2, y2) =
   let w = x2 - x in
   let h = y2 - y in
      Sdlvideo.rect ~x:x ~y:y ~w:w ~h:h
;;

(*
let char_of_key option { Sdlevent.ke_state = state; 
                  Sdlevent.keysym = key ;
                  Sdlevent.keymod = kmod ;
                  Sdlevent.keycode = kcode } 
=
   let c = 
      try Sdlkey.char_of_key key
      with Invalid_argument _ -> Char.chr 127 
   in
      if kmod land Sdlkey.kmod_shift <> 0 then
         uppercase option c
      else c
;;
*)

let int_of_key option { Sdlevent.ke_state = state; 
		Sdlevent.keysym = key ;
		Sdlevent.keymod = kmod ;
		Sdlevent.keycode = kcode } 
=
  let i = 
    try (Sdlkey.int_of_key key)
    with Invalid_argument _ -> -1
   in
      if (not ((kmod land Sdlkey.kmod_ctrl) = 0))
      && (not ((kmod land Sdlkey.kmod_alt) = 0))
      then
         i + 904 (* + 1000 - 96 *)
      else if kmod land Sdlkey.kmod_ctrl <> 0 then
         i - 96
      else if not ((kmod land Sdlkey.kmod_shift) = 0) then
         Char.code (uppercase option (Char.chr i))
      else if kmod land Sdlkey.kmod_alt <> 0 then
         i + 1000
      else i
;;

let sdl_init opts =
   Sdl.init ~auto_clean:true [ `VIDEO ] ;
   Sdlkey.enable_unicode true;
   let lst =
      if opts#get_fullscreen then
         [ `SWSURFACE ; `FULLSCREEN ]
      else
         [ `SWSURFACE ]
   in
   Sdlvideo.set_video_mode 640 480 lst;
;;

let rec get_keydown option =
    let evt = Sdlevent.wait_event () in
	match evt with
         | Sdlevent.KEYDOWN ke ->
              int_of_key option ke
         | _ -> get_keydown option
;;

let read_key option =
   get_keydown option
;;

let if_key op =
   match Sdlevent.poll () with
      None -> raise Not_found
    | Some evt ->
        (
         match evt with
            Sdlevent.KEYDOWN ke ->
               int_of_key op ke
          | _ -> raise Not_found
        )
;;

let rec list_backspaced list =
   match list with
      [] -> []
    | element :: [] -> []
    | element :: l -> element :: list_backspaced l
;;

let rec pack_string count strlist newstring =
   match count with
      0 -> newstring
    | _ -> newstring.[count-1] <- List.nth strlist (count-1);
         pack_string (count-1) strlist newstring
;;

let string_of_list str =
   let len = List.length str in
      let newstring = String.create len in
         pack_string len str newstring
;;

(* This was needed to generate the font file for basic_graphics

let grab_font_letter i x y =
   Sdlvideo.lock i;
   let m = Array.make_matrix 16 8 false in
   for count_y = 0 to 15 do
      for count_x = x to (x + 7) do
         let v = Sdlvideo.get_pixel i count_x count_y in
            let b = match v with 0l -> false | _ -> true in
               m.(count_y).((count_x - x)) <- b
      done
   done;
   Sdlvideo.unlock i;
   m
;;

let process_fontimg i =
   let ds = Array.make 256 (Array.make_matrix 1 1 false) in
   for count = 0 to 255 do
      let startpos = count * 8 in
         let r = grab_font_letter i startpos 0 in
            ds.(count) <- r
   done;
   let ochan = open_out_bin "font.dat" in
      Marshal.to_channel ochan ds [];
      close_out ochan
;;

let fontstring () =
   let s = String.make 256 ' ' in
      for count = 0 to 255 do
         if (count > 16) && (count < 240) then
            s.[count] <- Char.chr count
      done;
   s
;;

let special_font_init font rgb =
   let s = fontstring () in
   let render = Sdlttf.SOLID rgb in
   let text = Sdlttf.render_text font render s in
      process_fontimg text
(*
      Sdlvideo.save_BMP text "font.bmp"
*)
;;

*)

let draw_character c x y rgb font charbox screen =
   let use_colour = 
      Sdlvideo.map_RGB screen Sdlvideo.black 
   in
   Sdlvideo.fill_rect charbox use_colour;
   let render = Sdlttf.SOLID rgb in
   let text = Sdlttf.render_text font render (String.make 1 c) in
   let (w, h, _) = Sdlvideo.surface_dims text in
   let r = Sdlvideo.rect ~x: 0 ~y: 0 ~w ~h in
   Sdlvideo.blit_surface ~src: text ~dst: charbox ~dst_rect: r ();
   let r2 = Sdlvideo.rect ~x: x ~y: y ~w ~h in
   Sdlvideo.blit_surface ~src: charbox ~dst: screen ~dst_rect: r2 ();
;;

class terminal (opts:Ui_opt.option) =
 let scr = sdl_init opts in
   object (self)
      val screen = scr
      val nnnn = Sdlttf.init ()
      val font = Sdlttf.open_font opts#get_fontfile 
            (int_of_string opts#get_font_points)
      val charbox = Sdlvideo.create_RGB_surface_format scr [] 
                    ~w:8 ~h:16
      val option = opts
      val colour = S_colour.get_colourset opts
      val keyset = Key_sdl.new_keyset
      method cell_to_pixel xcell ycell =
         let xpixel = xcell * option#get_font_width in
            let ypixel = (ycell * option#get_font_height) in
               (xpixel, ypixel)
      method move_to_curs (cursor:Cursor.cursor) = ()
      method clear_cursor (cursor:Cursor.cursor) = ()
      method print_ccodechr (character:int) (cursor:Cursor.cursor)=
         match (Char.chr character) with
            '\n' -> cursor#newline ()
          | _ ->
            let x = cursor#getx + cursor#get_x_offset in
            let y = cursor#gety + cursor#get_y_offset in
            let dx, dy = self#cell_to_pixel x y in
               let uc = colour#get_colour option#get_c_colour in
               draw_character (Char.chr character) dx dy uc
                     font charbox screen;
               cursor#incx
      method print_ccodechr_up (character:int) (cursor:Cursor.cursor)=
         match (Char.chr character) with
            '\n' -> cursor#lineup ()
          | _ ->
            let x = cursor#getx + cursor#get_x_offset in
            let y = cursor#gety + cursor#get_y_offset in
            let dx, dy = self#cell_to_pixel x y in
            let uc = colour#get_colour option#get_c_colour in
               draw_character (Char.chr character) dx dy uc
                     font charbox screen;
               cursor#incx
      method print_codechr (character:int) (cursor:Cursor.cursor)= ()
      method print_str (string:string) cursor =
         for count = 0 to ((String.length string) -1) do
            self#print_ccodechr (Char.code string.[count]) cursor
         done;
      method putsymbol symbol cursor =
         let character_set = option#get_key "graphics" in
            let s = String.create 1 in
              (
               s.[0] <- Symbol.to_char symbol character_set;
               self#print_str s cursor
              )
      method print_str_up (string:string) cursor =
         for count = 0 to ((String.length string) -1) do
            self#print_ccodechr_up (Char.code string.[count]) cursor
         done;
      method get_string cursor=
         let str = ref [] in
            let character = ref ' ' in
               (while not (!character = Char.chr 13) do
                  Sdlvideo.update_rect screen;
                  let inkey = keyset#get_key (read_key option) in
                  character :=
                     if inkey = "backsp" then '\008'
                     else if inkey = "enter" then Char.chr 13
                     else if String.length inkey = 0 then '?'
                     else
                        inkey.[0];
                  ;
                  if not (!character = Char.chr 13) then
                  (
                     match !character with
(*                        (Char.chr 13) -> () *)
                        '\008' ->
                        if List.length !str > 0 then
                           (
                            str := list_backspaced !str;
                            cursor#decx;
                            self#clear_cursorcell cursor;
                            ()
                           )
                      | '?' -> ()
                      | _ ->
                        let c = Char.code !character in
                           if c < 256 then
                              (str := !str @ [!character];
                              self#print_ccodechr c cursor;
                              ()
                              )
                  )
               done;
               string_of_list !str)
      method set_colour (colouri:U_colour.colour) =
         option#set_c_colour colouri;
      method internal_set_colour (colouri:U_colour.colour) = ()
      method internal_unset_colour (colouri:U_colour.colour) = () 
      method imp_setup_screen = ()
      method imp_ask q cursor = 
         (self#print_str q cursor;
         let a = self#get_string cursor in
            cursor#newline () ;
            a)
      method clear_cursorcell (cursor:Cursor.cursor) =
         let x = cursor#getx + cursor#get_x_offset in
         let y = cursor#gety + cursor#get_y_offset in
            let dx, dy = self#cell_to_pixel x y in
            let uc = colour#get_colour option#get_c_colour in
               draw_character ' ' dx dy uc font charbox screen
      method clear_cursorwindow (cursor:Cursor.cursor) =
         let prevcolour = option#get_c_colour in
            let x,y = self#cell_to_pixel cursor#get_x_offset
                                         cursor#get_y_offset in
               (
                self#set_colour option#get_b_colour;
                let y_offset = (option#get_font_height *
                 cursor#getboundy)
                 in
                  let use_colour = 
                   Sdlvideo.map_RGB screen Sdlvideo.black 
                  in
                   Sdlvideo.fill_rect 
                      ~rect:
                      (make_rect (x, y,
                       (option#get_font_width *
                        cursor#getboundx),
                       (y_offset + y)
                      )) screen use_colour;
                self#set_colour prevcolour;
                ()
               )
      method in_key = keyset#get_key (read_key option)
      method unblocked_key =
         try
            true, (keyset#get_key (if_key option))
         with Not_found ->
            false, ""
      method sync_screen = Sdlvideo.update_rect screen
      method end_ui = ()
(*
         let rgb = colour#get_colour U_colour.Green in
            special_font_init font rgb
*)
   end
;;

let new_terminal opts =
   new terminal opts
;;
