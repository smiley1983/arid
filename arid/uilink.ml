(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    uilink.ml

*  What does it do?

**    Provides the UI bridge: People wishing to build SDL or 
Windoze versions of 
this program will need to uncomment the appropriate (let terminal_obj =)
and comment out the inappropriate one. Comments are signified by 
brackets with an asterisk directly next to it on the inside. They can be 
(*nested*) (* like (* so *) *).

*)


(* This one provides SDL only *)
(*
let terminal_obj terminaltype option =
   match terminaltype with
    | _ -> Ui_sdl.new_terminal option
;;
*)


(* This one provides Curses only *)
(*
let terminal_obj terminaltype option =
   match terminaltype with
    | _ -> Ui_curs.new_terminal option 
;;
*)

(* This one provides SDL and Curses and base_graphic *)
(*
let terminal_obj terminaltype option =
   match terminaltype with
    | "sdl" -> Ui_sdl.new_terminal option
    | "base_graphic" -> Ui_basgx.new_terminal option
    | _ -> Ui_curs.new_terminal option 
;;
*)

(* This one provides Curses and Base_Graphics *)
(* *)
let terminal_obj terminaltype option =
   match terminaltype with
    | "base_graphic" -> Ui_basgx.new_terminal option
    | _ -> Ui_curs.new_terminal option 
;;
(* *)

(* This one provides Base_Graphics only (use for Windoze compiles) *)
(*
let terminal_obj terminaltype option =
    Ui_basgx.new_terminal option
;;
*)

class ui option=
   object (self)
      val interface = terminal_obj option#get_terminaltype option
      method initialise =
         interface#imp_setup_screen
      method end_ui = interface#end_ui
      method set_colour colour = interface#set_colour colour
      method box_ask q cursor = 
         interface#imp_ask q cursor
      method box_say thing cursor =
         interface#print_str thing cursor
      method box_putsymbol symbol cursor =
         interface#putsymbol symbol cursor
      method box_say_up thing cursor =
         interface#print_str_up thing cursor
      method clear_win cursor= interface#clear_cursorwindow cursor
      method in_key = interface#in_key
      method sync_screen = interface#sync_screen
      method unblocked_key =
         try
            interface#unblocked_key
         with Not_found -> false, ""
   end
;;


let new_ui option = new ui option;;
