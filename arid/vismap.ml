(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    vismap.ml

*  What does it do?

**    vision map handling

*)


let get_vistime mstruct x y =
   let tile = Mapstruc.get_rtile mstruct x y in
      Mapblock.vis_time tile
;;

let get_msltime mstruct x y =
   let tile = Mapstruc.get_rtile mstruct x y in
      Mapblock.get_missile_time tile
;;

let paint_stairs overmap =
   let stairs = Mapstruc.get_portal overmap in
   for count = 0 to (List.length stairs - 1) do
      let stair = (List.nth stairs count) in
       if (Mapplane.get_p_ref (Mapstruc.get_cplane overmap))
          = (Stair.get_from_plane stair)
      then
        (let symbol = Stair.get_symbol stair in
         let colour = Stair.get_colour stair in
         let lx = Stair.get_lx stair in
         let ly = Stair.get_ly stair in
         let rtile = Mapstruc.get_rtile overmap lx ly in
            if not (rtile == Mapblock.rtile) then
            (
               Mapblock.set_vis_symbol rtile symbol;
               Mapblock.set_vis_colour rtile colour;
               Mapblock.set_cthing rtile true;
               Mapblock.set_physblock rtile false;
               Mapblock.set_special_colour rtile 
                  (Stair.special_colour stair);
            )
        )
   done
;;

let paint_nonsentient overmap nslist protolist =
   for count = 0 to (List.length nslist - 1) do
      let nonsent = (List.nth nslist count) in
         let symbol = Item.get_symbol nonsent protolist in
         let colour = Item.get_colour nonsent protolist in
         let lblock = Item.get_losblock nonsent in
         let pblock = Item.get_physblock nonsent in
         let lx = Item.get_locx nonsent in
         let ly = Item.get_locy nonsent in
         let rtile = Mapstruc.get_rtile overmap lx ly in
            if (not (rtile == Mapblock.rtile))
            && (not (Mapblock.is_physblocked rtile)) then
            (
               Mapblock.set_vis_symbol rtile symbol;
               Mapblock.set_vis_colour rtile colour;
               Mapblock.set_losblock rtile lblock;
               Mapblock.set_physblock rtile pblock;
               Mapblock.set_cthing rtile true;
               if pblock then
                  Mapblock.set_blockname rtile 
                        (Language.get_iname nonsent protolist)
            )
   done;
   paint_stairs overmap
;;

let paint_sentient overmap sentient protolist =
   let symbol = Person.get_symbol sentient protolist in
   let colour = Person.get_colour sentient protolist in
   let lblock = Person.get_losblock sentient in
   let pblock = Person.get_physblock sentient in
   let lx = Person.get_locx sentient in
   let ly = Person.get_locy sentient in
   let rtile = Mapstruc.get_rtile overmap lx ly in
      if (not (rtile == Mapblock.rtile))
(*      && (not (Mapblock.is_physblocked rtile)) *)
      then
      (
         Mapblock.set_vis_symbol rtile symbol;
         Mapblock.set_vis_colour rtile colour;
         Mapblock.set_losblock rtile lblock;
         Mapblock.set_physblock rtile pblock;
         Mapblock.set_cperson rtile true;
         if pblock then
            Mapblock.set_blockname rtile 
                  (Person.get_pname sentient protolist)
      )
;;

let paint_sentient_list overmap slist time protolist =
   for count = 0 to (List.length slist - 1) do
      let sent = (List.nth slist count) in
(*3.09 warning
         let lx = Person.get_locx sent in
         let ly = Person.get_locy sent in
*)
(*
         let vtime = get_vistime overmap lx ly in
            if time <= vtime then
*)
               paint_sentient overmap sent protolist
   done
;;

let update_vtb_terrain mstruct bx by ex ey =
   for ycount = by to ey do
      for xcount = bx to ex do  (*DEBUG -1*)
         let (symbol, colour, pblock, lblock, _) = 
            Mapstruc.get_scb_stat mstruct xcount ycount
         in 
         let tname = Terrain.get_name 
                        (Mapstruc.get_terrain mstruct xcount ycount)
         in
         let rtile = 
            Mapstruc.get_rtile mstruct (xcount) (ycount)
         in if rtile == Mapblock.rtile then 
               (* (print_string "vtbrt error";
               raise Not_found) *)  () else
            (Mapblock.set_vis_symbol rtile symbol;
             Mapblock.set_vis_colour rtile colour;
             Mapblock.set_losblock rtile lblock;
             Mapblock.set_physblock rtile pblock;
             Mapblock.set_blockname rtile tname;
             Mapblock.set_cperson rtile false;
             Mapblock.set_cthing rtile false;
            )
      done
   done
;;

let update_vistile_block mstruct bx by ex ey =
(*   try *)
      update_vtb_terrain mstruct bx by ex ey
(*   with _ -> (print_string "vtb error"; () ) *)
;;

let get_symb_col mstruct x y =
   let tile = Mapstruc.get_rtile mstruct x y in
      (Mapblock.get_last_vis_symbol tile, Mapblock.vis_colour tile,
       Mapblock.get_special_colour tile)
;;

let get_true_symb_col mstruct x y =
   let tile = Mapstruc.get_rtile mstruct x y in
      (Mapblock.vis_symbol tile, Mapblock.vis_colour tile,
       Mapblock.get_special_colour tile)
;;

let set_vistime mstruct x y time =
   let tile = Mapstruc.get_rtile mstruct x y in
      Mapblock.set_vis_time tile time
;;

let paint_dirty mstruct plane =
   let dirty = Mapstruc.get_dirty mstruct in
   if List.length dirty > 0 then
   for count = 0 to (List.length dirty - 1) do
      let (xcount, ycount) = List.nth dirty count in
         let (symbol, colour, pblock, lblock, tname) = 
            Mapstruc.get_scb_stat mstruct xcount ycount
         in let rtile = Mapstruc.get_rtile mstruct xcount ycount in
      if not (rtile == Mapblock.rtile) then
      (
            Mapblock.set_vis_symbol rtile symbol;
            Mapblock.set_vis_colour rtile colour;
            Mapblock.set_losblock rtile lblock;
            Mapblock.set_physblock rtile pblock;
            Mapblock.set_blockname rtile tname;
            Mapblock.set_cperson rtile false;
            Mapblock.set_cthing rtile false;
            Mapstruc.clean_dirty mstruct xcount ycount
      )
   done
(* ;  Mapstruc.clear_all_dirty mstruct *)
;;

let update_vismap mstruct plane slist nslist prevtime protolist =
      (paint_dirty mstruct plane (* prevtime *) ;
       paint_nonsentient mstruct nslist protolist;
       paint_sentient_list mstruct slist prevtime protolist
      )
;;

let was_seen plane lx ly time =
   let rtile = Mapplane.grid_get_rtile plane lx ly in
      if Mapblock.vis_time rtile < time then false
      else true
;;

let can_see_person plane pson time =
   let lx = Person.get_locx pson in
   let ly = Person.get_locy pson in
      was_seen plane lx ly time
;;
