(*
    ARID - Another Roguelike In Development
    Copyright (C) 2004-2006  Jude Hungerford

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

*  What is this file?

**    w_plane.ml

*  What does it do?

**    Provides types for structures which provide the details from which 
new planes and staircase connections are generated.

*)


type plane_variety =
   Overland
 | Dungeon
 | Banishment_dungeon
 | Perfect_maze
 | Shop_maze
 | Stone_dungeon
 | Steel_dungeon
 | Hole_in_rock
 | Circular_room
 | Rectangular_room
 | Triangular_room
 | Dank_pit
 | Abomination_lair
 | Twisted_tunnel
 | Hopeless_cycle
;;

type plane_reference =
 {
   prefix : string;
   number : int;
   max_depth : int;
   size : int;
   links_back : bool;
   variety : plane_variety;
   bsize_x : int;
   bsize_y : int;
   lightlevel : int;
 }
;;

(* add BUILT *)
(* add BSIZE *)

let worldmap =
 {
   prefix = "worldmap";
   number = 0;
   max_depth = 0;
   size = 10;
   links_back = true;
   variety = Overland;
   bsize_x = 26;
   bsize_y = 26;
   lightlevel = 8;
 }
;;

let new_worldmap s =
   {worldmap with size = s}
;;

let does_link_back pre_plane = pre_plane.links_back;;

(* 
TOFIX - where_exit needs to be changed to where_exit_from plus 
where_exit_to
*)

type exit_ref =
 {
   mutable exit_done : bool;
   from_least_number : int;
   from_greatest_number : int;
   from_prefix : string;
   from_lx : int;
   from_ly : int;
   from_size : int;
   from_max_depth : int;
   from_variety : plane_variety;
   from_bsize_x : int;
   from_bsize_y : int;
   to_least_number : int;
   to_greatest_number : int;
   to_prefix : string;
   to_lx : int;
   to_ly : int;
   to_size : int;
   to_max_depth : int;
   to_variety : plane_variety;
   to_bsize_x : int;
   to_bsize_y : int;
   mutable where_exit : int;
   two_way : bool;
   plotlink : Plotlink.plotlink list;
 }
;;

let fix_where_exit_falls ext_ref =
   if ext_ref.where_exit = (-1) then
   (
      let range = 
         (ext_ref.from_greatest_number - ext_ref.from_least_number) 
      in
         let rndm =
            if range < 1 then ext_ref.from_greatest_number
            else (Algo.mersenne (range + 1)) + ext_ref.from_least_number
         in 
         ext_ref.where_exit <- rndm;
         rndm
   )
   else ext_ref.where_exit
;;

let build_now eref p_ref =
   ignore (fix_where_exit_falls eref);
   if eref.exit_done then
      print_string "exit done already\n";
   if (not eref.exit_done)
   && eref.from_prefix = p_ref.prefix
   && p_ref.number = eref.where_exit then
      true
   else false
;;

(*
TOFIX - currently, this does not pay attention to the variable exit 
location when reverse building; it uses the least number (shallowest
level)
*)

let reverse_build_now eref p_ref =
   ignore (fix_where_exit_falls eref);
   if eref.exit_done then
      print_string "exit done already\n";
   if (not eref.exit_done)
   && (eref.to_prefix = p_ref.prefix)
   && (eref.to_least_number = p_ref.number) then
      true
   else false
;;

let is_exit_done eref = eref.exit_done;;

let exit_is_done e = e.exit_done <- true;;

(* 
TOFIX - see above regarding exit range, and shallowest level
*)

let ref_of_exit extref is_down =
   extref.exit_done <- true;
   if is_down then
      {
         prefix = extref.to_prefix;
         number = extref.to_least_number;
         max_depth = extref.to_max_depth;
         size = extref.to_size;
         links_back = extref.two_way;
         variety = extref.to_variety;
         bsize_x = extref.to_bsize_x;
         bsize_y = extref.to_bsize_y;
         lightlevel = 8;
      }
   else
      let new_number = 
         fix_where_exit_falls extref
(*
         if not (extref.where_exit = -1) then extref.where_exit
         else
           (
            let diff = extref.exit_end - extref.exit_start in
               let where = Algo.mersenne diff + extref.exit_start in
               extref.where_exit <- where;
               where
           )
*)
      in
      let new_max_depth = extref.from_max_depth in
      let new_size = extref.from_size in
      let new_variety = extref.from_variety in
        {
         prefix = extref.from_prefix;
         number = new_number;
         max_depth = new_max_depth;
         size = new_size;
         links_back = extref.two_way;
         variety = new_variety;
         bsize_x = extref.from_bsize_x;
         bsize_y = extref.from_bsize_y;
         lightlevel = 8;
        }
;;

type plane_link =
 {
   mutable link : exit_ref list
 }
;;

let proto_link = 
 {
   link = [] 
 }
;;

let banish_worldmap =
 {
   prefix = "fresh air";
   number = 0;
   max_depth = 0;
   size = 10;
   links_back = true;
   variety = Overland;
   bsize_x = 26;
   bsize_y = 26;
   lightlevel = 12;
 }
;;

let b_pit =
 {
   prefix = "pit of banishment";
   number = 0;
   max_depth = 12;
   size = 1;
   links_back = true;
   variety = Banishment_dungeon;
   bsize_x = 72;
   bsize_y = 26;
   lightlevel = 8;
 }
;;

let b_maze =
 {
   prefix = "maze";
   number = 0;
   max_depth = 0;
   size = 1;
   links_back = true;
   variety = Perfect_maze;
   bsize_x = 26;
   bsize_y = 26;
   lightlevel = 8;
 }
;;

let b_town_of_blackfriars =
 {
   prefix = "town of blackfriars";
   number = 1;
   max_depth = 1;
   size = 1;
   links_back = true;
   variety = Shop_maze;
   bsize_x = 69;
   bsize_y = 21;
   lightlevel = 10;
 }
;;

(* *)
let banish_worldmap_to_bdungeon_exit =
 {
   exit_done = false;
   from_least_number = banish_worldmap.number;
   from_greatest_number = banish_worldmap.max_depth;
   from_prefix = banish_worldmap.prefix;
   from_lx = 120;
   from_ly = 120;
   from_size = banish_worldmap.size;
   from_max_depth = banish_worldmap.max_depth;
   from_variety = banish_worldmap.variety;
   from_bsize_x = 26;
   from_bsize_y = 26;
   to_least_number = 0;
   to_greatest_number = 0;
   to_prefix = b_pit.prefix;
   to_lx = -1;
   to_ly = -1;
   to_size = b_pit.size;
   to_max_depth = b_pit.max_depth;
   to_variety = b_pit.variety;
   to_bsize_x = 26;
   to_bsize_y = 26;
   where_exit = -1;
   two_way = true;
   plotlink = [];
 }
;;
(* *)

let banish_pit_to_maze_exit =
 {
   exit_done = false;
   from_least_number = b_pit.max_depth;
   from_greatest_number = b_pit.max_depth;
   from_prefix = b_pit.prefix;
   from_lx = -1;
   from_ly = -1;
   from_size = b_pit.size;
   from_max_depth = b_pit.max_depth;
   from_variety = b_pit.variety;
   from_bsize_x = b_pit.bsize_x;
   from_bsize_y = b_pit.bsize_y;
   to_least_number = b_maze.number;
   to_greatest_number = b_maze.number;
   to_prefix = b_maze.prefix;
   to_lx = -1;
   to_ly = -1;
   to_size = b_maze.size;
   to_max_depth = b_maze.max_depth;
   to_variety = b_maze.variety;
   to_bsize_x = b_maze.bsize_x;
   to_bsize_y = b_maze.bsize_y;
   where_exit = -1;
   two_way = true;
   plotlink = [];
 }
;;

let banish_maze_to_blackfriars_exit =
 {
   exit_done = false;
   from_least_number = b_maze.max_depth;
   from_greatest_number = b_maze.max_depth;
   from_prefix = b_maze.prefix;
   from_lx = -1;
   from_ly = -1;
   from_size = b_maze.size;
   from_max_depth = b_maze.max_depth;
   from_variety = b_maze.variety;
   from_bsize_x = b_maze.bsize_x;
   from_bsize_y = b_maze.bsize_y;
   to_least_number = b_town_of_blackfriars.number;
   to_greatest_number = b_town_of_blackfriars.number;
   to_prefix = b_town_of_blackfriars.prefix;
   to_lx = -1;
   to_ly = -1;
   to_size = b_town_of_blackfriars.size;
   to_max_depth = b_town_of_blackfriars.max_depth;
   to_variety = b_town_of_blackfriars.variety;
   to_bsize_x = b_town_of_blackfriars.bsize_x;
   to_bsize_y = b_town_of_blackfriars.bsize_y;
   where_exit = -1;
   two_way = true;
   plotlink = [];
 }
;;

let banish_link =
 {
   link =
      [
         {banish_pit_to_maze_exit with exit_done = false};
         {banish_maze_to_blackfriars_exit with exit_done = false};
      ]
 }
;;

let ascending_1_baselevel =
 {
   prefix = "hole in the rock";
   number = 1;
   max_depth = 1;
   size = 1;
   links_back = true;
   variety = Hole_in_rock;
   bsize_x = 26;
   bsize_y = 26;
   lightlevel = 8;
 }
;;

let tiny_rl_startlevel =
 {
   prefix = "circular room";
   number = 0;
   max_depth = 0;
   size = 1;
   links_back = true;
   variety = Circular_room;
   bsize_x = 26;
   bsize_y = 26;
   lightlevel = 8;
 }
;; 

let ascending1_victory_exit =
 {
   exit_done = false;
   from_least_number = 1;
   from_greatest_number = 1;
   from_prefix = "paradise";
   from_lx = -1;
   from_ly = -1;
   from_size = 2;
   from_max_depth = 1;
   from_variety = Dungeon;
   from_bsize_x = 26;
   from_bsize_y = 26;
   to_least_number = 1;
   to_greatest_number = 1;
   to_prefix = "dungeon";
   to_lx = -1;
   to_ly = -1;
   to_size = 2;
   to_max_depth = 2;
   to_variety = Dungeon;
   to_bsize_x = 26;
   to_bsize_y = 26;
   where_exit = -1;
   two_way = true;
   plotlink = [Plotlink.Ascending1_exit];
 }
;;

let dungeon_to_holeinrock_exit =
 {
   exit_done = false;
   from_least_number = 2;
   from_greatest_number = 2;
   from_prefix = "dungeon";
   from_lx = -1;
   from_ly = -1;
   from_size = 2;
   from_max_depth = 2;
   from_variety = Dungeon;
   from_bsize_x = 26;
   from_bsize_y = 26;
   to_least_number = 1;
   to_greatest_number = 1;
   to_prefix = "hole in the rock";
   to_lx = -1;
   to_ly = -1;
   to_size = 1;
   to_max_depth = 1;
   to_variety = Hole_in_rock;
   to_bsize_x = 26;
   to_bsize_y = 26;
   where_exit = -1;
   two_way = true;
   plotlink = [];
 }
;;

let ascending_1_link = 
 {
(*
   link = [
           {ascending1_victory_exit with exit_done = false};
           {dungeon_to_holeinrock_exit with exit_done = false}
          ]
*)
   link = [{banish_worldmap_to_bdungeon_exit with exit_done = false}]
 }
;;

(*
let worldmap_to_dungeon_exit =
 {
   exit_done = false;
   exit_start = 0;
   exit_end = 0;
   from_prefix = "worldmap";
   from_lx = 120;
   from_ly = 120;
   from_size = worldmap.size;
   to_prefix = "dungeon";
   to_number = 0;
   to_max_depth = 2;
   from_max_depth = 0;
   to_size = 1;
   to_variety = Dungeon;
   from_variety = Overland;
   where_exit = -1;
   two_way = true;
   plotlink = [];
 }
;;

let dungeon_to_red_dungeon_exit =
 {
   exit_done = false;
   exit_start = 0;
   exit_end = 1;
   from_prefix = "dungeon";
   from_lx = -1;
   from_ly = -1;
   from_size = worldmap_to_dungeon_exit.to_size;
   to_prefix = "red dungeon";
   to_number = 0;
   to_max_depth = 2;
   from_max_depth = worldmap_to_dungeon_exit.to_max_depth;
   to_size = 1;
   to_variety = Dungeon;
   from_variety = worldmap_to_dungeon_exit.to_variety;
   where_exit = -1;
   two_way = true;
   plotlink = [];
 }
;;

let dungeon_to_blue_dungeon_exit =
 {
   exit_done = false;
   exit_start = 0;
   exit_end = 1;
   from_prefix = "dungeon";
   from_lx = -1;
   from_ly = -1;
   from_size = worldmap_to_dungeon_exit.to_size;
   to_prefix = "blue dungeon";
   to_number = 0;
   to_max_depth = 2;
   from_max_depth = worldmap_to_dungeon_exit.to_max_depth;
   to_size = 1;
   to_variety = Dungeon;
   from_variety = worldmap_to_dungeon_exit.to_variety;
   where_exit = -1;
   two_way = true;
   plotlink = [];
 }
;;

let blue_dungeon_to_abyss_exit =
 {
   exit_done = false;
   exit_start = 2;
   exit_end = 2;
   from_prefix = "blue dungeon";
   from_lx = -1;
   from_ly = -1;
   from_size = dungeon_to_blue_dungeon_exit.to_size;
   to_prefix = "abyss";
   to_number = 0;
   to_max_depth = 0;
   from_max_depth = dungeon_to_blue_dungeon_exit.to_max_depth;
   to_size = 2;
   to_variety = Dungeon;
   from_variety = dungeon_to_blue_dungeon_exit.to_variety;
   where_exit = 2;
   two_way = true;
   plotlink = [];
 }
;;

let red_dungeon_to_abyss_exit =
 {
   exit_done = false;
   exit_start = 2;
   exit_end = 2;
   from_prefix = "red dungeon";
   from_lx = -1;
   from_ly = -1;
   from_size = dungeon_to_red_dungeon_exit.to_size;
   to_prefix = "abyss";
   to_number = 0;
   to_max_depth = 0;
   from_max_depth = dungeon_to_red_dungeon_exit.to_max_depth;
   to_size = 2;
   to_variety = Dungeon;
   from_variety = dungeon_to_red_dungeon_exit.to_variety;
   where_exit = 2;
   two_way = true;
   plotlink = [];
 }
;;

let test_link = 
 {
   link = [{worldmap_to_dungeon_exit with exit_done = false};
           {dungeon_to_red_dungeon_exit with exit_done = false};
           {dungeon_to_blue_dungeon_exit with exit_done = false};
           {blue_dungeon_to_abyss_exit with exit_done = false};
           {red_dungeon_to_abyss_exit with exit_done = false}
          ]
 }
;;
*)

let get_links pl = pl.link;;

let next_p_ref p_ref down =
   if down then
      if p_ref.number < (p_ref.max_depth) then
         {p_ref with number = (p_ref.number + 1)}
      else
         raise Not_found
   else if p_ref.number > 0 then
      {p_ref with number = (p_ref.number - 1)}
   else raise Not_found
;;

let get_prefix pr = pr.prefix;;
let get_number pr = pr.number;;
let get_size pr = pr.size;;

let planename pr = pr.prefix ^ (string_of_int (pr.number + 1));;

let get_bsize_x pr = pr.bsize_x;;

let get_bsize_y pr = pr.bsize_y;;

let get_variety pr = pr.variety;;

let get_from_lx er = er.from_lx;;
let get_from_ly er = er.from_ly;;

let get_from_prefix pr = pr.from_prefix;;

let get_plotlink er = er.plotlink;;
